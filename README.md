![logo][logo]

---

![Solarus](https://img.shields.io/badge/Solarus-1.5.3-yellow?style=plastic)
![lua](https://img.shields.io/badge/lua-5.1-indigo?style=plastic)
![version](https://img.shields.io/badge/version-0.1.1-brightgreen.svg?style=plastic)

# Overview

This repository houses an original action-adventure RPG called *The Epic of Den*, originally conceived in 2014 for Java, and was put on hold. As of March 2017, the game is currently being built using the free and open-source game engine [Solarus][solarus-engine]. For more details, check out the [official website][solarus] (features [here][solarus-features], quest editor [here][quest-editor]).

This game was created for the following reasons:

* To demonstrate the capabilities of the Solarus Engine.
* To expand the free and open-source gaming library.

- **Release Date:** TBD
- **Platforms:** Windows 7 or higher, MacOS, Linux
- **Players:** 1
- **Length:** 10-15 hours
- **Licenses:** Creative Commons License version 4.0 (CC BY-SA 4.0), GNU General Public License version 3 (GPL v3)
- **Languages:** English (other languages coming soon)

[logo]: /data/logos/logo.png
[quest-editor]: https://gitlab.com/solarus-games/solarus-quest-editor
[solarus]: https://www.solarus-games.org/
[solarus-engine]: https://gitlab.com/solarus-games/solarus
[solarus-features]: https://solarus-games.org/features
