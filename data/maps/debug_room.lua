-- Lua script of map debug_room.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()
local hero = game:get_hero()
--local shop = require("scripts/menus/shop")
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local item_manager = require("scripts/maps/item_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  -- Door manager test.
  door_manager:open_doors_block_moved(map, "big_room_block", "big_room_door",  800, "secret")
  door_manager:open_doors_switch_activated(map, "debug_hit_switch", "door_push", 800, "secret")
  door_manager:open_doors_switch_activated(map, "tile_switch", "tile_door",  1000, "secret")
  door_manager:open_doors_if_savegame_exist(map, "big_room_activated", "big_room_door_")

  -- Item manager.
  item_manager:disappear_chest(map, "chest_fire_cane")
  item_manager:chest_appears_switch_activated(map, "switch_fire_cane", "chest_fire_cane", 800, "secret")
  item_manager:chest_appears_if_opened(map, "chest_fire_cane")

  map:set_doors_open("big_room_door_", true)
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end

function eldran:on_interaction()
  game:start_dialog("_debug.eldran.1", math.random(100))
end

function big_room_sensor_1:on_activated()
  door_manager:open_doors_if_block_moved(map, "big_room_block", "big_room_door_")
  big_room_sensor_1:set_enabled(false)
  big_room_sensor_2:set_enabled(false)
end

function big_room_sensor_2:on_activated()
  door_manager:open_doors_if_block_moved(map, "big_room_block", "big_room_door_")
  big_room_sensor_1:set_enabled(false)
  big_room_sensor_2:set_enabled(false)
end

function enemy_appear_sensor_1:on_activated()
  --enemy_manager:appear_enemies(map, "eeper_group_1_")
end

-- Camera test.
function press_test_switch:on_activated() 
  local camera = map:get_camera()
  audio_manager:play_sound("switch")
  
  sol.timer.start(map, 0, function()
    hero:freeze()
    local x, y = 104, 184
    local speed = 200
    local _, _, layer = hero:get_position()
    camera:pan_to(x, y, speed, function()
      audio_manager:play_sound("chest_appears")
      map:create_pickable({
        name = "random_potion",
        layer = layer,
        x = x,
        y = y,
        treasure_name = "potion",
      })

      sol.timer.start(camera, 800, function()
        camera:pan_to_hero(hero, speed)
      end)
    end)

    --[[local movement = sol.movement.create("target")
    movement:set_target(camera:get_position_to_track(x,y))
    movement:set_speed(speed)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)

    movement:start(camera, function()
      sol.audio.play_sound("chest_appears")
      map:create_pickable({
        name = "random_potion",
        layer = layer,
        x = x,
        y = y,
        treasure_name = "potion",
      })
      --[[if random_potion then
        random_potion:set_can_disappear(false)
      end
    
      sol.timer.start(camera, 800, function()
        local movement_back = sol.movement.create("target")
        movement_back:set_target(camera:get_position_to_track(hero))
        movement_back:set_speed(speed)
        movement_back:set_smooth(false)
        movement_back:set_ignore_obstacles(true)

        movement_back:start(camera, function()
          camera:start_tracking(hero)
        end)
      end)
    end)--]]
    --[[map:move_camera(x, y, speed, function()
      audio_manager:play_sound("chest_appears")
      map:create_pickable({
        name = "rand_potion",
        layer = layer,
        x = x,
        y = y,
        treasure_name = "potion", 
      })
    end)--]]
    --hero:unfreeze()
  end)
end