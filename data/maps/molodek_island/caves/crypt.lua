local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local item_manager = require("scripts/maps/item_manager")

-- Make function here to shorten dialog label call.
local function get_dialog_id(dialog_id)
  return "_map.area.molodek_island.caves.crypt." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup doors.
  door_manager:open_doors_enemy_dead(map, "enemy_group_1_", "door_group_1_", 800, "secret")
  door_manager:open_doors_enemy_dead(map, "subboss", "door_subboss_", 1000, "secret")
  door_manager:open_doors_switches_activated(map, "switch_group_1_", "door_block", 800, "secret")
  door_manager:open_doors_torches_lit(map, "torch_group_1_", "door_group_medal_", 800, "secret")
  door_manager:open_doors_torches_lit(map, "torch_group_3_", "door_stairwell", 800, "secret")

  -- Setup items.
  item_manager:disappear_chest(map, "chest_fire_cane")
  item_manager:disappear_chest(map, "chest_stamina_drink")
  item_manager:chest_appears_if_opened(map, "chest_fire_cane")
  item_manager:chest_appears_if_opened(map, "chest_stamina_drink")
 
  map:set_doors_open("door_group_1_", true)
  map:set_doors_open("door_fire_cane_", true)
  map:set_doors_open("door_group_medal_", false)
  map:set_doors_open("door_switch_crypt", false)
end)

-- Play cutscene when step switch pressed.
function map:play_cutscene_cave_door_open()
  local camera = map:get_camera()
  local hero = map:get_hero()
  local door = map:get_entity("door_switch_crypt")

  sol.timer.start(map, 0, function()
    local speed = 100
    hero:freeze()
    game:set_suspended(true)
    camera:pan_to_entity(door, speed, function()
      map:open_doors("door_switch_crypt")
      audio_manager:play_sound("secret")

      sol.timer.start(camera, 1500, function()
        camera:pan_to_hero(hero, speed)
      end)
    end)
  end)
end

function sensor_close_doors_1:on_activated()
end

function sensor_close_doors_2:on_activated()
  door_manager:close_doors_enemy_not_dead(map, "subboss", "door_subboss_")
end

function sensor_close_doors_3:on_activated()
  map:close_doors("door_fire_cane_")
end

function switch_crypt:on_activated()
  map:play_cutscene_cave_door_open()
end

-- Write down caution on sign.
function sign_fire_cane:on_interaction()
  local sign_id = get_dialog_id("sign.1")
  game:start_dialog(sign_id)
end

-- Write down hints on sign.
function sign_key_manor_gate:on_interaction()
  local sign_id = get_dialog_id("sign.2")
  game:start_dialog(sign_id)
end
