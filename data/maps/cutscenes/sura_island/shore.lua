local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")

local black_screen
local BLACK = { 0, 0, 0 }

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- TODO: Play ambient sea waves sound/music.
  game:hide_hud()
  game:disable_pause_menu()

  -- Setup screen to fade out. Keep invisible until needed.
  black_screen = sol.surface.create()
  black_screen:fill_color(BLACK)
  black_screen:set_opacity(0)

  -- Setup initial hero.
  map:initialize_hero()
end)

function map:initialize_hero()
  hero:set_visible(false)
  hero_ko:create_sprite(hero:get_tunic_sprite_id())
  hero_ko:get_sprite():set_animation("sleeping")
end

function map:on_opening_transition_finished()
  
  -- Start cutscene.
  map:play_cutscene()
end

function map:on_draw(screen)

  black_screen:draw(screen)
end

-- Cutscene: Fade in to dusk sky,
-- then pan down to collapsed player on beach.
-- Swordmaker notices, and lumbers to player. Fade out.
function map:play_cutscene()
  -- Setup cutscene.
  local swordmaker_sprite = swordmaker:get_sprite()
  cutscene.builder(game, map, hero)
    .freeze()
    .wait(2000)
    .dont_wait_for.sprite_animation(swordmaker_sprite,"walking")
    .movement({
      type = "path",
      entity = swordmaker,
      properties = {
        path = {{4,4,4,4,4,4,4,4,4,4}},
        speed = 50,
        ignore_obstacles = false,
      }
    })
    .dont_wait_for.sprite_animation(swordmaker_sprite,"stopped")
    .wait(1000)
    .dont_wait_for.sprite_animation(swordmaker_sprite,"walking")
    .movement({
      type = "path",
      entity = swordmaker,
      properties = {
        path = {{4,4,4,4,4,4,4,4}},
        speed = 100,
        ignore_obstacles = false,
      }
    })
    .dont_wait_for.sprite_animation(swordmaker_sprite,"stopped")
    .wait(3000)
    .exec(function()
      black_screen:fade_in(100, function()
        sol.timer.start(map, 2000, function()
          hero:set_visible(true)
          hero:teleport("sura_island/interiors/swordmaker/2f", "start")
        end)
      end)
    end)
  .start()
end
