-- Lua script of intro.
-- This script is executed every time the hero enters this map.
local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")

local dialog_index = 0

-- End dialog.
local function intro_finish()
  -- Wake up in prison ship.
  sol.timer.start(2000, function()
    hero:teleport("ship/c", "start")
  end)
end

-- Continue dialog.
local function dialog_call()
  -- Loop through dialog until all lines done.
  if dialog_index < 2 then
    game:start_dialog("_cutscene.intro." .. dialog_index, function()
      dialog_index = dialog_index + 1
      sol.timer.start(1000, dialog_call)
    end)
  else
    intro_finish()
  end
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Do not activate hero, and hide all displays.
  hero:freeze()
  game:hide_hud()
  game:disable_pause_menu()

  -- Start cutscene.
  map:play_cutscene_intro()
end)

-- Play intro cutscene.
function map:play_cutscene_intro()
  cutscene.builder(game, map, hero)
    .wait(1000)
    .sound("door_open")
    .wait(1500)
    .sound("item_fall")
    .wait(2000)
    .sound("door_closed")
    .wait(3000)
    .exec(function()
      black_screen:set_enabled(true)
      game:start_commentary("ship.1")
      dialog_call()
    end)
  .start()
end