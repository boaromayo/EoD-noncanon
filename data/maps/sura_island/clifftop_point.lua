-- Lua script of map sura_island/clifftop_point (section D-4).
local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  if destination == from_cliff_d4_sub then
    game:show_hud()
  end
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end

function bush_hollow:on_cut()
  audio_manager:play_sound("secret")
end

function bush_hollow:on_lifting()
  audio_manager:play_sound("secret")
end