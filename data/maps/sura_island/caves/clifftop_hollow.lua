local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local item_manager = require("scripts/maps/item_manager")

local switch_order = { 4, 1, 3, 2 }
local switches_activated = 0

-- Make function here to shorten dialog label call.
local function get_dialog_id(dialog_id)
  return "_map.area.sura_island.caves.clifftop_hollow." .. dialog_id
end

-- Check if correct switch was pressed.
local function check_switches_activated(selected_switch)
  local correct_switch = map:get_entity("switch_" .. switch_order[switches_activated+1])

  if selected_switch == correct_switch then
    audio_manager:play_sound("switch")
    switches_activated = switches_activated + 1
    if switches_activated == #switch_order then
      game:set_value("switch_clifftop_finish", true)
      map:play_cutscene_show_chest()
    end
    selected_switch.is_activated = true
  else
    local switches = map:get_entities("switch_")
    for switch in switches do
      switch:set_activated(false)
      switch.is_activated = false
    end
    if switches_activated > 0 then
      audio_manager:play_sound("door_closed")
    end
    switches_activated = 0
  end
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup items.
  item_manager:disappear_chest(map, "chest_clifftop")
  item_manager:chest_appears_if_opened(map, "chest_clifftop")
  item_manager:chest_appears_if_savegame_exist(map, "switch_clifftop_finish", "chest_clifftop")

  -- Setup switches.
  map:initialize_switches()
end)

-- Initialize switches.
function map:initialize_switches()
  local switches = map:get_entities("switch_")
  
  -- Reset/activate switches based on puzzle completion.
  for switch in switches do
    if not game:get_value("switch_clifftop_finish") then
      switch.is_activated = false
      switch.on_activated = check_switches_activated
    else
      switch.is_activated = true
    end
  end

  if not game:get_value("switch_clifftop_finish") then
    switches_activated = 0
  else
    switches_activated = #switch_order
  end
end

-- Cutscene to make chest appear.
function map:play_cutscene_show_chest()
  local camera = map:get_camera()
  local hero = map:get_hero()
  local chest = map:get_entity("chest_clifftop")
  
  sol.timer.start(map, 0, function()
    local speed = 80
    hero:freeze()
    game:set_suspended(true)

    camera:pan_to_entity(chest, speed, function()
      item_manager:appear_chest(map, "chest_clifftop", 150, "secret")
      
      sol.timer.start(camera, 1750, function()
        camera:pan_to_hero(hero, speed)
      end)
    end)
  end)
end

-- Write down order on sign.
function sign_order:on_interaction()
  local sign_id = get_dialog_id("sign.1")
  game:start_dialog(sign_id)
end