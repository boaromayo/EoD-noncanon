local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup doors.
  door_manager:open_doors_switch_activated(map, "switch_passage_1", "door_passage_1", 1000, "secret")

  -- Setup switches.
  switch_passage_1:set_enabled(false)
  switch_passage_2.is_activated = false

  map:set_doors_open("door_passage_", false)
end)

-- Pan camera to door, and unlock.
function map:play_cutscene_cave_door_open()
  local camera = map:get_camera()
  local hero = map:get_hero()
  local door = map:get_entity("door_passage_2")
  
  sol.timer.start(map, 250, function()
    local speed = 100
    hero:freeze()
    game:set_suspended(true)
    
    camera:pan_to_entity(door, speed, function()
      map:open_doors("door_passage_2")
      audio_manager:play_sound("secret")
      
      sol.timer.start(map, 2000, function()
        camera:pan_to_hero(hero, speed)
      end)
    end)
  end)
end

function switch_block:on_activated()
  sol.timer.start(map, 500, function()
    audio_manager:play_sound("chest_appears")
    switch_passage_1:set_enabled(true)
  end)
end

function switch_block:on_inactivated()
  switch_passage_1:set_enabled(false)
end

local function check_switch_activated(switch)

  if not switch.is_activated then
    map:play_cutscene_cave_door_open()
    switch.is_activated = true
  end
end

function switch_passage_2:on_activated()
  check_switch_activated(self)
end