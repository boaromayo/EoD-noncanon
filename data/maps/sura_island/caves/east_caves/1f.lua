local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local item_manager = require("scripts/maps/item_manager")
local door_manager = require("scripts/maps/door_manager")
local switch_manager = require("scripts/maps/switch_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()

  -- Setup doors.
  door_manager:open_doors_enemy_dead(map, "enemy_group_1_", "door_passage_", 500, "secret")
  door_manager:open_doors_if_savegame_exist(map, "door_e_cave_passage_open", "door_passage")

  -- Setup switches.
  switch_manager:set_savegame_if_switch_activated(map, "ladder_passage_active", "switch_ladder_passage")

  -- Ladder should appear if passage switch activated.
  ladder_passage:set_enabled(game:get_value("ladder_passage_active") or false)

  -- Keep passage doors open if enemies alive and player has not entered room.
  map:set_doors_open("door_passage", true)
end)

-- Initialize map music.
function map:initialize_music()
  audio_manager:play_music("More-Sewer-Creepers_Looping")
end

-- When sensor activated, lock doors.
function sensor_lock_door:on_activated()
  door_manager:close_doors_enemy_not_dead(map, "enemy_group_1_", "door_passage_")
end

-- Open maze wall if chest opened.
function chest_e_cave_peso:on_opened()

  local hero = map:get_hero()
  local item, variant, _ = chest_e_cave_peso:get_treasure()
  hero:start_treasure(item, variant, "chest_e_cave_peso_open", function()
    sol.timer.start(map, 500, function()
      audio_manager:play_sound("door_open")
      wall_maze:set_enabled(false)
    end)
  end)
end