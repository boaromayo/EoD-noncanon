-- Lua script of map sura_island/caves/cellar_cave.
local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local enemy_manager = require("scripts/maps/enemy_manager")
local item_manager = require("scripts/maps/item_manager")
local switch_manager = require("scripts/maps/switch_manager")

-- Event called at initialization time, as soon as this map is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()

  -- Setup doors.
  door_manager:open_doors_block_moved(map, "block_1", "door_cave_1", 500, "secret")
  --door_manager:open_doors_enemy_dead(map, "enemy_group_1_", "door_cave_sword_", 800, "secret")
  --map:set_doors_open("door_cave_sword_", true)

  -- Setup switches.
  switch_cave_bridge.is_activated = false

  switch_manager:activate_switch_if_savegame_exist(map, "cave_bridge_open", "switch_cave_bridge")

  -- Setup enemies.
  enemy_manager:appears_if_savegame_exist(map, "chest_cave_sword_open", "enemy_group_2_")

  -- Setup entities.
  local bridge_open = game:get_value("cave_bridge_open") or false
  cave_bridge_1:set_enabled(bridge_open)
  cave_bridge_2:set_enabled(bridge_open)
end)

-- Initialize map music.
function map:initialize_music()
  audio_manager:play_music("More-Sewer-Creepers_Looping")
end

-- Spawn enemies when chest is opened, and set main story chapter up.
function chest_sword:on_opened()
  local hero = map:get_hero()
  local item, variant, _ = chest_sword:get_treasure()

  hero:start_treasure(item, variant, "chest_cave_sword_open", function()
    local cave_sword_obtained = 7
    enemy_manager:create_enemies(map, "placeholder_enemy_group_1_", "mud_jelly", "enemy_group_1_")
    enemy_manager:appear_enemies(map, "enemy_group_2_") 
    game:set_main_story(cave_sword_obtained)
  end)
  -- Lock exit until all enemies slain.
  -- TODO: Create down face cave doors
  --door_manager:close_doors_enemy_not_dead(map, "enemy_group_1_", "door_cave_sword_")
end

-- Helper function to make bridge appear.
-- TODO: Make entity manager to handle bridge entities.
local function bridge_switch_on_activated(switch, bridges, savegame, delay, sfx)
  if not game:get_value(savegame) and not switch.is_activated then
    delay = delay or 500
    sfx = sfx or "bomb"

    audio_manager:play_sound("switch")
    switch.is_activated = true
    hero:freeze()
    sol.timer.start(map, delay, function()
      local i = 1 -- Counter for bridge to appear part-by-part.
      for bridge in map:get_entities(bridges) do
        sol.timer.start(map, i * delay, function()
          audio_manager:play_sound(sfx)
          bridge:set_enabled(true)
        end)
        i = i + 1
      end
      sol.timer.start(map, (i * delay) * 2, function()
        game:set_value(savegame, true)
        audio_manager:play_sound("secret")
        hero:unfreeze()
      end)
    end)
  end
end

-- Activate bridge if switch activated.
function switch_cave_bridge:on_activated()
  jumper_bridge_1:set_enabled(false)
  jumper_bridge_2:set_enabled(false) 
  bridge_switch_on_activated(self, "cave_bridge_", "cave_bridge_open")
end