local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()
end)

function map:on_finished()

  -- Return music to default volume.
  sol.audio.set_music_volume(initial_volume)
end

-- Initialize map music.
function map:initialize_music()
  -- TODO: Remove in actual game, expand audio manager to handle
  -- softer track.
  sol.audio.play_music("Soaring-Over-the-Heartland_Looping")
  if initial_volume > 50 then
    sol.audio.set_music_volume(initial_volume / 2)
  end
end