local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local item_manager = require("scripts/maps/item_manager")

map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()
  -- Setup items.
  item_manager:disappear_chest(map, "chest_surana")
  item_manager:chest_appears_switches_activated(map, "switch_group_", "chest_surana", 500, "secret")
  item_manager:chest_appears_if_opened(map, "chest_surana")
end)

-- Initialize map music.
function map:initialize_music()
  audio_manager:play_music("More-Sewer-Creepers_Looping")
end