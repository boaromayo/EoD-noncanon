local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local switch_manager = require("scripts/maps/switch_manager")

local switches_activated

local function check_switch_activated(switch)

  if not switch.is_activated then
    local num_switches = map:get_entities_count("switch_")
    switches_activated = switches_activated + 1
    if switches_activated == num_switches then
      game:set_value("switch_south_cave_open", true)
      map:play_cutscene_cave_door_open()
    end
    switch.is_activated = true -- Prevents any repeated activation.
  end
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup doors.
  door_manager:open_doors_if_savegame_exist(map, "door_cave_switch_south_open", "door_switch_south")

  -- Setup switches.
  switch_manager:activate_switch_if_savegame_exist(map, "switch_south_cave_open", "switch_")

  map:initialize_switches()
end)

-- Initialize puzzle switches on the map.
function map:initialize_switches()
  local switches = map:get_entities("switch_")

  -- Reset/activate switches based on puzzle completion.
  for switch in switches do
    if not game:get_value("switch_south_cave_open") then
      switch.is_activated = false
      switch.on_activated = check_switch_activated
    else
      switch.is_activated = true
    end
  end

  if not game:get_value("switch_south_cave_open") then
    switches_activated = 0
  else
    local num_switches = map:get_entities_count("switch_")
    switches_activated = num_switches
  end
end

function map:play_cutscene_cave_door_open()
  local hero = map:get_hero()
  local camera = map:get_camera()
  local door = map:get_entity("door_switch_south")
  
  sol.timer.start(map, 250, function()
    local speed = 120
    hero:freeze()
    game:set_suspended(true)

    camera:pan_to_entity(door, speed, function()    
      map:open_doors("door_switch_south")
      audio_manager:play_sound("secret")
      
      sol.timer.start(camera, 1750, function()
        camera:pan_to_hero(hero, speed)
      end)
    end)
  end)
end