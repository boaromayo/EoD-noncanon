local map = ...
local game = map:get_game()
local item_manager = require("scripts/maps/item_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function()

  -- Setup items.
  item_manager:disappear_chest(map, "chest_stone_moko")
  item_manager:chest_appears_if_opened(map, "chest_stone_moko")
end)

-- Custom function to check if all enemies are dead.
-- Not using item manager since camera is going
-- to be used to pan into next room, and make chest appear.
local chest = map:get_entity("chest_stone_moko")
for enemy in map:get_entities("enemy_group_1_") do
  function enemy:on_dead()
    if not map:has_entities("enemy_group_1_") and
        not chest:is_open() then
      local hero = map:get_hero()
      local camera = map:get_camera()

      sol.timer.start(map, 0, function()
        hero:freeze()
        game:set_suspended(true)

        local chest_x, chest_y, _ = chest:get_position()
        local movement = sol.movement.create("target")
        local speed = 80
        movement:set_target(camera:get_position_to_track(chest_x, chest_y))
        movement:set_speed(speed)
        movement:set_ignore_obstacles(true)
        movement:set_smooth(false)

        movement:start(camera, function()
          item_manager:appear_chest(map, "chest_stone_moko", 150, "secret")

          sol.timer.start(camera, 1750, function()
            local back_movement = sol.movement.create("target")
            back_movement:set_target(camera:get_position_to_track(hero))
            back_movement:set_speed(speed)
            back_movement:set_ignore_obstacles(true)
            back_movement:set_smooth(false)

            back_movement:start(camera, function()
              camera:start_tracking(hero)
              hero:unfreeze()
              game:set_suspended(false)
            end)
          end)
        end)
      end)
    end
  end
end