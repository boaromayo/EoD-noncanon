-- Lua script of map sura_island/interiors/shop/b1.
local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

end)

function map:check_jail()
  game:start_dialog("_door.cell_locked")
end

function jail_door_shop_npc:on_interaction()
  map:check_jail()
end