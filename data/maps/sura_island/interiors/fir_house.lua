-- Lua script of map Fir House, a place where Fern, a lone woman, lives.
local map = ...
local game = map:get_game()
local fern = map:get_entity("Fern")

-- Check event conditions.
local fern_quest = game:get_value("fern")

-- Event called as soon as this map is loaded.
map:register_event("on_started", function(map)
  -- Check if quest finished, move Fern
  -- to shop bench if it is.
  if fern_quest == 6 then
    fern:set_position(256, 56)
  else
    fern:set_position(80, 56)
  end

  -- Set up door opening for player.
  local sensor = map:get_entity("door_fir_sensor")
  local door = map:get_entity("door_fir")
  local hero = game:get_hero()

  -- Close door by default.
  door:set_enabled(true)

  -- Check if player made contact near
  -- door. If so, open it.
  sensor.on_activated_repeat = function()
    if hero:get_direction() == 3 and -- Player facing south, and door is closed.
        door:is_enabled() then
      sol.audio.play_sound("door_open")
      door:set_enabled(false)
    end
  end
end)

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc.fern." .. dialog_id
end

--[[
--=========================
Fern dialogs:
hello: say hello
hello2: greet after dungeon finish
request: ask for request
yes: accept
no: oh, well
no_item: ask if hero has item
weapon: chest has a weapon to use
tip: give tips on item
reward: praise player and give prize
--=========================
--]]
function map:fern_dialog()
  -- In any case fern event was not initialized.
  if fern_quest == nil then
    fern_quest = 0
    game:set_value("fern", 0)
  end
  -- Introduce fern to player.
  if fern_quest == 0 then
    game:start_dialog(call_dialog_id("hello"), function()
      game:start_dialog(call_dialog_id("request"), function(answer)
        if answer == 1 then -- If yes.
          game:start_dialog(call_dialog_id("yes"), function()
            if game:get_value("c1") == true then -- If got sword, skip weapon dialogue.
              game:set_value("fern", 2)
              fern_quest = game:get_value("fern")
            else
              game:start_dialog(call_dialog_id("weapon"))
              game:set_value("fern", 2)
              fern_quest = game:get_value("fern")
            end
          end)
        else -- If no, be disappointed.
          game:start_dialog(call_dialog_id("no"))
        end
      end)
    end) -- Call decision function.
  elseif fern_quest == 1 then
    game:start_dialog(call_dialog_id("weapon")) -- Mention weapon in chest.
    game:set_value("fern", 2)
    fern_quest = game:get_value("fern")
    game:start_dialog(call_dialog_id("tip"))
  elseif fern_quest == 2 then
    if game:get_value("c1") == true then -- If sword gotten, skip weapon dialogue.
      game:set_value("fern", 3)
      fern_quest = game:get_value("fern")
      game:start_dialog(call_dialog_id("no_item")) -- If item not found yet.
    else
      game:start_dialog(call_dialog_id("tip")) -- Tell tips on item.
      game:set_value("fern", 1)
      fern_quest = game:get_value("fern")
    end
  elseif fern_quest == 3 then
    game:start_dialog(call_dialog_id("no_item")) -- If item not found yet.
  elseif fern_quest == 4 then
    game:start_dialog(call_dialog_id("reward")) -- Give reward if quest finished.
    game:get_reward("very-important-item") -- Rewards very important item.
    game:set_value("fern", 5)
    fern_quest = game:get_value("fern")
  else
    game:start_dialog(call_dialog_id("hello2")) -- When revisit, greet from bench.
  end
  -- After getting weapon in chest,
  -- ask if hero has item.
  if (fern_quest == 1 or fern_quest == 2) 
      and game:get_value("c1") == true then
    game:set_value("fern", 3)
    fern_quest = game:get_value("fern")
  end
end