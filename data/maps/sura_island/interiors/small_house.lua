-- Lua script of map sura_island/interiors/small_house.
local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  map:initialize_music()
end)

-- Event called when player leaves the map.
function map:on_finished()

  -- Return music to default volume.
  sol.audio.set_music_volume(initial_volume)
end

-- Initialize map music.
function map:initialize_music()

  if initial_volume > 50 then
    sol.audio.set_music_volume(initial_volume / 2)
  end
end

--[[
function mother:on_interaction()
end--]]