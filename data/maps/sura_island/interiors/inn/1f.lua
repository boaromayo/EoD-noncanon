-- Lua script of map sura_island/interiors/inn/1f.
local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

local npc_manager = require("scripts/maps/npc_manager")

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  map:initialize_music()

  map:initialize_npc_behavior()
end)

-- Event called when player leaves the map.
function map:on_finished()

  -- Return music to default volume.
  sol.audio.set_music_volume(initial_volume)
end

function map:initialize_music()
  if initial_volume > 50 then
    sol.audio.set_music_volume(initial_volume / 2)
  end
end

function map:initialize_npc_behavior()
  -- Setup NPC map visibility based on story progress.
  npc_manager:show_npcs_main_story(map, 1, "reception")

  npc_manager:show_npcs_main_story(map, 4, "tour_guide")

  npc_manager:hide_npcs_main_story(map, 7, "tour_guide")

  -- Setup NPC dialogs.
  --npc_manager:set_dialog_main_story(map, 4, "chef", call_dialog_id("chef.1"))

  --npc_manager:set_dialog_main_story(map, 4, "sous_chef", call_dialog_id("sous_chef.1"))
end

function map:talk_tour_guide()
  local dialogs = {
    call_dialog_id("tour_guide.1"),
    call_dialog_id("tour_guide.2"),
  }
  map:npc_toggle_talk("tour_guide_met", dialogs)
end

-- TODO: Add toggle functionality to NPC manager.
function map:npc_toggle_talk(npc_flag, dialogs)
  local met_npc = game:get_value(npc_flag)
  if not met_npc then
    game:start_dialog(dialogs[1], function()
      game:set_value(npc_flag, true)
    end)
  else
    game:start_dialog(dialogs[2], function()
      game:set_value(npc_flag, false)
    end)
  end
end