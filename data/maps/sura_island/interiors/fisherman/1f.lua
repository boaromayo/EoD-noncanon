-- Lua script of map sura_island/interiors/fisherman/1f.
local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

local npc_manager = require("scripts/maps/npc_manager")

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  map:initialize_music()

  map:initialize_npc_behavior()
end)

-- Event called when player leaves map.
function map:on_finished()

  -- Return music to default volume.
  sol.audio.set_music_volume(initial_volume)
end

-- Initialize map music.
function map:initialize_music()

  if initial_volume > 50 then
    sol.audio.set_music_volume(initial_volume / 2)
  end
end

function map:initialize_npc_behavior()
  -- Setup NPC based on story progress.
  npc_manager:show_npcs_main_story(map, 7, "loiterer") 
  
  npc_manager:set_dialog_main_story(map, 7, "loiterer", call_dialog_id("loiterer.1"))
end