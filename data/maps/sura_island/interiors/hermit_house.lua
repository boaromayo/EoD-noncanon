-- Lua script of map sura_island/interiors/hermit_house.
local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map) 

  map:initialize_music()
end)

-- Initialize map music.
function map:initialize_music()

  audio_manager:play_music("Secret-Sea-Cave_Looping")
end
