-- Lua script of map sura_island/interiors/swordmaker/1f.
local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

local audio_manager = require("scripts/audio_manager")
local npc_manager = require("scripts/maps/npc_manager")

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  map:initialize_music()

  map:initialize_npc_behavior()
end)

-- Event called when the player leaves the map.
function map:on_finished()

  -- Return music to default volume. 
  sol.audio.set_music_volume(initial_volume)
end

-- Initialize map music.
function map:initialize_music()
  
  if game:main_story(6) and game:main_story_at_most(9) then
    audio_manager:play_music("The-Idol_Looping")
  else
    -- TODO: Remove in actual game, expand audio manager to handle
    -- audio volume.
    audio_manager:play_music("Soaring-Over-the-Heartland_Looping")
    if initial_volume > 50 then
      sol.audio.set_music_volume(initial_volume / 2)
    end
  end
end

-- Initialize NPC behavior.
function map:initialize_npc_behavior()
  -- Setup NPC visibility.
  npc_manager:hide_npcs_main_story(map, 1, "swordmaker_shop")
  npc_manager:hide_npcs_main_story(map, 1, "swordmaker_working")
  npc_manager:hide_npcs_main_story(map, 1, "swordmaker_sitting")

  npc_manager:show_npcs_main_story(map, 4, "swordmaker_sitting")

  npc_manager:hide_npcs_main_story(map, 6, "swordmaker_sitting")

  npc_manager:show_npcs_main_story(map, 6, "swordmaker_shop")
  npc_manager:show_npcs_main_story(map, 6, "shop_npc")

  npc_manager:hide_npcs_main_story(map, 8, "swordmaker_shop")
  npc_manager:hide_npcs_main_story(map, 8, "shop_npc")

  npc_manager:show_npcs_main_story(map, 8, "swordmaker_working")

  npc_manager:hide_npcs_main_story(map, 10, "swordmaker_working")

  npc_manager:show_npcs_main_story(map, 10, "swordmaker_shop")
  npc_manager:show_npcs_main_story(map, 10, "shop_npc")

  -- Setup NPC dialogs.
  --npc_manager:set_dialog_main_story(map, 10, "swordmaker_shop", call_dialog_id("swordmaker.shop.1"))
end

function sensor_hero_spotted:on_activated()
  if game:main_story_at(4) then
    -- TODO: Have swordmaker ask hero to speak to him. 
  end
end

function sensor_stop_1:on_activated()
  map:swordmaker_stop()
end

function sensor_stop_2:on_activated()
  map:swordmaker_stop()
end

-- Swordmaker tells hero to come back.
function map:swordmaker_stop()
  if game:main_story_at(4) then
    game:start_dialog(call_dialog_id("swordmaker.come_back.1"), function()
      local EAST, WEST = 0, 2
      local movement = nil
      if hero:get_direction() == EAST then
        movement = "444"
      elseif hero:get_direction() == WEST then
        movement = "000"
      end
      hero:walk(movement, false, false)
    end)
  end
end

-- Swordmaker NPC processing.
function map:talk_swordmaker()
  if game:main_story_at(4) then
    -- Intro.
    game:start_dialog(call_dialog_id("swordmaker.sitting.1"), function()
      -- Swordmaker gives wallet.
      hero:start_treasure("wallet", 1, "village_get_wallet", function()
        game:add_money(20)
        game:update_main_story(5)
      end)
    end)
  elseif game:main_story_at(5) then
    -- Swordmaker tells directions to boat man.
    game:start_dialog(call_dialog_id("swordmaker.sitting.2"))
  elseif game:main_story_at(6) then
    -- Swordmaker requests hero to fetch sword.
    if not game:get_value("sword_request") then
      game:start_dialog(call_dialog_id("swordmaker.shop.1"), function()
        game:set_value("sword_request", true)
      end)
    elseif game:get_value("sword_request") then
      game:start_dialog(call_dialog_id("swordmaker.shop.2"))
    end
  elseif game:main_story(7) then
    if not game:get_value("temple_request") then
      game:start_dialog(call_dialog_id("swordmaker.sword_get.1"), function()
        game:set_value("temple_request", true)
      end)
    elseif game:get_value("temple_request") then
      game:start_dialog(call_dialog_id("swordmaker.sword_get.2"))
    end
  elseif game:main_story(9) then
    -- TODO: more dialog for swordmaker.
  end
end

function swordmaker_sitting:on_interaction()
  map:talk_swordmaker()
end

function swordmaker_shop:on_interaction()
  map:talk_swordmaker()
end

function shop_npc:on_interaction()
  local hero = map:get_hero()
  local swordmaker = swordmaker_shop:get_sprite()
  local to_hero = swordmaker_shop:get_direction4_to(hero)
  swordmaker:set_direction(to_hero)
  map:talk_swordmaker()
end

function swordmaker_working:on_interaction()
  map:talk_swordmaker()
end