-- Lua script of map sura_island/interiors/swordmaker/2f.
local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")
local npc_manager = require("scripts/maps/npc_manager")
local audio_manager = require("scripts/audio_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup hero if warped from shore cutscene.
  if game:main_story_at(4) and not game:get_value("village_hero_awake") then
    game:hide_hud()
    game:disable_pause_menu()
    game:set_life(game:get_max_life())
    map:sleep_hero()
  else
    npc_manager:hide_npcs_savegame_exist(map, "village_hero_awake", "swordmaker_looking")
  end
end)

function map:sleep_hero()
  hero:set_visible(false)
  hero_sleeping:create_sprite(hero:get_tunic_sprite_id())
  hero_sleeping:get_sprite():set_animation("sleeping")
end

function map:on_opening_transition_finished(destination)

  if destination:get_name() == "start" and not game:get_value("village_hero_awake") then
    map:play_cutscene_awake()
  end
end

-- Cutscene: Play cutscene of swordmaker looking on.
-- After a bit, he heads downstairs. Then, hero wakes up.
function map:play_cutscene_awake()
  local swordmaker_sprite = swordmaker_looking:get_sprite()
  cutscene.builder(game, map, hero)
    .freeze()
    .wait(3000)
    .dont_wait_for.sprite_animation(swordmaker_sprite,"walking")
    .set_direction(swordmaker_sprite,3)
    .movement({
      type = "path",
      entity = swordmaker_looking,
      properties = {
        path = {{4,4,4,4,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2}},
        speed = 60,
        ignore_obstacles = false,
      }
    })
    .dont_wait_for.sprite_animation(swordmaker_sprite,"stopped")
    .exec(function()
      audio_manager:play_sound("stairs_down_start")
      swordmaker_looking:set_enabled(false)
    end)
    .wait(3000)
    .and_then(function(cont)
      hero_sleeping:set_enabled(false)
      hero:set_visible(true)
      hero:get_sprite():set_animation("stopped")
      hero:start_jumping(4, 120, true) -- Hero jumps west by, technically, 24 pixels, but to prevent colliding with the bed, overshoot fivefold.
      audio_manager:play_sound("hero_lands")
      cont()
    end)
    .wait(100)
    .exec(function()
      game:show_hud()
      game:enable_pause_menu()
      game:set_value("village_hero_awake", true)
    end)
    .unfreeze()
  .start()
end