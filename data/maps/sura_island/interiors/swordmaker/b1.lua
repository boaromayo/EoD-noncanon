-- Lua script of map sura_island/interiors/swordmaker/b1.
local map = ...
local game = map:get_game()
local initial_volume = sol.audio.get_music_volume()

local audio_manager = require("scripts/audio_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  map:initialize_music()
end)

-- Event called when player leaves the map.
function map:on_finished()

  -- Return music to default volume.
  sol.audio.set_music_volume(initial_volume)
end

-- Initialize map music.
function map:initialize_music()

  -- Play village music.
  audio_manager:play_music("Soaring-Over-the-Heartland_Looping")
  if initial_volume > 50 then
    sol.audio.set_music_volume(initial_volume / 2)
  end
end
