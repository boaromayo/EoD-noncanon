-- Lua script of map sura_island/village_exterior (section A-4).
local map = ...
local game = map:get_game()

local audio_manager = require("scripts/audio_manager")
local item_manager = require("scripts/maps/item_manager")
local npc_manager = require("scripts/maps/npc_manager")

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island.village.npc." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()
  
  -- Setup NPC behavior (visibility, dialogs, movements, animations).
  map:initialize_npc_behavior()

  -- Setup items.
  --item_manager:chest_appears_if_savegame_exist(map, "chest_village_perch_open", "chest_perch")
end)

function map:initialize_music()
  audio_manager:play_music("Soaring-Over-the-Heartland_Looping")
end

function map:initialize_npc_behavior()
  -- Setup direction constants for NPC.
  local DOWN = 3 

  -- Setup NPC map visibility.
  npc_manager:show_npcs_main_story(map, 1, "flower_girl")
  npc_manager:show_npcs_main_story(map, 1, "flower_boy")
  npc_manager:show_npcs_main_story(map, 1, "loiterer")
  npc_manager:show_npcs_main_story(map, 1, "old_man")
  npc_manager:show_npcs_main_story(map, 1, "bruto")
  npc_manager:show_npcs_main_story(map, 1, "gossip")

  npc_manager:show_npcs_main_story(map, 4, "boatman")
  npc_manager:show_npcs_main_story(map, 4, "boat_handler_")
  npc_manager:show_npcs_main_story(map, 4, "tourist_")

  npc_manager:hide_npcs_main_story(map, 7, "flower_girl")
  npc_manager:hide_npcs_main_story(map, 7, "flower_boy")
  npc_manager:hide_npcs_main_story(map, 7, "loiterer")
  npc_manager:hide_npcs_main_story(map, 9, "tourist_")

  npc_manager:hide_npcs_main_story(map, 1, "flower_girl_collapsed")
  npc_manager:hide_npcs_main_story(map, 1, "flower_boy_collapsed")

  npc_manager:show_npcs_main_story(map, 7, "flower_girl_collapsed")
  npc_manager:show_npcs_main_story(map, 7, "flower_boy_collapsed")

  npc_manager:hide_npcs_main_story(map, 10, "flower_girl_collapsed")
  npc_manager:hide_npcs_main_story(map, 10, "flower_boy_collapsed")
  npc_manager:hide_npcs_main_story(map, 10, "boatman")
  npc_manager:hide_npcs_main_story(map, 10, "boat_handler_1")

  -- Setup NPC dialogs.
  npc_manager:set_dialog_main_story(map, 1, "flower_girl", call_dialog_id("flowergirl.1"))
  npc_manager:set_dialog_main_story(map, 1, "flower_boy", call_dialog_id("flowerboy.1"))
  npc_manager:set_dialog_main_story(map, 1, "old_man", call_dialog_id("old_man.1"), function()
  old_man:get_sprite():set_direction(DOWN)
end)
  npc_manager:set_dialog_main_story(map, 1, "loiterer", call_dialog_id("loiterer.1"))
  npc_manager:set_dialog_main_story(map, 1, "bruto", call_dialog_id("bruto.1"))

  npc_manager:set_dialog_main_story(map, 4, "boatman", call_dialog_id("boat_man.1"), function()
  game:remove_money(20)
  game:start_dialog(call_dialog_id("boat_man.paying"), function()
    game:update_main_story(6)
  end)
end)
  npc_manager:set_dialog_main_story(map, 4, "boat_handler_1", call_dialog_id("handler1.1"))
  
  npc_manager:set_dialog_main_story(map, 6, "boatman", call_dialog_id("boat_man.2"))
  npc_manager:set_dialog_main_story(map, 6, "boat_handler_1", call_dialog_id("handler1.2"))

  npc_manager:set_dialog_main_story(map, 7, "flower_girl_collapsed", call_dialog_id("flowergirl.2"))
  npc_manager:set_dialog_main_story(map, 7, "flower_boy_collapsed", call_dialog_id("flowerboy.2"))
  --npc_manager:set_dialog_main_story(map, 7, "old_man", call_dialog_id("old_man.2"), function()
  --old_man:get_sprite():set_direction(DOWN)
--end)

  -- Setup NPC movements.
  local movement_circle_path = { 0,0,0,0,0,0,2,2,2,2,2,2,2,2,4,4,4,4,4,4,4,4,6,6,6,6,6,6,6,6,0,0 }
  local flower_girl_movement = sol.movement.create("path")
  local flower_boy_movement = sol.movement.create("path")

  flower_girl_movement:set_path(movement_circle_path)
  flower_girl_movement:set_speed(70)
  flower_girl_movement:set_loop(true)
  flower_girl_movement:set_ignore_obstacles(true)

  flower_boy_movement:set_path(movement_circle_path)
  flower_boy_movement:set_speed(65)
  flower_boy_movement:set_loop(true)
  flower_boy_movement:set_ignore_obstacles(true)

  local tourist_movement = sol.movement.create("random")

  tourist_movement:set_speed(40)
  tourist_movement:set_ignore_obstacles(false)
  tourist_movement:set_smooth(false)

  npc_manager:set_movement_main_story(map, 1, flower_girl_movement, "flower_girl")
  npc_manager:set_movement_main_story(map, 1, flower_boy_movement, "flower_boy")

  --npc_manager:set_movement_main_story(map, 4, tourist_movement, "tourist_1")
  npc_manager:set_movement_main_story(map, 4, tourist_movement, "tourist_3")

  npc_manager:stop_movement_main_story(map, 7, "flower_girl")
  npc_manager:stop_movement_main_story(map, 7, "flower_boy")

  --npc_manager:stop_movement_main_story(map, 9, "tourist_1")
  npc_manager:stop_movement_main_story(map, 9, "tourist_3")

  -- Setup NPC animations.
  npc_manager:set_animation(map, "flower_boy_collapsed", "sleeping")
  npc_manager:set_animation(map, "flower_girl_collapsed", "sleeping")
  npc_manager:set_animation(map, "tourist_2", "gushing")
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

  -- Show fancy location title if first visit. Otherwise,
  -- show regular location title.
  -- TODO: Animate fancy location title.
  --[[local hero = map:get_hero()
  if game:get_value("village_visited") == nil then
    hero:freeze()
    sol.timer.start(map, 5000, function()
      game:set_value("village_visited", true)
      hero:unfreeze()
    end)
  else
    -- TODO: Animate normal location title.
  end--]]
end

-- Check player unarmed or not.
-- TODO: Make better weapon check in later updates.
function sensor_weapon_check:on_activated()
  local has_weapon = game:has_item("stick") or game:has_item("sword")

  if not has_weapon then
    game:start_dialog("_map.area.sura_island.village.no_weapon", function()
      local movement = "666"
      hero:walk(movement, false, false)
    end)
  end
end

-- NPC interactions for multi-dialog NPCs.
function gossip:on_interaction()
  map:talk_gossip()
end

function boat_handler_2:on_interaction()
  map:talk_boat_handler_2()
end

function tourist_1:on_interaction()
  map:talk_tourist_1()
end

function tourist_2:on_interaction()
  map:talk_tourist_2()
end

function tourist_2:facing_bruto()
  local direction = tourist_2:get_direction4_to(bruto)
  return tourist_2:get_sprite():get_direction() == direction
end

function tourist_2:face_bruto()
  local direction = tourist_2:get_direction4_to(bruto)
  local sprite = tourist_2:get_sprite()
  sprite:set_direction(direction)
  sprite:set_animation("gushing")
end

function tourist_2:face_hero()
  local hero = map:get_hero()
  local direction = tourist_2:get_direction4_to(hero)
  local sprite = tourist_2:get_sprite()
  sprite:set_direction(direction)
  sprite:set_animation("stopped")
end

function tourist_3:on_interaction()
  map:talk_tourist_3()
end

-- Implementation of NPC interactions.
function map:talk_gossip()
  local LEFT = 2
  local gossip_status = game:get_value("gossip_state") or 1
  if game:main_story(1) then
    if gossip_status < 4 then
      game:start_dialog(call_dialog_id("gossip." .. gossip_status), function()
        game:set_value("gossip_state", gossip_status + 1)
        gossip:get_sprite():set_direction(LEFT) -- FIXME: Need to avoid repetition for sprite in facing left after talk.
      end)
    else
      game:start_dialog(call_dialog_id("gossip." .. gossip_status), function()
        game:set_value("gossip_state", 1)
        gossip:get_sprite():set_direction(LEFT)
      end)
    end
  end -- TODO: Make more dialog for gossip, and place in a gossip handler script, can use gossip as a way to point player to next direction in-game. This is also useful in avoiding repetition.
end

function map:talk_boat_handler_2()
  local dialogs = {
    call_dialog_id("handler2.1"), 
    call_dialog_id("handler2.2"),
  }
  map:npc_toggle_talk("shiphandler_met", dialogs)
end

function map:talk_tourist_1()
  local dialogs = {
    call_dialog_id("tourist1.1"), 
    call_dialog_id("tourist1.2"),
  }
  map:npc_toggle_talk("tourist_1_met", dialogs)
end

function map:talk_tourist_2()
  if game:main_story(4) and game:main_story_at_most(8) then
    tourist_2:get_sprite():set_ignore_suspend(true)
    local first_meeting = game:get_value("tourist_2_met")
    if not tourist_2:facing_bruto() then
      tourist_2:face_bruto()
    end
    if not first_meeting then
      game:start_dialog(call_dialog_id("tourist2.gushing"), function()
        tourist_2:face_hero()
        game:start_dialog(call_dialog_id("tourist2.1"), function()
          tourist_2:face_bruto()
          game:set_value("tourist_2_met", true)
        end)
      end)
    else
      tourist_2:face_hero()
      game:start_dialog(call_dialog_id("tourist2.2"), function()
        tourist_2:face_bruto()
        game:start_dialog(call_dialog_id("tourist2.gushing2"), function()
          game:set_value("tourist_2_met", false)
        end)
      end)
    end
  end
end

function map:talk_tourist_3()
  local dialogs = {
    call_dialog_id("tourist3.1"), 
    call_dialog_id("tourist3.2"),
  }
  map:npc_toggle_talk("tourist_3_met", dialogs)
end

-- TODO: Add toggle functionality to NPC manager.
function map:npc_toggle_talk(npc_flag, dialogs)
  local met_npc = game:get_value(npc_flag)
  if not met_npc then
    game:start_dialog(dialogs[1], function()
      game:set_value(npc_flag, true)
    end)
  else
    game:start_dialog(dialogs[2], function()
      game:set_value(npc_flag, false)
    end)
  end
end