-- Lua script of map sura_island/temple_entrance (section C-3).
local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)
  
  -- Setup map music.
  map:initialize_music()
end

function map:initialize_music()
  audio_manager:play_music("Lost-Island_Looping")
end
