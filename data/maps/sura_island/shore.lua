-- Lua script of map sura_island/shore (close-up of southern shore).
local map = ...
local game = map:get_game()

-- Helper function to shorten dialog id calls.
local function call_dialog_id(dialog_id)
  return "_map.area.sura_island." .. dialog_id
end

-- Setup hero to move backward.
local function move_back(movement)
  movement = movement or "6666"
  local hero = map:get_hero()
  hero:walk(movement, false, false)
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)

  -- TODO: Play ambient sea waves sound/music.
end)

function sensor_not_pass:on_activated()
  game:start_dialog(call_dialog_id("wrongway.1"), move_back)
end