-- Lua script of map ship/a.
local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local enemy_manager = require("scripts/maps/enemy_manager")

local traders_defeated = game:get_value("traders_a_deck_defeated")

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.ship.a." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()

  -- Setup enemies.
  map:initialize_enemies()

  -- Setup doors.
  door_manager:open_doors_enemy_dead(map, "enemy_trader_", "door_battle_", 500, "secret")

  map:set_doors_open("door_battle_", true)
end)

-- Initialize map music.
function map:initialize_music()
  -- Update main story to player escaping ship and play music if not playing.
  local track = "City-of-the-Disturbed_Looping"
  if game:main_story(2) and sol.audio.get_music() ~= track then
    audio_manager:play_music(track)
  end
end

-- Initialize enemies.
function map:initialize_enemies()
  if not traders_defeated then
    enemy_manager:disappear_enemies(map, "enemy_trader_")
  end
end

-- When approaching next room, lock door and start battle.
function sensor_battle_traders:on_activated()
  local saved_music = sol.audio.get_music()

  -- Check enemies defeated and is first time visiting, if not, play cutscene.
  if not traders_defeated then
    cutscene.builder(game, map, hero)
      .freeze()
      .exec(function()
        audio_manager:stop_music()
        door_manager:close_doors_enemy_not_dead(map, "enemy_trader_", "door_battle_")
      end)
      .wait(400)
      .set_direction(hero,1)
      .wait(600)
      .set_direction(hero,3)
      .wait(600)
      .exec(function()
        enemy_manager:appear_enemies(map, "enemy_trader_")
      end)
      .dialog(call_dialog_id("trader.1"))
      .unfreeze()
      .exec(function()
        audio_manager:play_music(saved_music)
        sensor_battle_traders:set_enabled(false)
      end)
    .start()
  end
end