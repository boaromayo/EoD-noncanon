-- Lua script of map ship/c.
local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")
local audio_manager = require("scripts/audio_manager")

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.ship.c." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)

  -- Setup music.
  map:initialize_music()

  -- Setup initial hero animation.
  map:initialize_hero(destination)

  -- If player has a stick in inventory, 
  -- make stick barrels not drop sticks anymore.
  if game:get_value("stick_get") then
    local barrels = map:get_entities("stickbarrel")
    for barrel in barrels do
      barrel:set_treasure(nil)
    end
  end
end)

-- Initialize map music.
function map:initialize_music()
  local track = "City-of-the-Disturbed_Looping"
  if game:main_story(2) and sol.audio.get_music() ~= track and game:get_value("den_free") then
    audio_manager:play_music(track)
  end
end

-- Initialize hero animation.
function map:initialize_hero(destination)
  if game:main_story_at(0) and destination:get_name() == "start" then
    hero:get_sprite():set_animation("sleeping")
  end
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished(destination)
  if game:main_story_at(0) and destination:get_name() == "start" then
    -- Setup cutscene for hero in prison.
    map:play_cutscene_look_around()
  end
end

-- Cutscenes.
function map:play_cutscene_look_around()
  -- Setup walking speeds for hero.
  local slow_speed = 40
  local normal_speed = hero:get_walking_speed()
  -- Setup initial movements for hero.
  --local movement1 = "66000"
  --local movement2 = "0022"
  --local movement3 = "66"

  -- Slow hero down a bit.
  hero:set_walking_speed(slow_speed)

  -- Setup first cutscene after fade-in finished.
  cutscene.builder(game, map, hero)
    .freeze()
    -- Redundant, but keeps hero sleeping after transition.
    .dont_wait_for.hero_animation("sleeping")
    .wait(2000)
    .dont_wait_for.hero_animation("stopped")
    -- Look east, west, then south.
    .set_direction(hero,0)
    .wait(1000)
    .set_direction(hero,2)
    .wait(500)
    .set_direction(hero,3)
    .wait(1000)
    -- Start moving south, then east.
    .set_direction(hero,3)
    .dont_wait_for.hero_animation("walking")
    .movement({
      type = "path",
      entity = hero,
      properties = {
        path = {{6,6}},
        speed = slow_speed,
        ignore_obstacles = false
      }
    })
    .set_direction(hero,0)
    .movement({
      type = "path",
      entity = hero,
      properties = {
        path = {{0,0,0}},
        speed = slow_speed,
        ignore_obstacles = false
      }
    })
    .wait(300)
    .dont_wait_for.hero_animation("stopped")
    .wait(1700)
    -- Look west, north, then south.
    .set_direction(hero,2)
    .wait(500)
    .set_direction(hero,1)
    .wait(500)
    .set_direction(hero,3)
    .wait(2000)
    -- Move north.
    .set_direction(hero,0)
    .dont_wait_for.hero_animation("walking")
    .movement({
      type = "path",
      entity = hero,
      properties = {
        path = {{0,0}},
        speed = slow_speed,
        ignore_obstacles = false
      }
    })
    .set_direction(hero,1)
    .movement({
      type = "path",
      entity = hero,
      properties = {
        path = {{2,2}},
        speed = slow_speed,
        ignore_obstacles = false
      }
    })
    .dont_wait_for.hero_animation("stopped")
    .wait(2000)
    -- Move back south.
    .set_direction(hero,3)
    .dont_wait_for.hero_animation("walking")
    .movement({
      type = "path",
      entity = hero,
      properties = {
        path = {{6,6}},
        speed = slow_speed,
      }
    })
    .dont_wait_for.hero_animation("stopped")
    .wait(2000)
    -- Allow player to move hero.
    .unfreeze()
    .exec(function()
      -- Set walking speed to default.
      hero:set_walking_speed(normal_speed)
      game:set_value("main_story", 1)
      if not game:get_value("possession_prison_key") then
        map:countdown(10000, map.play_cutscene_whisper)
      end
    end)
  .start()
end

function map:play_cutscene_whisper()
  cutscene.builder(game, map, hero)
    .freeze()
    .dialog(call_dialog_id("lyn.1"))
    .wait(1000)
    .set_direction(hero,0)
    .wait(800)
    .set_direction(hero,2)
    .wait(800)
    .set_direction(hero,3)
    .unfreeze()
    .exec(function()
      -- Start countdown of 8 seconds, then play next cutscene.
      map:countdown(8000, map.play_cutscene_give_key)
    end)
  .start()
end

function map:play_cutscene_give_key()
  cutscene.builder(game, map, hero)
    .freeze()
    .dialog(call_dialog_id("lyn.2"))
    .wait(400)
    .set_direction(hero,0)
    .dont_wait_for.hero_animation("walking")
    .movement(
      {
        type = "target",
        entity = hero,
        properties = {
          speed = 60,
          target = {124,204},
          smooth = true,
          ignore_obstacles = true
        }
      }
    )
    .dont_wait_for.hero_animation("stopped")
    .wait(400)
    .exec(function()
      game:start_dialog(call_dialog_id("lyn.3"), function(answer)
        if answer == 1 then
          game:start_dialog(call_dialog_id("lyn.3a"), function()
            game:start_dialog(call_dialog_id("lyn.4"))
          end)
        elseif answer == 2 then
          game:start_dialog(call_dialog_id("lyn.3b"), function()
            game:start_dialog(call_dialog_id("lyn.4"))
          end)
        end
      end)
    end)
    .wait(500)
    .exec(function()
      hero:start_treasure("key_prison", 1)
      hero:unfreeze()
    end)
  .start()
end

function map:play_cutscene_begin_escape()
  cutscene.builder(game, map, hero)
    .freeze()
    .dialog(call_dialog_id("lyn.1"))
    .set_direction(hero,1)
    .wait(400)
    .dont_wait_for.hero_animation("walking")
    .movement(
      {
        type = "target",
        entity = hero,
        properties = {
          speed = 60,
          target = {96,224},
          smooth = true,
          ignore_obstacles = true
      }
    })
    .dont_wait_for.hero_animation("stopped")
    .dialog(call_dialog_id("lyn.5"))
    .set_direction(hero,3)
    .wait(500)
    .exec(function()
      game:update_main_story(2)
      game:show_life_bar()
      audio_manager:play_music("City-of-the-Disturbed_Looping")
      hero:unfreeze()
    end)
  .start()
end

-- If Den leaves cell, have their rescuer whisper more tips.
function free_sensor:on_activated()
  if not game:get_value("den_free") then
    map:play_cutscene_begin_escape()
  end
  game:set_value("den_free", true)
end

-- Jail cell checking.
function map:check_jail()
  if game:has_item("key_prison") then
    audio_manager:play_sound("door_open")
    for jail_cell in map:get_entities("jail_door_den") do
      jail_cell:set_enabled(false)
    end
    local _ , _, layer = jail_door_den:get_position()
    --local layer = self:get_layer() -- For version >= 1.6 only
    map:create_dynamic_tile({
      name = "jail_ceiling",
      layer = layer + 1,
      x = 88,
      y = 208,
      width = 16,
      height = 16,
      pattern = "152",
      enabled_at_start = true
    })
  else
    game:start_dialog("_door.cell_locked")
  end
end

-- If hero interacts with the jail cell with key in hand,
-- open the cell.
function jail_door_den_npc:on_interaction()
  if jail_door_den:is_enabled() then
    map:check_jail()
  end
end