-- Lua script of map ship/a_ext.
local map = ...
local game = map:get_game()
local hero = game:get_hero()
local cutscene = require("scripts/maps/cutscene")
local audio_manager = require("scripts/audio_manager")

local black_screen
local BLACK = { 0, 0, 0 }

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.ship.a." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()

  black_screen = sol.surface.create()
  black_screen:fill_color(BLACK)
  black_screen:set_opacity(0)
end)

-- Initialize map music.
function map:initialize_music()
  -- TODO: Add ocean waves ambience.
  audio_manager:stop_music()
end

function map:on_draw(screen)

  black_screen:draw(screen)
end

-- Show traders.
function map:show_traders()
  local placeholder_1 = map:get_entity("buccaneer_placeholder")
  local placeholder_2 = map:get_entity("buccaneer_plus_placeholder")
  local placeholder_3 = map:get_entity("buccaneer_three_placeholder")
  local x1, y1, z1 = placeholder_1:get_position()
  local x2, y2, z2 = placeholder_2:get_position()
  local x3, y3, z3 = placeholder_3:get_position()
  local d1 = placeholder_1:get_direction()
  local d2 = placeholder_2:get_direction()
  local d3 = placeholder_3:get_direction()
  local buc_1 = map:create_npc({
    sprite = "enemies/buccaneer",
    name = "trader_1",
    x = x1,
    y = y1,
    layer = z1,
    direction = d1,
    subtype = 1,
  })
  local buc_2 = map:create_npc({
    sprite = "enemies/buccaneer_plus",
    name = "trader_2",
    x = x2,
    y = y2,
    layer = z2,
    direction = d2,
    subtype = 1,
  })
  local buc_3 = map:create_npc({
    sprite = "enemies/buccaneer",
    name = "trader_3",
    x = x3,
    y = y3,
    layer = z3,
    direction = d3,
    subtype = 1,
  })
  placeholder_1:set_enabled(false)
  placeholder_2:set_enabled(false)
  placeholder_3:set_enabled(false)
end

-- Show captain boss.
function map:show_boss()
  local placeholder = map:get_entity("boss_captain_placeholder")
  local x, y, layer = placeholder:get_position()
  local direction = placeholder:get_direction()
  local captain = map:create_enemy({
    breed = "boss/captain",
    name = "boss_captain",
    x = x,
    y = y,
    layer = layer,
    direction = direction,
  })
  --captain:get_sprite():set_animation("eyes_closed")
  placeholder:set_enabled(false)
end

-- Close railings.
function map:close_railings()
  audio_manager:play_sound("door_closed")
  local railings = map:get_entities("railing_")
  for railing in railings do
    railing:set_enabled(true)
  end
end

-- Cutscene for ship exterior.
--[[function map:play_cutscene_trapped_on_deck()
  cutscene.builder(game, map, hero)
    .freeze()
    .wait(100)
    .exec(function()
      map:close_railings()
    end)
    .wait(500)
    .set_direction(hero,1)
    .wait(500)
    .set_direction(hero,3)
    .wait(250)
    .exec(function()
      game:hide_life_bar()
      game:hide_weapon_icon()
      map:show_boss()
    end)
    .wait(10)
    .and_then(function(cont)
      map:play_cutscene_to_captain(cont)
    end)
    .and_then(function(cont)
      game:show_life_bar()
      game:show_weapon_icon()
      audio_manager:play_music("RPG-Battle-Climax-2")
      cont()
    end)
    .unfreeze()
  .start()
end

-- Pan camera to captain.
function map:play_cutscene_to_captain(callback)
  local camera = map:get_camera()
  local hero = map:get_hero()
  local captain = map:get_entity("boss_captain")

  sol.timer.start(map, 1000, function()
    local speed = 80
    
    camera:pan_to_entity(captain, speed, function()
      game:start_dialog(call_dialog_id("captain.1"), function()
        --captain:get_sprite():set_animation("opening_eyes")
        sol.timer.start(camera, 800, function()
          -- Cut back to player, and proceed to next step in cutscene.
          camera:start_tracking(hero)
          callback()
        end)
      end)
    end)
  end)
end--]]

-- Pan camera to traders.
function map:play_cutscene_to_traders(callback)
  local camera = map:get_camera()
  local hero = map:get_hero()
  local buccaneer = map:get_entity("trader_1")
  sol.timer.start(map, 1000, function()
    local speed = 80
    
    camera:pan_to_entity(buccaneer, speed, function()
      game:start_dialog(call_dialog_id("trader.3"), function()
        sol.timer.start(camera, 800, function()
          -- Cut back to player, and proceed to next step in cutscene.
          camera:start_tracking(hero)
          callback()
        end)
      end)
    end)
  end)
end

-- Cutscene for ship escape.
function map:play_cutscene_escape_ship()
  cutscene.builder(game, map, hero)
    .dialog(call_dialog_id("trader.2"))
    .exec(function()
      hero:freeze()
      map:show_traders()
    end)
    .wait(300)
    .set_direction(hero,3)
    .wait(300)
    .and_then(function(cont)
      map:play_cutscene_to_traders(cont)
    end)
    .wait(100)
    .set_direction(hero,0)
    .dialog(call_dialog_id("escape.1"))
    .exec(function()
      hero:freeze()
      game:hide_life_bar()
      game:hide_weapon_icon()
      black_screen:fade_in(100, function()
        sol.timer.start(map, 2000, function()
          game:set_value("main_story", 4)
          if game:has_item("stick") then
            game:get_item("stick"):set_variant(0)
          end
          hero:teleport("cutscenes/sura_island/shore", "start")
        end)
      end)
    end)
  .start()
end

-- Start cutscene if player made contact with escape boat.
function sensor_escape_ship:on_activated()
  map:play_cutscene_escape_ship()
end

function rudder:on_interaction()
  local rudder_touched = game:get_value("ship_rudder_touched")

  if not rudder_touched or rudder_touched == false then
    game:start_dialog(call_dialog_id("rudder.1"), function()
      game:set_value("ship_rudder_touched", true)
    end)
  else
    game:start_dialog(call_dialog_id("rudder.2"), function()
      game:set_value("ship_rudder_touched", false)
    end)
  end
end