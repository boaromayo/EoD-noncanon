-- Lua script of map ship/b.
local map = ...
local game = map:get_game()
local npc_manager = require("scripts/maps/npc_manager")

-- Setup directional constants
local EAST, NORTH, WEST, SOUTH = 0, 1, 2, 3

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.ship.b." .. dialog_id
end

-- Setup hero to move backward.
local function move_back()
  local movement = nil
  if hero:get_direction() == EAST then
    movement = "444"
  elseif hero:get_direction() == WEST then
    movement = "000"
  elseif hero:get_direction() == NORTH then
    movement = "666" -- The devil's number woooooo
  elseif hero:get_direction() == SOUTH then
    movement = "222"
  end
  hero:walk(movement, false, false)
end

-- Move hero away if hero's approaching crowded territory.
local function sensor_wrong_way_on_activated()
  if game:has_item("sword") then
    game:start_dialog(call_dialog_id("wrongway.2"), move_back)
  else
    game:start_dialog(call_dialog_id("wrongway.1"), move_back)
  end
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function()

  -- Setup NPC pacing path.
  local pacing_path = {6,6,6,6,6,6,2,2,2,2,2,2,2,2,6,6}
  local pacing_movement = sol.movement.create("path")
  pacing_movement:set_path(pacing_path)
  pacing_movement:set_speed(20)
  pacing_movement:set_loop(true)

  npc_manager:set_movement_main_story(map, 1, pacing_movement, "trader_pacing")

  -- Assign sensors to walk back hero.
  for sensor in map:get_entities("sensor_wrong_way") do
    sensor.on_activated = sensor_wrong_way_on_activated
  end
end)

-- Call dialog when hero faces captain's quarters door.
function sensor_door_captain:on_activated()
  if hero:get_direction() == NORTH then
    game:start_dialog("_door.capt_locked")
  end
end