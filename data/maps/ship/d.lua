-- Lua script of map ship/d.
local map = ...
local game = map:get_game()
local cutscene = require("scripts/maps/cutscene")
local audio_manager = require("scripts/audio_manager")
local npc_manager = require("scripts/maps/npc_manager")

-- To shorten calling of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.ship.d." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup music.
  map:initialize_music()

  -- Setup NPC behavior.
  npc_manager:hide_npcs_savegame_with_value(map, "stick_get_from_ground", true, "jail_door_stairs")

  npc_manager:set_dialog_main_story(map, 1, "jail_door_a_npc_1", call_dialog_id("bheth.1"))
  npc_manager:set_dialog_main_story(map, 1, "jail_door_a_npc_2", call_dialog_id("rhyan.1"))
  npc_manager:set_dialog_main_story(map, 1, "jail_door_b_npc", call_dialog_id("wise_man.1"))

  npc_manager:set_animation(map, "bheth", "idle")
  npc_manager:set_animation(map, "rhyan", "sleeping")
end)

-- Initialize music.
function map:initialize_music()
  audio_manager:play_music("City-of-the-Disturbed_Looping")
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
end

-- Event called after treasure was obtained.
function map:on_obtained_treasure(item, variant, savegame_variable)
  if savegame_variable == "stick_get_from_ground" then
    game:show_weapon_icon()
    map:play_cutscene_jailer_appears()
  end
end

-- Move back when approaching engine room.
function sensor_engine_lock:on_activated()
  game:start_dialog("_door.locked", function()
    local movement = "222"
    hero:walk(movement, false, false)
  end)
end

-- When player is near or interacts with Lyn, start convo.
function sensor_on_lyn_jail:on_activated()
  if game:main_story_less_than(3) then
    game:start_dialog(call_dialog_id("lyn.psst"), function()
      hero:walk("00", false, false)
      sol.timer.start(map, 100, function()
        map:talk_lyn()
      end)
    end)
  end
end

-- Start Lyn conversation.
function lyn_npc:on_interaction()
  map:talk_lyn()
end

-- Lyn conversation.
function map:talk_lyn()
  if game:main_story_less_than(3) then
    cutscene.builder(game, map, hero)
      .freeze()
      .wait(100)
      .exec(function()
        local direction = lyn:get_direction4_to(hero)
        hero:set_direction(math.abs((direction % 4) - 2)) -- face Lyn.
      end)
      .dont_wait_for.hero_animation("stopped")
      .dialog(call_dialog_id("lyn.1"))
      .exec(function()
        game:set_value("main_story", 3)
        hero:unfreeze()
      end)
    .start()
  else
    game:start_dialog(call_dialog_id("lyn.2"))
  end
end

-- Jailer pirate cutscene conversation. 
-- Automatically appear after player gets stick in chest.
function map:play_cutscene_jailer_appears()
  cutscene.builder(game, map, hero)
    .freeze()
    .wait(400)
    .exec(function()
      audio_manager:play_sound("door_open")
      for jail_door in map:get_entities("jail_door_stairs") do
        jail_door:set_enabled(false)
      end
      local _, _, layer = jail_door_stairs:get_position()
      map:create_dynamic_tile({
        name = "jail_ceiling",
        layer = layer + 1,
        x = 24,
        y = 320,
        width = 16,
        height = 16,
        pattern = "152",
        enabled_at_start = true
      })
    end)
    .set_direction(hero,2)
    .wait(800)
    .exec(function()
      local jailer = map:create_enemy({
        name = "jailer",
        layer = 0,
        x = 32,
        y = 352,
        direction = 1, -- Facing north
        breed = "buccaneer",
        savegame_variable = "ship_jailer_defeated",
      })
      local jailer_sprite = jailer:get_sprite()
      -- Force jailer to stop and face north.
      jailer:stop_movement()
      jailer_sprite:set_animation("stopped")
      jailer_sprite:set_direction(1)
    end)
    .and_then(function()
      map:play_cutscene_jailer_movement()
    end)
 .start()
end

-- Handle camera and jailer movements here.
function map:play_cutscene_jailer_movement()
  local camera = map:get_camera()
  local jailer = map:get_entity("jailer")
  local jailer_sprite = jailer:get_sprite()
  local armory_sensor = map:get_entity("sensor_armory")

  sol.timer.start(map, 0, function()
    local camera_speed = 60
    -- We assume the hero is frozen here.
    camera:pan_to_entity(jailer, camera_speed, function()
      camera:start_tracking(jailer)
      jailer_sprite:set_animation("walking")
      local move = sol.movement.create("target")
      local jailer_speed = 48
      move:set_speed(jailer_speed)
      move:set_target(armory_sensor)
      move:set_smooth(true)
      move:start(jailer, function()
        jailer_sprite:set_animation("stopped")
        jailer_sprite:set_direction(0)
      end)

      sol.timer.start(map, 7500, function()
        camera:pan_to_hero(hero, camera_speed)
        jailer:restart()
      end)
    end)
  end)
end