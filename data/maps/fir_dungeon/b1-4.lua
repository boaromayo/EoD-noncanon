-- Lua script of map fir_dungeon/b1-4.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  -- Check if doors are open, if not, open them.
  if game:get_value("d2") == 1 or 
      game:get_value("d2") == nil then
    if door_lock_1:is_closed() or 
        door_lock_2:is_closed() then
      map:open_doors("door_lock")
    end
  end
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
end

-- Call sensor to lock doors if enemies exist.
function lock_sensor:on_activated()
  -- Close all blocks until all enemies destroyed.
  if game:get_value("d2") == nil or 
      map:has_entities("green_slime_door") then
    game:set_value("d2", 0)
    map:close_doors("door_lock")
  end
end

-- Check if enemies in room are dead, then open "doors".
for enemy in map:get_entities("green_slime_door") do
  function enemy:on_dead()
    if not map:has_entities("green_slime_door") then
      hero:freeze()
      sol.timer.start(self, 200, function()
        sol.audio.play_sound("door_open")
        game:set_value("d2", 1)
        map:open_doors("door_lock")
        sol.timer.start(self, 200, function()
          sol.audio.play_sound("secret")
          hero:unfreeze()
        end)
      end)
    end
  end
end