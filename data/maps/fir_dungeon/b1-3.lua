-- Lua script of map fir_dungeon/b1-4.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()
local block = map:get_entity("switch_block")
local door_block = map:get_entity("door_block")

local boss_fighting = false
local bgm_reserve = nil

-- Event called as soon as this map is loaded.
map:register_event("on_started", function(map)
  -- You can initialize the movement and sprites of various
  -- map entities here.
  -- If center block has been pushed, 
  -- make "door" invisible.
  if game:get_value("d1") == 1 then
    door_block:set_enabled(false)  
  end

  -- Check if boss is not dead
  if boss ~= nil then
    boss:set_enabled(false)
  end
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
end

-- Call as soon as hero moves center block,
-- opening the west path.
function block:on_moved()
  -- Play rock break sound effect and
  -- make "door" disappear.
  if game:get_value("d1") == nil then
    sol.audio.play_sound("switch")
    sol.timer.start(self, 500, function()
      sol.audio.play_sound("secret")
      game:set_value("d1", 1)
    end)
    door_block:set_enabled(false)
  end
end

-- Call as soon as hero triggers sensor, and boss is not defeated.
function boss_sensor:on_activated()
  if not game:get_value("boss_1_defeated") and
      not boss_fighting then
    bgm_reserve = sol.audio.get_music()
    hero:freeze()
    -- Begin boss process.
    sol.timer.start(self, 300, function()
      local volume = sol.audio.get_music_volume()
      map:close_doors("door_boss")
      sol.audio.set_music_volume(volume - 20)
      -- Slowly fade out music.
      sol.timer.start(self, 1000, function()
        sol.audio.set_music_volume(volume - 40)
        sol.timer.start(self, 500, function()
          sol.audio.set_music_volume((volume - 20) / 2)
          sol.timer.start(self, 250, function()
            sol.audio.set_music_volume((volume - 20) / 4)
            sol.timer.start(self, 125, function()
              sol.audio.set_music_volume(0)
              sol.audio.stop_music()
              sol.timer.start(self, 2500, function()
                -- Play boss music.
                sol.audio.set_music_volume(volume)
                sol.audio.play_music("boss_intro", function()
                  sol.audio.play_music("RPG-Battle-Climax-2")
                  -- Unleash boss!
                  --if boss ~= nil then
                    --map:create_enemy(boss)
                    --boss:set_enabled(true)
                  --end
                  -- Set flag to indicate fighting sub-boss.
                  boss_fighting = true
                  hero:unfreeze()
                end)
              end)
            end)
          end)
        end)
      end)
    end)
  end -- Boss process end.
end

-- If boss dead, unlock doors.
if boss ~= nil then
  function boss:on_dead()
    game:set_value("boss_1_defeated", true)
    if bgm_reserve ~= nil then
      sol.audio.play_music(bgm_reserve)
    end
    hero:freeze()
    sol.timer.start(self, 700, function()
      map:open_doors("door_boss")
      sol.timer.start(self, 200, function() 
        sol.audio.play_sound("secret")
        hero:unfreeze()
      end)
    end)
  end
end