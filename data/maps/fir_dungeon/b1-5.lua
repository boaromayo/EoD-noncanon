-- Lua script of map fir_dungeon/b1-5.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()
local camera = map:get_camera()

local sub_boss_fighting = false
local bgm_reserve = nil

-- Table for sub-boss
local mage = {
  name = "mage",
  x = 160,
  y = 56,
  direction = 1,
  layer = 0,
  breed = "mage",
  treasure_name = "block_key"
}

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- You can initialize the movement and sprites of various
  -- map entities here.
  if mage ~= nil then
    mage:set_enabled(false)
  end

  map:set_doors_open("sub_boss_door", true)
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
end

-- Activate sensor and unleash sub-boss if not defeated yet.
function sub_boss_sensor:on_activated()
  if not game:get_value("sub_boss_1_defeated") and 
      not sub_boss_fighting then
    -- Begin sub-boss processing.
    bgm_reserve = sol.audio.get_music()
    hero:freeze()
    sol.timer.start(self, 300, function()
      -- Lock player in.
      local volume = sol.audio.get_music_volume()
      map:close_doors("sub_boss_door")
      sol.audio.set_music_volume(volume - 20)
      -- Slowly fade out music.
      sol.timer.start(self, 1000, function()
        sol.audio.set_music_volume(volume - 40)
        sol.timer.start(self, 500, function()
          sol.audio.set_music_volume((volume - 20) / 2)
          sol.timer.start(self, 250, function()
            sol.audio.set_music_volume((volume - 20) / 4)
            sol.timer.start(self, 125, function()
              sol.audio.set_music_volume(0)
              sol.audio.stop_music()
              sol.timer.start(self, 2500, function()
                -- Play boss music.
                sol.audio.set_music_volume(volume)
                sol.audio.play_music("boss_intro", function()
                  sol.audio.play_music("RPG-Battle-Climax-2")
                  -- Unleash sub-boss!
                  if mage ~= nil then
                    map:create_enemy(mage)
                    --mage:set_enabled(true)
                  end
                  -- Set flag to indicate fighting sub-boss.
                  sub_boss_fighting = true
                  hero:unfreeze()
                end)
              end)
            end)
          end)
        end)
      end)
      --camera:set_position()
    end) -- End sub-boss processing.
  -- When player meets sub-boss but did not defeat.
  elseif sub_boss_fighting then
    -- Save music for later.
    bgm_reserve = sol.audio.get_music()
    hero:freeze()
    -- Lock player in.
    map:close_doors("sub_boss_door")
    sol.timer.start(self, 500, function()
      -- Play boss music now.
      sol.audio.play_music("boss_intro", function()
        sol.audio.play_music("boss")
        -- Unleash sub-boss!
        if mage ~= nil then
          map:create_enemy(mage)
          --mage:set_enabled(true)
        end
        hero:unfreeze()
      end)
    end)
  end -- Sub-boss processing end.
end

-- Check if sub-boss has fallen.
if mage ~= nil then
  function mage:on_dead()
    game:set_value("sub_boss_1_defeated", true)
    -- Play saved bg music.
    if bgm_reserve ~= nil then
      sol.audio.play_music(bgm_reserve)
    end
    hero:freeze()
    -- Unlock the "door" and drop the treasure.
    sol.timer.start(self, 200, function()
      local boss_key_chest = {
        name = "chest_boss_key",
        layer = 0,
        x = 160,
        y = 32,
        treasure_name = "boss_key",
        treasure_savegame_variable = "fir_dungeon_boss_key",
        sprite = "entities/chest"
      }
      map:open_doors("sub_boss_door")
      sol.audio.play_sound("chest_appears")
      map:create_chest(boss_key_chest)
      sol.timer.start(self, 200, function()
        sol.audio.play_sound("secret")
        hero:unfreeze()
      end)
    end)
  end
end