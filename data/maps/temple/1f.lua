-- Lua script of dungeon map temple/1f.
local map = ...
local game = map:get_game()

local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local item_manager = require("scripts/maps/item_manager")
local switch_manager = require("scripts/maps/switch_manager")

local function call_dialog_id(dialog_id)
  return "_map.dungeon.temple.1f." .. dialog_id
end

-- Check specific lanterns to light them when map is entered.
local function check_lantern(savegame, lantern)
  if savegame and 
      game:get_value(savegame) == true then
    local lantern_sprite = map:get_entity(lantern):get_sprite()
    if lantern_sprite:has_animation("lit") then
      lantern_sprite:set_animation("lit")
    end
  end
end

local function wrong_switch_activated(switch)
  hero:freeze()
  sol.timer.start(switch, 500, function()
    audio_manager:play_sound("wrong")
    hero:unfreeze()
  end)
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)

  -- Setup music.
  map:initialize_music()

  -- Setup doors.
  map:set_doors_open("door_entrance", true)

  door_manager:open_doors_block_moved(map, "block_east", "door_east", 500, "secret")
  door_manager:open_doors_switch_activated(map, "tile_switch_1", "door_entrance", 800, "secret")
  door_manager:open_doors_switch_activated(map, "tile_switch_2_ne", "door_switch_west_1", 800, "secret")
  door_manager:open_doors_switches_activated(map, "tile_switch_b", "door_switch_west_2", 800, "secret")
  door_manager:open_doors_switch_activated(map, "shortcut_switch_west", "door_up_west", 0, "secret")
  door_manager:open_doors_switch_activated(map, "shortcut_switch_east", "door_up_east", 0, "secret")

  door_manager:open_doors_if_savegame_exist(map, "switch_pressed_entrance", "door_entrance")
  door_manager:open_doors_if_savegame_exist(map, "switch_pressed_west", "door_switch_west_1")
  door_manager:open_doors_if_savegame_exist(map, "switch_pressed_west_2", "door_switch_west_2")
  door_manager:open_doors_if_savegame_exist(map, "block_east_pushed", "door_east")
  door_manager:open_doors_if_savegame_exist(map, "chalice_west_open", "door_shortcut_west")
  door_manager:open_doors_if_savegame_exist(map, "chalice_east_open", "door_shortcut_east")
  door_manager:open_doors_if_savegame_exist(map, "temple_main_hall_opened", "door_main_hall")

  -- Setup items.
  --item_manager:disappear_chest(map, "chest_money")
  --item_manager:chest_appears_switches_activated(map, "switch_chest_money", "chest_money", 500, "chest_appears")
  item_manager:chest_appears_if_opened(map, "chest_money")

  -- Setup switches.
  tile_switch_1:set_activated(true)
  switch_main_hall.is_activated = game:get_value("temple_main_hall_opened") or false

  switch_manager:set_savegame_if_switch_activated(map, "switch_pressed_west", "tile_switch_2_ne")
  switch_manager:activate_switch_if_savegame_exist(map, "switch_pressed_west", "tile_switch_2_")
  switch_manager:activate_switch_if_savegame_exist(map, "temple_main_hall_opened", "switch_main_hall")

  -- Setup other dungeon entities.
  check_lantern("chalice_west_open", "temple_main_hall_lantern_west")
  check_lantern("chalice_east_open", "temple_main_hall_lantern_east")

  -- Disable these settings when releasing non-demo alpha version.
  game:hide_tech_bar()
end)

-- Initialize map music.
function map:initialize_music()
  audio_manager:play_music("Lost-Jungle_Looping")
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished(destination)

  -- Setup entities. Deactivate first puzzle when entering dungeon.
  if destination == entrance then
    map:set_doors_open("door_entrance", false)
    tile_switch_1:set_activated(false)
  end
end

function map:open_door_main_hall()
  hero:freeze()
  audio_manager:play_sound("secret")
  sol.timer.start(map, 800, function()
    map:open_doors("door_main_hall")
    hero:unfreeze()
  end)
end

function sign_temple:on_interaction()
  local sign_id = call_dialog_id("sign.1")
  game:start_dialog(sign_id)
end

local function move_north()
  -- Setup hero to move northward.
  local movement = "222"
  hero:walk(movement, false, false)
end

local function close_doors_temple(door_group)
  map:close_doors(door_group)
end

function switch_main_hall:on_activated()
  local switch = switch_main_hall

  audio_manager:play_sound("switch")
  if game:get_value("chalice_west_open")
      and game:get_value("chalice_east_open")
      and not switch.is_activated then
    switch.is_activated = true
    map:open_door_main_hall()
  else
    wrong_switch_activated(switch)
    sol.timer.start(map, 500, function()
      switch.is_activated = false
      switch:set_activated(switch.is_activated)
    end)
  end
end

function sensor_temple_entrance:on_activated()
  game:start_dialog("_door.temple_locked", move_north)
end

tile_switch_2_nw.on_activated = wrong_switch_activated
tile_switch_2_sw.on_activated = wrong_switch_activated
tile_switch_2_se.on_activated = wrong_switch_activated
