-- Lua script of dungeon map temple/b1.
local map = ...
local game = map:get_game()

local audio_manager = require("scripts/audio_manager")
local item_manager = require("scripts/maps/item_manager")
local door_manager = require("scripts/maps/door_manager")
local switch_manager = require("scripts/maps/switch_manager")

-- To shorten call of dialog ids.
local function call_dialog_id(dialog_id)
  return "_map.dungeon.temple.b1." .. dialog_id
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map)

  -- Setup doors.
  door_manager:open_doors_switch_activated(map, "switch_door_perch", "door_perch")

  -- Setup switches.
  switch_door_perch.is_activated = game:get_value("door_perch_open") or false

  switch_manager:activate_switch_if_savegame_exist(map, "door_perch_open", "switch_door_perch")
end)

-- Event called when obtaining the treasure.
function map:on_obtaining_treasure(item, variant, savegame)
  if savegame == "temple_treasure" then
    local get_strange_stone = 9
    game:set_main_story(get_strange_stone)
    item_manager:get_secular_treasure(map)
  end
end

-- Event called when teletransporter to library activated.
-- Replace with boss room if this is non-demo version.
function to_lib_entrance:on_activated()
  audio_manager:play_music("The-Idol_Looping")
end

-- Event called when going back to anteroom.
-- Play back dungeon music.
function to_anteroom:on_activated()
  audio_manager:play_music("Lost-Jungle_Looping")
end

-- Event called when reached end of bridge.
-- Collapse bridges behind player.
function sensor_bridge_collapse:on_activated()
  -- Do not trigger event when bridges are gone.
  if not map:get_entity("collapse_bridge_1"):is_enabled() then
    return
  end

  local hero = map:get_hero()
  local camera = map:get_camera()

  local SOUTH = 3 -- Hero direction to face.
  local delay = 500
  local i = 1 -- Counter to collapse bridges one-by-one.

  audio_manager:play_sound("quake")
  camera:shake({amplitude = 4, duration = 1000})
  hero:get_sprite():set_direction(SOUTH)
  hero:freeze()
  sol.timer.start(map, 1000, function()
    for bridge in map:get_entities("collapse_bridge_") do
      sol.timer.start(map, i * delay, function()
        if i % 3 == 1 then -- Give some breathing room on sfx by calling it every odd section.
          audio_manager:play_sound("falling_on_hole")
        end
        bridge:set_enabled(false)
      end)
      i = i + 1
    end
    sol.timer.start(map, i * delay, function()
      hero:unfreeze()
    end)
  end)
end