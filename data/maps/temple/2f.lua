-- Lua script of dungeon map temple/2f.
local map = ...
local game = map:get_game()
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local item_manager = require("scripts/maps/item_manager")
local switch_manager = require("scripts/maps/switch_manager")

local black_screen

local function wrong_switch_activated(switch)

  audio_manager:play_sound("switch")
  hero:freeze()
  sol.timer.start(switch, 500, function()
    audio_manager:play_sound("wrong")
    switch:set_activated(false)
    hero:unfreeze()
  end)
end

-- Check specific lanterns to light them when map is entered.
local function check_lanterns(savegame, lanterns)
  if savegame and 
      game:get_value(savegame) == true then
    for lantern in map:get_entities(lanterns) do
      if lantern:get_type() == "dynamic_tile" then
        local lantern_name = lantern:get_name()
      
        if lantern_name:match("lit") then
          lantern:set_enabled(true)
        else
          lantern:set_enabled(false)
        end
      else
        local lantern_sprite = lantern:get_sprite()
        if lantern_sprite:has_animation("lit") then
          lantern_sprite:set_animation("lit")
        end
      end
    end
  end
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)
  
  -- Setup doors.  
  map:set_doors_open("door_to_chalice_west_", true)
  map:set_doors_open("door_shortcut_west", game:get_value("chalice_west_open") or false)
  map:set_doors_open("door_shortcut_east", game:get_value("chalice_east_open") or false)

  door_manager:open_doors_enemy_dead(map, "enemy_group_west_", "door_to_chalice_west_", 800, "secret")
  door_manager:open_doors_if_savegame_exist(map, "chalice_west_open", "door_shortcut_west")
  door_manager:open_doors_if_savegame_exist(map, "chalice_east_open", "door_shortcut_east")

  -- Setup items.
  item_manager:disappear_chest(map, "chest_w_temple_key")
  item_manager:disappear_chest(map, "chest_e_temple_key")
  item_manager:chest_appears_switches_activated(map, "sand_block_switch_", "chest_e_temple_key", 500, "secret")
  item_manager:chest_appears_if_opened(map, "chest_w_temple_key")
  item_manager:chest_appears_if_opened(map, "chest_e_temple_key")

  -- Setup switches.
  bridge_switch_w.is_activated = game:get_value("bridge_west_open") or false
  bridge_switch_e.is_activated = game:get_value("bridge_east_open") or false
  chalice_switch_w.is_activated = game:get_value("chalice_west_open") or false
  chalice_switch_e.is_activated = game:get_value("chalice_east_open") or false

  switch_manager:activate_switch_if_savegame_exist(map, "bridge_west_open", "bridge_switch_w")
  switch_manager:activate_switch_if_savegame_exist(map, "bridge_east_open", "bridge_switch_e")
  switch_manager:activate_switch_if_savegame_exist(map, "chalice_west_open", "chalice_switch_w")
  switch_manager:activate_switch_if_savegame_exist(map, "chalice_east_open", "chalice_switch_e")

  -- Setup other dungeon entities.
  lock_block:set_enabled(false)

  for bridge in map:get_entities("extend_bridge_1_") do
    bridge:set_enabled(game:get_value("bridge_west_open") or false)
  end
  for bridge in map:get_entities("extend_bridge_2_") do
    bridge:set_enabled(game:get_value("bridge_east_open") or false)
  end

  check_lanterns("chalice_west_open", "cauldron_w_")
  check_lanterns("chalice_west_open", "temple_chalice_west")
  check_lanterns("chalice_east_open", "cauldron_e_")
  check_lanterns("chalice_east_open", "temple_chalice_east")

  black_screen = sol.surface.create()
  black_screen:fill_color({ 0, 0, 0 })
  black_screen:set_opacity(0)
end)

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished(destination)
  
  map:check_cutscene_open_shortcut_door(destination)
end

function map:on_draw(screen)

  black_screen:draw(screen)
end

-- Check hero status when cutscene plays.
function map:check_cutscene_open_shortcut_door(destination)
  local dest_name = destination:get_name()
  local from_switch = dest_name:match("shortcut_scene")
  local from_cutscene = dest_name:match("chalice")  

  if from_switch then
    black_screen:fade_out(20, function()
      hero:freeze()
      hero:set_invincible()
      game:set_pause_allowed(false)
    end)
    map:play_cutscene_open_shortcut_door(destination)
  elseif from_cutscene then
    hero:set_visible(true)
    black_screen:fade_out(20, function()
      game:set_pause_allowed(true)
      hero:set_invincible(false)
      hero:unfreeze()
    end)
  end
end

-- Cutscene to play to open shortcut doors.
function map:play_cutscene_open_shortcut_door(destination)
  local dest_name = destination:get_name()
  local from_switch_west = dest_name == "shortcut_scene_west"
  local from_switch_east = dest_name == "shortcut_scene_east"
  local from_cutscene = dest_name:match("chalice")
  
  if from_switch_west or from_switch_east then
    for enemy in map:get_entities("enemy_group_shortcut_") do
      enemy:set_enabled(false)
    end
    sol.timer.start(map, 1500, function()
      if from_switch_west then
        game:set_value("chalice_west_open", true)
        map:open_doors("door_shortcut_west")
      elseif from_switch_east then
        game:set_value("chalice_east_open", true)
        map:open_doors("door_shortcut_east")
      end
      audio_manager:play_sound("secret")

      sol.timer.start(map, 1500, function()
        black_screen:fade_in(20, function()
          for enemy in map:get_entities("enemy_group_shortcut_") do
            enemy:set_enabled(true)
          end
          if from_switch_west then
            hero:teleport("temple/2f", "chalice_west")
          elseif from_switch_east then
            hero:teleport("temple/2f", "chalice_east")
          end
        end)
      end)
    end)
  end
end

local function right_switch_activated(switch)
  local chest = map:get_entity("chest_w_temple_key")
  local chest_x, chest_y, _ = chest:get_position()
  hero:freeze()
  sol.timer.start(switch, 500, function()
    if lock_block and lock_block:is_enabled() then
      lock_block:set_enabled(false)
    end
    switch:set_enabled(false)
    -- Pan camera, reveal chest, and pan back to player.
    sol.timer.start(map, 500, function()
      game:set_suspended(true)

      local camera = map:get_camera()
      local speed = 90
      local movement = sol.movement.create("target")
      movement:set_target(camera:get_position_to_track(chest_x, chest_y))
      movement:set_speed(speed)
      movement:set_ignore_obstacles(true)
      movement:set_smooth(false)

      movement:start(camera, function()
        item_manager:appear_chest(map, "chest_w_temple_key", 150, "secret")

        sol.timer.start(map, 1500, function()
          local back_movement = sol.movement.create("target")
          back_movement:set_target(camera:get_position_to_track(hero))
          back_movement:set_speed(speed)
          back_movement:set_ignore_obstacles(true)
          back_movement:set_smooth(false)

          back_movement:start(camera, function()
            camera:start_tracking(hero)
            hero:unfreeze()
            game:set_suspended(false)
          end)
        end)
      end)
    end)
  end)
end

-- TODO: Make entity manager handle bridge appearing functionality.
local function bridge_switch_on_activated(switch, bridges, savegame, delay)
  if not game:get_value(savegame) and not switch.is_activated then
    delay = delay or 500

    audio_manager:play_sound("switch")
    switch.is_activated = true
    hero:freeze()
    sol.timer.start(map, delay, function()
      local i = 1 -- Counter to ensure bridge sections appear one-by-one.
      for bridge in map:get_entities(bridges) do
        sol.timer.start(map, i * delay, function()
          audio_manager:play_sound("bomb") -- HACK: Substitute sfx.
          bridge:set_enabled(true)
        end)
        i = i + 1
      end
      sol.timer.start(map, i * delay + 1000, function()
        game:set_value(savegame, true)
        audio_manager:play_sound("secret")
        hero:unfreeze()
      end)
    end)
  end
end

-- TODO: Improve cutscene.
local function chalice_switch_on_activated(switch, savegame)
  if not game:get_value(savegame) and not switch.is_activated then
    audio_manager:play_sound("switch")
    switch.is_activated = true
    hero:freeze()
    sol.timer.start(map, 500, function()
      local dest_name = savegame == "chalice_west_open" and "west" or "east"
      local lantern = map:get_entity("temple_chalice_" .. dest_name)
      local cauldrons = map:get_entities("cauldron_" .. dest_name:sub(1,1) .. "_lit_")

      -- Light lantern and cauldrons.
      for cauldron in cauldrons do
        cauldron:set_enabled(true)
      end      
      lantern:get_sprite():set_animation("lit")
      audio_manager:play_sound("fire_ball")

      sol.timer.start(map, 1000, function()
        black_screen:fade_in(20, function()
          hero:set_visible(false)
          hero:teleport("temple/2f", "shortcut_scene_" .. dest_name)
        end)
      end)
    end)
  end
end

step_switch.on_activated = right_switch_activated

-- TODO: Make custom extendable bridge entity.
function bridge_switch_w:on_activated()
  bridge_switch_on_activated(self, "extend_bridge_1_", "bridge_west_open")
end

function bridge_switch_e:on_activated() 
  bridge_switch_on_activated(self, "extend_bridge_2_", "bridge_east_open")
end

-- TODO: Create cutscene lighting up chalice.
function chalice_switch_w:on_activated()
  chalice_switch_on_activated(self, "chalice_west_open")
end

function chalice_switch_e:on_activated()
  chalice_switch_on_activated(self, "chalice_east_open")
end

function sensor_close_doors_1:on_activated()
  door_manager:close_doors_enemy_not_dead(map, "enemy_group_west_", "door_to_chalice_west_")
end
