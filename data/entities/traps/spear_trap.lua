-- Script of custom entity spear trap.
-- The spear trap activates when its line of sight is 
-- normal to the player's position.
local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local DMG_FRACTION = 0.1

local collided_entities = {}

-- Check if spear trap collided with player.
local function on_collided_entity(entity, other)
  -- Ensure entity is hurt once.
  if collided_entities[other] then
    return
  end
  collided_entities[other] = true

  -- Deal different kind of damage based on type of entity.
  if entity:get_sprite():get_animation() ~= "stopped" and 
      other:overlaps(entity) then
    if other:get_type() == "hero" then
      -- Fix damage if it harmed anything other than the player.
      local damage = game:get_max_life() * DMG_FRACTION
      if entity.damage ~= damage then
        entity:set_damage(damage)
      end
      other:start_hurt(entity, entity.damage)
    elseif other:get_type() == "enemy" then
      local enemy_damage = other:get_life() * 2
      entity:set_damage(enemy_damage)
      other:hurt(entity.damage)
    end
  end
end

-- Event called when spear trap is initialized.
function entity:on_created()

  entity:set_size(16, 80)
  entity:set_traversable_by(true)
  -- TODO: Play different sound.
  entity.activation_sound = "bow"

  -- Damage player gets is 10% of max life.
  local damage = game:get_max_life() * DMG_FRACTION
  entity:set_damage(damage)

  entity.is_activated = false

  -- When player in line of sight, activate.
  entity:add_collision_test("overlapping", function(entity, other)
    if not entity.is_activated and 
        entity.on_activated and 
        (other:get_type() == "hero" or other:get_type() == "enemy") then

      entity.is_activated = true
      -- Give player a tiny reprieve. Tiny. 
      sol.timer.start(entity, 400, function()
        entity:on_activated()
      end)
    end
  end)
end

-- Event called when spear trap is activated.
function entity:on_activated()
  local sprite = entity:get_sprite()

  -- Play sound after activation.
  sol.audio.play_sound(entity.activation_sound)
  -- Push out spear trap.
  sprite:set_animation("activating", function()
    -- Pull spear back in after a bit.
    sprite:set_animation("activated")

    -- Check collision between spear trap and other entity.
    entity:add_collision_test("sprite", on_collided_entity)
    sol.timer.start(entity, 1500, function()
      entity.is_activated = false
      sprite:set_animation("deactivating", function()
        sprite:set_animation("stopped")
        entity:on_deactivated()
      end)
    end)
  end)
end

-- Event called when spear trap is deactivated.
function entity:on_deactivated()
  collided_entities = {}
end

function entity:set_damage(damage)
  entity.damage = damage
end
