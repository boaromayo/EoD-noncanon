-- Script of custom entity spike trap.
-- Spikes come out from holes in the floor when
-- player steps on them.
local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local DMG_FRACTION = 0.1

-- Event called when the custom entity is initialized.
function entity:on_created()

  entity:get_traversable_by(true)

  entity.activation_sound = "sword3"

  -- Damage player gets is 10% of max life.
  local damage = math.ceil(game:get_max_life() * DMG_FRACTION)
  entity:set_damage(damage)

  entity.is_activated = false

  -- When player steps on trap, activate.
  entity:add_collision_test("overlapping", function() 
    if not entity.is_activated and 
        entity.on_activated then

      entity.is_activated = true
      -- Play sound after activation confirmed, but before timer to prevent repeating.
      sol.audio.play_sound(entity.activation_sound)
      -- Give player some reprieve. Some.
      sol.timer.start(entity, 400, function()
        entity:on_activated()      
      end)
    end
  end)
end

-- Event called when spike trap is activated.
function entity:on_activated()
  local sprite = entity:get_sprite()

  sprite:set_animation("activated", function()
    -- Push spikes in immediately.
    entity.is_activated = false
    sprite:set_animation("deactivated")
  end)

  -- Check collision between spike trap box and other entity box.
  for other in entity:get_entities_in_rectangle(entity:get_bounding_box()) do
    if entity:overlaps(other) then
      if other:get_type() == "hero" then
        -- Fix damage to adjust for player.
        local damage = math.ceil(game:get_max_life() * DMG_FRACTION)
        if entity.damage ~= damage then
          entity:set_damage(damage)
        end

        -- Only hurt other entity once.
        if not entity.hurt_other then
          other:start_hurt(entity, sprite, entity.damage)
          entity.hurt_other = true
        end
      elseif other:get_type() == "enemy" then
        local enemy_damage = math.ceil(other:get_life() * 1.25)
        entity:set_damage(enemy_damage)
        
        if not entity.hurt_other then
          other:hurt(entity.damage)
          entity.hurt_other = true
        end
      end
    end
  end

  entity.hurt_other = false
end

function entity:set_damage(damage)
  entity.damage = damage
end
