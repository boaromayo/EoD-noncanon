-- Script for the fire from fire cane.
local fire = ...
local map = fire:get_map()
local sprite
local direction
local enemies_touched = {}

function fire:on_created()
  
  sprite = fire:get_sprite() or fire:create_sprite("entities/fire")
  sprite:set_direction(fire:get_direction())
  fire:set_size(16, 16)
  fire:set_origin(8, 13)
  
  function sprite:on_animation_finished()
    fire:destroy()
  end
end

-- Set entities that fire can pass through.
fire:set_can_traverse("hero", true)
fire:set_can_traverse("crystal_block", true)
fire:set_can_traverse("stairs", true)
fire:set_can_traverse("jumper", true)
fire:set_can_traverse("stream", true)
fire:set_can_traverse("pickable", true)

-- Set grounds that fire can pass through.
fire:set_can_traverse_ground("shallow_water", true)
fire:set_can_traverse_ground("deep_water", true)
fire:set_can_traverse_ground("lava", true)
fire:set_can_traverse_ground("hole", true)
fire:set_can_traverse_ground("prickles", true)
fire:set_can_traverse_ground("low_wall", true)

fire:set_can_traverse(true)

local function is_bush(entity)
  local sprite = entity:get_sprite()
  if sprite == nil then
    return false
  end
  
  local sprite_id = sprite:get_animation_set()
  
  return sprite_id:match("^entities/destructibles/bush_") or 
    sprite_id == "entities/destructibles/bush"
end

local function is_vine(entity)
  local sprite = entity:get_sprite()
  if sprite == nil then
    return false
  end
  
  local sprite_id = sprite:get_animation_set()
  
  return sprite_id:match("^entities/destructibles/vine_") or
    sprite_id == "entities/destructibles/vine"
end

local function on_collided_plant(fire, entity) 
  -- Check if this is a bush.
  if entity:get_type() == "destructible" then
    if is_bush(entity) then
      local bush = entity
      local bush_sprite = entity:get_sprite()
      local bush_id = bush_sprite:get_animation_set()
      local bush_x, bush_y, bush_layer = bush:get_position()
      local bush_item = { bush:get_treasure() }
      
      fire:stop_movement()
      sprite:set_animation("stopped")
      
      
      if bush_item ~= nil then
        local pickable = map:create_pickable({
          x = bush_x,
          y = bush_y,
          layer = bush_layer,
          treasure_name = bush_item[1],
          treasure_variant = bush_item[2],
          treasure_savegame_variable = bush_item[3],
        })
      end
      if bush_sprite:has_animation("burning") then
        bush_sprite:set_animation("burning", function()
          sol.timer.start(bush, 2000, function()
            bush_sprite:set_animation("destroy")
            bush:set_enabled(false)
          end)
        end)
      else
        bush_sprite:set_animation("destroy")
        bush:set_enabled(false)
      end
    -- Check if this is a vine.
    elseif is_vine(entity) then
      local vine = entity
      local vine_sprite = entity:get_sprite()
      local vine_id = vine_sprite:get_animation_set()
      local vine_x, vine_y, vine_layer = vine:get_position()
      local vine_item = { vine:get_treasure() }
      
      fire:stop_movement()
      sprite:set_animation("stopped")
    
      if vine_sprite:has_animation("burning") then
        vine_sprite:set_animation("burning", function()
          sol.timer.start(vine, 2000, function()
            vine_sprite:set_animation("destroy")
            vine:set_enabled(false)
          end)
        end)
      else
        vine_sprite:set_animation("destroy")
        vine:set_enabled(false)
      end
        
      if vine_item ~= nil then
        local pickable = map:create_pickable({
          x = vine_x,
          y = vine_y,
          layer = vine_layer,
          treasure_name = vine_item[1],
          treasure_variant = vine_item[2],
          treasure_savegame_variable = vine_item[3],
        })
      end
    else
      return 
    end
  end
  return false
end

local function on_collided_enemy(fire, entity)
  if entity:get_type() == "enemy" then
    local enemy = entity
    if enemies_touched[enemy] then
      return
    end
    enemies_touched[enemy] = true
    
    -- TODO: place enemy hurt behavior to fire here
    --local reaction = enemy:on_hurt_fire(enemy:get_sprite())
    --enemy:set_attack_consequence("fire", reaction)
    
    sol.timer.start(fire, 200, function()
      fire:remove()
    end)
  end
end

local function on_collided_overlap(fire, entity)
  if entity:get_type() == "switch" then
    local switch = entity
    local switch_sprite = switch:get_sprite()
    
    if switch ~= nil
        and not switch:is_walkable() then
      
      if not switch:is_activated() then
        sol.audio.play_sound("switch")
        switch:set_activated(true)
      end
    end
  end
end

function fire:on_obstacle_reached()
  fire:remove()
end

function fire:destroy()
  if sprite:has_animation("destroy") then
    sprite:set_animation("destroy", function()
      fire:remove()
    end)
  else
    fire:remove()
  end
end

fire:add_collision_test("sprite", on_collided_plant)
fire:add_collision_test("sprite", on_collided_enemy)
fire:add_collision_test("overlapping", on_collided_overlap)