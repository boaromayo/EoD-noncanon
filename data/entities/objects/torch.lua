-- Special thanks to Christopho for torch script.
-- This entity is a torch that can be lit by fire and unlit by water or ice.
-- Methods: is_lit(), is_unlit()
-- Events: on_lit(), on_unlit()

-- Torches marked as "timed_torch" have a limited
-- time to stay lit.
local torch = ...
local map = torch:get_map()
local sprite
local lit_time = 10000

function torch:on_created()
  torch:set_size(16, 16)
  torch:set_origin(8, 13)
  torch:set_traversable_by(false)
  
  sprite = torch:get_sprite() or torch:create_sprite("entities/torch")
  
  torch:set_lit(false)
  
  local name = torch:get_name()
  
  if name:match("^timed_torch") then
    torch:time_lit(lit_time)
  end
end

-- Set how long torch is lit in milliseconds, 
-- no duration or nil means torch is lit forever.
function torch:set_time_lit(duration)
  torch.duration = duration or nil
end

function torch:set_lit(light)
  if light then
    sprite:set_animation("lit")
    if torch:get_time_lit() ~= nil then
      sol.timer.start(self, torch.duration, function()
        torch:set_lit(false)
      end)
    end
  else
    sprite:set_animation("unlit")
  end
end

function torch:get_time_lit()
  return torch.duration
end

function torch:is_lit()
  return sprite:get_animation() == "lit"
end

local function light_torch(torch)
  torch:set_lit(true)
  if torch.on_lit ~= nil then
    torch:on_lit()
  end
end

local function unlight_torch(torch)
  torch:set_lit(false)
  if torch.on_unlit ~= nil then
    torch:on_unlit()
  end
end

local function on_collided(torch, entity)
  if entity:get_type() == "custom_entity" then
    local model = entity:get_model()
    
    if model == "fire"
        or model == "fireball" 
        or model == "firepea"
        or model:match("^fire_") then
      if not torch:is_lit() then
        light_torch(torch)
        
        sol.timer.start(entity, 50, function()
          entity:stop_movement()
          entity:remove()
        end)
      end
    elseif model == "water" 
        or model == "ice" 
        or model:match("^water_") 
        or model:match("^wind_") then
      if torch:is_lit() then
        unlight_torch(torch)
        
        sol.timer.start(entity, 50, function()
          entity:stop_movement()
          entity:remove()
        end)
      end
    end
    entity:remove()
  end
end

torch:add_collision_test("sprite", on_collided)
torch:add_collision_test("overlapping", on_collided)
