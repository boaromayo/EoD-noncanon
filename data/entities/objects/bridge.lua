-- Bridge script.
local entity = ...
local game = entity:get_game()
local map = entity:get_map()

-- Event called when the custom entity is initialized.
function entity:on_created()

  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("empty", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("deep_water", true)

  -- Consider this bridge crossable.
  entity:set_modified_ground("traversable")
end
