-- Moving platforms script.
-- Based on the original moving platforms 
-- script by Max Mraz.
local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local movement

-- Event called when the custom entity is initialized.
function entity:on_created()

  -- Set speed and pause duration (how long it stays in one point), and size.
  entity.speed = 50
  entity.pause_duration = 1500
  entity.width, entity.height = entity:get_size()

  -- Moving platforms are useful to bypass chasms.
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("empty", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("shallow_water", false)
  entity:set_can_traverse_ground("traversable", false)
  -- Skip jumpers too.
  entity:set_can_traverse("jumper", true)

  -- Ensure this platform is crossable.
  entity:set_modified_ground("traversable")
  entity:set_layer_independent_collisions(false)

  -- Start movement.
  movement = sol.movement.create("path")
  -- Path moves in eight directions instead of four, hence the doubling.
  local direction = entity:get_direction()
  direction = direction * 2
  movement:set_path({direction})
  movement:set_speed(entity.speed)
  movement:set_loop(true)
  movement:start(entity)

  entity:add_collision_test("touching", function(entity, other)
    if other:get_type() == "wall" then
      entity:on_obstacle_reached(movement)
    end
  end)
end

-- Event called when the obstacle is reached.
function entity:on_obstacle_reached(movement)
  -- Turn platform back.
  movement:stop()
  movement = sol.movement.create("path")
  local direction = entity:get_direction()
  direction = ((direction + 2) % 4)
  -- Double direction value for path movement.
  movement:set_path({direction * 2})
  movement:set_speed(entity.speed)
  movement:set_loop(true)

  -- Change direction of platform sprite.
  -- Makes hero move along with platform.
  entity:set_direction(direction)

  sol.timer.start(entity, entity.pause_duration, function()
    movement:start(entity)
  end)
end

-- Event called when the entity changes position.
function entity:on_position_changed()
  -- Make hero move along with platform.
  if not entity:is_on_platform(hero) then
    return
  end

  local dx, dy = 0, 0 -- Entity's change in movement.
  local x, y, layer = hero:get_position()
  local direction = entity:get_direction()

  -- Move platform based on direction.
  if direction == 0 then
    dx = 1
  elseif direction == 1 then
    dy = -1
  elseif direction == 2 then
    dx = -1
  elseif direction == 3 then
    dy = 1
  end

  -- Move hero if collision not flush with platform.
  if not hero:test_obstacles(dx, dy, layer) then
    hero:set_position(x + dx, y + dy, layer)
  end
end

-- Check if another entity is on platform.
function entity:is_on_platform(other)
  local other_x, other_y, other_layer = other:get_position()
  local entity_x, entity_y, entity_layer = entity:get_center_position()

  -- Give some room on platform for player's comfort.
  local padding = 6
  local width = entity.width + padding
  local height = entity.height + padding

  -- Is other entity on same layer as platform? If not, return false.
  if other_layer ~= entity_layer then
    return false
  end

  -- Is other entity within bounds of platform?
  return (other_x > (entity_x - (width / 2))) and 
    (other_x < (entity_x + (width / 2))) and
    (other_y > (entity_y - (height / 2))) and
    (other_y < (entity_y + (height / 2)))
end
