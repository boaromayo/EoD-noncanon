-- Script for enemy mud jelly.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

-- Initialize values for mud jelly behavior.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 1,
  damage = 1,
  normal_speed = 12,
  running_speed = 24,
  awake_animation = "appearing",
  sleep_animation = "disappearing",
  detection_distance = 60,
}

behavior:create(enemy, stats)
