-- Script for enemy goloss.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

-- Status for the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  width = 56,
  height = 56,
  life = 3,
  damage = 5,
  defense = 4,
  normal_speed = 20
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  movement = sol.movement.create("target")
  movement:set_target(hero)
  movement:set_speed(stats.normal_speed)
  movement:start(enemy)
end
