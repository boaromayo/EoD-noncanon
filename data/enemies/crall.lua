-- Script for enemy crall.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local MAX_DIRECTIONS = 4 -- Setup maximum number of directions.

-- Status of the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 1,
  damage = 1,
  defense = 2,
  normal_speed = 20,
  max_distance = 30 + math.random(20)
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  --enemy:set_defense(stats.defense)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()
  movement = sol.movement.create("straight")
  movement:set_speed(0)
  movement:start(enemy)
  local direction = math.random(MAX_DIRECTIONS) - 1
  enemy:go(direction)
end

-- Have enemy move in an assigned movement.
-- Note the crall is not normally an attacker, but goes around in
-- random directions.
function enemy:go(direction)
  if sprite ~= nil then
    sprite:set_animation("crawling")
    sprite:set_direction(direction)

    -- Set new movement.
    movement = enemy:get_movement()
    movement:set_max_distance(stats.max_distance)
    movement:set_smooth(true)
    movement:set_speed(stats.normal_speed)
    movement:set_angle(direction * math.pi / 2)
  end
end

-- Change random direction of enemy.
function enemy:change_direction(direction)
  if sprite ~= nil then
    sprite:set_animation("stopped")
    sprite:set_direction(direction)
    enemy:go(direction)
  end
end

-- When enemy encounters an obstacle,
-- Stop, then move in a different direction.
function enemy:on_obstacle_reached(movement)
  -- Move in a different direction.
  if sprite ~= nil then
    if sprite:get_animation() == "crawling" then
      local direction = math.random(MAX_DIRECTIONS) - 1
      enemy:change_direction(direction)
    end
  end
end

-- When enemy's movement is finished,
-- Stop, then move in a different direction.
function enemy:on_movement_finished(movement)
  enemy:on_obstacle_reached(movement)
end