-- Script for enemy maktik.
local enemy = ...
local map = enemy:get_map()
local sprite
local movement
local direction = 0 -- Enemy facing right by default.

local MAX_DIRECTIONS = 4 -- Setup maximum number of directions.

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  width = 32,
  height = 32,
  life = 1,
  damage = 1,
  defense = 1,
  normal_speed = 30,
  pushed_back_when_hurt = false,
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  --enemy:set_defense(stats.defense)
  enemy:set_size(stats.width, stats.height)
  enemy:set_origin(stats.width / 2, stats.height - 3)
  enemy:set_pushed_back_when_hurt(stats.pushed_back_when_hurt)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  -- Start up enemy movement.
  local next_direction = sprite:get_direction()
  local duration = math.random(4000, 8000)
  enemy:turn(next_direction)

  -- After a certain amount of time, have enemy drop ooze.
  sol.timer.start(map, duration, function()
    enemy:ooze()
  end)
end

-- Event called when the enemy changes position.
--[[function enemy:on_position_changed()

  -- Check if obstacle in the way, and turn if so.
  enemy:turn((direction - 1) % MAX_DIRECTIONS)
end--]]

-- Event called when the enemy runs into an obstacle and cannot move.
function enemy:on_obstacle_reached()

  -- Change direction if obstacle is reached.
  enemy:turn((direction + 1) % MAX_DIRECTIONS)
end

-- Drop ooze as enemy is dying.
function enemy:on_dying()
  --enemy:ooze_fast()
end

-- Turn enemy to a different direction.
function enemy:turn(new_direction)

  -- Have enemy go forward to new direction.
  movement = sol.movement.create("straight")
  movement:set_angle(new_direction * math.pi / 2)
  movement:set_speed(stats.normal_speed)
  movement:set_smooth(false)
  movement:start(enemy)
  direction = new_direction
end

-- Have enemy drop ooze.
function enemy:ooze()

  enemy:stop_movement()
  --enemy:set_animation("squeezing")
  -- TODO: Drop ooze (decide if enemy or custom entity)
  sol.timer.start(map, 2000, function()
    enemy:restart()
  end)
end

-- Have enemy drop ooze without playing animation.
function enemy:ooze_fast()
  -- TODO: Drop ooze (decide if enemy or custom entity)
end
