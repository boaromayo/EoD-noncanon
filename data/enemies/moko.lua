-- Script for enemy moko.
local enemy = ...
local map = enemy:get_map()
local behavior = require("enemies/lib/pursue_hero")

-- Status of the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 4,
  damage = 2,
  defense = 1,
  normal_speed = 36,
  running_speed = 36,
  --dying_sound = "explosion", -- For engine version 2.0 or higher.
}

behavior:create(enemy, stats)

-- Spawn a mini-moko, or drop a pickable, or do nothing when enemy dies.
function enemy:on_dying()

  local function create_mini_moko()
    sol.timer.start(enemy, 500, function()
      local moko_mini = enemy:create_enemy({
        name = "moko_",
        breed = "moko_mini",
      })
      local mvmt = sol.movement.create("straight")
      mvmt:set_angle(3 * math.pi / 4)
      mvmt:set_speed(60)
      mvmt:set_max_distance(24)
      mvmt:start(moko_mini)
      -- Slight delay before moving mini-moko.
      sol.timer.start(map, 300, function()
        moko_mini:restart()
      end)
    end)
  end
  
  local function drop_item()
    local x, y, layer = enemy:get_position()
    map:create_pickable({
      x = x,
      y = y,
      layer = layer,
      treasure_name = "random",
      treasure_variant = 1,
    })
  end

  -- Randomize chances for an enemy spawn, or an item drop, or nothing.
  local rand = math.random(100)
  if rand > 0 and rand <= 30 then
    create_mini_moko()
  elseif rand > 30 and rand <= 60 then
    drop_item()
  end
end