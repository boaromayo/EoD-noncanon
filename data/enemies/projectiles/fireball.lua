-- Script for the fireball projectile.
-- This fireball pursues hero, and can be
-- destroyed with the swing of a sword.
local enemy = ...
local sprite

function enemy:on_created()
  
  sprite = "enemies/" .. enemy:get_breed()
  enemy:create_sprite(sprite)
  enemy:set_life(1)
  enemy:set_damage(2)
  enemy:set_obstacle_behavior("flying")
  enemy:set_can_hurt_hero_running(true)
  enemy:set_size(16, 16)
  enemy:set_origin(8, 13)
end

function sprite:on_animation_finished()
  if sprite ~= nil then
    enemy:remove()
  end
end

function enemy:on_restarted(direction)
  local hero = enemy:get_map():get_hero()
  local direction = direction or enemy:get_angle(hero)
  enemy:go(direction)
end

function enemy:go(direction)
  local movement = sol.movement.create("straight")
  movement:set_speed(72)
  movement:set_smooth(false)
  movement:set_angle(direction)
  movement:start(enemy)
end

function enemy:destroy()
  if sprite:has_animation("destroy") then
    sprite:set_animation("destroy", function()
      enemy:remove()
    end
  else
    enemy:remove()
  end
end

function enemy:on_obstacle_reached()
  enemy:destroy()
end

function enemy:on_hurt_by_sword(hero, enemy_sprite)
  enemy:destroy()
end

function enemy:on_attacking_hero(hero, enemy_sprite)
  hero:start_hurt(enemy, enemy_sprite, enemy:get_damage())
  enemy:destroy()
end

function enemy:on_dying()
  -- Do not call default death animation.
  enemy:destroy()
end