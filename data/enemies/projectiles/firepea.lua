-- Script for the firepea.
-- A firepea is a smaller version of the fireball.
-- Unlike the fireball, the firepea is invincible
-- and can pursue hero, and move in 8 directions.
local enemy = ...
local sprite

function enemy:on_created()
  
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(2)
  enemy:set_size(8, 8)
  enemy:set_origin(4, 5)
  enemy:set_obstacle_behavior("flying")
  enemy:set_can_hurt_hero_running(true)
  enemy:set_invincible()
end

function enemy:on_restarted()
  local hero = enemy:get_map():get_hero()
  local direction = enemy:get_angle(hero)
  enemy:go(direction)
end

function enemy:go(direction)
  local movement = sol.movement.create("straight")
  movement:set_speed(80)
  movement:set_smooth(false)
  movement:set_angle(direction)
  movement:start(enemy)
end

function enemy:destroy()
  if sprite:has_animation("destroy") then
    sprite:set_animation("destroy", function()
      enemy:remove()
    end)
  else
    enemy:remove()
  end
end

function enemy:on_obstacle_reached()
  enemy:destroy()
end

function enemy:on_attacking_hero(hero, enemy_sprite)
  hero:start_hurt(enemy, enemy_sprite, enemy:get_damage())
  enemy:remove()
end