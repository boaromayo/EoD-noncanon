-- Script for the dart projectile.
-- This dart fires in a straight line in
-- the hero's direction, and is indestructible.
local enemy = ...
local sprite

-- Event called when the enemy is initialized.
function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_size(8, 8)
  enemy:set_origin(4, 4)
  enemy:set_obstacle_behavior("flying")
  enemy:set_can_hurt_hero_running(true)
  enemy:set_invincible()
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  -- Aim dart at hero. Calculate angle where the hero is.
  local hero = enemy:get_game():get_hero()
  local direction = enemy:get_direction4_to(hero)
  local angle = direction * math.pi / 2

  local movement = sol.movement.create("straight")
  movement:set_max_distance(400)
  movement:set_angle(angle)
  movement:set_speed(192)
  movement:set_smooth(false)
  movement:start(enemy)
end

-- Process dart land and destroy.
function enemy:destroy()
  sprite:set_animation("landed")
  enemy:stop_movement()
  
  -- Take out dart after landing.
  sol.timer.start(enemy, 500, function()
    enemy:remove()
  end)
end

function enemy:on_obstacle_reached()
  sol.audio.play_sound("arrow_hit")
  enemy:destroy()
end

function enemy:on_attacking_hero(hero, enemy_sprite)
  hero:start_hurt(enemy, sprite, enemy:get_damage())
  enemy:destroy()
end
