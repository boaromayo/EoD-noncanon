-- Script for enemy blue gel.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

-- Initialize values for blue gel behavior.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 6,
  damage = 3,
  normal_speed = 20,
  running_speed = 40,
  awake_animation = "appearing",
  sleep_animation = "disappearing",
  detection_distance = 40,
}

behavior:create(enemy, stats)