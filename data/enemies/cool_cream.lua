-- Script for enemy cool cream.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

-- Initialize values for cool cream behavior.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 10,
  damage = 8,
  normal_speed = 14,
  running_speed = 28,
  awake_animation = "appearing",
  sleep_animation = "disappearing",
  detection_distance = 40,
}

behavior:create(enemy, stats)