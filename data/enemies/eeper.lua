-- Script for enemy eeper.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 2,
  damage = 1,
  defense = 0,
  normal_speed = 30,
  running_speed = 40,
  default_animation = "walking",
  awake_animation = "walking",
  sleep_animation = "hanging",
  detection_distance = 80,
  obstacle_behavior = "flying",
}

behavior:create(enemy, stats)