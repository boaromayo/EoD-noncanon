-- Script for enemy gasfrost.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

-- Status of the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 1,
  damage = 5,
  defense = 1,
  normal_speed = 30,
  running_speed = 56,
  --dying_sound = "wail" -- For engine version 2.0 or higher.
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  --enemy:set_defense(stats.defense)
  --enemy:set_dying_sound(stats.dying_sound) -- For engine version 2.0 or higher.

  local width, height = stats.sprite:get_size()
  enemy:set_size(width, height)
  enemy:set_origin(width / 2, height - 3)

  enemy:set_attack_consequence("sword", "ignored")
  enemy:set_attack_consequence("arrow", "ignored")
  enemy:set_attack_consequence("thrown_item", "ignored")
  enemy:set_attack_consequence("hookshot", "ignored")
  enemy:set_attack_consequence("boomerang", "ignored")
  enemy:set_attack_consequence("fire", 2)
  enemy:set_attack_consequence("explosion", 2)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  movement = sol.movement.create("target")
  movement:set_target(hero)
  movement:set_speed(stats.running_speed)
  movement:start(enemy)
end
