-- Script for enemy moko mini, moko's offspring.
local enemy = ...
local map = enemy:get_map()
local behavior = require("enemies/lib/pursue_hero")

-- Status of the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 2,
  damage = 1,
  defense = 1,
  normal_speed = 24,
  running_speed = 24,
  --dying_sound = "explosion", -- For engine version 2.0 or higher.
}

behavior:create(enemy, stats)

-- Randomly drop a pickable, or do nothing.
function enemy:on_dying()
  
  local function drop_item()
    local x, y, layer = enemy:get_position()
    map:create_pickable({
      x = x,
      y = y,
      layer = layer,
      treasure_name = "random",
      treasure_variant = 1,
    })
  end
  
  -- Randomize chances for an item drop.
  local rand = math.random(100)
  if rand > 0 and rand <= 30 then
    drop_item()
  end
end