-- Script for enemy buccaneer plus.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
--local sword_sprite
--local behavior = require("enemies/lib/soldier")

local pursuing_hero = false
local near_hero = false

-- Status of the enemy.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  --sword_sprite = "enemies/" .. enemy:get_breed() .. ".weapon",
  life = 8,
  damage = 3,
  defense = 0,
  normal_speed = 48,
  running_speed = 72,
  pushed_back_when_hurt = true,
  push_hero_on_sword = false,
  detection_distance = 120,
  ignore_obstacles = false,
}

-- Call behavior "soldier" script.
--behavior:create(enemy, stats)

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  --sword_sprite = enemy:create_sprite(stats.sword_sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  enemy:set_hurt_style("normal")
  enemy:set_pushed_back_when_hurt(stats.pushed_back_when_hurt)
  enemy:set_push_hero_on_sword(stats.push_hero_on_sword)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  if near_hero then
    enemy:pursue()
  else
    enemy:check_hero()
    enemy:move_random()
  end
end

-- Check if enemy is near hero.
function enemy:check_hero()
  near_hero = enemy:check_distance_from_hero() and enemy:is_in_same_region(hero)

  if near_hero and not pursuing_hero then
    enemy:pursue()
  elseif pursuing_hero and not near_hero then
    enemy:move_random()
  end

  sol.timer.stop_all(enemy)
  sol.timer.start(enemy, 100, function()
    enemy:check_hero()
  end)
end

-- Event called when enemy movement is changed.
function enemy:on_movement_changed(movement)
  local direction = movement:get_direction4()
  local sprite = enemy:get_sprite()
  sprite:set_direction(direction)
end

-- Event called when enemy hits an obstacle.
function enemy:on_obstacle_reached()
  if not pursuing_hero then
    enemy:check_hero()
    enemy:move_random()
  end
end 

-- Check enemy's distance from hero.
function enemy:check_distance_from_hero()
  --local enemy_layer = enemy:get_layer() -- Only for versions >= 1.6.
  --local hero_layer = hero:get_layer()
  local _, _, enemy_layer = enemy:get_position()
  local _, _, hero_layer = hero:get_position()

  return (enemy_layer == hero_layer or enemy:has_layer_independent_collisions()) 
    and (enemy:get_distance(hero) < stats.detection_distance)
end

-- When enemy sees player, chase after them.
function enemy:pursue()
  pursuing_hero = true

  local movement = sol.movement.create("target")
  movement:set_target(hero)
  movement:set_speed(stats.running_speed)
  movement:set_ignore_obstacles(stats.ignore_obstacles)
  movement:start(enemy)

  sprite:set_animation("walking")
end

-- When player is not nearby, wander randomly.
function enemy:move_random()
  pursuing_hero = false

  local movement = sol.movement.create("random_path")
  movement:set_speed(stats.normal_speed)
  movement:set_ignore_obstacles(stats.ignore_obstacles)
  movement:start(enemy)
end