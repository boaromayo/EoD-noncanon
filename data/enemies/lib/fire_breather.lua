-- Based off of the fire-breathing statue script by Christopho.
-- This script shows the behavior for enemies who shoot fireballs,
-- or fire breath.

local behavior = {}

-- Example of use from an enemy script:

-- local enemy = ...
-- local behavior = require("enemies/lib/fire_breather")
-- local stats = {
--  sprite = "enemies/dragon_statue",
--  projectile = "projectiles/fire_breath",
--  projectile_x = 0,
--  projectile_y = 0,
--  projectile_sfx = "poof",
--  projectile_duration = 2000,
--  invincible = true,
--  push_hero = false,
--  ignore_obstacles = false,
--  detection_distance = 60,
--  firing_delay = 800
-- }
-- behavior:create(enemy, stats)

-- The status is a table.
-- All its values are optional except the sprite, projectile, and the projectile's
-- initial location.

function behavior:create(enemy, stats)

  local map = enemy:get_map()
  local hero = map:get_hero()
  
  local projectiles = {}

  -- Set status defaults here.
  if stats.projectile_sfx == nil then
    stats.projectile_sfx = ""
  end
  if stats.invincible == nil then
    stats.invincible = true
  end
  if stats.push_hero == nil then
    stats.push_hero = false
  end
  if stats.ignore_obstacles == nil then
    stats.ignore_obstacles = false
  end
  if stats.detection_distance == nil then
    stats.detection_distance = 100
  end
  if stats.firing_delay == nil then
    stats.firing_delay = 5000
  end

  function enemy:on_created()
    enemy:set_life(1)
    enemy:set_damage(0)
    enemy:create_sprite(stats.sprite)
    enemy:set_pushed_back_when_hurt(false)
    enemy:set_can_attack(false)
    enemy:set_size(16, 16)
    enemy:set_origin(8, 13)
    enemy:set_firing(true)

    if stats.invincible then
      enemy:set_invincible()
      enemy:set_defense(9999)
    end
  end
  
  local function is_nearby(hero, enemy)
    local _, _, hero_layer = hero:get_position()
    local _, _, enemy_layer = enemy:get_position()
    return (enemy_layer == hero_layer or enemy:has_layer_independent_collisions()) and (enemy:get_distance(hero) < stats.detection_distance)
  end
  
  function enemy:on_restarted()
    sol.timer.start(enemy, stats.firing_delay, function()
      if not enemy.firing then
        return true
      end
      
      if is_nearby(hero, enemy) and enemy:is_in_same_region(hero) then
        local _, _, projectile_layer = enemy:get_position()
        projectiles[#projectiles + 1] = enemy:create_enemy({
          x = stats.projectile_x,
          y = stats.projectile_y,
          layer = projectile_layer,
          breed = stats.projectile,
        })
        projectiles[#projectiles]:set_layer_independent_collisions(true)
        if stats.projectile_duration ~= nil then
          -- Remove projectile once duration is reached.
          local projectile = projectiles[#projectiles]
          sol.timer.start(projectile, stats.projectile_duration, function()
            if projectile.destroy then
              projectile:destroy()
            else
              projectile:remove()
            end
          end)
        end
      end
      return true
    end)
  end
  
  -- Enables or disables firing of projectiles.
  function enemy:set_firing(firing)
    enemy.firing = firing
  end
  
  function enemy:on_removed()
    -- Remove all open projectiles on the map.
    for _, projectile in ipairs(projectiles) do
      if projectile.destroy then
        projectile:destroy()
      else
        projectile:remove()
      end
    end
  end
end

return behavior