local behavior = {}

-- Special thanks to Christopho for the original idea of this behavior script.

-- This script checks the behavior of enemies that is in a sleep state, 
-- and to run away from the hero if the enemy sees them,
-- and disappears if the enemy if it loses sight of the hero.
-- The enemy has only two new sprites animation: an asleep one,
-- and an awaking transition.
-- a different walking one can be set in the properties, though.

-- Example of use from an enemy script:

-- local enemy = ...
-- local behavior = require("enemies/lib/run_from_hero")
-- local stats = {
--  sprite = "enemies/runbug",
--  life = 1,
--  damage = 2,
--  defense = 2,
--  normal_speed = 48,
--  running_speed = 60,
--  default_animation = "walking",
--  awake_animation = "awaken",
--  sleep_animation = "sleep",
--  hurt_style = "normal",
--  push_hero_on_sword = false,
--  pushed_when_hurt = true,
--  ignore_obstacles = false,
--  obstacle_behavior = "hurt",
--  detection_distance = 100,
--  movement_create = function()
--    local m = sol.movement.create("random_path")
--    return m
--  end
-- }
-- behavior:create(enemy, stats)

-- The enemy status is a table 
-- containing the properties and behavior of the enemy.
-- All its values are optional except the sprite.

-- Create enemy behavior here.
function behavior:create(enemy, stats)
  local run_from_hero = false
  local awake = false
  local disappear = false
  
  -- Set enemy status and default behavior.
  if stats.life == nil then
    stats.life = 2
  end
  if stats.damage == nil then
    stats.damage = 1
  end
  if stats.defense == nil then
    stats.defense = 0
  end
  if stats.normal_speed == nil then
    stats.normal_speed = 32
  end
  if stats.running_speed == nil then
    stats.running_speed = 48
  end
  if stats.default_animation == nil then
    stats.default_animation = "walking"
  end
  if stats.awake_animation == nil then
    stats.awake_animation = "awaken"
  end
  if stats.sleep_animation == nil then
    stats.sleep_animation = "sleep"
  end
  if stats.hurt_style == nil then
    stats.hurt_style = "normal"
  end
  if stats.pushed_back_when_hurt == nil then
    stats.pushed_back_when_hurt = true
  end
  if stats.push_hero_on_sword == nil then
    stats.push_hero_on_sword = false
  end
  if stats.ignore_obstacles == nil then
    stats.ignore_obstacles = false
  end
  if stats.detection_distance == nil then
    stats.detection_distance = 160
  end
  if stats.obstacle_behavior == nil then
    stats.obstacle_behavior = "normal"
  end
  if stats.movement_create == nil then
    stats.movement_create = function()
      local m = sol.movement.create("random")
      return m
    end
  end
  
  -- Event called when enemy is initialized.
  function enemy:on_created()
    -- Initialize enemy stats and behavior here.
    local sprite = enemy:create_sprite(stats.sprite)
    enemy:set_life(stats.life)
    enemy:set_damage(stats.damage)
    enemy:set_hurt_style(stats.hurt_style)
    enemy:set_pushed_back_when_hurt(stats.pushed_back_when_hurt)
    enemy:set_push_hero_on_sword(stats.push_hero_on_sword)
    enemy:set_obstacle_behavior(stats.obstacle_behavior)
    
    stats.width, stats.height = enemy:get_sprite():get_size()
    enemy:set_size(stats.width, stats.height)
    enemy:set_origin(stats.width / 2, stats.height - 3)
    
    -- When sprite finished awakening animation, run from player.
    function sprite:on_animation_finished(animation)
      if animation == stats.awake_animation then
        enemy:finish_animation()
      end
    end
    sprite:set_animation(stats.sleep_animation)
  end
  
  -- Event called when enemy movement has changed.
  function enemy:on_movement_changed(movement)
    if awake then
      local direction = movement:get_direction4()
      enemy:get_sprite():set_direction(direction)
    end
  end
  
  -- Event called when enemy reaches an obstacle.
  function enemy:on_obstacle_reached(movement)
    if awake and run_from_hero then
      enemy:disappear()
    end
  end
  
  -- Event called when the enemy should start or restart its movements.
  -- This is called for example after the enemy is created or after
  -- it was hurt or immobilized.
  function enemy:on_restarted()
    enemy:sleep()
    enemy:check_hero()
  end
  
  function enemy:check_hero()
    local hero = enemy:get_map():get_hero()
    local near_hero
    
    -- Check if enemy sees hero nearby.
    local function is_nearby(enemy, hero)
      local _, _, hero_layer = hero:get_position()
      local _, _, enemy_layer = enemy:get_position()
      return (enemy_layer == hero_layer and enemy:has_layer_independent_collisions()) and
        (enemy:get_distance(hero) < stats.detection_distance)
    end
    
    near_hero = is_nearby(enemy, hero) and enemy:is_in_same_region(hero)
    
    if awake and near_hero and not run_from_hero then
      enemy:run_away()
    elseif awake and not near_hero and run_from_hero then
      enemy:sleep()
    elseif not awake and near_hero then
      enemy:wake_up()
    end
    
    -- Freeze enemy for a bit and delay before changing behavior.
    sol.timer.stop_all(enemy)
    sol.timer.start(enemy, 500, function()
      enemy:check_hero()
    end)
  end
  
  function enemy:wake_up()
    enemy:stop_movement()
    enemy:finish_animation()
    if stats.awake_animation ~= nil then
      enemy:get_sprite():set_animation(stats.awake_animation)
    end
  end

  function enemy:finish_animation()
    enemy:get_sprite():set_animation(stats.default_animation)
    awake = true
    enemy:run_away()
  end
  
  function enemy:sleep()
    enemy:stop_movement()
    enemy:get_sprite():set_animation(stats.sleep_animation)
    awake = false
  end
  
  function enemy:move_random()
    run_from_hero = false
    
    local movement = stats.movement_create()
    movement:set_speed(stats.normal_speed)
    movement:set_ignore_obstacles(stats.ignore_obstacles)
    movement:start(enemy)
  end
  
  -- Enemy sees hero and runs away from them.
  function enemy:run_away()
    run_from_hero = true
    
    local movement = sol.movement.create("")
    movement:set_speed(stats.running_speed)
    movement:set_ignore_obstacles(stats.ignore_obstacles)
    movement:start(enemy)
  end
  
  function enemy:disappear()
    enemy:get_sprite():fade_out(300)
    enemy:set_enabled(false)
  end
end

return behavior