-- Lua script of enemy boss/captain.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
--local sword_sprite
local movement

local hit_by_hero = false
local times_hit = 0

local close_to_hero = false
local near_hero = false
local pursuing_hero = false
local is_guarding = false

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  --sword_sprite = "enemies/" .. enemy:get_breed() .. "_sword",
  life = 20,
  damage = 3,
  defense = 1,
  normal_speed = 50,
  running_speed = 50,
  pushed_back_when_hurt = false,
  push_hero_on_sword = true,
  attacking_distance = 40,
  detection_distance = 80,
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  --sword_sprite = enemy:create_sprite(stats.sword_sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  enemy:set_hurt_style("boss")
  enemy:set_pushed_back_when_hurt(stats.pushed_back_when_hurt)
  enemy:set_push_hero_on_sword(stats.push_hero_on_sword)

  local width, height = enemy:get_sprite():get_size()
  enemy:set_size(width, height)
  enemy:set_origin(width / 2, height - 6)
  
  if game:main_story_at(2) then
    is_first_battle = true
  end
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  if close_to_hero then
    enemy:slash()
  elseif near_hero and not close_to_hero then
    enemy:pursue()
  else
    enemy:wait()
  end
  enemy:check_hero()
end

-- Check if enemy is near hero.
function enemy:check_hero()

  local function is_nearby(enemy, hero, distance)
    --local enemy_layer = enemy:get_layer()
    --local hero_layer = hero:get_layer()
    local _, _, enemy_layer = enemy:get_position()
    local _, _, hero_layer = hero:get_position()    

    return (enemy_layer == hero_layer or enemy:has_layer_independent_collisions())
      and (enemy:get_distance(hero) < distance)
  end

  near_hero = is_nearby(enemy, hero, stats.detection_distance) and enemy:is_in_same_region(hero)
  close_to_hero = is_nearby(enemy, hero, stats.attacking_distance) and enemy:is_in_same_region(hero)

  if near_hero and not close_to_hero then
    enemy:pursue()
  elseif close_to_hero and near_hero then
    enemy:slash()
  else
    enemy:wait()
  end
end

-- Event called when the movement has changed.
function enemy:on_movement_changed(movement)
  local direction = movement:get_direction4(movement)
  sprite:set_direction(direction)
end

-- Event called when the enemy is hit by the player's weapon.
-- Note on_hurt_by_sword or on_custom_attack_received is not used
-- since the hero is not using the in-built "sword" attack.
function enemy:on_hurt(attack)
  -- Test if enemy getting hit once or more than once.
  print("Times hit: " .. times_hit)
  if hit_by_hero then
    guarding = true
    enemy:guard()
    return
  end
  times_hit = times_hit + 1

  hit_by_hero = true
  if times_hit >= 5 then
    game:start_dialog("ship.a.captain.2")
  end
end

-- Chase after hero when nearby.
function enemy:pursue()
  pursuing_hero = true

  sprite:set_animation("walking")
  
  -- Start pursuing movement.
  movement = sol.movement.create("target")
  movement:set_target(hero)
  movement:set_speed(stats.running_speed)
  movement:set_smooth(false)
  movement:start(enemy)
end

-- Start slash when close to hero.
function enemy:slash()
  pursuing_hero = false

  -- Perform slashing attack and move away and wait.
  sprite:set_animation("slashing")
  sol.timer.start(enemy, 800, function()
    enemy:wait()
  end)
end

-- Wait for hero to go close to enemy.
function enemy:wait()
  pursuing_hero = false
  -- Stop moving enemy.
  sprite:set_animation("stopped")
  -- Find and look at hero.
  local facing_direction = enemy:get_direction4_to(hero)
  sprite:set_direction(facing_direction)
end

-- Guard when hero attacks too much.
function enemy:guard()
  if is_guarding then
    return
  end
  enemy:set_guard(true)
  sprite:set_animation("guarding")
  sol.timer.start(enemy, 1000, function()
    enemy:set_guard(false)
    enemy:wait()
  end)
end

-- Toggle guarding mode.
function enemy:set_guard(guarding)
  is_guarding = guarding
  enemy:set_invisible(is_guarding)
end