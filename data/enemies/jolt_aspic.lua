-- Script for enemy jolt aspic.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")
local game = enemy:get_game()
local map = game:get_map()
local hero = game:get_hero()
-- Enemy effect.
local electrified = false

-- Initialize values for jolt aspic behavior.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 13,
  damage = 4,
  shock_damage = 6,
  normal_speed = 18,
  running_speed = 36,
  awake_animation = "appearing",
  sleep_animation = "disappearing",
  detection_distance = 40,
}

behavior:create(enemy, stats)

function enemy:on_hurt(attack)
  if electrified then
    hero:start_hurt(math.floor(stats.damage / 2))
    hero:freeze()
    hero:set_animation("electrocuted")
    sol.timer.start(map, 1000, function()
      hero:unfreeze()
    end)
  else
    enemy:get_attack_consequence(attack)
  end
end

function enemy:on_attacking_hero(hero, enemy_sprite)
  if electrified then
    game:remove_life(stats.damage - 1)
    hero:start_hurt(enemy, stats.shock_damage)
    hero:freeze()
    hero:set_animation("electrocuted")
    sol.timer.start(map, 1000, function()
      hero:unfreeze()
    end)
  end
end

-- Stop and electrify the enemy for a certain amount of time.
function enemy:electrify()
  sol.timer.stop_all()
  enemy:stop_movement()
  electrified = true
  enemy:get_sprite():set_animation("shocked")
  sol.timer.start(enemy, 3000, function()
    electrified = false
    enemy:restart()
  end)
end
