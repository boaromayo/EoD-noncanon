-- Script for enemy mage.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  local max_life = 10

  sprite = enemy:create_sprite("enemies/" .. enemy.get_breed())
  enemy:set_life(max_life)
  enemy:set_damage(4)
  enemy:set_size()
  enemy:set_origin(8, 13)
  enemy:set_hurt_style("boss")

end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()

  movement = sol.movement.create("random")
  movement:set_target(hero)
  movement:set_speed(36)
  movement:start(enemy)
end

-- Event called when enemy is hurt by a certain weapon or attack.
function enemy:on_hurt(attack)
  local life = enemy:get_life()
  -- Life is now half or below half, start moving slightly faster.
  if life <= life / 2 then
    movement:set_speed(40)
    movement:start(enemy)
  end
end