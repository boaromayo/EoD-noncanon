-- Script for enemy hot flan.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

-- Initialize values for hot flan behavior.
local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 15,
  damage = 8,
  normal_speed = 14,
  running_speed = 28,
  awake_animation = "appearing",
  sleep_animation = "disappearing",
  detection_distance = 40,
}

behavior:create(enemy, stats)