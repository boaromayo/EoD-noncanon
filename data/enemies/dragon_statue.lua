-- Script for enemy dragon statue.
local enemy = ...
local behavior = require("enemies/lib/fire_breather")

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  projectile = "enemies/projectiles/fireball",
  projectile_x = 0,
  projectile_y = -5,
  projectile_speed = 60,
  projectile_sfx = "",
  projectile_duration = 2000,
  invincible = true,
  detection_distance = 200,
  firing_delay = 500,
}

behavior:create(enemy, stats)