-- Script for enemy coil.
local enemy = ...
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement
local quarter_angle = math.pi * 0.5

local is_charging = false

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  width = 24,
  height = 24,
  life = 3,
  damage = 2,
  defense = 0,
  normal_speed = 20,
  charging_speed = 80,
  line_detection_distance = 80,
  min_wander_distance = 20,
  max_wander_distance = 80,
  max_charge_distance = 100,
  resting_duration = 600,
}

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite(stats.sprite)
  enemy:set_life(stats.life)
  enemy:set_damage(stats.damage)
  enemy:set_size(stats.width, stats.height)
  enemy:set_origin(stats.width / 2, stats.height - 3)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()
  enemy:set_charging(false)
  enemy:check_hero()
  enemy:move_random()
end

-- Event called at every cycle of the game loop while enemy is alive.
function enemy:on_update()
  enemy:check_hero()
end

-- Check if enemy is near and lined up with player, and if enemy sees player.
function enemy:check_hero()
  local near_hero = enemy:check_distance_from_hero() and
    enemy:is_in_same_region(hero)
  local facing_hero = enemy:check_facing_hero()

  if not is_charging and near_hero and facing_hero then
    enemy:charge()
  end
end

local function layer_match()
  -- TODO: Change to get_layer() >= 1.5.
  local _, _, enemy_layer = enemy:get_position()
  local _, _, hero_layer = hero:get_position()

  return enemy_layer == hero_layer or enemy:has_layer_independent_collisions()
end

-- Calculate distance differently here. Check if enemy is aligned with player 
-- since enemy:get_distance() checks radially instead of linearly.
function enemy:check_distance_from_hero()
  local x, y, _ = enemy:get_position()
  local hero_x, hero_y, _ = hero:get_position()
  local x_offset, y_offset = sprite:get_xy()
  x, y = x + x_offset, y + y_offset

  return layer_match() and ((math.abs(hero_x - x) < stats.line_detection_distance) and 
    (math.abs(hero_y - y) < stats.line_detection_distance))
end

-- Check enemy facing player.
function enemy:check_facing_hero()
  local direction = enemy:get_direction4_to(hero)
  return sprite:get_direction() == direction
end

-- Charging attack.
function enemy:charge()
  -- Stop moving, enemy saw player!
  enemy:set_charging(true)
  enemy:stop_movement()
  -- Create movement straight towards player.
  movement = sol.movement.create("straight")
  movement:set_speed(stats.charging_speed)
  movement:set_max_distance(stats.max_charge_distance)
  movement:set_angle(enemy:get_direction4_to(hero) * quarter_angle)
  movement:set_smooth(false)
  movement:start(enemy)
  sprite:set_animation("charging")
  sprite:set_direction(movement:get_direction4())

  -- Event called when the enemy's movement is finished.
  function enemy:on_movement_finished()
    enemy:stop_charge()
  end
  -- Event called when the enemy runs into an obstacle.
  function enemy:on_obstacle_reached(movement)
    enemy:stop_charge()
  end
end

-- Stop charge, and restart enemy.
function enemy:stop_charge()
  sprite:set_animation("stopped")
  sol.timer.start(enemy, stats.resting_duration, function() 
    enemy:restart()
  end)
end

-- When player is not nearby, wander randomly in a straight trajectory.
function enemy:move_random()
  local max_wander_range = math.random(stats.min_wander_distance, stats.max_wander_distance)
  local angles = {0, quarter_angle, quarter_angle * 2, quarter_angle * 3}

  sprite:set_animation("walking")
  movement = sol.movement.create("straight")
  movement:set_max_distance(max_wander_range)
  movement:set_angle(angles[math.random(#angles)])
  movement:set_speed(stats.normal_speed)
  movement:set_smooth(true)
  movement:start(enemy)
  sprite:set_direction(movement:get_direction4())

  function enemy:on_movement_finished()
    enemy:stop_charge()
  end

  function enemy:on_obstacle_reached(movement)
    movement:stop()
    enemy:stop_charge()
  end
end

-- Toggle charging enemy.
-- NOTE: Setters/getters are always low priority on enemy scripts.
function enemy:set_charging(charging)
  is_charging = charging
end