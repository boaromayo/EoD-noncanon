-- Script for enemy dragon statue head.
local enemy = ...
local behavior = require("enemies/lib/fire_breather")

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  projectile = "projectiles/firepea",
  projectile_x = 0,
  projectile_y = 0,
  projectile_sfx = "fire_ball",
  projectile_duration = 2000,
  invincible = true,
  detection_distance = 120,
  firing_delay = 2000,
}

behavior:create(enemy, stats)
