-- Script for enemy beak.
local enemy = ...
local behavior = require("enemies/lib/wait_for_hero")

local stats = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 2,
  damage = 1,
  defense = 0,
  normal_speed = 30,
  default_animation = "stopped",
  awake_animation = "walking",
  sleep_animation = "stopped",
  detection_distance = 100,
  obstacle_behavior = "flying",
}

behavior:create(enemy, stats)