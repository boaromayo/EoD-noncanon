-- Setup a debug console to input Lua functions that can be enabled by pressing the console key (debug mode only).
-- The original debug console script created by Christopho.
local mono_font = "8_bit"
local mono_font_size = 12

-- CONSOLE.
local console = {
  
  -- Properties
  font = mono_font,                         -- Monospace font.
  font_size = mono_font_size,               -- Font size (in pixels).
  char_width = 8,                           -- Width of font characters.
  line_spacing = 0,                         -- Space between 2 lines (in pixels).
  
  margin = 4,                               -- Margin of console (in pixels).
  padding = 8,                              -- Padding size of console (in pixels).
  height_offset = 120,                      -- Console height offset.
  
  color = { 32, 32, 32 },                   -- Console background color.
  font_color = { 255, 255, 255 },           -- Font color.
  selection_color = { 64, 128, 192 },       -- Selection highlight color.
  selection_opacity = 80,                   -- Opacity of selection highlight.
  selection_height = mono_font_size - 2,    -- Selection height size.
  cursor_sprite = nil,                      -- Cursor sprite.
  cursor_sprite_id = "console/cursor",      -- Cursor sprite id.
  
  history = {},                             -- Table containing history.
  history_capacity = 99,                    -- Maximum amount of commands in history.
  history_filename = "history",             -- History file name.
  debug_filename = "debug",                 -- Debug file name.
  
  surface = nil,                            -- Main drawing surface.
  line_surfaces = {},                       -- Line text surfaces.
  line_icons = {},                          -- Line icon surfaces.
  
  current_line = 0,                         -- The line the cursor is currently on.
  last_line = 0,                            -- The line the cursor was previously on.
  max_lines = 1,                            -- Maximum amount of lines.
  current_command = {},                     -- List of currently performed console commands.
  
  clipboard = {},                           -- Clipboard to store copied text.
}

-- Gets the first argument and a list of other arguments.
-- This function is used to get the results from the pcall() function.
local function get_results(...)
  -- Get number of arguments.
  local num_args = select("#", ...)
  
  -- Get success.
  local success = false
  if num_args > 0 then
    success = select(1, ...)
  end
  
  -- Build the list.
  local list = {}
  for i = 2, num_args do
    local arg = select(i, ...)
    if arg ~= nil then
      table.insert(list, arg)
    else
      table.insert(list, "nil")
    end
  end
  
  return success, list
end

-- Environment index function. 
-- This creates shortcut commands in the console.
local function environment_index(environment, key)
  local game = sol.main.game
  
  if key == "print" then
    return console.print
  elseif key == "clear" then
    return console.clear
  end
  
  if game ~= nil then
    if key == "game" then
      return game
    elseif key == "map" then
      return game:get_map()
    elseif key == "tp" then
      return function(...)
        game:get_hero():teleport(...)
        sol.menu.stop(console)
      end
    end
    
    local entity = game:get_map():get_entity(key)
    if entity ~= nil then
      return entity
    end
  end
  
  local debug = console.debug_env[key]
  if debug ~= nil then
    return debug
  end
  
  return _G[key]
end

-- Initialize console.
function console:initialize()
  -- Disable console on start.
  self.enabled = false
  
  local width, height = sol.video.get_quest_size()
  local console_width, console_height
  local inner_width, inner_height
  local margin_diff, padding_diff = self.margin * 2, self.padding * 2
  
  console_width = width - margin_diff
  console_height = height - margin_diff - self.height_offset
  inner_width = console_width - padding_diff
  inner_height = console_height - padding_diff
  
  -- Create drawing surface.
  self.main_surface = sol.surface.create(console_width, console_height)
  self.main_surface:fill_color(self.color)
  
  -- Calculate constraints of console table.
  self.max_chars = math.floor(inner_width / self.char_width)
  
  self.line_height = self.font_size + self.line_spacing
  self.max_lines = math.floor(inner_height / self.line_height)
  
  local spacing = inner_height - (self.font_size * self.max_lines)
  self.line_height = self.font_size + math.floor(spacing / self.max_lines)
  
  -- Initialize text surfaces.
  if not self.line_surfaces or not self.line_icons then
    self.line_surfaces = {}
    self.line_icons = {}
  end
  
  for i = 1, self.max_lines do
    self.line_surfaces[i] = sol.text_surface.create({
      font = self.font,
      font_size = self.font_size,
      color = self.font_color,
      vertical_alignment = "top",
    })
    self.line_icons[i] = ""
  end
  
  -- Initialize position.
  self.last_line = 1
  self.current_line = 1
  
  if not self.current_command then
    self.current_command = {}
  end
  
  -- Create cursor sprite.
  self.cursor_sprite = sol.sprite.create(self.cursor_sprite_id)
  self.cursor_sprite_width, self.cursor_sprite_height = self.cursor_sprite:get_size()
  
  -- Create history.
  if not self.history then
    self.history = {}
  end
  self.temp_history = {{}}
  self.history_position = 1
  self.cursor = 0
  
  -- Create selection.
  self.selection = 0
  self.selection_surface = sol.surface.create(width, height)
  self.selection_surface:set_opacity(self.selection_opacity)
  
  if not self.clipboard then
    self.clipboard = {}
  end
  
  self:load_history()
  self.history_is_saved = false
  
  -- Setup debug environment.
  self.debug_env = {}
  local debug = sol.main.load_file(self.debug_filename)
  
  -- If not able to load file, try .lua extension.
  if type(debug) ~= "function" then
    debug = sol.main.load_file(self.debug_filename)
  end
  if type(debug) == "function" then
    self.debug_env = debug(self) or {}
  end
  
  -- Setup environment.
  self.environment = {}
  setmetatable(self.environment, {
    __index = environment_index,
    __newindex = _G
  })

  -- Draw first line.
  self:draw_line()
end

-- Draw the current line.
function console:draw_line()
  -- Get the current line with the prompt.
  local line = "$ " .. table.concat(self:get_current_line())
  if #self.current_command > 0 then
    line = "$ " .. line
  end
  
  -- Print the current line.
  self.last_line = self.current_line
  self:print_text(self.current_line, line)
  
  -- Clear all next lines.
  for i = self.last_line, self.max_lines do
    self.line_surfaces[i]:set_text("")
  end
  
  -- Redraw the selection surface.
  self:draw_selection()
end

-- Draws the selection surface.
function console:draw_selection()
  -- Clear selection surface.
  self.selection_surface:clear()
  
  if self.cursor ~= self.selection then
    local origin = self.margin + self.padding
    local cursor = math.min(self.cursor, self.selection)
    local selection = math.max(self.cursor, self.selection)
    local current_line, current_char = self:get_cursor_position(cursor)
    local selected_line, selected_char = self:get_cursor_position(selection)
    
    local x = origin + (current_char * self.char_width)
    local y = origin + ((current_line - 1) * self.line_height)
    local w
    local h = self.selection_height
    
    if current_line == selected_line then
      --Print simple selection.
      w = origin + (selected_char * self.char_width) - x
      
      self.selection_surface:fill_color(self.selection_color, x, y, w, h)
    else
      -- Print first selection line.
      w = (self.max_chars * self.char_width) - x + origin
      
      if w > 0 then
        self.selection_surface:fill_color(self.selection_color, x, y, w, h)
      end
      -- Print intermediate selection lines.
      x = origin
      w = self.max_chars * self.char_width
      for i = current_line + 1, selected_line - 1 do
        y = y + self.line_height
        
        self.selection_surface:fill_color(self.selection_color, x, y, w, h)
      end
      -- Print last selection line.
      y = y + self.line_height
      w = selected_char * self.char_width

      if w > 0 then
        self.selection_surface:fill_color(self.selection_color, x, y, w, h)
      end
    end
  end
end

-- Get the current line.
function console:get_current_line()
  return self.temp_history[self.history_position]
end

-- Clear console.
function console.clear()
  -- Clear surfaces and icons.
  for i = 1, #console.line_surfaces do
    console.line_surfaces[i]:set_text("")
    console.line_icons[i] = ""
  end
  
  -- Reset positions.
  console.last_line = 1
  console.current_line = 1
  console.current_command = {}
  
  -- Rebuild current line.
  console:draw_line()
  
  -- Reset cursor sprite.
  console.cursor_sprite:set_frame(0)
end

-- Prints text at the end of the last line.
function console.print(...)
  -- Get number of arguments.
  local num_args = select("#", ...)
  
  -- Get the current text of the line.
  local text = ""
  if console.last_line <= console.max_lines then
    text = console.line_surfaces[console.last_line]:get_text()
  end
  
  -- Check for each argument printed.
  for i=1, num_args do
    local arg = select(i, ...)
    
    -- If text is not empty, add a space.
    if text ~= "" then
      text = text .. " "
    end
    
    -- Check type of the argument and add to text.
    local type_name = sol.main.get_type(arg)
    if type_name == "string" or type_name == "number" then
      text = text .. arg
    elseif type_name == "boolean" then
      text = text .. (arg and "true" or "false")
    else
      text = text .. type_name
    end
  end
  
  -- Print text at the last line.
  console:print_text(console.last_line, text)
end

-- Prints text at a specific line.
function console:print_text(line_index, text)
  -- Split text in lines.
  local lines = {}
  for line in string.gmatch(text .. "\n", "[^\n]*\n") do
    while #line > self.max_chars do
      table.insert(lines, string.sub(line, 1, self.max_chars))
      line = string.sub(line, self.max_chars + 1)
    end
    table.insert(lines, line)
  end
  
  -- Set text of surfaces.
  for i, line in pairs(lines) do
    local index = math.min(line_index + i - 1, self.max_lines)
    
    if self:go_to_line() and self.current_line > 1 then
      self.current_line = self.current_line - 1
    end
    
    local drawable_text = line:gsub("\n", " ")
    self.line_surfaces[index]:set_text(drawable_text)
  end
end

-- Print error messages.
function console:print_error(msg)
  -- Print error message.
  msg = msg:gsub("^.%]:", "")
  self.print("  " .. msg)
end

-- Shift console down to next line.
function console:go_to_line()
  -- Console not shifted down by defualt.
  local shifted = false
  
  if self.last_line > self.max_lines then
    self.last_line = self.max_lines
    shifted = true -- Shift console by one line.
    
    -- Shift text surfaces.
    table.insert(self.line_surfaces, table.remove(self.line_surfaces, 1))
    
    self.line_surfaces[self.last_line]:set_text("")
    
    -- Shift icons.
    table.remove(self.line_icons, 1)
    table.insert(self.line_icons, "")
  end
  
  self.last_line = self.last_line + 1
  return shifted
end

-- Adds characters at the cursor position.
function console:add_chars(chars)
  if self.cursor ~= self.selection then
    self:remove_chars()
  end
  
  local line = self:get_current_line()

  for _, character in pairs(chars) do
    self.cursor = self.cursor + 1
    table.insert(line, self.cursor, character)
  end
  
  self.selection = self.cursor

  -- Redraw current line.
  self:draw_line()
  
  -- Reset cursor sprite.
  self.cursor_sprite:set_frame(0)
end

-- Removes a character at the cursor position or selection.
function console:remove_chars(after)
  after = after or false
  
  local line = self:get_current_line()
  
  if self.cursor ~= self.selection then
    local cursor = math.min(self.cursor, self.selection)
    local selection = math.max(self.cursor, self.selection)
    
    for i = cursor + 1, selection do
      table.remove(line, cursor + 1)
    end
    self.cursor = cursor
    self.selection = selection
  elseif after then
    table.remove(line, self.cursor + 1)
  else
    table.remove(line, self.cursor)
    self:shift_cursor(-1)
  end
  
  -- Redraw current line.
  self:draw_line()
  
  -- Reset cursor position.
  self.cursor_sprite:set_frame(0)
end

-- Get cursor line number and character position.
function console:get_cursor_position(cursor)
  local line = self:get_current_line()
  local char_num = #self.current_command > 0 and 3 or 2
  local line_num = self.current_line
  
  for i=1, cursor do
    -- Shift to next line if character is newline.
    if line[i] == "\n" then
      line_num = line_num + 1
      char_num = 0
    -- Shift to next line if character limit is reached for that line.
    elseif char_num >= self.max_chars then
      line_num = line_num + 1
      char_num = 1
    else
      char_num = char_num + 1
    end
  end
  
  return line_num, char_num
end

-- Moves cursor position.
function console:shift_cursor(shift, select)
  self.cursor = math.min(math.max(self.cursor + shift, 0), #self:get_current_line())
  if not select then
    self.selection = self.cursor
  end
  
  -- Redraw selection surface.
  self:draw_selection()
  
  -- Reset cursor sprite position.
  self.cursor_sprite:set_frame(0)
end

-- Add current line to the current command.
function console:add_line()
  -- If the command is not empty, add a new line.
  if self.current_command[1] then
    table.insert(self.current_command, "\n")
  end
  
  -- Add current line to the command.
  for _, character in pairs(self:get_current_line()) do
    table.insert(self.current_command, character)
  end
  
  -- Try executing the command.
  self:execute_command()
  
  -- If at the end, go to next line (shift text surfaces).
  if self.last_line > self.max_lines or 
      self.current_line == self.max_lines then
    self:go_to_line()
  end

  -- Set current line to the next after printing output.
  self.current_line = math.min(self.last_line, self.max_lines)
  
  -- Redraw current line.
  self:draw_line()
  
  -- Reset cursor sprite.
  self.cursor_sprite:set_frame(0)
end

-- Executes the current command.
function console:execute_command()
  -- Get the code.
  local line = table.concat(self.current_command)
  local code, error_msg = loadstring(line)
  local autoprint = false
  
  -- If error, try to autoprint.
  if code == nil then
    code = loadstring("print(' ', " .. line .. ")")
    autoprint = true
  else -- else, try to return.
    local retcode, err = loadstring("return " .. line)
    if not err then
      code = retcode
    end
  end
  
  -- if valid code
  if code ~= nil then
    -- Set return icon at the last line
    if autoprint then
      self.line_icons[self.last_line] = "return"
    end
    
    -- Add command to history.
    self:add_to_history()
    
    -- Execute the code.
    setfenv(code, self.environment)
    local success, results = get_results(pcall(code))
    
    -- If unsuccessful, print error
    if not success then
      self:print_error(results[1])
    elseif results[1] then -- If there are results
      -- Set return icon to the last line
      self.line_icons[self.last_line] = "return"
      -- Print results
      self.print(" ", unpack(results))
    end
  -- Else if incomplete
  elseif error_msg:sub(-7) == "'<eof>'" then
    self:reset_temp_history()
  -- Else
  else
    -- Add command to history.
    self:add_to_history()
    -- Print error.
    self:print_error(error_msg)
  end
end

-- Adds the current command to the history and reset the temporary history.
function console:add_to_history()
  -- Add to history.
  table.insert(self.history, self.current_command)
  if #self.history > self.history_capacity then
    table.remove(self.history, 1)
  end
  self.history_is_saved = true
  
  -- Clear current command.
  self.current_command = {}
  
  -- Reset temporary history.
  self:reset_temp_history()
end

-- Reset the temporary history.
function console:reset_temp_history()
  -- Reset temp history.
  self.temp_history = {}
  for _, line in pairs(self.history) do
    local l = {}
    for _, character in pairs(line) do
      table.insert(l, character)
    end
    table.insert(self.temp_history, l)
  end
  
  -- Add new empty line and set as current line.
  table.insert(self.temp_history, {})
  self.history_position = #self.temp_history
  self.cursor = 0
  self.selection = 0
end

-- Moves the history.
function console:shift_history(shift)
  -- Move history to temporary history.
  self.history_position = math.min(math.max(self.history_position + shift, 1), #self.temp_history)
  self.cursor = #self:get_current_line()
  self.selection = self.cursor
  
  -- Redraw current line.
  self:draw_line()
  
  -- Reset cursor sprite.
  self.cursor_sprite:set_frame(0)
end

-- Loads the history from the write directory.
function console:load_history()
  local history = sol.main.load_file(self.history_filename)
  
  if type(history) == "function" then
    self.history = history() or {}
    self:reset_temp_history()
  end
end

-- Saves the history into the write directory.
function console:save_history()
  if self.history_is_saved then
    return
  end
  
  local file = sol.file.open(self.history_filename, "w")
  if file == nil then
    return
  end
  
  file:write("return {\n")
  -- Loop through all the characters and write to a file.
  -- Special character cases for quotes and newlines 
  -- and backslashes are handled below.
  for _, command in pairs(self.history) do
    file:write("  {")
    for _, character in pairs(command) do
      file:write("\"")
      if character == "\"" then
        file:write("\\\"")
      elseif character == "\n" then
        file:write("\\n")
      elseif character == "\\" then
        file:write("\\\\")
      else
        file:write(character)
      end
      file:write("\",")
    end
    file:write("},\n")
  end
  file:write("}")
    
  file:close()
  self.history_is_saved = true
end

-- Copies the current selection to the clipboard.
function console:copy_to_clipboard()
  if self.cursor == self.selection then
    return false
  end
  
  local line = self:get_current_line()
  local cursor = math.min(self.cursor, self.selection)
  local selection = math.max(self.cursor, self.selection)
  
  if not self.clipboard then
    self.clipboard = {}
  end
  for i = cursor + 1, selection do
    table.insert(self.clipboard, line[i])
  end
  
  -- Reset cursor position.
  self.cursor_sprite:set_frame(0)
  
  return true
end

-- Paste the clipboard onto the current selection.
function console:paste_from_clipboard()
  if self.clipboard > 0 then
    self:add_chars(self.clipboard)
  end
end

-- Event called when console is started.
function console:on_started()
  self.enabled = true
end

-- Event called when console is stopped.
function console:on_finished()
  self.enabled = false
  console:save_history()
end

-- Event called when user presses a key while console is active.
function console:on_key_pressed(key, modifiers)
  if key == "escape" then
    sol.menu.stop(console)
  elseif key == "backspace" then
    console:remove_chars()
  elseif key == "delete" then
    console:remove_chars(true)
  elseif key == "return" or key == "kp return" then
    if modifiers.control then
      console:add_chars({"\n"})
    else
      console:add_line()
    end
  elseif key == "left" then
    console:shift_cursor(-1, modifiers.shift)
  elseif key == "right" then
    console:shift_cursor(1, modifiers.shift)
  elseif key == "home" then
    self.cursor = 0
    if not modifiers.shift then
      self.selection = 0
    end
  elseif key == "end" then
    self.cursor = #console:get_current_line()
    if not modifiers.shift then
      self.selection = self.cursor
    end
    -- Redraw selection surface.
    console:draw_selection()
  elseif key == "up" then
    console:shift_history(-1)
  elseif key == "down" then
    console:shift_history(1)
  elseif modifiers.control then
    if key == "x" then
      -- Cut selection.
      if console:copy_to_clipboard() then
        console:remove_chars()
      end
    elseif key == "c" then
      -- Copy selection.
      console:copy_to_clipboard()
    elseif key == "v" then
      -- Paste clipboard.
      console:paste_from_clipboard()
    end
  end
end

-- Event called when user enters text while console is the active menu.
function console:on_character_pressed(character)
  local handled = false
  if not character:find("%c") then
    console:add_chars({character})
    handled = true
  end
  
  return handled
end

-- Event called to draw the console.
function console:on_draw(surface)
  local origin = self.margin + self.padding
  
  -- Draw main surface.
  self.main_surface:draw(surface, self.margin, self.margin + self.height_offset)
  
  -- Draw text surfaces.
  local x = origin
  local y = x + self.height_offset
  
  for i=1, #self.line_surfaces do
    local line_surface = self.line_surfaces[i]
    local line_icon = self.line_icons[i]
    
    --[[if line_icon ~= "" then
      self.icons_sprite:set_animation(line_icon)
      local icon_y = y + math.floor((self.line_height - self.icons_sprite_height) / 2)
      self.icons_sprite:draw(surface, origin, icon_y)
      end
    --]]
    
    line_surface:draw(surface, x, y)
    y = y + self.line_height
  end
  
  -- Draw selection.
  self.selection_surface:draw(surface)
  
  -- Draw cursor sprite.
  local line, character = console:get_cursor_position(self.cursor)
  x = origin + ((character - 1) * self.char_width + self.char_width)
  y = origin + ((line - 1) * self.line_height) + math.floor((self.line_height - self.cursor_sprite_height) / 2) + self.height_offset
  self.cursor_sprite:draw(surface, x, y)
end

console:initialize()
return console