-- This script manages scene menus,
-- usually for pre-gameplay.

-- Load the starting scene menus.
local starting_scenes = require("scripts/menus/starting_scenes_config")

-- Make scene menu manager.
local scene_manager = {}
local scene_list = {size = 0}

-- Initialize all scene menus.
function scene_manager:initialize(scenes)
  local scenes = scenes or starting_scenes

  -- Exit if no scenes are in the starting list.
  if #scenes == 0 then
    return
  end

  -- Add in scene menus.
  for _, scene in ipairs(scenes) do
    scene_list:add(scene)
  end
end

-- Add scene menus to list.
function scene_list:add(scene)
  scene_list.size = scene_list.size + 1
  scene_list[scene_list.size] = scene
end

-- Play scenes.
function scene_manager:play()

  -- Start first from the scene list.
  sol.menu.start(sol.main, scene_list[1])
  for i, scene in ipairs(scene_list) do
    scene.on_finished = function()
      local next_scene = scene_list[i+1]
      if next_scene ~= nil then
        sol.menu.start(sol.main, next_scene)
      end
    end
  end
end

return scene_manager