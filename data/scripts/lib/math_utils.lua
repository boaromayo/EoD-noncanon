-- This script adds math-based utility features.
local math_utils = {}

-- Checks a value between min and max bounds. If outside those bounds, set to min or max.
-- value - value to check
-- min - minimum bounds
-- max - maximum bounds
function math_utils.clamp(value, min, max)
  return value < min and min or (value > max and max or value)
end

-- Rounds a value up to the nearest tenth decimal place.
-- value - value to check
function math_utils.round(value)
  return math.floor((value + 5) / 10)
end

return math_utils