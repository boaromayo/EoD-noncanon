-- This script is to set default settings for the
-- font and language used in-game.

-- Based on the language manager script
-- created by Christopho, under the terms
-- of the General Public License (GPL).

-- To add this script in the game, call:
-- require("scripts/language_manager")

local language_manager = {}

local default_language = "en"

-- Returns the default language ID used in-game.
function language_manager:get_default_language()
  return default_language
end

-- Returns the font and font size used as the
-- default text.
function language_manager:get_default_font()
  return "lunchds", 16
end

-- Returns the font and font size used for 
-- dialogue text.
function language_manager:get_text_font()
  return "lunchds_blue"
end

-- Returns the font and font size used for 
-- in-game menu text.
function language_manager:get_menu_font()
  return "lunchds_blue"
end

-- Returns the font used for small text and 
-- debug console.
function language_manager:get_small_font()
  return "8_bit", 12
end

return language_manager