-- This feature adds new features for items.

-- The following features will be added:

--  item:set_is_weapon()
--  item:is_weapon()
--  item:has_weapon()
--  item:set_indestructible()
--  item:is_indestructible()
--  item:set_max_weapon_life()
--  item:get_max_weapon_life()
--  item:set_weapon_life()
--  item:get_weapon_life()
--  item:add_weapon_life()
--  item:remove_weapon_life()
--  item:is_weapon_broken()
--  item:is_damaged()
--  item:set_attack()
--  item:get_attack()

-- The following features will be overrided:

--  N/A

local item_meta = sol.main.get_metatable("item")

-- Make this item into a weapon.
function item_meta:set_is_weapon(weapon)
  self.weapon = weapon or false
end

-- Check if item is a weapon.
function item_meta:is_weapon()
  return self.weapon or false
end

-- Check if player has the selected weapon.
function item_meta:has_weapon()
  return self:is_weapon() and self:has_variant()
end

-- Set whether weapon can be damaged or not.
-- No value means false.
function item_meta:set_indestructible(indestructible)
  self.indestructible = indestructible or false
end

-- Check if weapon cannot be damaged.
function item_meta:is_indestructible()
  return self.indestructible == true or false
end

-- Set weapon's maximum life.
function item_meta:set_max_weapon_life(amount)
  local item = self
  if not item:is_indestructible() 
      and item:is_weapon() then
    item:set_amount_savegame_variable("weapon_life")
    if amount <= 0 then
      return
    end
    item:set_max_amount(amount)
  end
end
  
-- Get weapon's maximum life.
function item_meta:get_max_weapon_life()
  return self:is_weapon() and self:get_max_amount() or 0
end

-- Set weapon's life to a certain amount.
function item_meta:set_weapon_life(amount)
  local item = self
  if item:is_weapon() then
    local max_life = item:get_max_weapon_life()
  
    if amount > max_life then
      item:set_amount(max_life)
    elseif amount < 0 then
      item:set_amount(0)
    else
      item:set_amount(amount)
    end
  end
end

-- Get weapon's current life.
function item_meta:get_weapon_life()
  return self:is_weapon() and self:get_amount() or 0
end

-- Repair weapon.
function item_meta:add_weapon_life(amount)
  self:set_weapon_life(self:get_weapon_life() + amount)
end

-- Damage weapon (ie. when weapon
-- makes contact with enemy, or an object).
function item_meta:remove_weapon_life(amount)
  -- Do not call when negative damage is taken.
  if amount < 0 then
    return
  end
  self:set_weapon_life(self:get_weapon_life() - amount)
end

-- Check if weapon is broken.
function item_meta:is_weapon_broken()
  return self:is_weapon() and self:get_weapon_life() == 0 or false
end

-- Check if weapon damaged.
function item_meta:is_damaged()
  return self:get_weapon_life() ~= self:get_max_weapon_life()
end

-- Set attack power to weapon.
function item_meta:set_attack(atk)
  if self:is_weapon() then
    self.attack = atk or 0
  end
end

-- Get attack power of weapon.
function item_meta:get_attack()
  return self:is_weapon() and self.attack or 0
end

return true