-- This feature states additional enemy features.

-- The following features will be added:

--  enemy:set_attack_consequence()
--  enemy:set_defense()
--  enemy:get_defense()
--  enemy:calculate_damage()
--  enemy:fall()
--  enemy:check_falling_behavior()

local enemy_meta = sol.main.get_metatable("enemy")

-- This helper function is used for versions earlier than
-- Solarus 1.6. This also modifies the built-in 
-- set_attack_consequence() method by adding more enemy reactions
-- from the hero's weapon.
function enemy_meta:set_attack_consequence(attack, reaction)
  local enemy = self
  
  if type(reaction) == "number" then
    local damage = enemy:calculate_damage(reaction)
    if damage == 0 then
      enemy:set_attack_consequence(attack, "protected")
    end
    enemy:hurt(damage)
  elseif reaction == "immobilized" then
    enemy:immobilize()
  elseif reaction == "protected" then
    audio_manager:play_sound("sword_tapping")
  elseif reaction == "custom" then
    enemy:on_custom_attack_received(attack)
  end
  -- TODO: Keep here until better way of enemy attack responses are found.
  enemy:check_falling_behavior()
end

-- This function sets the enemy's defense value.
function enemy_meta:set_defense(def)
  self.defense = def or 0
end

-- This function gets the enemy's defense value.
function enemy_meta:get_defense()
  return self.defense or 0
end

-- Calculate final damage based on enemy stats.
function enemy_meta:calculate_damage(damage)
  local final_dmg = damage - self:get_defense()
  
  if damage < 1 then
    final_dmg = 0
  end
  
  return final_dmg
end

-- Perform enemy falling process.
function enemy_meta:fall()
  local enemy = self
  local enemy_sprite = enemy:get_sprite()
  
  audio_manager:play_sound("falling")
  if enemy_sprite:has_animation("falling") then
    enemy_sprite:set_animation("falling", function()
      enemy:remove()
    end)
  else
    enemy:remove()
  end
end

-- Check whether enemy has the conditions to 
-- fall into a hole.
function enemy_meta:check_falling_behavior()
  local enemy = self
  local behavior = enemy:get_obstacle_behavior()
  local ground = enemy:get_ground_below()
  
  -- If enemy is on top of a hole, 
  -- and its behavior is normal, then fall...
  if ground == "hole" and behavior == "normal" then
    enemy:fall()
  end
end