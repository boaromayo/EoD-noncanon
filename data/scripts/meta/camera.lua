-- This feature adds features to the game's camera.

-- The following features will be added:

--  camera:shake()
--  camera:pan_to()
--  camera:pan_to_entity()
--  camera:pan_to_hero()

local camera_meta = sol.main.get_metatable("camera")

-- Shake camera.
function camera_meta:shake(config, callback)
  -- Configuration needed, or shake will not execute.
  if config == nil then
    return
  end

  local camera = self
  local shake_frame = 300 -- time per one "shake"
  local angles = { 0, math.pi / 2, math.pi, 3 * math.pi / 2 }

  local amplitude = config.amplitude or 3
  local speed = config.speed or 40
  local shake_count = config.shake_count or 2
  local duration = config.duration or (shake_count * shake_frame)
  local direction = config.direction or "horizontal"

  -- Shaking flag, and previous angle stored.
  local is_shaking = false
  local prev_angle = direction == "vertical" and angles[2] or angles[1]

  local movement
  
  -- Start camera shake.
  local function shake_count()
    -- Setup movement for camera shake.
    movement = sol.movement.create("straight")
    movement:set_speed(speed)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)
    movement:set_max_distance(amplitude)
    movement:set_angle(prev_angle)

    -- Switch to opposite angle based on shaking direction.
    if direction == "vertical" then
      prev_angle = prev_angle == angles[2] and angles[4] or angles[2]
    else
      prev_angle = prev_angle == angles[1] and angles[3] or angles[1]
    end

    if not is_shaking then
      is_shaking = true
    end

    -- Execute shake.
    movement:start(camera, function()
      -- Keep shaking until duration passed.
      if is_shaking then
        shake_count()
      end
    end)

    -- Once finished, pan back to hero.
    sol.timer.start(camera, duration, function()
      local hero = camera:get_map():get_hero()
      is_shaking = false
      movement:stop()
      camera:start_tracking(hero)
      if callback then
        callback()
      end
    end)
  end

  shake_count() -- Start shake effect.
end

-- Pan camera to a certain position.
function camera_meta:pan_to(x, y, speed, callback)
  local camera = self
  local speed = speed or 100

  local movement = sol.movement.create("target")
  movement:set_target(camera:get_position_to_track(x, y))
  movement:set_speed(speed)
  movement:set_ignore_obstacles(true)
  movement:set_smooth(false)
  movement:start(camera, function()
    if callback then
      callback()
    end
  end)
end

-- Pan camera to an in-map entity.
function camera_meta:pan_to_entity(entity, speed, callback)
  local camera = self
  local x, y, _ = entity:get_position()
  camera:pan_to(x, y, speed, callback)
end

-- Pan camera to hero. Works properly if camera's current focus is on anything but the hero.
function camera_meta:pan_to_hero(hero, speed)
  local camera = self
  local game = camera:get_game()

  -- Custom camera movement going back to hero.
  local movement = sol.movement.create("target")
  movement:set_target(camera:get_position_to_track(hero))
  movement:set_speed(speed)
  movement:set_ignore_obstacles(true)
  movement:set_smooth(false)
  movement:start(camera, function()
    camera:start_tracking(hero)
    hero:unfreeze()
    if game:is_suspended() then
      game:set_suspended(false)
    end
  end)
end