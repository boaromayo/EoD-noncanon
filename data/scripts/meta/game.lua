-- This file adds game features to this quest.

-- The following features will be added:

--  game:start_commentary()
--  game:set_easing()
--  game:run_chronometer()
--  game:get_time_played()
--  game:get_time_played_string()
--  game:is_dungeon()
--  game:has_weapon()
--  game:has_subweapon()
--  game:set_attack()
--  game:set_defense()
--  game:set_base_attack()
--  game:set_base_defense()
--  game:add_base_attack()
--  game:add_base_defense()
--  game:take_base_attack()
--  game:take_base_defense()
--  game:set_attack_abs()
--  game:set_defense_abs()
--  game:set_window_style()
--  game:set_main_story()
--  game:get_attack()
--  game:get_defense()
--  game:get_base_attack()
--  game:get_base_defense()
--  game:get_window_style()
--  game:main_story()
--  game:main_story_at()
--  game:main_story_at_least()
--  game:main_story_at_most()
--  game:main_story_greater_than()
--  game:main_story_less_than()
--  game:update_main_story()
--  game:can_move_object()
--  game:can_lift()
--  game:cannot_swim()

-- The following functions will be overrided:

-- game:on_command_pressed()
-- game:on_map_changed()
-- game:set_item_assigned()

require("scripts/multi_events")
local game_meta = sol.main.get_metatable("game")

-- This function sets an easing value for animation.
-- Taken from Robert Penner's easing equations.

------------------------------------------------------------------------------
-- Copyright (c) 2001 Robert Penner
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without 
-- modification, are permitted provided that the following conditions are met:
-- * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
-- * Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- * Neither the name of the author nor the names of contributors may be used
--    to endorse or promote products derived from this software without
--    specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES: LOSS OF USE, DATA, OR PROFITS: OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
------------------------------------------------------------------------------

-- t - current time
-- b - beginning or start of animation
-- c - change = (end - beginning) of tween
-- d - duration (how long) of the tween
-- p - the exponent used to determine type of easing 
-- (i.e., 1 = linear, 2 = quad, 3 = cubic, 4 = quartic, 5 = strong)
function game_meta:set_easing(t, b, c, d, p)
  local object = {}
  setmetatable(object, self)
  self.__index = {p=1}
  
  return c * math.pow(t / d, p) + b
end

-- Call game developer commentary dialog. Only trigger this if
-- "dev commentary" is enabled in the menu. It is set to false by
-- default.
function game_meta:start_commentary(dialog)
  local game = self

  if game:get_value("dev_commentary") then
    game:start_dialog("_dev." .. dialog)
  end
end

-- Override commands pressed here. For instance, have "attack" or "sword"
-- command call "item_1" command, or the "weapon" command, instead.
function game_meta:on_command_pressed(command)
  if command == "attack" then
    command = "item_1"
    return true
  end
end

-- Chronometer functions here.
-- Start running game time here.
function game_meta:run_chronometer()
  local game = self
  local timer = sol.timer.start(game, 100, function()
    local time = game:get_value("time_played") or 0
    time = time + 100
    game:set_value("time_played", time)
    return true -- Repeat the timer.
  end)
  timer:set_suspended_with_map(false)
end

-- Fetch the current game time.
function game_meta:get_time_played()
  local game = self
  local milliseconds = game:get_value("time_played") or 0
  local total_seconds = math.floor(milliseconds / 1000)
  return total_seconds
end

-- Get the string of the current game time.
function game_meta:get_time_played_string()
  local total_seconds = self:get_time_played()
  local hours = math.floor(math.floor(total_seconds / 60) / 60)
  local minutes = math.floor(total_seconds / 60) % 60
  local seconds = total_seconds % 60
  local time_string = string.format("%02d:%02d:%02d", hours, minutes, seconds)
  return time_string
end

-- TODO: Move this to another file.
-- Check if this world is a dungeon.
--[[function game_meta:is_dungeon()
  local world = self:get_map():get_world()
  if world == nil then
    return false
  end
  local dungeon_match = world:match("^(temple)(manor)(lighthouse)(volcano)") 
  return dungeon_match
end--]]

-- Check at least one weapon is in player possession.
function game_meta:has_weapon()
  local weapons = { "stick", "sword", "sharptongue", "lightbrand", "fire_cane", "ice_rod", "bolt_wand", "seal_staff" }
  for _, weapon in ipairs(weapons) do
    if self:has_item(weapon) then
      return true
    end
  end
end

-- Check at least one subweapon is in player possession
-- (for now, check if player has knife or cross).
function game_meta:has_subweapon()
  return self:has_item("knife") or self:has_item("cross")
end

-- TODO: Move attack and defense stats to hero meta script.
-- Set status here.
function game_meta:set_attack(atk)
  if self:get_value("base_atk_power") and
      self:get_value("base_atk_power") > 0 then
    atk = self:get_base_attack() + atk
  else
    self:set_value("base_atk_power", atk)
    atk = self:get_base_attack()
  end
  self:set_value("attack_power", atk)
end

function game_meta:set_defense(def)
  if self:get_value("base_def_power") and
      self:get_value("base_def_power") > 0 then
    def = self:get_base_defense() + def
  else
    self:set_value("base_def_power", def)
    def = self:get_base_defense()
  end
  self:set_value("defense_power", def)
end

function game_meta:set_base_attack(atk)
  local old_base_atk = self:get_base_attack()
  local old_atk = self:get_attack()
  self:set_value("base_atk_power", atk)
  self:set_value("attack_power", old_atk + atk - old_base_atk)
end

function game_meta:set_base_defense(def)
  local old_base_def = self:get_base_defense()
  local old_def = self:get_defense()
  self:set_value("base_def_power", def)
  self:set_value("defense_power", old_def + def - old_base_def)
end

function game_meta:add_base_attack(atk)
  self:set_base_attack(self:get_base_attack() + atk)
end

function game_meta:add_base_defense(def)
  self:set_base_defense(self:get_base_defense() + def)
end

function game_meta:take_base_attack(atk)
  self:set_base_attack(self:get_base_attack() - atk)
end

function game_meta:take_base_defense(def)
  self:set_base_defense(self:get_base_defense() - def)
end

-- Set absolute attack power, ignoring base attack.
function game_meta:set_attack_abs(atk)
  self:set_value("attack_power", atk)
end

-- Set absolute defense power, ignoring base defense.
function game_meta:set_defense_abs(def)
  self:set_value("defense_power", def)
end

-- Set game configuration.
function game_meta:set_window_style(style)
  self:set_value("window_style", style)
end

-- Set main story variable.
function game_meta:set_main_story(var)
  self:set_value("main_story", var)
end

-- Update main story. Useful for when current map
-- need to be refreshed after main story change.
function game_meta:update_main_story(var)
  local game = self
  local map = game:get_map()

  game:set_main_story(var)
  if map and map.initialize_music then
    map:initialize_music()
  end
  if map and map.initialize_npc_behavior then
    map:initialize_npc_behavior()
  end
end

-- Return total attack power (base attack + weapon).
function game_meta:get_attack()
  return self:get_value("attack_power") or 0
end

-- Return total defense power (base defense + armor).
function game_meta:get_defense()
  return self:get_value("defense_power") or 0
end

-- Return hero's base attack power.
function game_meta:get_base_attack()
  return self:get_value("base_atk_power") or 0
end

-- Return hero's base defense power.
function game_meta:get_base_defense()
  return self:get_value("base_def_power") or 0
end

-- Fetch window style.
function game_meta:get_window_style()
  return self:get_value("window_style") or 1
end

-- Fetch main story variable. 
-- Alias function to main_story_at_least().
function game_meta:main_story(value)
  return self:main_story_at_least(value) or false
end

-- Fetch main story when
-- exactly this value.
function game_meta:main_story_at(value)
  return self:get_value("main_story") == value or false
end

-- Fetch if main story is
-- greater than or equal to this value.
function game_meta:main_story_at_least(value)
  return self:get_value("main_story") >= value or false
end

-- Fetch if main story is
-- less than or equal to this value.
function game_meta:main_story_at_most(value)
  return self:get_value("main_story") <= value or false
end

-- Fetch if main story is
-- greater than this value.
function game_meta:main_story_greater_than(value)
  return self:get_value("main_story") > value or false
end

-- Fetch if main story is
-- less than this value.
function game_meta:main_story_less_than(value)
  return self:get_value("main_story") < value or false
end

-- Check if player is able to move
-- a heavy object at least up to a certain level.
function game_meta:can_move_object(level)
  return game:get_ability("pull") == level 
    and game:get_ability("push") == level
end

-- Check if player is able to lift 
-- at least up to a certain level.
function game_meta:can_lift(level)
  return game:get_ability("lift") == level
end

-- Check if player cannot swim.
function game_meta:cannot_swim()
  return game:get_ability("swim") == 0
end

return true