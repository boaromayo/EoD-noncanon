-- This file adds features to destructible entities.

-- The following features will be modified:

-- destructible:on_created()

require("scripts/multi_events")
local destructible_meta = sol.main.get_metatable("destructible")

-- For bushes and grass, be cut by default.
-- For bushes and vases, set default destruction sound effect.
destructible_meta:register_event("on_created", function(destructible)
  -- Get sprite source names first.
  local name = destructible:get_sprite():get_animation_set()

  if name:match("bush") or name:match("grass") then
    destructible:set_can_be_cut(true)
  end
  
  if name:match("bush") then
    destructible:set_destruction_sound("walk_on_grass")
  end
  if name:match("vase") then
    destructible:set_destruction_sound("falling_rock")
  end
end)

return true
