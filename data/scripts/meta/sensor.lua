-- This file adds features to sensors to this quest.

-- The following functions/events will be overrided:

--  sensor:on_activated()

local sensor_meta = sol.main.get_metatable("sensor")

local audio_manager = require("scripts/audio_manager")

-- Trigger event based on the name of the sensor.
function sensor_meta:on_activated()
  local game = self:get_game()
  local map = self:get_map()
  local hero = map:get_hero()
  local name = self:get_name() or ""
  
  -- When no name found for sensor, get out of event. This is to prevent
  -- errors from printing.
  if name == nil then
    return
  end

  if name:match("sensor_save_solid_ground") then
    hero:save_solid_ground()
    return
  end
  if name:match("sensor_reset_solid_ground") then
    hero:reset_solid_ground()
    return
  end
  if name:match("^sensor_layer_up") then
    debug_print("layer up 1")
    local x, y, layer = hero:get_position()
    if layer < map:get_max_layer() then
      hero:set_position(x, y, layer + 1)
    end
    return
  elseif name:match("^sensor_layer_down") then
    debug_print("layer down 1")
    local x, y, layer = hero:get_position()
    if layer > map:get_min_layer() then
      hero:set_position(x, y, layer - 1)
    end
    return
  end
end

return true