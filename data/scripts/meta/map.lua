-- This file provides additional map features for the game.

-- The following features will be added:

--  map:set_sideview()
--  map:is_sideview()
--  map:countdown()

-- The following features will be overrided:

--  N/A

-- The following events will be aliased:

--  map:on_started()

require("scripts/debug_manager")

local map_meta = sol.main.get_metatable("map")

-- Set current map to a sideview map.
function map_meta:set_sideview(sideview)
  local map = self
  map.sideview = sideview or false
end

-- Check if current map is a sideview.
function map_meta:is_sideview()
  local map = self
  return map.sideview or false
end

-- Setup timer to countdown to a callback function.
function map_meta:countdown(countdown, callback)
  local map = self
  sol.timer.start(map, countdown, function()
    callback()
  end)
end

-- Alias on_started event to add features without overriding event.
map_meta:register_event("on_started", function(map)
  local game = map:get_game()
  local hero = map:get_hero()

  -- Check for any tiles marked "invisible" and hide them.
  for tile in map:get_entities("invisible") do
    tile:set_visible(false)
  end

  -- Check for sensors.
  -- Save solid ground position.
  for sensor in map:get_entities("sensor_save_solid_ground") do
    function sensor:on_activated()
      hero:save_solid_ground()
    end
  end

  -- reset hero solid ground position.
  for sensor in map:get_entities("sensor_reset_solid_ground") do
    function sensor:on_activated()
      hero:reset_solid_ground()
    end
  end

  -- Layer up.
  local function layer_up_sensor_on_activated(sensor)
    local x, y, layer = hero:get_position()
    hero:set_position(x, y, layer + 1)
  end

  for sensor in map:get_entities("^sensor_layer_up") do
    sensor.on_activated = layer_up_sensor_on_activated
  end

  -- Layer down.
  local function layer_down_sensor_on_activated(sensor)
    local x, y, layer = hero:get_position()
    hero:set_position(x, y, layer - 1)
  end

  for sensor in map:get_entities("^sensor_layer_down") do
    sensor.on_activated = layer_down_sensor_on_activated
  end

end)

return true