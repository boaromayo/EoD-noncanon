-- The Lua script initializes game variables for a new game.
-- This overrides the game_manager's initialize_new_savegame() function
-- as a way to set the starting location and stats for the player.

local initializer = {}

-- Include in-game assets.
function initializer:initialize(game)
  sol.main.load_file("scripts/menus/dialog_box")(game)
  sol.main.load_file("scripts/hud/hud")(game)
  sol.main.load_file("scripts/menus/game_over")(game)
  sol.main.load_file("scripts/menus/pause")(game)
end

-- Sets the initial values for the new game.
function initializer:initialize_new_savegame(game)
  game:set_max_life(10)
  game:set_life(game:get_max_life())
  game:set_ability("lift", 1) -- Set lift ability only.
  game:set_window_style(1) -- Set to default window style.
  game:set_main_story(0) -- Start main story.
  game:set_attack(3) -- Set attack and defense stats.
  game:set_defense(1)
end

function initializer:initialize_demo_stats(game, story)
  game:set_max_life(10)
  game:set_life(game:get_max_life())
  game:set_ability("lift", 1) -- Start with lift ability only.
  game:set_main_story(story) -- Start main story.
  game:set_attack(1) -- Set attack and defense stats.
  game:set_defense(1)
  game:set_value("time_played", 0) -- Set in-game timer.
  
  -- For demo stages after prison ship, give Den weapon and some items.
  if game:main_story_greater_than(4) then
    game:enable_pause_menu()
    game:get_item("wallet"):set_variant(1)

    if game:main_story(7) then
      local sword = game:get_item("sword")
      sword:set_variant(1)
      game:set_item_assigned(1, sword)
      game:set_attack(sword:get_attack())
    end
  end
end

-- Set initial values for demo.
function initializer:initialize_demo(game, cursor)
  if cursor == 1 then
    self:initialize_demo_stats(game, 0)
  elseif cursor == 2 then
    self:initialize_demo_stats(game, 6)
  elseif cursor > 2 then
    self:initialize_demo_stats(game, 7)
  end
end

return initializer