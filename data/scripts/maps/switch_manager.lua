-- This feature is used to check
-- whether switches in a world were activated. 

-- Based on the switch manager script
-- by Christopho and binbin.

-- To add this script in the game, call:
-- require("scripts/maps/switch_manager")

local switch_manager = {}

-- Activate switch(es) if savegame variable exists.
function switch_manager:activate_switch_if_savegame_exist(map, savegame, switches)
  local game = map:get_game()
  for switch in map:get_entities(switches) do
    if switch and savegame and
      game:get_value(savegame) == true then
      switch:set_activated(true)
    end
  end
end

-- Set savegame variable if single switch activated.
function switch_manager:set_savegame_if_switch_activated(map, savegame, switch_name)
  local game = map:get_game()
  local switch = map:get_entity(switch_name)
  if switch and switch.is_activated
      and not game:get_value(savegame) then
    game:set_value(savegame, true)
  end
end

return switch_manager