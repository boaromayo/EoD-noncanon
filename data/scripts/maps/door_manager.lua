-- This feature is used in dungeons to check
-- whether door(s) in a room should be opened based on the
-- player fulfilling certain conditions (ie.,
-- destroyed enemies, activated switches, lit torches). 
-- It also checks whether doors should be closed 
-- based on actions the player has not done.

-- Based on the door manager script
-- by Christopho and binbin.

-- To add this script in the game, call:
-- require("scripts/maps/door_manager")

local door_manager = {}
local audio_manager = require("scripts/audio_manager")

-- Open the doors in the room when all enemies are defeated.
function door_manager:open_doors_enemy_dead(map, enemies, doors, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil

  local function enemy_on_dead()
    if not map:has_entities(enemies) then
      sol.timer.start(map, delay, function()
        local hero = map:get_hero()
        hero:freeze()
        if sfx then
          audio_manager:play_sound(sfx)
        end
        sol.timer.start(map, delay, function()
          map:open_doors(doors)
          hero:unfreeze()
        end)
      end)
    end
  end
  
  for enemy in map:get_entities(enemies) do
    enemy.on_dead = enemy_on_dead
  end
end

-- Close the doors in the room when all enemies are alive.
-- This script applies to major enemies (sub-bosses/bosses) too.
function door_manager:close_doors_enemy_not_dead(map, enemies, doors)
  if map:has_entities(enemies) then
    map:close_doors(doors)
  end
end

-- Open doors in a room when a major enemy (sub-boss/boss) is dead.
function door_manager:open_doors_boss_dead(map, doors, boss_variable)
  local game = map:get_game()
  if game:get_value(boss_variable) == true then
    map:open_doors(doors)
  end
end

-- Open doors in a room when a block is moved.
function door_manager:open_doors_block_moved(map, blocks, doors, delay, sfx)
  local delay = delay or nil
  local sfx = sfx or nil
  
  local function block_on_moved(block)
    if not block.is_moved then
      block.is_moved = true
      local hero = map:get_hero()
      if delay then
        hero:freeze()
      end
      map:open_doors(doors)
      if sfx then
        audio_manager:play_sound(sfx)
      end
      if delay then
        sol.timer.start(map, delay, function()
          hero:unfreeze()
        end)
      end
    end
  end
    
  for block in map:get_entities(blocks) do
    block.is_moved = false
    block.on_moved = block_on_moved
  end
end

-- Check if a block has been moved, and open doors if it has.
function door_manager:open_doors_if_block_moved(map, blocks, doors)
  for block in map:get_entities(blocks) do
    if block.is_moved then
      map:open_doors(doors)
    else
      map:close_doors(doors)
    end 
  end
end

-- Open doors in a room when a switch is activated.
function door_manager:open_doors_switch_activated(map, switches, doors, delay, sfx)
  local delay = delay or nil
  local sfx = sfx or nil

  local function switch_on_activated(switch)
    if not switch.is_activated then
      switch.is_activated = true
      local hero = map:get_hero()
      if delay then
        hero:freeze()
      end
      map:open_doors(doors)
      if sfx then
        audio_manager:play_sound(sfx)
      end
      if delay then
        sol.timer.start(map, delay, function()
          hero:unfreeze()
        end)
      end
    end
  end
  
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    switch.on_activated = switch_on_activated
  end
end

-- Open doors in a room when switches are activated.
function door_manager:open_doors_switches_activated(map, switches, doors, delay, sfx)
  local delay = delay or 500
  local sfx = sfx or nil

  local switches_left = 0
  local function switch_on_activated(switch)
    if not switch.is_activated then
      switch.is_activated = true
      switches_left = switches_left - 1
      if switches_left == 0 then
        local hero = map:get_hero()
        if delay then
          hero:freeze()
        end
        map:open_doors(doors)
        if sfx then
          audio_manager:play_sound(sfx)
        end
        if delay then
          sol.timer.start(map, delay, function()
            hero:unfreeze()
          end)
        end
      end
    end
  end

  local switches_here = false
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    if not switch.is_activated then
      switches_left = switches_left + 1
    end
    switch.on_activated = switch_on_activated
    switches_here = true
  end

  -- Check if switches activated, then play sound again, if available.
  if switches_here and switches_left == 0 then
    if sfx then
      audio_manager:play_sound(sfx)
    end
  end
end

-- Set doors open quietly if a savegame exists.
function door_manager:open_doors_if_savegame_exist(map, savegame, doors)
  local game = map:get_game()
  if savegame and game:get_value(savegame) == true then
    map:set_doors_open(doors, true)
  end
end

-- Destroy a weak wall in a room.
function door_manager:destroy_wall(map, weak_wall, sfx)
  local sfx = sfx or nil
  
  map:remove_entities(weak_wall)
  if sfx then
    audio_manager:play_sound(sfx)
  end
end

-- Check if weak wall was destroyed.
function door_manager:check_wall_if_destroyed(map, weak_wall, wall_variable)
  local game = map:get_game()
  if game:get_value(wall_variable) == true then
    map:remove_entities(weak_wall)
  end
end

-- Open doors in a room when torches are lit.
function door_manager:open_doors_torches_lit(map, torches, doors, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local torches_left = 0
  
  local function torch_on_lit()
    local doors_closed = false
    for door in map:get_entities(doors) do
      if door:is_closed() then
        doors_closed = true
      end
    end
    if doors_closed then
      torches_left = torches_left - 1
      if torches_left == 0 then
        local hero = map:get_hero()
        hero:freeze()
        map:open_doors(doors)
        sol.timer.delay(map, delay, function()
          if sfx then
            audio_manager:play_sound(sfx)
          end
        end)
      end
    end
  end
  
  -- Fetch all torches with a certain prefix.
  for torch in map:get_entities(torches) do
    if torch.is_lit and not torch:is_lit() then
      torches_left = torches_left + 1
    end
    torch.on_lit = torch_on_lit
    -- Check if all torches are all lit, then reopen door if they are.
    if torches_left == 0 then
      map:open_doors(doors)
      if sfx then
        audio_manager:play_sound(sfx)
      end
    end
  end
end

-- Close doors when torches are unlit.
function door_manager:close_doors_torches_unlit(map, torches, doors)
  local torches_left = 0
  
  local function torch_on_unlit()
    for door in map:get_entities(doors) do
      if door:is_closed() then
        torches_left = torches_left + 1
        if torches_left ~= 0 then
          map:close_doors(doors)
        end
      end
    end
  end
  
  -- Fetch all torches with a certain prefix.
  for torch in map:get_entities(torches) do
    if torch.is_lit and torch:is_lit() then
      torches_left = torches_left - 1
    end
    torch.on_unlit = torch_on_unlit
    -- Close doors if there are lit torches.
    if torches_left ~= 0 then
      map:close_doors(doors)
    end
  end
end

return door_manager