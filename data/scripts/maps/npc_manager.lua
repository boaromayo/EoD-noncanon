-- This features handles NPCs (non-player characters),
-- whether to make them appear or disappear
-- or to have different dialogs
-- based on certain conditions (ie., the story
-- advancing, side quests, player's actions).

-- To add this script in-game, call
-- require("scripts/maps/npc_manager")

local npc_manager = {}

-- Show NPC or NPCs when a savegame activated.
function npc_manager:show_npcs_savegame_exist(map, savegame, npc_prefix)
  local game = map:get_game()
  
  if savegame
      or (savegame and game:get_value(savegame)) then
    for npc in map:get_entities(npc_prefix) do
      self:appear_npc(map, npc)
    end
  end
end

-- Hide NPC or NPCs when a savegame activated.
function npc_manager:hide_npcs_savegame_exist(map, savegame, npc_prefix)
  local game = map:get_game()
  
  if savegame
      or (savegame and game:get_value(savegame)) then
    for npc in map:get_entities(npc_prefix) do
      self:disappear_npc(map, npc)
    end
  end
end

-- Show NPCs when a savegame is at a value.
function npc_manager:show_npcs_savegame_with_value(map, savegame, value, npc_prefix)
  local game = map:get_game()
  
  if game:get_value(savegame) == value then
    for npc in map:get_entities(npc_prefix) do
      self:appear_npc(map, npc)
    end
  end
end

-- Hide NPCs when a savegame is at a value.
function npc_manager:hide_npcs_savegame_with_value(map, savegame, value, npc_prefix)
  local game = map:get_game()
  
  if game:get_value(savegame) == value then
    for npc in map:get_entities(npc_prefix) do
      self:disappear_npc(map, npc)
    end
  end
end

-- Show NPCs based on the progression of the main story.
function npc_manager:show_npcs_main_story(map, value, npc_prefix)
  local game = map:get_game()
  
  if game:main_story(value) then
    for npc in map:get_entities(npc_prefix) do
      self:appear_npc(map, npc)
    end
  end
end

-- Hide NPCs based on the progression of the main story.
function npc_manager:hide_npcs_main_story(map, value, npc_prefix)
  local game = map:get_game()
  
  if game:main_story(value) then
    for npc in map:get_entities(npc_prefix) do
      self:disappear_npc(map, npc)
    end
  end
end

-- Set NPC dialog when a savegame exists. Useful when the player has an item
-- or completes a quest.
function npc_manager:set_dialog_savegame_exist(map, savegame, npc_prefix, dialog, callback)
  local game = map:get_game()
  
  if savegame
      or (savegame and game:get_value(savegame)) then
    local npc = map:get_entity(npc_prefix)
    function npc:on_interaction()
      game:start_dialog(dialog, function()
        if callback then
          callback()
        end
      end)
    end
  end
end

-- Set NPC dialog when a savegame is at a value. Useful when the player has an item
-- or completes a quest.
function npc_manager:set_dialog_with_value(map, savegame, value, npc_prefix, dialog, callback)
  local game = map:get_game()
  
  if game:get_value(savegame) == value then
    local npc = map:get_entity(npc_prefix)
    function npc:on_interaction()
      game:start_dialog(dialog, function()
        if callback then
          callback()
        end
      end)
    end
  end
end

-- Set dialog for NPC based on progression of the main story.
function npc_manager:set_dialog_main_story(map, value, npc_prefix, dialog, callback)
  local game = map:get_game()
  
  if game:main_story(value) then
    local npc = map:get_entity(npc_prefix)
    function npc:on_interaction()
      game:start_dialog(dialog, function()
        if callback then
          callback()
        end
      end)
    end
  end
end

-- Set movement for an NPC when a savegame exists. Useful when the player has an item
-- or completes a quest.
function npc_manager:set_movement_savegame_exist(map, savegame, movement, npc_prefix)
  local game = map:get_game()
  
  if savegame
      or (savegame and game:get_value(savegame)) then
    self:start_movement(map, movement, npc_prefix)
  end
end

-- Set movement for an NPC when a savegame is at a value. Useful when the player has an item
-- or completes a quest.
function npc_manager:set_movement_with_value(map, savegame, value, movement, npc_prefix)
  local game = map:get_game()
  
  if game:get_value(savegame) == value then
    self:start_movement(map, movement, npc_prefix)
  end
end

-- Set movement for an NPC based on progression of the main story.
function npc_manager:set_movement_main_story(map, value, movement, npc_prefix)
  local game = map:get_game()
  
  if game:main_story_at_least(value) then
    self:start_movement(map, movement, npc_prefix)
  end
end

-- Set movement for an NPC exactly at a point in the main story.
function npc_manager:set_movement_main_story_at(map, value, movement, npc_prefix)
  local game = map:get_game()

  if game:main_story_at(value) then
    self:start_movement(map, movement, npc_prefix)
  end
end

-- Stop movement for an NPC when a savegame exists.
function npc_manager:stop_movement_savegame_exist(map, savegame, npc_prefix)
  local game = map:get_game()
  
  if savegame
      or (savegame and game:get_value(savegame)) then
    self:stop_movement(map, npc_prefix)
  end
end

-- Stop movement for an NPC when a savegame is at a value.
function npc_manager:stop_movement_with_value(map, savegame, value, npc_prefix)
  local game = map:get_game()
  
  if game:get_value(savegame) == value then
    self:stop_movement(map, npc_prefix)
  end
end

-- Stop movement for an NPC based on progression of the main story.
function npc_manager:stop_movement_main_story(map, value, npc_prefix)
  local game = map:get_game()
  
  if game:main_story_at_least(value) then
    self:stop_movement(map, npc_prefix)
  end
end

-- Stop movement for an NPC exactly at a point in the main story.
function npc_manager:stop_movement_main_story_at(map, value, npc_prefix)
  local game = map:get_game()

  if game:main_story_at(value) then
    self:stop_movement(map, npc_prefix)
  end
end

-- Start movement for the selected NPC.
function npc_manager:start_movement(map, movement, npc_prefix)
  local npc = map:get_entity(npc_prefix)
  movement:start(npc)
end

-- Stop movement for the selected NPC.
function npc_manager:stop_movement(map, npc_prefix)
  local npc = map:get_entity(npc_prefix)
  npc:stop_movement()
end

-- Set animation for the selected NPC.
function npc_manager:set_animation(map, npc_prefix, animation)
  local npc = map:get_entity(npc_prefix)

  if npc and npc:is_enabled() then
    local sprite = npc:get_sprite()
    if sprite and sprite:has_animation(animation) then
      sprite:set_animation(animation)
    end
  end
end

-- Make NPC appear.
function npc_manager:appear_npc(map, npc)
  if npc and not npc:is_enabled() then
    npc:set_enabled(true)
  end
end

-- Make NPC disappear.
function npc_manager:disappear_npc(map, npc)
  if npc and npc:is_enabled() then
    npc:set_enabled(false)
  end
end

return npc_manager