local sensor_manager = {}

-- Enable sensors if story calls for it.
function sensor_manager:enable_sensors_story_activated(map, value, sensors)
  local game = map:get_game()
  
  if game:main_story(value) then
    for sensor in map:get_entities(sensors) do
      self:enable_sensor(map, sensor)
    end
  end
end

-- Enabled sensors if switch(es) activated.
function sensor_manager:enable_sensors_switch_activated(map, switch, sensors)
  
  local function switch_on_activated()
    if not switch.is_activated then
      switch.is_activated = true
      for sensor in map:get_entities(sensors) do
        self:enable_sensor(map, sensor)
      end
    end
  end
  
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    switch.on_activated = switch_on_activated
  end
end

-- Disable sensors if switch(es) activated.
function sensor_manager:disable_sensors_switch_activated(map, switches, sensors)
  
  local function switch_on_activated()
    if not switch.is_activated then
      switch.is_activated = true
      for sensor in map:get_entities(sensors) do
        self:disable_sensor(map, sensor)
      end
    end
  end
  
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    switch.on_activated = switch_on_activated
  end
end

-- Turn on, or enable, sensor.
function sensor_manager:enable_sensor(map, sensor)
  local sensor_entity = map:get_entity(sensor)
  sensor_entity:set_enabled(true)
end

-- Turn off, or disable, sensor.
function sensor_manager:disable_sensor(map, sensor)
  local sensor_entity = map:get_entity(sensor)
  if sensor_entity:exists() then
    sensor_entity:set_enabled(false)
  end
end

return sensor_manager