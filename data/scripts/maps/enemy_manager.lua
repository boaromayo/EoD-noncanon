-- This feature handles enemies (groups or a boss),
-- whether to enable or disable enemies
-- based on certain conditions (ie. activated switch,
-- beating a certain enemy once, getting an item, for boss
-- and subboss battles).

-- Based on the original enemy manager script
-- created by Christopho and binbin.

-- To add this script in-game, call:
-- require("scripts/maps/enemy_manager")

local enemy_manager = {}

-- Call a function when a group of enemies is dead.
function enemy_manager:on_enemies_dead(map, enemies, callback)
  if not map:has_entities(enemies) then
    callback()
  end
end

-- Make enemies appear.
-- Assume enemies are in-map but not active.
function enemy_manager:appear_enemies(map, enemies)
  for enemy in map:get_entities(enemies) do
    enemy:set_enabled(true)
  end
end

-- Create enemies from placeholders.
function enemy_manager:create_enemies(map, placeholders, breed_name, enemy_prefix)
  for placeholder in map:get_entities(placeholders) do
    local x, y, layer = placeholder:get_position()
    local direction = placeholder:get_direction()
    local enemy = map:create_enemy({
      breed = breed_name or "enemies/" .. breed_name,
      name = enemy_prefix,
      x = x,
      y = y,
      layer = layer,
      direction = direction,
    })
  end
end

-- Hide enemies.
-- Assume enemies are in map.
function enemy_manager:disappear_enemies(map, enemies)
  for enemy in map:get_entities(enemies) do
    enemy:set_enabled(false)
  end
end

-- Make enemies appear when a chest is opened.
function enemy_manager:appears_on_chest_opened(map, chest, enemies)
  local chest_entity = map:get_entity(chest)
  
  function chest_entity:on_opened()
    local hero = map:get_hero()
    local name, variant, savegame = chest_entity:get_treasure()
    hero:start_treasure(name, variant, savegame, function()
      enemy_manager:appear_enemies(map, enemies)
    end)
  end
end

-- Make enemies appear when a switch is activated.
function enemy_manager:appears_on_switch_activated(map, switch, enemies, sfx)
  local switch_entity = map:get_entity(switch)
  
  function switch_entity:on_activated()
    if sfx then
      sol.audio.play_sound(sfx)
    end
    self:appear_enemies(map, enemies)
  end
end

-- Make enemies appear if savegame is enabled.
function enemy_manager:appears_if_savegame_exist(map, savegame, enemies)
  local game = map:get_game()

  if savegame and game:get_value(savegame) == true then
    self:appear_enemies(map, enemies)
  else
    self:disappear_enemies(map, enemies)
  end
end

-- Create enemies when a chest is opened.
function enemy_manager:create_enemies_on_chest_opened(map, chest, placeholders, breed_name, enemy_prefix)
  local chest_entity = map:get_entity(chest)
  
  function chest_entity:on_opened()
    local hero = map:get_hero()
    local name, variant, savegame = chest_entity:get_treasure()
    hero:start_treasure(name, variant, savegame, function()
      enemy_manager:create_enemies(map, placeholders, breed_name, enemy_prefix)
    end)
  end
end

-- Create enemies if savegame is enabled.
function enemy_manager:create_enemies_if_savegame_exist(map, savegame, placeholders, breed_name, enemy_prefix)
  local game = map:get_game()

  if savegame and game:get_value(savegame) == true then
    self:create_enemies(map, placeholders, breed_name, enemy_prefix)
  end
end

return enemy_manager