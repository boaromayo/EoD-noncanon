-- This feature is used to check in maps for certain
-- conditions, such as searching a terrain type, 
-- to reduce the calling of certain methods in 
-- the map metatable.
-- Note this script does not add any new functions 
-- to the map metatable.

-- To add this script in the game, call:
-- require("scripts/maps/map_manager")

local map_manager = {}
local climbing_state = require("scripts/states/climbing")

function map_manager:is_ladder(map, x, y, layer)
  local layer = layer or 0
  return map:get_ground(x, y, layer) == "ladder"
end

function map_manager:make_hero_climb(map)
  local hero = map:get_hero()
  local x, y, _ = hero:get_position()
  local speed = hero:get_walking_speed()
  local hero_sprite = hero:get_sprite()
  local hero_on_ladder = self:is_ladder(map, x, y, hero:get_layer())
  local animation_old = hero_sprite:get_animation()
  local animation
  
  if hero_on_ladder then
    hero:start_climbing()
  end
    --[[if speed > 0 and animation ~= "climbing_walking" then
      animation = "climbing_walking"
    else
      animation = "climbing_stopped"
    end
  else
    if speed > 0 and animation ~= "walking" then
      animation = "walking"
    else
      animation = "stopped"
    end
  end--]]
  -- Set new hero animation.
  --[[if hero_sprite and animation ~= animation_old then
    hero_sprite:set_animation(animation)
  end--]]
end

return map_manager