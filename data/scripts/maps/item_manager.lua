-- This feature is used to call an item (in a chest 
-- or as a pickable) whenever a player fulfills 
-- certain actions (ie., destroyed enemies, 
-- activated switches, lit torches).

-- Based on the treasure manager script
-- by Christopho and binbin.

-- To add this script in-game, call:
-- require("scripts/maps/item_manager")

-- NOTE: When used here, "pickable" is 
-- the item prefix on the map, while 
-- "item" is the item itself.

local item_manager = {}
local audio_manager = require("scripts/audio_manager")

-- Chest appears when enemies are defeated.
function item_manager:chest_appears_enemy_dead(map, enemies, chest, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local function enemy_on_dead()
    local game = map:get_game()
    if not map:has_entities(enemies) then
      local chest_entity = map:get_entity(chest)
      local _, _, savegame = chest_entity:get_treasure()
      if not savegame
          or savegame
          and not game:get_value(savegame) then
        self:appear_chest(map, chest, delay, sfx)
      end
    end
  end
  
  for enemy in map:get_entities(enemies) do
    enemy.on_dead = enemy_on_dead
  end
end

-- Pickable appears when enemies are defeated.
function item_manager:pickable_appears_enemy_dead(map, enemies, pickable, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local function enemy_on_dead()
    local game = map:get_game()
    if not map:has_entities(enemies) then
      local item = map:get_entity(pickable)
      if item ~= nil then
        local _, _, savegame = item:get_treasure()
        if not savegame
            or savegame
            and not game:get_value(savegame) then
          self:appear_pickable(map, pickable, delay, sfx)
        end
      end
    end
  end
  
  for enemy in map:get_entities(enemies) do
    enemy.on_dead = enemy_on_dead
  end
end

-- Chest appears when a switch is activated.
function item_manager:chest_appears_switch_activated(map, switches, chest, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local function switch_on_activated(switch)
    local game = map:get_game()
    if not switch.is_activated then
      switch.is_activated = true
      local chest_entity = map:get_entity(chest)
      local _, _, savegame = chest_entity:get_treasure()
      if not savegame
          or savegame
          and not game:get_value(savegame) then
        self:appear_chest(map, chest, delay, sfx)
      end
    end
  end
  
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    switch.on_activated = switch_on_activated
  end
end

-- Pickable appears when a switch is activated.
function item_manager:pickable_appears_switch_activated(map, switches, pickable, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local function switch_on_activated(switch)
    local game = map:get_game()
    if not switch.is_activated then
      switch.is_activated = true
      local item = map:get_entity(pickable)
      local _, _, savegame = item:get_treasure()
      if not savegame
          or savegame
          and not game:get_value(savegame) then
        self:appear_pickable(map, pickable, delay, sfx)
      end
    end
  end
  
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    switch.on_activated = switch_on_activated
  end
end

-- Chest appears when a block is pushed.
function item_manager:chest_appears_block_pushed(map, block, chest, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil

  for block in map:get_entities(block) do
    if block.is_pushed then
      local game = map:get_game()
      local chest_entity = map:get_entity(chest)
      local _, _, savegame = chest_entity:get_treasure()
      if not savegame
          or savegame
          and not game:get_value(savegame) then
        self:appear_chest(map, chest, delay, sfx)
      end
    end
  end
end

-- Chest appears when switches are activated.
function item_manager:chest_appears_switches_activated(map, switches, chest, delay, sfx)
  local delay = delay or 500
  local sfx = sfx or nil
  
  local switches_left = 0
  local function switch_on_activated(switch)
    local game = map:get_game()
    local chest_dropped = false
    if not chest_dropped then
      switches_left = switches_left - 1
      if switches_left == 0 then
        local chest_entity = map:get_entity(chest)
        local _, _, savegame = chest_entity:get_treasure()
        if not savegame
            or savegame
            and not game:get_value(savegame) then
          self:appear_chest(map, chest, delay, sfx)
        end
      end
    end
  end

  local switches_here = false
  for switch in map:get_entities(switches) do
    switch.is_activated = false
    if not switch.is_activated then
      switches_left = switches_left + 1
    end
    switch.on_activated = switch_on_activated
    switches_here = true
  end

  -- Check if switches activated, then play sound again, if available.
  if switches_here and switches_left == 0 then
    if sfx then
      audio_manager:play_sound(sfx)
    end
  end
end

-- Chest appears when torches are lit.
function item_manager:chest_appears_torches_lit(map, torches, chest, delay, sfx)
  local delay = delay or 500
  local sfx = sfx or nil
  
  local torches_left = 0
  local function torch_on_lit()
    local game = map:get_game()
    local chest_dropped = false
    if not chest_dropped then
      torches_left = torches_left - 1
      if torches_left == 0 then
        local chest_entity = map:get_entity(chest)
        local _, _, savegame = chest_entity:get_treasure()
        if not savegame
            or savegame
            and not game:get_value(savegame) then
          self:appear_chest(map, chest, delay, sfx)
        end
      end
    end
  end
  
  local torches_here = false
  for torch in map:get_entities(torches) do
    if torch:is_lit() then
      torches_left = torches_left + 1
    end
    torch.on_lit = torch_on_lit
    torches_here = true
  end
  
  -- Check if all torches lit, play sound again, if available.
  if torches_here and torches_left == 0 then
    if sfx then
      audio_manager:play_sound(sfx)
    end
  end
end

-- Chest appears if the chest is already opened.
function item_manager:chest_appears_if_opened(map, chest)
  local chest_entity = map:get_entity(chest)
  if chest_entity and chest_entity:is_open() then
    chest_entity:set_enabled(true)
  end
end

-- Chest appears if a savegame variable exists.
function item_manager:chest_appears_if_savegame_exist(map, savegame, chest)
  local game = map:get_game()
  if savegame and game:get_value(savegame) == true then
    self:chest_appear_fast(map, chest)
  else
    self:disappear_chest(map, chest)
  end
end

-- Make health boost ("rainbow drink") appear 
-- if major enemy (sub-boss/boss) defeated.
function item_manager:max_life_appear_enemy_dead(map, boss_variable)
  local game = map:get_game()
  if game:get_value(boss_variable) == true then
    -- TODO: Ensure a variant can be dropped.
    self:appear_pickable(map, "rainbow_drink", 300)
  end
end

-- Make chest appear.
function item_manager:appear_chest(map, chest, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local chest_entity = map:get_entity(chest)
  sol.timer.start(map, delay, function()
    local hero = map:get_hero()
    hero:freeze()
    sol.timer.start(map, delay, function()
      if sfx then
        audio_manager:play_sound(sfx)
      end
      chest_entity:set_enabled(true)
      hero:unfreeze()
    end)
  end)
end

-- Make pickable appear.
function item_manager:appear_pickable(map, pickable, delay, sfx)
  local delay = delay or 200
  local sfx = sfx or nil
  
  local item = map:get_entity(pickable)
  if item then
    sol.timer.start(map, delay, function()
      local hero = map:get_hero()
      hero:freeze()
      sol.timer.start(map, delay, function()
        if sfx then
          audio_manager:play_sound(sfx)
        end
        item:set_enabled(true)
        hero:unfreeze()
      end)
    end)
  end
end

-- Make chest appear no-delay, no-sound.
function item_manager:appear_chest_fast(map, chest)
  local chest_entity = map:get_entity(chest)
  if chest_entity then
    chest_entity:set_enabled(true)
  end
end

-- Make pickable appear no-delay, no-sound.
function item_manager:appear_pickable_fast(map, pickable)
  local item = map:get_entity(pickable)
  if item then
    item:set_enabled(true)
  end
end

-- Make chest disappear.
function item_manager:disappear_chest(map, chest)
  local chest_entity = map:get_entity(chest)
  chest_entity:set_enabled(false)
end

-- Make pickable disappear.
function item_manager:disappear_pickable(map, pickable)
  local item = map:get_entity(pickable)
  if item then
    item:set_enabled(false)
  end
end

-- Manage secular treasures, or drops.
function item_manager:get_secular_treasure(map)
  local cutscene = require("scripts/maps/cutscene")
  local game = map:get_game()
  local name = map:get_world()
  local hero = map:get_hero()
  local saved_music = sol.audio.get_music()
  
  function map:start_secular_treasure_get_cinematic()
    cutscene.builder(game, map, hero)
      .freeze()
      .wait(250)
      .set_direction(hero, 3)
      .exec(function()
        audio_manager:stop_music()
      end)
      .wait(1000)
      .exec(function()
        audio_manager:play_music("dungeon_finished")
      end)
      .wait(3000)
      .exec(function()
        audio_manager:play_music(saved_music)
      end)
      .exec(function()
        game:start_dialog("_treasure.secular_treasure." .. name .. ".1")
      end)
      .wait(300)
      .exec(function()
        if name == "temple" then
          game:end_demo()
        end
      end)
    .start()
  end
  
  map:start_secular_treasure_get_cinematic()
end

return item_manager