-- Logo by boaromayo.

-- Create the boaromayo logo, or the second splash
-- screen for the game.
local boaromayo_logo = {}

-- Timer to countdown logo fade out.
local timer = nil

-- Flag to prevent multiple fade outs after input received.
local is_skipping = false

-- Constants.
local FADE_DELAY = 30
local LOGO_WIDTH, LOGO_HEIGHT = sol.video.get_quest_size()

-- Start menu.
function boaromayo_logo:on_started()
  -- Create logo components.
  self.surface = sol.surface.create(LOGO_WIDTH, LOGO_HEIGHT)
  self.logo = sol.surface.create("menus/boaromayo-splash-small.png")
  -- Start logo.
  self.surface:fill_color({0, 0, 0})
  self.surface:fade_in(FADE_DELAY)
  self.logo:fade_in(FADE_DELAY)
  
  self:setup_countdown()
end

-- Setup countdown timer to wait before fading out logo.
function boaromayo_logo:setup_countdown()
  local delay = FADE_DELAY * 100
  timer = sol.timer.start(self, delay, function()
    self:on_fade_out()
  end)
end

-- Draw method.
function boaromayo_logo:on_draw(screen)
  -- Get destination surface size to properly position logo.
  local width, height = screen:get_size()
  self.logo:draw(self.surface)
  self.surface:draw(screen, width / 2 - (LOGO_WIDTH / 2), height / 2 - (LOGO_HEIGHT / 2))
end

-- Event called when player presses a key.
function boaromayo_logo:on_key_pressed(key)

  local handled = false

  -- If escape key pressed, quit game.
  if key == "escape" then
    sol.main.exit()
    handled = true
  -- Handle input to fade out logo.
  else
    -- Shut off countdown timer if initialized and fade out logo.
    if timer ~= nil then
      timer:stop()
      timer = nil
      -- Prepare to close menu.
      self:on_fade_out()
    end
    handled = true
  end
  return handled -- Return to check if event was handled.
end

-- Events called when player presses a controller button.
function boaromayo_logo:on_joypad_button_pressed(button)

  -- Handle input to fade out logo.
  if button == 1 or button == 9 then
    return self:on_key_pressed("return")
  elseif button == 2 then
    return self:on_key_pressed("space")
  end
end

-- Fade out logo processing.
function boaromayo_logo:on_fade_out()
  -- Prevent additional fade outs if button pressed.
  if is_skipping then
    return
  end  
  is_skipping = true

  -- Fade out logo, then close menu after a second.
  self.logo:fade_out(FADE_DELAY)
  self.surface:fade_out(FADE_DELAY)

  sol.timer.start(self, 1000, function()
    sol.menu.stop(self)
  end)
end

return boaromayo_logo