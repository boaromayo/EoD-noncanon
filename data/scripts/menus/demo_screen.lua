-- Selection screen for demo purposes only.
-- Called after title screen.

-- Load managers.
local ui_manager = require("scripts/menus/lib/ui_manager")
local audio_manager = require("scripts/audio_manager")
local language_manager = require("scripts/language_manager")
local game_manager = require("scripts/game_manager")

-- Setup filename.
local filename = "save1.dat"

-- Setup other stats.
local genders = { "boy", "girl" }
-- NOTE: Put demo locations here.
local locations = {
  { name = "prison_ship", place = "cutscenes/intro", destination = "begin" },
  { name = "cellar_cave", place = "sura_island/caves/cellar_cave", destination = "entrance" },
  { name = "temple", place = "temple/1f", destination = "entrance" },
  --{ name = "village", place = "sura_island/village_ext", destination = "entrance" },
  --{ name = "crypt", place = "molodek_island/caves/crypt", destination = "entrance" },
  --{ name = "debug", place = "sura_island/caves/east_caves/1f", destination = "entrance" }
  --{ name = "demo_island", place = "chelsea_island/caves/magic_square_cave", destination = "entrance" }
}
-- Make positions for other menu items.
local settings_pos = #locations + 1
local credits_pos = #locations + 2

local demo_screen = {}

local window_width = 264
local window_height = 192

local fade_delay = 30

function demo_screen:on_started()
  -- Setup demo game.
  self.game = sol.game.load(filename)

  self:load_resources()
  self:initialize_defaults()
  self:initialize_modes()

  -- Fade into demo screen.
  self:fade_in_components(fade_delay)

  if not sol.audio.get_music() then
    sol.timer.start(self, 1000, function()
      audio_manager:play_music("Peaceful-Evening_Looping")
    end)
  end
end

function demo_screen:load_resources()
  -- Sky background colors.
  local sea_color = { 77, 166, 220 }
  local dusk_color = { 218, 92, 36 }
  local night_color = { 20, 80, 173 }
  
  -- Setup sky color based on time of day.
  local hour = tonumber(os.date("%H"))
  if hour > 6 and hour < 17 then
    self.background_color = sea_color
  elseif hour >= 17 and hour <= 18 then
    self.background_color = dusk_color
  else
    self.background_color = night_color
  end

  local font = language_manager:get_text_font()
  local disabled_font = "lunchds_gray"

  -- Load assets.
  self.surface = sol.surface.create(sol.video.get_quest_size())

  self.area_select_window = nil
  self.misc_window = nil
  self.character_window = nil

  self.title_text = sol.text_surface.create({
    font = font
  })
  
  self.choices_text = {}
  
  self.settings_text = sol.text_surface.create({
    font = font,
    text_key = "demo_screen.settings",
    horizontal_alignment = "left"
  })

  self.credits_text = sol.text_surface.create({
    font = font,--disabled_font,
    text_key = "demo_screen.credits",
    horizontal_alignment = "left"
  })

  self.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  
  self.boy_sprite = sol.sprite.create("hero/den_" .. genders[1])
  self.girl_sprite = sol.sprite.create("hero/den_" .. genders[2])
end

function demo_screen:initialize_defaults()
  -- Toggle to allow input.
  self.allow_input = false
  
  -- Set up positions for assets.
  local screen_w, screen_h = sol.video.get_quest_size()
  local x = (screen_w - window_width) / 2
  local y = (screen_h - (window_height + 16)) / 2

  self.window_position = { x = x, y = y }
  self.misc_window_position = { x = x, y = y + window_height - 16 }

  -- Setup windows.
  self.area_select_window = ui_manager:create_window(self.window_position.x, self.window_position.y, 
    window_width, window_height - 16)
  self.misc_window = ui_manager:create_window(self.misc_window_position.x, self.misc_window_position.y, 
    window_width, 32)
  self.character_window = ui_manager:create_window(self.window_position.x, self.window_position.y, 
    window_width, window_height)

  -- For starting location in demo.
  self.starting_location = 0
  
  -- Menu choices table.
  self.choices = {}
  
  -- Character gender selection index. (1 for boy, 2 for girl)
  self.character_gender = 0
  
  -- Set up modes for menu. (main, characters, credits)
  self.mode = "main"
end

function demo_screen:initialize_modes()
  if self.mode == "main" then
    self:initialize_demo_select()
  elseif self.mode == "character" then
    self:initialize_character_select()
  elseif self.mode == "settings" then
    self:initialize_settings()
  elseif self.mode == "credits" then
    self:on_credits_demo()
  end
end

function demo_screen:initialize_demo_select()
  -- Setup choices.
  for i, location in ipairs(locations) do
    self.choices[i] = "demo_screen." .. location.name
  end
  self.choices[#self.choices + 1] = "demo_screen.settings"
  self.choices[#self.choices + 1] = "demo_screen.credits"
  -- Set up cursor.
  self.cursor = self.previous_index or 1
  -- Set up title.
  self.title_text:set_text_key("demo_screen.select")
  -- Set up location choice text.
  for i, choice in ipairs(self.choices) do
    if i == #self.choices then
      break
    end
    if not self.choices_text[i] then
      self.choices_text[#self.choices_text + 1] = sol.text_surface.create({
        font = self.title_text:get_font(),
        text_key = choice
      })
    end
  end
    
  -- Start up cursor.
  sol.timer.start(self, 1000, function()
    self.cursor_sprite:set_animation("question")
    self.cursor_sprite:set_frame(0)
    self.allow_input = true
    self:set_cursor_position(self.cursor)
  end)
end

function demo_screen:initialize_character_select()
  self.choices = { "den_boy", "den_girl" }
  self.cursor = 1
  self.title_text:set_text_key("file_screen.chara_select.title")
  sol.timer.start(self, 100, function()
    -- Initialize character and cursor animation.
    if self.cursor == 1 then
      self.boy_sprite:set_animation("walking")
    elseif self.cursor == 2 then
      self.girl_sprite:set_animation("walking")
    end
      
    self.cursor_sprite:set_animation("question")
    self.cursor_sprite:set_frame(0)
    self.allow_input = true
  end)
  self:set_cursor_position(self.cursor)
end

function demo_screen:initialize_settings()
  local settings_menu = require("scripts/menus/settings")
  sol.menu.start(self, settings_menu, true)
  self.mode = "main"
end

function demo_screen:set_mode(mode)
  self.mode = mode
  self:initialize_modes()
end

function demo_screen:set_cursor_position(index)
  self.cursor = (index - 1) % #self.choices + 1

  -- Reset cursor animation when changed.
  self.cursor_sprite:set_frame(0)
end

-- For when credits is selected.
function demo_screen:on_credits_demo()
  local credits_menu = require("scripts/menus/credits")
  sol.timer.start(self, 200, function()
    self.surface:fade_out(fade_delay)
    sol.timer.start(self, 2000, function()
      sol.menu.start(sol.main, credits_menu, true)
    end)
  end)
end

function demo_screen:move_cursor(x,y)
  self.cursor_sprite:set_xy(x, y)
end

-- Drawing function main menu for demo.
function demo_screen:on_draw_main_demo()
  -- Setup offsets.
  local offset = 24
  local qrtr_offset = offset / 4
  local choices_offset = (#self.choices_text + 1) * 2
  local cursor_y_offset = 80

  -- Draw main windows.
  self.area_select_window:draw(self.surface)
  self.misc_window:draw(self.surface)

  -- Text for select title.
  self.title_text:draw(self.surface, 
    window_width / 2 - (offset * 2 + qrtr_offset), self.window_position.y + offset)

  -- Text for choices.
  for i = 1, #locations do
    self.choices_text[i]:draw(self.surface, self.window_position.x + offset, 
      (i-1) * offset - choices_offset + cursor_y_offset)
  end

  -- Text for settings.
  self.settings_text:draw(self.surface, 
    self.misc_window_position.x + offset, 
    self.misc_window_position.y + 16)

  -- Text for credits.
  self.credits_text:draw(self.surface,
    self.misc_window_position.x + (offset * 5),
    self.misc_window_position.y + 16)

  -- Draw cursor.
  self.cursor_sprite:draw(self.surface)

  -- Move cursor based on position. For non-location choices, place cursor in bottom box.
  if self.cursor <= #locations then
    self:move_cursor(self.window_position.x + (offset / 2), 
      (self.cursor - 1) * offset - choices_offset + cursor_y_offset + (offset / 4))
  else
    local bottom_box_offset = (self.cursor - #locations - 1) * (offset * 4)
    self:move_cursor(self.window_position.x + (offset / 2) + bottom_box_offset, 
      self.misc_window_position.y + offset - 2)
  end
end

-- Drawing function character select.
function demo_screen:on_draw_character_select_demo()
  -- Draw character select window.
  self.character_window:draw(self.surface)
  
  -- Change title to character select.
  self.title_text:draw(self.surface,
    window_width / 2 - 60, self.window_position.y + 24)
  
  -- Draw cursor.
  self.cursor_sprite:draw(self.surface)
  
  -- Have character sprites face player.
  local SOUTH = 3
  self.boy_sprite:set_direction(SOUTH)
  self.girl_sprite:set_direction(SOUTH)
  
  -- Draw Den hero/heroine sprites.
  self.boy_sprite:draw(self.surface, window_width / 2 - 40, window_height - 40)
  self.girl_sprite:draw(self.surface, window_width / 2 + 80, window_height - 40)
  
  -- Move cursor based on position.
  -- Character sprites start walking when cursor does not select them,
  -- so their walking animation is activated opposite their position.
  if self.cursor == 1 then
    self.girl_sprite:set_animation("walking")
    self:move_cursor(window_width / 2 - 60, window_height - 40)
  elseif self.cursor == 2 then
    self.boy_sprite:set_animation("walking")
    self:move_cursor(window_width / 2 + 60, window_height - 40)
  end
end

-- Drawing function.
function demo_screen:on_draw(screen)
  -- Draw background.
  self.surface:clear()
  self.surface:fill_color(self.background_color)

  -- Draw based on current mode.
  if self.mode == "character" then
    self:on_draw_character_select_demo()
  else
    self:on_draw_main_demo()
  end

  local width, height = screen:get_size()
  self.surface:draw(screen, width / 2 - 160, height / 2 - 120)
end

-- Event called when command is executed.
function demo_screen:on_command_pressed(command)

  local handled = false

  -- Enable input when menu is fully visible.
  if self.allow_input then
    if command == "action" or command == "attack" then
      self:on_input_decision()
      handled = true
    elseif command == "cancel" then
      self:on_input_cancel()
      handled = true
    elseif command == "up" or command == "left" then
      self:set_cursor_position(self.cursor - 1)
      handled = true
    elseif command == "down" or command == "right" then
      self:set_cursor_position(self.cursor + 1)
      handled = true
    end

    -- Play cursor sound when moving directional commands.
    if command == "up" or command == "down" or
        command == "left" or command == "right" then
      audio_manager:play_sound("cursor")
    end
  end

  return handled
end

-- Event called when player presses a key.
function demo_screen:on_key_pressed(key)

  local handled = false

  if self.allow_input then
    if key == "escape" then
      -- Pressing ESC key quits the game.
      sol.main.exit()
      handled = true
    elseif key == "return" or key == "space" then
      handled = self:on_command_pressed("action")
    elseif key == "b" then
      handled = self:on_command_pressed("cancel")
    elseif key == "up" or key == "left" then
      handled = self:on_command_pressed("up")
    elseif key == "down" or key == "right" then
      handled = self:on_command_pressed("down")
    end
  end

  return handled
end

-- Event called when player moves control pad.
local RIGHT = 0
local UP = 2
local LEFT = 4
local DOWN = 6
function demo_screen:on_joypad_hat_moved(hat, direction8)

  if self.allow_input then
    -- Check control pad moved.
    if direction8 >= 0 then
      -- Call command based on control pad direction.
      if direction8 == UP then
        return self:on_command_pressed("up")
      elseif direction8 == LEFT then
        return self:on_command_pressed("left")
      elseif direction8 == DOWN then
        return self:on_command_pressed("down")
      elseif direction8 == RIGHT then
        return self:on_command_pressed("right")
      end
    end
  end
end

-- Event called when player moves control stick.
function demo_screen:on_joypad_axis_moved(axis, state)

  if self.allow_input then
    -- Check if control stick moved.
    if axis > 0 then
      -- Call command based on new state.
      if state < 0 then
        return self:on_command_pressed("up")
      elseif state > 0 then
        return self:on_command_pressed("down")
      end
    elseif axis == 0 then
      if state < 0 then
        return self:on_command_pressed("left")
      elseif state > 0 then
        return self:on_command_pressed("right")
      end
    end
  end
end

-- Event called when player presses a controller button.
function demo_screen:on_joypad_button_pressed(button)

  if self.allow_input then

    if button == 1 or button == 9 then -- Cross and start confirms.
      return self:on_command_pressed("action")
    elseif button == 2 then -- Circle cancels.
      return self:on_command_pressed("cancel")
    end
  end
end

-- Event called for handling decisions after confirm.
function demo_screen:on_input_decision()
  if self.mode == "main" then
    if self.cursor <= #locations then -- Go to character select.
      audio_manager:play_sound("ok")
      self.cursor_sprite:set_animation("none")
      self:set_starting_location(self.cursor)
      self.previous_index = self.cursor
      self:set_mode("character")
    elseif self.cursor == settings_pos then -- Other menus.
      audio_manager:play_sound("ok")
      self:set_mode("settings")
    elseif self.cursor == credits_pos then
      audio_manager:play_sound("ok")
      self:set_mode("credits")
    end
  elseif self.mode == "character" then
    self.allow_input = false
    audio_manager:play_sound("ok")
    self.cursor_sprite:set_animation("none")
    self:set_character_gender(self.cursor)
    self:on_pre_finish()
  end
end

-- Event called for handling decisions when cancelled.
function demo_screen:on_input_cancel()
  if self.cursor <= #self.choices then
    audio_manager:play_sound("hero_hurt")
    if self.mode == "character" then
      self:stop_character_animation()
      self:set_mode("main")
    end
  elseif self.cursor > #self.choices then
    audio_manager:play_sound("wrong")
  end
end

function demo_screen:stop_character_animation()
  self.boy_sprite:set_animation("stopped")
  self.girl_sprite:set_animation("stopped")
end

-- Set methods for starting location and character selected.
function demo_screen:set_starting_location(index)
  self.starting_location = index
end

function demo_screen:set_character_gender(index)
  self.character_gender = index
end

-- Setup demo before starting.
function demo_screen:setup_demo()
  local game = self.game
  local gender = genders[self.character_gender]
  local location_index = self.starting_location
  local location = locations[location_index]
  
  game:set_value("den_gender", gender)
  game:set_value("demo_starting_location_index", location_index)
  game:set_starting_location(location.place, location.destination)
end

-- Fade in components.
function demo_screen:fade_in_components(time)
  self.area_select_window:get_surface():fade_in(time)
  self.misc_window:get_surface():fade_in(time)
end

-- Event called before transitioning to finish.
function demo_screen:on_pre_finish()
  sol.timer.start(self, 200, function()
    self.surface:fade_out(fade_delay)
    sol.timer.start(self, 2000, function()
      sol.menu.stop(self)
      if self.mode == "character" then
        self:setup_demo()
        game_manager:start_demo_game(self.game)
      end
    end)
  end)
end

return demo_screen