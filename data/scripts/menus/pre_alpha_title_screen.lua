-- Pre-alpha demo disclaimer title screen.
-- This is to inform the player that this is a pre-alpha demo copy of the game.
-- NOTE: Remove in the final release.

-- Setup scene table.
local pre_alpha_title = {}

-- Load managers.
local language_manager = require("scripts/language_manager")

-- Setup global timer.
local timer = nil

-- Setup anti-skipper to prevent fade-out from restarting.
local is_skipping = false

-- Delay constant.
local FADE_DELAY = 30

-- Start menu.
function pre_alpha_title:on_started()
  -- Setup menu resources.
  self:load_resources()
  
  -- Initialize text.
  self:initialize_text()

  -- Setup timer to fade out after 5 seconds.
  timer = sol.timer.start(self, 5000, function()
    self:on_pre_finish()
  end)
end

-- Load resources for scene.
function pre_alpha_title:load_resources()
  -- Setup text settings.
  local pure_white = {255, 255, 255}
  local color = pure_white
  local font, font_size = language_manager:get_default_font()

  -- Create text.
  self.line1_text = sol.text_surface.create({
    color = color,
    font = font,
    font_size = font_size,
    text_key = "pre_alpha_title.line1",
    horizontal_alignment = "center",
  })
  self.line2_text = sol.text_surface.create({
    color = color,
    font = font,
    font_size = font_size,
    text_key = "pre_alpha_title.line2",
    horizontal_alignment = "center",
  })
  self.line3_text = sol.text_surface.create({
    color = color,
    font = font,
    font_size = font_size,
    text_key = "pre_alpha_title.line3",
    horizontal_alignment = "center",
  })
  self.line4_text = sol.text_surface.create({
    color = color,
    font = font,
    font_size = font_size,
    text_key = "pre_alpha_title.line4",
    horizontal_alignment = "center",
  })
end

-- Initialize text.
function pre_alpha_title:initialize_text()
  self.disclaimer_lines = {
    self.line1_text,
    self.line2_text,
    self.line3_text,
    self.line4_text
  }
  
  for _, line in ipairs(self.disclaimer_lines) do
    line:fade_in(FADE_DELAY)
  end
end

-- Main drawing function. 
function pre_alpha_title:on_draw(screen)
  -- Get screen size for blit drawing.
  local width, height = screen:get_size()
  -- Draw text proper.
  self.line1_text:draw(screen, width / 2, height / 2 - 40)
  self.line2_text:draw(screen, width / 2, height / 2 - 20)
  self.line3_text:draw(screen, width / 2, height / 2)
  self.line4_text:draw(screen, width / 2, height / 2 + 20)
end

-- Event called when player presses a key.
function pre_alpha_title:on_key_pressed(key)

  local handled = false

  -- If escape key pressed, quit game.
  if key == "escape" then
    sol.main.exit()
    handled = true
  -- Handle input to fade out disclaimer.
  elseif key == "space" or key == "return" then
    -- Shut off global timer if it exists and fade out.
    if timer ~= nil then
      -- Stop timer and delete timer.
      timer:stop()
      timer = nil
      -- Prepare to close menu.
      self:on_pre_finish()
    end
    handled = true
  end
  return handled -- Return to check if event was handled.
end

-- Events called when player presses a controller button.
function pre_alpha_title:on_joypad_button_pressed(button)

  -- Handle input to fade out disclaimer.
  if button == 1 or button == 9 then
    return self:on_key_pressed("return")
  elseif button == 2 then
    return self:on_key_pressed("space")
  end
end

function pre_alpha_title:on_pre_finish()
  -- Prevent additional fade-outs if key pressed.
  if is_skipping then
    return
  end

  -- Prevent additional fade-outs.
  is_skipping = true

  -- Fade out surface and text.
  for _, line in ipairs(self.disclaimer_lines) do
    line:fade_out(FADE_DELAY)
  end
  
  sol.timer.start(self, 1000, function()
    sol.menu.stop(self)
  end)
end

return pre_alpha_title