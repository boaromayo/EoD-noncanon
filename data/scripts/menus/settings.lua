-- Settings/configuration menu
-- for all the game settings.

-- The following settings that can
-- be adjusted include:
-- - Current Language
-- - UI Window Style
-- - Dialogue Speed
-- - Input Command Configuration (for keyboard & gamepad | commands include: attack, action, cancel, item, weapon, subweapon)
-- - Music Volume
-- - Audio/Sound Volume
-- These can be reset to default.
local title_settings = {}

local ui_manager = require("scripts/menus/lib/ui_manager")
local audio_manager = require("scripts/audio_manager")
local math_utils = require("scripts/lib/math_utils")

local ROW_MAX = 5

local window_width = 264
local window_height = 224

function title_settings:on_started()
  
  self:load_resources()
  self:initialize()
  self:initialize_modes()
end

function title_settings:load_resources()
  -- Load in sky background.
  -- Background colors.
  local sea_color = { 77, 166, 220 }
  local dusk_color = { 218, 92, 36 }
  local night_color = { 20, 80, 173 }

  -- Setup sky color based on time of day.
  local hour = tonumber(os.date("%H"))
  if hour > 6 and hour < 17 then
    self.background_color = sea_color
  elseif hour >= 17 and hour <= 18 then
    self.background_color = dusk_color
  else
    self.background_color = night_color
  end

  -- TODO: Enable different font if window style changed
  -- local window_style = game:get_value("window_style") or 1
  local font = "lunchds_blue" --window_style == 1 and "lunchds_blue" or "lunchds"

  -- Main drawing surface.
  self.surface = sol.surface.create(sol.video.get_quest_size())

  -- Windows to draw contents.
  self.window = nil
  self.help_window = nil

  -- Setup default values for settings.
  self.settings = {
    {
      name = "language",
      enabled = true,
      values = sol.language.get_languages(),
      initial_value = sol.language.get_language(),
    },
    {
      name = "video_mode",
      enabled = true,
      values = sol.video.get_modes(),
      initial_value = sol.video.get_mode(),
    },
    {
      name = "dialog_speed",
      enabled = false,
      values = {
        fastest = 10,
        fast = 20,
        normal = 40,
        normal_slow = 50,
        slow = 60,
        slowest = 80,
      },
      initial_value = 40,
    },
    {
      name = "window_style",
      enabled = false,
      values = { 1 },
      initial_value = 1,
    },
    {
      name = "music_volume",
      enabled = true,
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = math_utils.round(sol.audio.get_music_volume()) * 10,
    },
    {
      name = "sound_volume",
      enabled = true,
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = math_utils.round(sol.audio.get_sound_volume()) * 10,
    },
    {
      name = "input_config",
      enabled = false,
      --[[values = {
        {
          name = "weapon",
          values = { "C", "X", "V" },
          initial_value = nil,
        },
        {
          name = "subweapon",
          values = { "C", "X", "V" },
          initial_value = nil,
        },
        {
          name = "item",
          values = { "C", "X", "V" },
          initial_value = nil,
        }
      },
      initial_value = "",--]]
    },
    {
      name = "dev_commentary",
      enabled = false,
      values = { "Enable", "Disable" },
      initial_value = "Disable",
    }
  }
  -- For settings to be accessed by player.
  self.enabled_settings = {}

  -- Setup texts.
  self.setting_text_names = {}
  self.setting_text_help = {}
  self.setting_text_values = {}
  self.help_text = sol.text_surface.create({
    font = font,
    horizontal_alignment = "center",
  })
  self.title_text = sol.text_surface.create({
    font = font,
  })

  -- Setup cursor sprites.
  self.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.left_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.right_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
end

function title_settings:initialize()
  -- Setup positions for windows.
  local screen_width, screen_height = self.surface:get_size()
  local x = (screen_width - window_width) / 2
  local y = (screen_height - window_height) / 2

  self.window_position = { x = x, y = y }

  self.window = ui_manager:create_window(self.window_position.x, self.window_position.y, 
    window_width, window_height - 32)
  self.help_window = ui_manager:create_window(self.window_position.x, 
    self.window_position.y + window_height - 32, window_width, 32)
  
  -- Set cursor position to topmost in menu.
  local index = 1

  -- Set current mode, or page of menu.
  self.mode = "main"

  -- Setup the strings of title and settings options.
  self.title_text:set_text(sol.language.get_string("settings.title"))

  -- Add enabled settings.
  for i, setting in ipairs(self.settings) do
    if setting.enabled then
      self.enabled_settings[#self.enabled_settings + 1] = setting
    end
  end

  -- Add text surfaces via loop, ignore if any spurious 
  -- redraws are performed.
  for i, setting in ipairs(self.enabled_settings) do
    if not self.setting_text_names[i] then
      self.setting_text_names[#self.setting_text_names + 1] = sol.text_surface.create({
        font = "lunchds_blue",--self.title_text:get_font(),
        text_key = "settings." .. setting.name,
        horizontal_alignment = "left",
      })
    end

    if not self.setting_text_help[i] then
      self.setting_text_help[#self.setting_text_help + 1] = "settings.description." .. setting.name
    end

    if not self.setting_text_values[i] 
        and setting.values ~= nil then
      self.setting_text_values[#self.setting_text_values + 1] = sol.text_surface.create({
        font = "lunchds_blue",--self.title_text:get_font(),
        horizontal_alignment = "center",
      })
      self:set_setting_value_text(setting, i)
    end
  end

  -- Move cursor to start.
  self:set_cursor_position(index)

  -- Set help text based on cursor position.
  self:set_description(self.cursor_index)
end

-- Initialize based on current mode.
function title_settings:initialize_modes()
  if self.mode == "main" then
    self:initialize_main_mode()
  elseif self.mode == "adjust" then
    self:initialize_adjust()
  elseif self.mode == "input" then
    self:initialize_input_menu()
  end
end

function title_settings:initialize_main_mode()
  -- Unpause cursor if previously paused.
  if self.cursor_sprite:is_paused() then
    self.cursor_sprite:set_paused(false)
  end
  -- Animate cursor.
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)

  self.left_cursor_sprite:set_animation("none")
  self.right_cursor_sprite:set_animation("none")
end

function title_settings:initialize_adjust()
  -- Freeze main cursor.
  self.cursor_sprite:set_frame(0)
  self.cursor_sprite:set_paused()

  self:setup_value_cursor(self.cursor_index)

  -- Show and animate left and right cursors.
  self.left_cursor_sprite:set_animation("cursor")
  self.left_cursor_sprite:set_frame(0)
  self.left_cursor_sprite:set_direction(0)

  self.right_cursor_sprite:set_animation("cursor")
  self.right_cursor_sprite:set_frame(0)
  self.right_cursor_sprite:set_direction(1)
end

-- Start transition to input menu.
function title_settings:initialize_input_menu()
  local input_config_menu = require("scripts/menus/input_config")
  sol.menu.start(self, input_config_menu, true)
  self.mode = "main"
end

-- Drawing function for menu components.
function title_settings:on_draw_components()
  -- Label position.
  local label_x, label_y

  -- Offsets.
  local offset = 24
  local qrtr_offset = offset / 4
  local setting_text_offset = (#self.setting_text_names + 1) * 2
  local cursor_y_offset = 80

  -- Draw main and help window and text.
  self.window:draw(self.surface)
  self.help_window:draw(self.surface)

  -- Title text.
  local title_width = self.title_text:get_size()
  label_x = (window_width / 2) - (title_width / 4)
  label_y = self.window_position.y + offset
  self.title_text:draw(self.surface, label_x, label_y)

  -- Setting labels.
  label_x = self.window_position.x + offset
  for i, setting in ipairs(self.setting_text_names) do
    label_y = (i - 1) * (offset + (qrtr_offset / 3)) - setting_text_offset + cursor_y_offset
    setting:draw(self.surface, label_x, label_y)
  end

  -- Setting values.
  label_x = self.window_position.x + (window_width - (offset * 2 + qrtr_offset))
  for i, value in ipairs(self.setting_text_values) do
    label_y = (i - 1) * (offset + (qrtr_offset / 3)) - setting_text_offset + cursor_y_offset
    value:draw(self.surface, label_x, label_y)
  end

  -- Description text.
  label_x = self.window_position.x + window_width / 2 + 3
  label_y = self.window_position.y + window_height - (qrtr_offset * 3 - 2)
  self.help_text:draw(self.surface, label_x, label_y)

  -- Move cursor based on index position.
  self.cursor_sprite:draw(self.surface)
  self.cursor_x = self.window_position.x + (offset / 2)
  self.cursor_y = (self.cursor_index - 1) * (offset + (qrtr_offset / 3)) - 
    setting_text_offset + cursor_y_offset + qrtr_offset
  self:move_cursor(self.cursor_x, self.cursor_y)

  self.left_cursor_sprite:draw(self.surface, 
    self.cursor_x + (window_width - (offset * 4 + qrtr_offset)), self.cursor_y)
  self.right_cursor_sprite:draw(self.surface, 
    self.cursor_x + window_width - (offset + 4), self.cursor_y)
end

-- Main drawing function.
function title_settings:on_draw(screen)
  local screen_width, screen_height = screen:get_size()
  local width, height = sol.video.get_quest_size()
  local cx = screen_width / 2
  local cy = screen_height / 2
  local x, y = cx - (width / 2), cy - (height / 2)

  -- Draw on-screen.
  self.surface:clear()
  self.surface:fill_color(self.background_color)

  -- Draw window based on mode.
  self:on_draw_components()

  self.surface:draw(screen, x, y)
end

-- Event called when command is executed.
function title_settings:on_command_pressed(command)
  local handled = false

  if command == "action" or command == "attack" then
    self:on_input_decision()
    handled = true
  elseif command == "cancel" or command == "pause" then
    self:on_input_cancel()
    handled = true
  elseif command == "up" or command == "left" 
      or command == "right" or command == "down" then
    -- When adjusting a setting, allow left/right commands only.
    if self.mode == "adjust" then
      if command == "left" then
        self:set_value_cursor_position(self.value_index - 1)
      elseif command == "right" then
        self:set_value_cursor_position(self.value_index + 1)
      else
        audio_manager:play_sound("wrong")
      end

      if command == "left" or command == "right" then
        audio_manager:play_sound("cursor")
      end
    elseif self.mode == "main" then
      if command == "up" or command == "left" then
        self:set_cursor_position(self.cursor_index - 1)
      elseif command == "down" or command == "right" then
        self:set_cursor_position(self.cursor_index + 1)
      end
      audio_manager:play_sound("cursor")
      -- Update description based on cursor position.
      self:set_description(self.cursor_index)
    end
    
    handled = true
  end

  return handled  
end

-- Event called when player presses a key.
function title_settings:on_key_pressed(key)
  local handled = false

  if key == "escape" or key == "b" then
    -- Can only exit the game via main menu.
    handled = self:on_command_pressed("cancel")
  elseif key == "return" or key == "space" then
    handled = self:on_command_pressed("action")
  elseif key == "up" then
    handled = self:on_command_pressed("up")
  elseif key == "left" then
    handled = self:on_command_pressed("left")
  elseif key == "right" then
    handled = self:on_command_pressed("right")
  elseif key == "down" then
    handled = self:on_command_pressed("down")
  end

  return handled
end

-- Event called based on decision entered.
function title_settings:on_input_decision()
  if self.mode == "main" then
    if self.cursor_index < ROW_MAX then
      self:set_mode("adjust")
      audio_manager:play_sound("ok")
    else
      self:set_mode("input")
    end
  elseif self.mode == "adjust" then
    self:set_mode("main")
    audio_manager:play_sound("ok")
  end
end

-- Event called for handling decisions when cancelled.
function title_settings:on_input_cancel()
  if self.mode == "main" then
    sol.menu.stop(self)
  elseif self.mode == "adjust" or self.mode == "input" then
    self:set_mode("main")
  end
  audio_manager:play_sound("hero_hurt")
end

-- Selected setting.
function title_settings:get_setting(index)
  return self.enabled_settings[index]
end

-- Setup value index of selected setting.
function title_settings:setup_value_cursor(index)
  local setting = self:get_setting(index)
  local values = setting.values

  for i, value in ipairs(values) do
    if value == setting.initial_value then
      self.value_index = i
      return
    end
  end
end

-- Set value of selected setting.
function title_settings:set_setting_value(setting, value)
  local current_setting = setting.name

  if current_setting == "language" then
    sol.language.set_language(value)
  elseif current_setting == "video_mode" then
    sol.video.set_mode(value)
  elseif current_setting == "music_volume" then
    sol.audio.set_music_volume(value)
  elseif current_setting == "sound_volume" then
    sol.audio.set_sound_volume(value)
  end

  setting.initial_value = value
end

-- Set text of the value of the selected setting.
function title_settings:set_setting_value_text(setting, index)
  local name = setting.name
  local value_text = self.setting_text_values[index]
  
  if name == "language" then
    value_text:set_text(sol.language.get_language_name())
  else
    value_text:set_text(setting.initial_value)
  end
end

-- Set current mode.
function title_settings:set_mode(mode)
  self.mode = mode
  self:initialize_modes()
end

-- Set description based on cursor index.
function title_settings:set_description(index)
  local help_text_key = self.setting_text_help[index]
  self.help_text:set_text(sol.language.get_string(help_text_key))
end

-- Set cursor's index.
function title_settings:set_cursor_position(index)
  local max_index = math.min(#self.setting_text_names, ROW_MAX)
  self.cursor_index = (index - 1) % max_index + 1

  -- Reset cursor animation when changed.
  self.cursor_sprite:set_frame(0)
end

-- Set cursor of a setting's values index.
function title_settings:set_value_cursor_position(index)
  local setting = self:get_setting(self.cursor_index)
  local num_values = #setting.values
  local max_index = math.max(1, num_values)

  self.value_index = (index - 1) % max_index + 1

  -- Update settings.
  self:set_setting_value(setting, setting.values[self.value_index])
  self:set_setting_value_text(setting, self.cursor_index)
end

-- Move cursor to certain position.
function title_settings:move_cursor(x,y)
  self.cursor_sprite:set_xy(x, y)
end

return title_settings