-- Prompt dialog box for decision making.
local prompt_box_builder = {}

local ui_manager = require("scripts/menus/lib/ui_manager")
local audio_manager = require("scripts/audio_manager")

function prompt_box_builder:show(context, text, decision_text, result, callback)
  -- Initialize prompt box menu.
  local prompt_box = {}

  function prompt_box:on_started()
    local width, height = sol.video.get_quest_size()
    width = width / 2
    height = height / 2

    -- Prompt box drawing surface.
    prompt_box.surface = sol.surface.create(width, height)

    prompt_box.window = ui_manager:create_window(0, 0, width, height)
    prompt_box.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")

    prompt_box.window:set_xy(width - 160, height - 120)

    -- Font.
    local font = "lunchds_blue"
    prompt_box.font = font

    -- Texts.
    prompt_box.texts = {}
    for i, text_line in ipairs(text) do
      prompt_box.texts[i] = sol.text_surface.create({
        font = prompt_box.font,
        text_key = text_line,
        horizontal_alignment = "center",
      })
    end

    prompt_box.num_text_lines = #prompt_box.texts

    -- Decision texts.
    prompt_box.decision_texts = {}
    prompt_box.decision_texts[1] = sol.text_surface.create({
      font = prompt_box.font,
      text_key = decision_text[1],
      horizontal_alignment = "center",
    })
    prompt_box.decision_texts[1].x = width / 2 - 96
    prompt_box.decision_texts[2] = sol.text_surface.create({
      font = prompt_box.font,
      text_key = decision_text[2],
      horizontal_alignment = "center",
    })
    prompt_box.decision_texts[2].x = width / 2 + 96
    prompt_box.num_decisions = 2

    -- Initialize states.
    prompt_box.allow_input = false

    -- Start menu.
    prompt_box.result = result
    prompt_box.cursor_sprite:set_animation("question")
    prompt_box.cursor_sprite:set_frame(0)

    -- Execute callback function when finished.
    prompt_box.callback = function(result) end
  end

  -- Prompt box drawing function.
  function prompt_box:on_draw(screen)
    -- Clean prompt surface.
    self.surface:clear()

    -- Draw window.
    prompt_box.window:draw(self.surface)

    -- Draw text.
    for i = 1, prompt_box.num_text_lines do
      local line = prompt_box.texts[i]
      local line_x 
      local line_y
      line:draw(self.surface, line_x, line_y)
    end
    for i = 1, prompt_box.num_decisions do
      local decision_x = prompt_box.decision_texts[i].x
      local decision_y
      prompt_box.decision_texts[i]:draw(self.surface, decision_x, decision_y)
    end

    self.surface:draw(screen)
  end

  -- Prompt input handling.
  function prompt_box:on_command_pressed(command)
    if self.allow_input then
      if command == "action" then
        if self.cursor_index == 1 then
        elseif self.cursor_index == 2 then
        end
      elseif command == "left" then
        self:set_cursor_position(self.cursor_index - 1)
      elseif command == "right" then
        self:set_cursor_position(self.cursor_index + 1)
      elseif command == "cancel" then
        sol.menu.stop(self)
      else
        -- Input not viable.
        prompt_box.cursor_sprite:set_frame(0)
        audio_manager:play_sound("wrong")
      end

      -- Play cursor sound if cursor moved.
      if command == "left" or command == "right" then
        audio_manager:play_sound("cursor")
      end
    end

    return true
  end

  -- Set cursor index.
  function prompt_box:set_cursor_position(index)
    prompt_box.cursor_index = index
    if prompt_box.cursor_index <= 0 then
      prompt_box.cursor_index = prompt_box.num_decisions
    elseif prompt_box.cursor_index > prompt_box.num_decisions then
      prompt_box.cursor_index = 1
    end
  end

  function prompt_box:move_cursor(x,y)
    prompt_box.cursor_sprite:set_xy(x,y)
  end

  function prompt_box:on_finished()
    prompt_box.callback(prompt_box.result)
  end

  -- Where the prompt box will show content.
  function prompt_box:show(context, text, decision_text, result, callback)
    -- Show prompt box menu.
    sol.menu.start(context, prompt_box, true)

    prompt_box.text = text
    prompt_box.decision_text = decision_text or {
      sol.language.get_string("prompt.yes"),
      sol.language.get_string("prompt.no")
    }

    for i, text_line in ipairs(prompt_box.text) do
      prompt_box.texts[i]:set_text(text_line)
    end
    for i, decision in ipairs(prompt_box.decision_text) do
      prompt_box.decision_texts[i]:set_text(decision)
    end

    prompt_box:set_cursor_position(result)
    prompt_box.callback = callback
  end

  prompt_box:show(context, text, decision_text, result, callback)
end

return prompt_box_builder