-- Import pause menu base script to
-- create settings pause submenu, which
-- inherits from said script.
local pause_menu_base = require("scripts/menus/pause_menu_base")
local audio_manager =   require("scripts/audio_manager")
local math_utils =      require("scripts/lib/math_utils")

local pause_settings = pause_menu_base:new()

local MAX_ROWS = 5

function pause_settings:on_started()
  pause_menu_base.on_started(self)
  
  self:load_setting_resources()
  self:initialize_defaults()
  self:initialize_mode()
end

function pause_settings:load_setting_resources()
  self.title_text = sol.text_surface.create({
    font = self.font,
    horizontal_alignment = "center",
  })
  self.select_cursor = sol.sprite.create("hud/dialog_box_message_cursor")

  -- Setup settings.
  self:load_general_settings()
  self:load_input_settings()
end

--====General settings====
function pause_settings:load_general_settings()
  -- Setup values for general settings.
  self.settings = {
    {
      name = "language",
      values = sol.language.get_languages(),
      initial_value = sol.language.get_language(),
      default_value = sol.language.get_language(),
    },
    {
      name = "video_mode",
      values = sol.video.get_modes(),
      initial_value = sol.video.get_mode(),
      default_value = sol.video.get_mode(),
    },
    {
      name = "music_volume",
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = math_utils.round(sol.audio.get_music_volume()) * 10,
      default_value = 100,
    },
    {
      name = "sound_volume",
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = math_utils.round(sol.audio.get_sound_volume()) * 10,
      default_value = 100,
    },
    {
      name = "input_config",
    }
  }
  -- Setting labels.
  self.setting_name_texts = {}
  self.setting_help_texts = {}
  self.setting_value_texts = {}

  -- Left and right cursor arrow sprites.
  self.left_arrow_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.right_arrow_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
end

--====Input mapping settings====
function pause_settings:load_input_settings()
  -- Input command surface to fit in commands and to handle overflow.
  self.command_surface = nil

  -- Setup configurable actions to extract strings.
  self.actions = {
    "weapon", "subweapon", "up", "left", "right", "down", "action", "pause"
  }
  -- Command names to retrieve input.
  -- NOTE: command: "attack" == "item_1"
  self.commands = {
    "attack", "item_2", "up", "left", "right", "down", "action", "pause"
  }
  -- Setup command text labels.
  self.command_title = sol.text_surface.create({
    font = self.font,
    horizontal_alignment = "left",
  })
  self.config_keyboard_title = sol.text_surface.create({
    font = self.font,
    horizontal_alignment = "left",
  })
  self.config_joypad_title = sol.text_surface.create({
    font = self.font,
    horizontal_alignment = "left",
  })
  -- Command labels.
  self.command_texts = {}
  self.config_keyboard_texts = {}
  self.config_joypad_texts = {}
end

function pause_settings:initialize_defaults()
  self.command_surface = sol.surface.create(256, 152)

  local index = 1
  
  self.cursor_x = 0
  self.cursor_y = 0
  self:set_cursor_position(index)

  -- Setup current mode.
  self.mode = "select"

  -- Setup title label.
  self.title_text:set_text(sol.language.get_string("settings.input_config.title"))
end

function pause_settings:initialize_mode()
  if self.mode == "select" then
    self:initialize_mode_main()
  elseif self.mode == "input" then
    self:initialize_mode_input()
  end
end

function pause_settings:initialize_mode_main()
  -- Flag when setting is being adjusted.
  self.adjust_setting = false

  -- Finished adjusting setting? Unpause and animate cursor.
  self:update_setting_cursor()

  -- Setup labels.
  for i, setting in ipairs(self.settings) do
    self.setting_name_texts[i] = sol.text_surface.create({
      font = self.font,
      text_key = "settings." .. setting.name,
      horizontal_alignment = "left",
    })

    self.setting_help_texts[i] = "settings.description." .. setting.name
    
    if setting.values ~= nil then
      self.setting_value_texts[i] = sol.text_surface.create({
        font = self.font,
        horizontal_alignment = "center",
      })
      self:set_setting_value_text(setting, i)
    end
  end

  self:update_caption()
end

function pause_settings:initialize_mode_input()
  -- Flag when button mapping is being performed.
  self.button_mapping = false

  -- Setup column labels.
  self.command_title:set_text_key("settings.input_config.command")
  self.config_keyboard_title:set_text_key("settings.input_config.keyboard")
  self.config_joypad_title:set_text_key("settings.input_config.gamepad")

  -- Setup command texts.
  for i, action in ipairs(self.actions) do
    self.command_texts[i] = sol.text_surface.create({
      font = self.font,
      text_key = "settings.input_config." .. action,
      horizontal_alignment = "left",
    })
    
    self.config_keyboard_texts[i] = sol.text_surface.create({
      font = self.font,
    })

    self.config_joypad_texts[i] = sol.text_surface.create({
      font = self.font,
    })
  end

  -- Setup keyboard and joypad inputs.
  for i = 1, #self.commands do
    local key_binding = self.game:get_command_keyboard_binding(self.commands[i])
    local button_binding = self.game:get_command_joypad_binding(self.commands[i])

    self.config_keyboard_texts[i]:set_text(key_binding:sub(1,9))
    self.config_joypad_texts[i]:set_text(button_binding:sub(1,9))
  end
end

-- Draw function for general settings.
function pause_settings:draw_settings(screen, width)
  local x, y

  -- Offsets to adjust positioning.
  local offset = 24
  local setting_text_offset = (#self.setting_name_texts + 1) * 2
  
  -- Position to draw setting labels.
  x = offset * 2
  for i, name in ipairs(self.setting_name_texts) do
    y = (i - 1) * (offset + 2) - setting_text_offset + 80
    name:draw(screen, x, y)
  end

  -- Position to draw values.
  x = width - offset * 4 + 6
  for i, value in ipairs(self.setting_value_texts) do
    y = (i - 1) * (offset + 2) - setting_text_offset + 80
    value:draw(screen, x, y) 
  end
end

-- Drawing function for action labels.
function pause_settings:draw_commands(screen)
  local x, y
  local offset = 16

  -- Position to draw column title.
  x = 36
  y = offset * 3
  self.command_title:draw(screen, x, y)

  -- Draw command titles.
  x = 44
  for i, command in ipairs(self.command_texts) do
    y = (i - 1) * (offset * 3)
    command:draw(screen, x, y)
  end
end

-- Drawing function for keyboard input column.
function pause_settings:draw_config_keyboard(screen)
  local x, y
  local offset = 16

  -- Position to draw column title.
  x = 96
  y = offset * 3
  self.config_keyboard_title:draw(screen, x, y)

  -- Draw keyboard inputs.
  x = 108
  for i, key in ipairs(self.config_keyboard_texts) do
    y = (i - 1) * (offset * 3)
    key:draw(screen, x, y)
  end
end

-- Drawing function for joypad input column.
function pause_settings:draw_config_joypad(screen)
  local x, y
  local offset = 16

  -- Position to draw column title.
  x = 192
  y = offset * 3
  self.config_joypad_title:draw(screen, x, y)

  -- Draw joypad inputs.
  x = 200
  for i, input in ipairs(self.config_joypad_texts) do
    y = (offset * 3) + i * (offset + 4)
    input:draw(screen, x, y)
  end
end

-- Drawing function for cursor.
function pause_settings:draw_cursor(screen, width)
  local cx = width / 2
  local x = cx - self.width / 2
  local offset = 24

  -- Move cursor based on index.
  self.cursor_x = x + offset + 12
  self.cursor_y = (self.cursor_index - 1) * (offset + 2) + 74
  
  self.select_cursor:draw(screen)
  self:move_cursor(self.cursor_x, self.cursor_y)
end

-- Drawing function for setting arrow cursors.
function pause_settings:draw_setting_cursors(screen, width)
  local cx = width / 2
  local x = cx - self.width / 2
  local offset = 24

  self.left_arrow_x = x + (width - (offset * 5 + 10))
  self.right_arrow_x = x + (width - (offset * 2))
  self.arrow_y = (self.cursor_index - 1) * (offset + 2) + 72

  self.left_arrow_cursor_sprite:draw(screen, self.left_arrow_x, self.arrow_y)
  self.right_arrow_cursor_sprite:draw(screen, self.right_arrow_x, self.arrow_y)
end

function pause_settings:on_draw(screen)
  local width, height = screen:get_size()
  
  -- Draw pause menu windows.
  self:draw_menu(screen)

  -- Draw caption text at bottom of menu.
  self:draw_caption(screen)

  -- Draw different menu based on mode.
  if self.mode == "select" then
    self:draw_settings(screen, width)
  elseif self.mode == "input" then
    self:draw_commands(screen)
    self:draw_config_keyboard(screen)
    self:draw_config_joypad(screen)
  end

  -- Draw cursor.
  self:draw_cursor(screen, width)

  -- Draw setting arrows.
  self:draw_setting_cursors(screen, width)
end

-- Event called when command is executed.
function pause_settings:on_command_pressed(command)
  local handled = pause_menu_base.on_command_pressed(self, command)
  
  if not handled then
    if command == "action" then
      self:on_input_confirm()
      handled = true
    elseif command == "cancel" then
      self:on_input_cancel()
      handled = true
    elseif command == "up" or command == "down" then
      -- Do not do anything when adjusting settings or inputs.
      if self.adjust_setting == true or 
          self.button_mapping == true then
        return true
      end
      if command == "up" then
        self:set_cursor_position(self.cursor_index - 1)
      elseif command == "down" then
        self:set_cursor_position(self.cursor_index + 1)
      end
      handled = true
    elseif command == "left" or command == "right" then
      -- Change setting when selected.
      if self.adjust_setting == true then
        if command == "left" then
          self:set_value_cursor_position(self.value_index - 1)
        elseif command == "right" then
          self:set_value_cursor_position(self.value_index + 1)
        else
          audio_manager:play_sound("wrong")
        end
      elseif self.adjust_setting == false then
        -- Otherwise, switch to different pause menu.
        if command == "left" then
          self:previous_menu()
        elseif command == "right" then
          self:next_menu()
        end
      end
      handled = true
    end
  
    -- Play sound when handling cursor.
    if command == "up" or command == "down"
        or command == "left" or command == "right" then 
      audio_manager:play_sound("cursor")
    end
  end
  
  return handled
end

-- Event called when player presses a key.
function pause_settings:on_key_pressed(key)
  local handled = pause_menu_base.on_key_pressed(self, key)

  if not handled then
    if key == "b" then
      handled = self:on_command_pressed("cancel")
    end
  end

  return handled
end

-- Event called when player presses a gamepad button.
function pause_settings:on_joypad_button_pressed(button)
  local handled = pause_menu_base.on_joypad_button_pressed(self, button)

  if not handled then
    --TODO: Add cancel option here.
  end

  return handled
end

-- Handle decision based on player choice.
function pause_settings:on_input_confirm()
  if self.mode == "select" then
    if self.cursor_index == #self.settings then
      self:set_mode("input")
    else
      self.adjust_setting = not self.adjust_setting
      self:update_setting_cursor()
      if self.adjust_setting == true then
        self:setup_value_cursor(self.cursor_index)
      end
    end
  elseif self.mode == "input" then
    self.button_mapping = not self.button_mapping
    self:update_setting_cursor()
    if self.button_mapping == true then
      -- TODO: Make button mapping indication.
    end
  end
  audio_manager:play_sound("ok")
end

-- Handle cancel input decision.
function pause_settings:on_input_cancel()
  if self.mode == "select" then
    -- Do not save when setting selected.
    if self.adjust_setting == true then
      self.adjust_setting = not self.adjust_setting
      self:update_setting_cursor()
      self:reset_setting(self.cursor_index)
    else
      self.game:set_paused(false)
    end
  elseif self.mode == "input" then
    if self.button_mapping == true then
      self.button_mapping = not self.button_mapping
      -- TODO: Do not save any settings.
    else
      self:set_mode("select")
    end
  end
  audio_manager:play_sound("hero_hurt")
end

function pause_settings:set_cursor_position(position)
  if self.cursor_index ~= position then
    self.cursor_index = (position - 1) % #self.settings + 1
    self:update_caption()
  end
end

function pause_settings:move_cursor(x, y)
  self.select_cursor:set_xy(x, y)
end

function pause_settings:get_setting(index)
  return self.settings[index]
end

function pause_settings:setup_value_cursor(index)
  local setting = self:get_setting(index)
  local values = setting.values

  for i, value in ipairs(values) do
    if value == setting.initial_value then
      self.value_index = i
      return
    end
  end
end

function pause_settings:set_value_cursor_position(position)
  local setting = self:get_setting(self.cursor_index)
  local num_values = #setting.values
  local max_index = math.max(1, num_values)

  self.value_index = (position - 1) % max_index + 1
  
  -- Update settings.
  self:set_setting_value(setting, setting.values[self.value_index])
  self:set_setting_value_text(setting, self.cursor_index)
end

function pause_settings:set_setting_value(setting, value)
  local current_setting = setting.name

  if current_setting == "language" then
    sol.language.set_language(value)
  elseif current_setting == "video_mode" then
    sol.video.set_mode(value)
  elseif current_setting == "music_volume" then
    sol.audio.set_music_volume(value)
  elseif current_setting == "sound_volume" then
    sol.audio.set_sound_volume(value)
  end

  setting.initial_value = value
end

function pause_settings:set_setting_value_text(setting, index)
  local name = setting.name
  local value_text = self.setting_value_texts[index]

  if name == "language" then
    value_text:set_text(sol.language.get_language_name())
  else
    value_text:set_text(setting.initial_value)
  end
end

-- Reset selected setting to default.
function pause_settings:reset_setting(index)
  local setting = self:get_setting(index)

  self:set_setting_value(setting, setting.default_value)
  self:set_setting_value_text(setting, index)
end

function pause_settings:set_mode(mode)
  self.mode = mode
  self:initialize_mode()
end

-- Show and animate left and right cursors, 
-- indicating setting is being adjusted.
function pause_settings:animate_setting_arrows()
  self.left_arrow_cursor_sprite:set_animation("cursor")
  self.left_arrow_cursor_sprite:set_frame(0)
  self.left_arrow_cursor_sprite:set_direction(0)

  self.right_arrow_cursor_sprite:set_animation("cursor")
  self.right_arrow_cursor_sprite:set_frame(0)
  self.right_arrow_cursor_sprite:set_direction(1)
end

function pause_settings:update_setting_cursor()
  if self.adjust_setting == true then
    self.select_cursor:set_frame(0)
    self.select_cursor:set_paused()
    self:animate_setting_arrows()
  elseif self.adjust_setting == false then
    if self.select_cursor:is_paused() then
      self.select_cursor:set_paused(false)
    end

    self.select_cursor:set_animation("question")
    self.select_cursor:set_frame(0)

    self.left_arrow_cursor_sprite:set_animation("none")
    self.right_arrow_cursor_sprite:set_animation("none")
  end
end

function pause_settings:update_caption()
  if self.mode == "select" then
    local index = self.cursor_index
    self:set_caption_key(self.setting_help_texts[index])
  elseif self.mode == "input" then
    self:set_caption_key("settings.input_config")
  else
    self:set_caption(nil)
  end
end

function pause_settings:on_finished()
  pause_menu_base.on_finished(self)
end

return pause_settings