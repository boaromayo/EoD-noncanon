-- Title screen for game.

local title_screen = {}

-- Load managers.
local audio_manager = require("scripts/audio_manager")
local language_manager = require("scripts/language_manager")

-- Set up global timer to set up fade out.
local timer = nil

-- Play sound when an input is recognized.
local input_press = false

function title_screen:on_started()
  self.mode = 0
  -- Create surface.
  self.surface = sol.surface.create(320, 240)
  -- Delay half a sec before fading into title.
  sol.timer.start(self, 1000, function()
    self:initialize_title()
  end)
  -- Load all sound assets before 
  -- calling the main title screen.
  --sol.audio.preload_sounds()
end

function title_screen:initialize_title()
  self.mode = 1
  -- Set in subtitle font and color.
  local font, font_size = language_manager:get_small_font()
  local white_color = { 255, 255, 255 }
  -- Load in sky background.
  local sea_color = { 77, 166, 220 }
  local dusk_color = { 218, 92, 36 }
  local night_color = { 20, 80, 173 }
  
  -- Setup sky color based on time of day.
  local hour = tonumber(os.date("%H"))
  if hour > 6 and hour < 17 then
    self.background_color = sea_color
  elseif hour >= 17 and hour <= 18 then
    self.background_color = dusk_color
  else
    self.background_color = night_color
  end

  -- Load title screen and other images.
  self.title_logo_img = sol.surface.create("menus/title_logo.png")
  --self.title_logo = sol.sprite.create("menus/title_logo")
  --self.seascape_img = sol.surface.create("menus/title_seascape.png")
  --self.waves_img = sol.surface.create("menus/title_waves.png")
  --self.island_img = sol.surface.create("menus/title_island.png")

  -- Set up version text.
  self.version_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    text = "v0.1.1pa",
    horizontal_alignment = "left",
    vertical_alignment = "bottom",
  })
  -- Set up copyright text.
  self.copyright_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    text_key = "title_screen.copyright",
    horizontal_alignment = "center",
    vertical_alignment = "bottom"
  })
  -- Make extra info toggler.
  self.appear_info_text = false

  -- Set up "press enter/spacebar" text.
  self.press_btn_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    text_key = "title_screen.button",
    horizontal_alignment = "center",
    vertical_alignment = "bottom"
  })
  -- Toggle for "press enter/spacebar" text.
  self.press_btn_loop = false
  self.press_btn_draw = false

  timer = sol.timer.start(self, 1000, function()
    self.surface:fade_in(30)
    self:title()
  end)
end

function title_screen:title()
  self.mode = 2

  -- Start title scene processing.
  timer = sol.timer.start(self, 1000, function()
    audio_manager:play_music("RPG-Battle-Climax_v001")
  end)

  sol.timer.start(self, 2000, function()
    self.appear_info_text = true
  end)

  -- Function to toggle "press enter/spacebar" text.
  local function press_btn_toggle()
    if self.press_btn_loop then 
      self.press_btn_draw = not self.press_btn_draw
      sol.timer.start(self, 500, press_btn_toggle)
    end
  end
  -- Continue calling function.
  self.press_btn_loop = true
  sol.timer.start(self, 3000, press_btn_toggle)

  -- Play a demo, or go to game, once music finished playing.
  sol.timer.start(self, 68000, function()
    self:stop_title()
  end)
end

function title_screen:stop_title()
  -- Turn off input press prompt.
  self.press_btn_loop = false
  self.press_btn_draw = false
  self.appear_info_text = false
  -- Shut off global timer if it exists.
  if timer ~= nil then
    timer:stop()
    timer = nil
    -- Go to select screen.
    self:on_pre_finish()
  end
end

-- Draw method.
function title_screen:on_draw(screen)

  self.surface:clear()

  -- Call drawing methods based on modes.
  if self.mode == 2 then
    self:on_draw_title()
  end

  -- Get size and draw to screen.
  local width, height = screen:get_size()
  self.surface:draw(screen, width / 2 - 160, height / 2 - 120)
end

function title_screen:on_draw_title()
  -- Draw the components.
  self.surface:fill_color(self.background_color)

  self.title_logo_img:draw(self.surface)
  --timer = sol.timer.start(self, 500, function()
  --self.title_logo:set_animation("appear")
    --sol.timer.start(self, 2000, function()
      --self.title_logo:set_animation("still")
    --end)
  --end)
  --self.seascape_img:draw(self.surface)
  --self.waves_img:draw(self.surface)
  --self.island_img:draw(self.surface)
  if self.appear_info_text then
    self.version_text:draw(self.surface, 10, 230)
    self.copyright_text:draw(self.surface, 160, 230)
  end
  if self.press_btn_draw then
    self.press_btn_text:draw(self.surface, 160, 216)
  end
end

-- Events called based on player keyboard input.
function title_screen:on_key_pressed(key)
  
  local handled = false

  -- Handle any key events.
  -- If ESC pressed, quit game.
  if key == "escape" then
    sol.main.exit()
    handled = true
  -- Otherwise, stop title and continue onto pre-finish.
  elseif key == "space" or key == "return" then
    -- Call sound on pre-finish.
    -- TODO: For finished version, only call sound when title finishes transition
    if self.mode == 2 then
      input_press = true
    end
    -- Stop title processing.
    self:stop_title()
    handled = true
  end

  return handled
end

-- Events called based on player gamepad input.
function title_screen:on_joypad_button_pressed(button)

  -- Handle any game button events.
  if button == 1 or button == 9 then
    return self:on_key_pressed("return")
  elseif button == 2 then
    return self:on_key_pressed("space")
  end
end

local function fade_out_image(img, time)
  img:fade_out(time)
end

-- Pre-finish processing for title screen.
function title_screen:on_pre_finish()
  if input_press then
    audio_manager:play_sound("ok")
  end
  audio_manager:stop_music()
  sol.timer.start(self, 500, function()
    fade_out_image(self.title_logo_img, 30)
    sol.timer.start(self, 1000, function()
      sol.menu.stop(self)
    end)
  end)
end

return title_screen