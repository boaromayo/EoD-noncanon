-- Import pause base menu script to 
-- create inventory submenu.
local pause_menu_base = require("scripts/menus/pause_menu_base")
local audio_manager =   require("scripts/audio_manager")

local pause_inventory = pause_menu_base:new()

-- List all item names.
local items = {
    "potion",
    "tonic",
    "august_water",
    "repair_oil",
    "elixyr",
    "ethyr",
    "pandesal",
    "pan_de_choco",
    "macopa",
    "milk",
    "stamina_drink",
    "iron_pill",
  }
  local weapons = {
    "stick",
    "sword",
    "sharptongue",
    "lightbrand",
    "fire_cane",
    "ice_rod",
    "bolt_wand",
    "seal_staff",
  }
  local subweapons = {
    "knife", 
    "seed_bag", 
    "shield", 
    "cross", 
    "hammer",
    "nitro_bottle",
    "lucky_coin",
    "stop_clock",
    "water_tome",
    "fire_tome",
    "wind_tome",
    "earth_tome",
  }
  local armor = {
    "cotton_shirt",
    "jerkin",
    --"thryl shirt",
  }
  local gear_names = {
    --"armor",
    --"boots",
    --"scale",
    "wallet",
  }
  local key_stone_names = {
    "strange_stone",
  }
  -- TODO: Find better way to organize these tables.
  --[[local seeds = {
    "seeds",
    "stones",
  }
  local shields = {
    "pot_lid",
    "shield",
    "gran_shield"
  }
  local crosses = {
    "cross",
    "alum_cross",
  }--]]

function pause_inventory:on_started()
  pause_menu_base.on_started(self)

  self:load_inventory_resources()
  self:initialize_defaults()
end

function pause_inventory:load_inventory_resources()
  self.sword_icon = sol.surface.create("menus/icons.png")
  self.shield_icon = sol.surface.create("menus/icons.png")
  self.life_icon = sol.surface.create("menus/icons.png")
  self.death_icon = sol.surface.create("menus/icons.png")
  self.time_icon = sol.surface.create("menus/icons.png")
  
  self.select_cursor = sol.sprite.create("menus/pause_cursor")
  --self.weapon_cursor = sol.sprite.create("menus/pause_set_cursor")
  --self.subwpn_cursor = sol.sprite.create("menus/pause_set_cursor")
  
  self.time_played_text = sol.text_surface.create({
    font = self.small_font,
    horizontal_alignment = "center",
    vertical_alignment = "middle",
  })
  self.attack_text = sol.text_surface.create({
    font = self.small_font,
    horizontal_alignment = "right",
    vertical_alignment = "middle",
    text = self.game:get_attack(),
  })
  self.defense_text = sol.text_surface.create({
    font = self.small_font,
    horizontal_alignment = "right",
    vertical_alignment = "middle",
    text = self.game:get_defense(),
  })
  self.life_text = sol.text_surface.create({
    font = self.small_font,
    horizontal_alignment = "right",
    vertical_alignment = "middle",
    text = self.game:get_max_life(),
  })
  self.death_text = sol.text_surface.create({
    font = self.small_font,
    horizontal_alignment = "right",
    vertical_alignment = "middle",
    text = 0,
  })
end

local function set_item_variant(game, items, item_name)
  if game:has_item(item_name) then
    local variant = game:get_item(item_name):get_variant()
    return items[variant]
  end
  return items[1]
end

function pause_inventory:initialize_defaults()
  -- Set cursor to first row, first column at weapon subwindow.
  local index = 1
  local row = math.floor(index / 6) + 1
  local col = index % 4 -- The index is on the first row, and there are four columns on the first and second rows.
  self:set_cursor_position(row, col)
  --self:set_key_item_cursor_position(0, 0)
  
  --self.time_icon:set_animation("time")
  --self.sword_icon:set_animation("sword")
  --self.shield_icon:set_animation("shield")
  --self.life_icon:set_animation("heart")
  --self.death_icon:set_animation("skull")
  
  self:setup_items()
end

function pause_inventory:fade_in(duration)
  self.attack_text:fade_in(duration)
  self.defense_text:fade_in(duration)
  self.life_text:fade_in(duration)
  
  -- Fade in weapons, items, and subweapons.
  for index, name in ipairs(weapons) do
    if self.weapon_sprites[index] ~= nil then
      self.weapon_sprites[index]:fade_in(duration)
    end
  end
  
  for index, name in ipairs(items) do
    if self.item_sprites[index] ~= nil then
      self.item_sprites[index]:fade_in(duration)
      
      if self.item_amounts[index] ~= nil then
        self.item_amounts[index]:fade_in(duration)
      end
    end
  end
  
  for index, name in ipairs(subweapons) do
    if self.subweapon_sprites[index] ~= nil then
      self.subweapon_sprites[index]:fade_in(duration)
      
      if self.amounts[index] ~= nil then
        self.amounts[index]:fade_in(duration)
      end
    end
  end
end

-- Update timer text to record total game time.
function pause_inventory:update_time()
  self.time_played_text:set_text(self.game:get_time_played_string())
end

-- Setup all available items.
function pause_inventory:setup_items()
  self.weapon_sprites = {}
  self.item_sprites = {}
  self.subweapon_sprites = {}
  self.gear_sprites = {}
  self.item_amounts = {}
  self.amounts = {}
  
  -- Check subweapons with more than one variant
  -- and assign based on current variant player has.
  local knife_index = 1
  local seed_index = 2
  --local shield_index = 3
  local cross_index = 4
  
  --subweapons[knife_index] = set_item_variant(self.game, knives, "knife")
  --subweapons[seed_index] = set_item_variant(self.game, seeds, "seeds")
  --subweapons[shield_index] = set_item_variant(self.game, shields, "shield")
  --subweapons[cross_index] = set_item_variant(self.game, crosses, "cross")
  
  -- Load in the weapon sprites.
  for index, name in ipairs(weapons) do
    local weapon = self.game:get_item(name)
    if name ~= nil then
      self.weapon_sprites[index] = sol.sprite.create("entities/items")
      self.weapon_sprites[index]:set_animation(name)
    end
      
      -- Check life of weapon.
      -- Show gauge if damaged.
      --[[if weapon:is_damaged() then
        local current = weapon:get_life()
        local max = weapon:get_max_life()
      end--]]
  end
  
  -- Next, the item sprites.
  for index, name in ipairs(items) do
    if name ~= nil then
      local item = self.game:get_item(name)
      self.item_sprites[index] = sol.sprite.create("entities/items")
      self.item_sprites[index]:set_animation(name)
      
      -- All items are consumable, thus, they have an amount.
      -- As a failsafe, leave the has_amount condition in.
      if item:has_amount() then
        local current = item:get_amount()
        self.item_amounts[index] = sol.text_surface.create({
          font = self.small_font,
          horizontal_alignment = "center",
          vertical_alignment = "bottom",
          text = current,
        })
      end
    end
  end
  
  -- Subweapon sprites go here.
  for index, name in ipairs(subweapons) do
    if name ~= nil then
      local subweapon = self.game:get_item(name)
      self.subweapon_sprites[index] = sol.sprite.create("entities/items")
      self.subweapon_sprites[index]:set_animation(name)
    
      -- Add a counter if subweapon has an amount.
      if subweapon:has_amount() then
        local current = subweapon:get_amount()
        self.amounts[index] = sol.text_surface.create({
          font = self.small_font,
          horizontal_alignment = "center",
          vertical_alignment = "bottom",
          text = current,
        })
      end
    end
  end
  
  -- Other equipment/gear (armor, boots, wallet, swimwear).
  for index, name in ipairs(gear_names) do
    if name ~= nil then
      local gear = self.game:get_item(name)
      local variant = gear:get_variant()
    
      if variant > 0 then
        self.gear_sprites[index] = sol.sprite.create("entities/items")
        self.gear_sprites[index]:set_animation(name)
        self.gear_sprites[index]:set_direction(variant - 1)
      end
    end
  end
  
  -- Key stones.
  --[[for index, name in ipairs(stone_names) do
    if name ~= nil then
      local stone = self.game:get_item(name)
      
    end
  end--]]
end

-- Draw the weapons onto inventory screen.
function pause_inventory:draw_weapons(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local menu_x = cx - self.width / 2
  local menu_y = cy - self.height / 2
  local ROW_MAX = 2
  local COL_MAX = 4
  local x
  local y = menu_y + 52
  local index = 0
  local block_size = 20
  local weapon_sprite

  for i=0, ROW_MAX do
    local x_spacing = 27
    local y_spacing = 4
    x = menu_x + 39
    -- When starting at the first row, move y down by 4.
    if i == 0 then
      y = y + y_spacing
    end
    -- Maximum column is minus 1 to prevent drawing overlap into other windows.
    for j=0, COL_MAX-1 do
      -- Note Lua starts its array index at 1, not 0.
      index = index + 1
      weapon_sprite = self.weapon_sprites[index]
      
      -- If a name exists, get the name.
      if weapon_sprite ~= nil then
        local weapon = self.game:get_item(weapons[index])
        -- if the player has the weapon, draw it to menu.
        if weapon:get_variant() > 0 then
          weapon_sprite:draw(screen, x, y)
          -- If the weapon is partly broken, draw gauge.
          --if weapon:get_weapon_life() < weapon:get_maximum_weapon_life() then
          --self.weapon_gauges[index]:draw(screen, x, y + (block_size - 4))
          --end
        end
      end
      x = x + x_spacing + block_size
    end
    y = y + y_spacing + block_size
  end
end

function pause_inventory:draw_items(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local menu_x = cx - self.width / 2
  local menu_y = cy - self.height / 2
  local x
  local y = menu_y + 105
  local ROW_MAX = 4
  local COL_MAX = 3
  local index = 0
  local block_size = 20
  local item_sprite
  
  for i=0, ROW_MAX do
    local x_spacing = 10
    local y_spacing = 7
    x = menu_x + 38
    if i == 0 then
      y = y + 6
    end
    for j=0, COL_MAX-1 do
      -- Bump item index up by 1.
      index = index + 1
      item_sprite = self.item_sprites[index]
      
      if item_sprite ~= nil then
        local item = self.game:get_item(items[index])
        -- If player has at least one of the item, draw it.
        if item:get_variant() > 0 and item:has_amount(1) then
          item_sprite:draw(screen, x, y)
          -- Draw amounts if they exist.
          if self.item_amounts[index] ~= nil then
            self.item_amounts[index]:draw(screen, x + 5, y + 5)
          end
        end
      end
      x = x + x_spacing + block_size
    end
    y = y + y_spacing + block_size
  end
end

function pause_inventory:draw_subweapons(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local menu_x = cx - self.width / 2
  local menu_y = cy - self.height / 2
  local x
  local y = menu_y + 105
  local ROW_MAX = 4
  local COL_MAX = 3
  local index = 0
  local block_size = 20
  local subweapon_sprite
  
  for i=0, ROW_MAX do
    local x_spacing = 5
    local y_spacing = 7
    x = menu_x + 128
    if i == 0 then
      y = y + 6
    end
    for j=0, COL_MAX-1 do
      -- Bump index up by 1 since arrays start at 1.
      index = index + 1
      subweapon_sprite = self.subweapon_sprites[index]
      
      if subweapon_sprite ~= nil then
        local subweapon = self.game:get_item(subweapons[index])
        -- If player has a subweapon, draw it.
        if subweapon:get_variant() > 0 then
          subweapon_sprite:draw(screen, x, y)
          -- Draw amounts if they exist.
          if self.amounts[index] ~= nil then
            self.amounts[index]:draw(screen, x + 5, y + 5)
          end
        end
      end
      x = x + x_spacing + block_size
    end
    y = y + y_spacing + block_size
  end
end

function pause_inventory:draw_gear(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local menu_x = cx - self.width / 2
  local menu_y = cy - self.height / 2
  local x = menu_x + 216
  local y = menu_y + 128
  local COL_MAX = 4
  local index = 0
  local block_size = 20
  local icon_x, icon_y
  local spacing = 20
  local gear_sprite
  
  for j=0, COL_MAX do
    if j == 0 then
      x = x + spacing / 4
    end
    
    index = index + 1
    icon_x = x + block_size / 2
    icon_y = y + block_size / 2

    gear_sprite = self.gear_sprites[index]
    
    if gear_sprite ~= nil then
      local gear = self.game:get_item(gear_names[index])
      -- If player is equipped with any gear, draw it.
      if gear:get_variant() > 0 then
        gear_sprite:draw(screen, icon_x, icon_y)
      end
    end
    x = x + spacing + block_size
  end
end

function pause_inventory:draw_status(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local x = cx - self.width / 2
  local y = cy - self.height / 2

  -- Draw status (attack, defense, life) icons.
  self.sword_icon:draw_region(
    0,
    (self.style - 1) * 8,
    8,
    8,
    screen,
    x + 208,
    y + 108
  )
  self.shield_icon:draw_region(
    8,
    (self.style - 1) * 8,
    8,
    8,
    screen,
    x + 248,
    y + 108
  )
  self.life_icon:draw_region(
    16,
    (self.style - 1) * 8,
    8,
    8,
    screen,
    x + 208,
    y + 124
  )
  self.death_icon:draw_region(
    24,
    (self.style - 1) * 8,
    8,
    8,
    screen,
    x + 248,
    y + 124
  )

  -- Draw stats.
  self.attack_text:draw(screen, x + 240, y + 112)
  self.defense_text:draw(screen, x + 280, y + 112)
  self.life_text:draw(screen, x + 240, y + 128)
  self.death_text:draw(screen, x + 280, y + 128)
end

function pause_inventory:draw_time(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local x = cx - self.width / 2
  local y = cy - self.height / 2

  -- Draw playtime icon.
  self.time_icon:draw_region(
    32,
    (self.style - 1) * 8,
    8,
    8,
    screen,
    x + 208,
    y + 140
  )

  self:update_time()
  self.time_played_text:draw(screen, x + 252, y + 144)
end

function pause_inventory:draw_cursor(screen, width, height)
  local cx = width / 2
  local cy = height / 2
  local x, y = cx - self.width / 2, cy - self.height / 2
  local x_offset = 39
  local y_offset = 54
  local x_spacing, y_spacing
  local row_offset = self.row - 1
  local col_offset = self.column - 1

  if self:is_weapon_area() then
    x_spacing = 47
    y_spacing = 24
  elseif self:is_item_area() then
    x_spacing = 30
    y_spacing = 26
  elseif self:is_subweapon_area() then
    -- TODO: Fix the widths of the item and subweapon windows,
    -- and ensure x-spacing is used once.
    if self.column == 4 then
      x_spacing = 30
    elseif self.column == 5 then
      x_spacing = 29
    else
      x_spacing = 28
    end
    y_spacing = 26
  end
  
  if self:is_weapon_area() then
    self.select_cursor:draw(screen, x + (x_spacing * col_offset) + x_offset,
      y + (y_spacing * row_offset) + y_offset - 2)
  else
    self.select_cursor:draw(screen, x + (x_spacing * col_offset) + x_offset, 
      y + (y_spacing * row_offset) + y_offset)
  end
end

function pause_inventory:on_draw(screen)
  local width, height = screen:get_size()

  -- Draw pause menu windows.
  self:draw_menu(screen)

  -- Draw caption text at bottom of menu.
  self:draw_caption(screen)

  -- Draw inventory content.
  self:draw_weapons(screen, width, height)
  self:draw_items(screen, width, height)
  self:draw_subweapons(screen, width, height)
  self:draw_gear(screen, width, height)

  self:draw_status(screen, width, height)
  self:draw_time(screen, width, height)

  self:draw_cursor(screen, width, height)
end

function pause_inventory:on_command_pressed(command)
  local handled = pause_menu_base.on_command_pressed(self, command)
  
  if not handled then
    if command == "action" then
      if self.game:get_command_effect(command) == nil then
        if self:is_weapon_area() then
          self:assign_weapon()
        elseif self:is_subweapon_area() then
          self:assign_subweapon()
        elseif self:is_item_area() then
          local item = self:get_selected_item()
          item:on_using()
          self:update_caption()
        end
      end
    elseif command == "up" then
      self:set_cursor_position(self.row - 1, self.column)
      handled = true
    elseif command == "left" then
      self:set_cursor_position(self.row, self.column - 1)
      handled = true
    elseif command == "right" then
      self:set_cursor_position(self.row, self.column + 1)
      handled = true
    elseif command == "down" then
      self:set_cursor_position(self.row + 1, self.column)
      handled = true
    end
  
    -- Play sound when handling cursor.
    if command == "up" or command == "down" 
      or command == "left" or command == "right" then
        audio_manager:play_sound("cursor")
    end
  end

  return handled
end

function pause_inventory:set_cursor_position(row, col)
  self.row = row
  self.column = col
  
  local MAX_ROWS = 6
  local MAX_COLUMNS
  
  -- Cycle cursor if it passes row limit.
  if self.row < 1 then
    self.row = MAX_ROWS
  elseif self.row > MAX_ROWS then
    self.row = 1
  end
  
  -- Cycle cursor column-wise based on the category cursor is in.
  if self:is_weapon_area() then
    MAX_COLUMNS = 4
  else
    MAX_COLUMNS = 6
  end
  
  if self.column < 1 then
    self.column = 1
    self:previous_menu()
  elseif self.column > MAX_COLUMNS then
    self.column = MAX_COLUMNS
    self:next_menu()
  end
  self.select_cursor:set_frame(0)
  self:update_caption()
end

function pause_inventory:update_caption()
  local item_name = self:get_item_name(self.row, self.column)
  local item = item_name and self.game:get_item(item_name) or nil
  local variant = item and item:get_variant()
  local amount = (item and item:has_amount()) and item:get_amount() or 0
  local weapon = item and item:has_weapon()
  
  if variant > 0 and (amount > 0 or not item:has_amount()) or weapon then
    self:set_caption_key("inventory." .. item_name)
  elseif variant > 1 and (amount > 0 or not item:has_amount()) then
    self:set_caption_key("inventory." .. item_name .. "." .. variant)
  else
    self:set_caption(nil)
  end
end

-- For debug purposes only.
function pause_inventory:get_index()
  if self:is_weapon_area() then
    return (self.row - 1) * 4 + self.column
  else
    return (self.row - 2) * 6 + self.column + 2
  end
end

function pause_inventory:get_item_name(row, col)
  local item_name
  local index
  
  if self:is_weapon_area() then
    index = (row * 4 - 4) + col
    item_name = weapons[index]
  elseif self:is_item_area() then
    index = ((row - 3) * 3) + col
    item_name = items[index]
  elseif self:is_subweapon_area() then
    index = ((row - 3) * 3) + col - 3
    item_name = subweapons[index]
  end
  
  return item_name
end

function pause_inventory:get_selected_item()
  local item_name = self:get_item_name(self.row, self.column)
  return self.game:get_item(item_name)
end

-- Prepare to equip selected weapon to the player, and place selected
-- weapon in weapon slot, or leftmost slot.
function pause_inventory:assign_weapon()
  local slot = 1
  local weapon = self:get_selected_item()
  
  -- If this weapon can be equipped and player has it, equip it.
  if weapon:is_assignable() and weapon:has_weapon() then
    -- Play equip sound.
    audio_manager:play_sound("ok")
      
    -- Set weapon to the weapon slot.
    self.game:set_item_assigned(slot, weapon)

    -- Update weapon stats.
    if self.game:get_attack() ~= weapon:get_attack() then
      self.game:set_attack(weapon:get_attack())
      self.attack_text:set_text(self.game:get_attack())
    end
  end
end

-- Prepare to equip selected subweapon to player, and place selected
-- subweapon into its respective slot, or rightmost slot.
function pause_inventory:assign_subweapon()
  local slot = 2
  local subweapon = self:get_selected_item()
  
  -- If subweapon can be equipped and player has it, equip it.
  if subweapon:is_assignable() and subweapon:get_variant() > 0 then
    -- Play equip sound.
    audio_manager:play_sound("ok")
    
    -- Set subweapon to the subweapon slot.
    self.game:set_item_assigned(slot, subweapon)
  end
end

function pause_inventory:is_weapon_area()
  return self.row > 0 and self.row <= 2
end

function pause_inventory:is_item_area()
  return self.row > 2 and (self.column > 0 and self.column <= 3)
end

function pause_inventory:is_subweapon_area()
  return self.row > 2 and (self.column > 3 and self.column <= 6)
end

function pause_inventory:on_finished()
  pause_menu_base.on_finished(self)
end

return pause_inventory