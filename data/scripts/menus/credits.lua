-- This menu represents the credits list.
-- The credits will perform both traditional scrolling and fade processes.

local credits = {}

local DEMO = true -- Remember to take out in full version!

local audio_manager = require("scripts/audio_manager")

-- Constants.
local ROLL_SPEED = 30
local FADE_IN_DELAY = 1500
local FADE_OUT_DELAY = 2000
local FADE_IN_TIME = 30
local FADE_OUT_TIME = 30

-- Screen dimensions.
local WIDTH, HEIGHT = sol.video.get_quest_size()

-- Fade-out processing acknowledged.
local is_fading = false

-- Function to get center position of screen.
local function center_screen_position()
  return WIDTH / 2, HEIGHT / 2
end

-- Event called at the start of the menu.
function credits:on_started()
  self:load_resources()
  self:initialize()

  -- Play title music.
  audio_manager:play_music("RPG-Battle-Climax_v001")

  if self.mode == "scroll" then
    self:roll()
  elseif self.mode == "fade" then
    self:fade()
  end
end

-- Load components.
function credits:load_resources()
  local black_color = { 0, 0, 0 }
  local padding = 10
  
  -- Load background color.
  self.background_color = black_color

  -- Load in array of dialog keys.
  self.credits_dialog_list = {
    "_credits.title",
    "_credits.story",
    "_credits.programming",
    "_credits.sprites",
    "_credits.font",
    "_credits.music",
    "_credits.sound",
    "_credits.thanks",
    "_credits.dedication",
  }
  
  -- Credits drawing surfaces.
  self.credits_surface = nil
  self.credits_line_surfaces = {}

  -- Height for scrolling credits.
  self.credits_height = 0

  -- Load current credit dialog.
  --self.current_credit = self.credits_dialog_list[1]

  -- Select mode of presenting credits.
  self.mode = "scroll"

  -- Main surface and callback function.
  self.surface = sol.surface.create(sol.video.get_quest_size())
  self.callback = nil
end

-- Initialize resources.
function credits:initialize()
  local font = "lunchds"
  local font_size = 16
  local padding = 10
  local text = ""

  -- Take text from dialog table, and make text surface line-by-line.
  for i, credit in ipairs(self.credits_dialog_list) do
    text = sol.language.get_dialog(credit).text
    for line in text:gmatch("([^\n]*)\n") do
      self.credits_line_surfaces[#self.credits_line_surfaces + 1] = sol.text_surface.create({
      font = font,
      font_size = font_size,
      text = line,
      horizontal_alignment = "center",
    })
    end
    -- Add extra lines of padding.
    for j = 1, padding do
      self.credits_line_surfaces[#self.credits_line_surfaces + 1] = sol.text_surface.create({
        font = font,
        font_size = font_size,
        horizontal_alignment = "center",
      })
    end
  end

  self.credits_height = #self.credits_line_surfaces * font_size + (padding + 6)
  self.credits_surface = sol.surface.create(WIDTH, self.credits_height)
end

-- Draw credits.
function credits:on_draw_credits()
  local width = WIDTH / 2
  local height = 0
  local offset = 16
  
  for i, line in ipairs(self.credits_line_surfaces) do
    height = 16 * i + offset
    line:draw(self.credits_surface, width, height)
  end

  self.credits_surface:draw(self.surface, 0, HEIGHT)
end

-- MAIN DRAWING EVENT.
function credits:on_draw(screen)    
  local screen_width, screen_height = screen:get_size()
  local cx = screen_width / 2
  local cy = screen_height / 2
  local x, y = cx - (WIDTH / 2), cy - (HEIGHT / 2)

  self.surface:clear()
  self.surface:fill_color(self.background_color)

  -- Draw credit components.
  self:on_draw_credits()

  self.surface:draw(screen, x, y)
end

-- Event called when player presses a key.
function credits:on_key_pressed(key)

  local handled = false

  if key == "escape" then
    sol.main.exit()
    handled = true
  elseif key == "return" or key == "space" then
    -- Stop credits.
    credits:stop()
    handled = true
  end
  return handled
end

-- Set movement of credits.
local function credit_movement()
  local movement = sol.movement.create("straight")
  movement:set_speed(ROLL_SPEED)
  movement:set_smooth(true)
  -- Move credits up.
  movement:set_angle(math.pi / 2)
  movement:set_max_distance(credits.credits_height)
  return movement
end

-- Call to roll credits.
function credits:roll()
  local movement = credit_movement()
  movement:start(self.credits_surface, function()
    if self.callback ~= nil then
      self.callback()
    else
      credits:stop()
    end
  end)
  
  -- Once at "END" credit, stop rolling, and slowly fade out.
  --[[if self.current_credit == self.credits_dialog_list[#self.credits_dialog_list] then
    local _, y, _ = self.current_credit:get_position()
    local _, cy = center_screen_position()
    local offset = 16
    if y < cy - offset then
      movement:stop()
      sol.timer.start(self, FADE_OUT_DELAY, function()
        --self.text_surface:fade_out(FADE_OUT_TIME)
        sol.timer.start(self, 1000, function()
          if self.callback ~= nil then
            self.callback()
          end
        end)
      end)
    end
  end--]]

  -- Play file select music if title music finished.
  sol.timer.start(self, 65000, function()
    audio_manager:play_music("Peaceful-Evening_Looping")
  end)
end

-- Call to fade-in, fade-out credits.
function credits:fade()
  --TODO: Create credits to fade out to.
end

-- Abrupt stop to credits.
function credits:stop()
  if is_fading then
    return
  end
  is_fading = true

  if self.credits_surface ~= nil then
    self.credits_surface:fade_out(FADE_OUT_TIME)
  end

  sol.timer.start(self, 2000, function()
    sol.menu.stop(self)
  end)
end

-- Event called once the menu is finished.
function credits:on_finished()
  audio_manager:stop_music()
  if DEMO then
    local demo_screen = require("scripts/menus/demo_screen")
    -- Go back to demo screen.
    sol.menu.start(sol.main, demo_screen)
  end
end

return credits