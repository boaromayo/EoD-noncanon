local input_config_menu = {}

local ui_manager = require("scripts/menus/lib/ui_manager")
local audio_manager = require("scripts/audio_manager")
local language_manager = require("scripts/language_manager")

local window_width = 264
local window_height = 224

local ROW_MAX = 5

-- TODO: Will need Solarus 2.0 to continue work on master input remapping menu. For now, input remapping will only be handled in the pause menu.
function input_config_menu:on_started()

  self:load_resources()
  self:initialize()
  -- Setup menu based on mode.
  self:initialize_modes()
end

function input_config_menu:load_resources()
  -- TODO: Make a sky background manager for title.
  -- Load in sky background.
  -- Background colors.
  local sea_color = { 77, 166, 220 }
  local dusk_color = { 218, 92, 36 }
  local night_color = { 20, 80, 173 }

  -- Setup sky color based on time of day.
  local hour = tonumber(os.date("%H"))
  if hour > 6 and hour < 17 then
    self.background_color = sea_color
  elseif hour >= 17 and hour <= 18 then
    self.background_color = dusk_color
  else
    self.background_color = night_color
  end

  -- Setup font. Change based on window style.
  local font = language_manager:get_text_font()

  -- Main drawing surface.
  self.surface = sol.surface.create(sol.video.get_quest_size())

  -- Window to draw contents.
  self.window = nil

  -- Command window surface. Needed to handle any text overflow.
  self.command_surface = nil
  self.command_y_visible = 0

  -- Setup configurable action strings.
  self.actions = {
    "weapon", "subweapon", "up", "left", "right", "down", "action", "pause"
  }
  -- Command action names to retrieve input. 
  -- NOTE: command "attack" == "item_1"
  self.config_actions = {
    "attack", "item_2", "up", "left", "right", "down", "action", "pause"
  }

  -- Setup text labels.
  self.config_text = {}
  self.title_text = sol.text_surface.create({
    font = font,
  })

  -- Command column title labels.
  self.command_title = sol.text_surface.create({
    font = font,
    horizontal_alignment = "left",
  })
  self.config_keyboard_title = sol.text_surface.create({
    font = font,
    horizontal_alignment = "left",
  })
  self.config_gamepad_title = sol.text_surface.create({
    font = font,
    horizontal_alignment = "left",
  })

  -- Command labels.
  self.command_text_names = {}
  self.config_keyboard_text_names = {}
  self.config_gamepad_text_names = {}

  -- Setup cursor sprites.
  self.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  -- TODO: Cursor sprites need finishing.
end

function input_config_menu:initialize()
  -- Setup position for window.
  local screen_width, screen_height = self.surface:get_size()
  local x = (screen_width - window_width) / 2
  local y = (screen_height - window_height) / 2

  self.window_position = { x = x, y = y }

  self.window = ui_manager:create_window(self.window_position.x, self.window_position.y, window_width, window_height)

  self.command_surface = sol.surface.create(window_width - 8, window_height - 60)

  -- Set cursor position to first item in menu.
  local index = 1

  -- Set current mode of menu.
  self.mode = "command"

  -- Setup title label.
  self.title_text:set_text(sol.language.get_string("settings.input_config.title"))

  -- Setup column title labels.
  self.command_title:set_text_key("settings.input_config.command")
  self.config_keyboard_title:set_text_key("settings.input_config.keyboard")
  self.config_gamepad_title:set_text_key("settings.input_config.gamepad")

  -- Setup command text.
  for i, action in ipairs(self.actions) do
    self.command_text_names[#self.command_text_names + 1] = sol.text_surface.create({
      font = language_manager:get_text_font(),
      text_key = "settings.input_config." .. action,
      horizontal_alignment = "left",
    })
  end

  self:set_cursor_position(index)
end

function input_config_menu:initialize_modes()
  if self.mode == "command" then
    self:initialize_command_mode()
  elseif self.mode == "input_map" then
    self:initialize_mapping_mode()
  end
end

function input_config_menu:initialize_command_mode()
  -- Animate main cursor.
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)
end

function input_config_menu:initialize_mapping_mode()
  -- Do not any trigger any input, just re-map the controls.
  -- Draw cursor to indicate command being worked on.
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)
  -- TODO: Start working on the remap settings.
end

-- Drawing function for menu.
function input_config_menu:draw_menu()
  -- Offsets.
  local offset = 24
  local cursor_y_offset = 80

  -- Draw window.
  self.window:draw(self.surface)

  -- Draw title label text.
  local title_width = self.title_text:get_size()
  local title_x = (window_width / 2) - (title_width / 3)
  local title_y = self.window_position.y + offset
  self.title_text:draw(self.surface, title_x, title_y)

  -- Draw columns.
  self:draw_commands()
  self:draw_config_keyboard()
  self:draw_config_gamepad()

  -- Draw subsurface to main surface.
  self.command_surface:draw_region(0, self.command_y_visible, window_width, window_height, self.surface)

  -- Move cursor based on position.
  self.cursor_sprite:draw(self.surface)
end

-- Drawing function for command column.
function input_config_menu:draw_commands()
  -- Label positions.
  local label_x, label_y
  local offset = 16

  -- Draw column title.
  label_x = 24
  label_y = offset + 8
  self.command_title:draw(self.surface, label_x, label_y)

  -- Draw actions.
  for i, action in ipairs(self.actions) do
    label_y = i * 6 + offset
    action:draw(self.command_surface, label_x, label_y)
  end
end

-- Drawing function for keyboard input column.
function input_config_menu:draw_config_keyboard()
end

-- Drawing function for gamepad input column.
function input_config_menu:draw_config_gamepad()
end

-- Main drawing function.
function input_config_menu:on_draw(screen)
  local screen_width, screen_height = screen:get_size()
  local width, height = sol.video.get_quest_size()
  local cx = screen_width / 2
  local cy = screen_height / 2
  local x, y = cx - (width / 2), cy - (height / 2)
  
  -- Draw menu on-screen.
  self.surface:clear()
  if sol.main.get_game() == nil then
    -- Assuming we're in the title menu, fill with background.
    self.surface:fill_color(self.background_color)
  end
  
  self:draw_menu()

  self.surface:draw(screen, x, y)
end

-- Event called when command is entered.
function input_config_menu:on_command_pressed(command)
  local handled = false

  if command == "action" or command == "attack" then
    self:on_input_decision()
    handled = true
  elseif command == "cancel" then
    self:on_input_cancel()
    handled = true
  elseif command == "up" or command == "left"
      or command == "right" or command == "down" then
    -- TODO: More input configuration here.
    if self.mode == "input_map" then
      -- Do not trigger any commands, only remap controls.
    else
      if command == "up" or command == "left" then
        self:set_cursor_position(self.cursor_index - 1)
      elseif command == "down" or command == "right" then
        self:set_cursor_position(self.cursor_index + 1)
      end
    end
    audio_manager:play_sound("cursor")
    handled = true
  end

  return handled
end

-- Event called when key is pressed.
function input_config_menu:on_key_pressed(key)
  local handled = false

  if key == "escape" then
    if sol.main.get_game() == nil then
      sol.main.exit()
      handled = true
    else
      handled = self:on_command_pressed("cancel")
    end
  elseif key == "return" or key == "space" then
    handled = self:on_command_pressed("action")
  elseif key == "b" then
    handled = self:on_command_pressed("cancel")
  end

  return handled
end

-- Event called based on decision entered.
function input_config_menu:on_input_decision()
  audio_manager:play_sound("ok")
end

-- Event called for handling decisions when input cancelled.
function input_config_menu:on_input_cancel()
  if self.mode == "command" then
    sol.menu.stop(self)
  else
    self:set_mode("command")
  end
  audio_manager:play_sound("hero_hurt")
end

-- Set menu's mode.
function input_config_menu:set_mode(mode)
  self.mode = mode
  self:initialize_modes()
end

-- Set menu cursor's index.
function input_config_menu:set_cursor_position(index)
  local max_index = math.min(#self.command_text_names, ROW_MAX)
  self.cursor_index = (index - 1) % max_index + 1

  -- Reset cursor animation when changed.
  self.cursor_sprite:set_frame(0)
end

return input_config_menu