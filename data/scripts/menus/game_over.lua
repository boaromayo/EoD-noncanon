-- Script for the game over scene.

-- There are different phases of the game over scene,
-- and some phases are only played depending 
-- on certain conditions:

-- "start_revive": if player has a certain item to revive them, play scene
-- "setup_resume": after revival, prepare to resume game
-- "resume": resume game
-- "init_resources": where gameover menu and components are setup
-- "start_animation": start animation for player falling
-- "fade_to_gameover": fade out to gameover scene
-- "start_title": popup "Game Over" title and play music with scene
-- "setup_menu": setup menu choices
-- "menu": decide on whether to continue or not
-- "finished": if player decided, continue or go to menu

local game = ...

-- Setup game over scene menu.
local function setup_gameover_menu(game)
  local audio_manager = require("scripts/audio_manager")

  -- Game over is already setup, don't repeat setting it up.
	if game.gameover_menu ~= nil then
	 return
	end

  -- Setup variables for game over scene.
  local gameover_menu = {}
  game.gameover_menu = gameover_menu

  local black_color = { 0, 0, 0 }
  local white_color = { 255, 255, 255 }
  local choices = { "Continue", "Exit" }

  gameover_menu.music = nil
  gameover_menu.surface = nil
  gameover_menu.background = nil
  gameover_menu.game_over_title_img = nil
  gameover_menu.game_over_img = nil
  gameover_menu.menu_choices_text = {}
  --gameover_menu.menu_choices_img = {}
  gameover_menu.fade_out_sprite = nil
  gameover_menu.fallen_sprite = nil
  gameover_menu.fallen_x = 0
  gameover_menu.fallen_y = 0
  gameover_menu.cursor_position = 0
  gameover_menu.cursor_sprite = nil
  gameover_menu.phase = nil -- Current phase of game over scene. TODO: Change var to integer.

  -- Add on gameover menu to game.
  function game:on_game_over_started()
    sol.menu.start(game:get_map(), gameover_menu)
  end

  function gameover_menu:on_started()
    local hero = game:get_hero()
    if hero ~= nil then
      -- Hide hero sprite.
	    hero:set_visible(false)
      -- Hide HUD.
      game:hide_hud()
      -- Setup game over resources.
      self:set_phase("init_gameover")
    end
  end

  function gameover_menu:branch_phase()
    if self.phase == "init_gameover" then
      self:initialize_resources()
    elseif self.phase == "start_animation" then
      self:start_animation()
    elseif self.phase == "fade_to_gameover" then
      self:initialize_fade_to_gameover()
    elseif self.phase == "start_title" then
      self:start_title()
    elseif self.phase == "setup_menu" then
      self:initialize_menu()
    elseif self.phase == "menu" then
      self:start_menu()
    elseif self.phase == "finished" then
      self:on_pre_decision()
    end
  end

  -- Switch menu phase.
  function gameover_menu:set_phase(phase)
    self.phase = phase
    self:branch_phase()
  end

  -- Setup menu and resources.
  function gameover_menu:initialize_resources()
    -- Setup images and sprites.
    local font = "lunchds"
    local font_size = 16
    local screen_width, screen_height = sol.video.get_quest_size()

    self.music = sol.audio.get_music()
    self.surface = sol.surface.create(screen_width, screen_height)
    self.background = sol.surface.create(screen_width, screen_height)
    self.background:set_opacity(0)
    for i, choice in ipairs(choices) do
      if not self.menu_choices_text[i] then
        self.menu_choices_text[#self.menu_choices_text + 1] = sol.text_surface.create({
          font = font,
          font_size = font_size,
          color = white_color,
          text = choice,
          horizontal_alignment = "center",
        })
      end
    end
    --self.game_over_title_img = sol.sprite.create("gameover_title")
    --self.game_over_img = sol.surface.create(--[["gameover_gravestone.png"--]])
    --[[self.menu_choices_img = {
      "Continue" = sol.sprite_create("gameover_continue"),
      "Exit" = sol.sprite.create("gameover_exit")
    }--]]

    -- Setup cursor.
    self.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
    self.cursor_sprite:set_animation("none")

    -- Setup player sprite.
    local gender = game:get_value("den_gender")
    local camera_x, camera_y = game:get_map():get_camera():get_position()
    local hero_x, hero_y = game:get_hero():get_position()
    self.fallen_x, self.fallen_y = hero_x - camera_x, hero_y - camera_y
    self.fallen_sprite = sol.sprite.create("hero/den_" .. gender)
    self.fallen_sprite:set_paused(true)
    self.fallen_sprite:set_xy(self.fallen_x, self.fallen_y)

    sol.timer.start(self, 500, function()
      self:set_phase("start_animation")
    end)
  end

  -- Start animation of hero falling.
  function gameover_menu:start_animation()
    local RIGHT = 0
    -- Fade map away.
    self.background:fill_color(black_color)
    self.background:fade_in(30, function()
      -- Stop map music.
      sol.audio.stop_music()
      -- Collapse hero.
      audio_manager:play_sound("hero_dying")
      self.fallen_sprite:set_paused(false)
      self.fallen_sprite:set_animation("dying")
      self.fallen_sprite:set_direction(RIGHT)
      sol.timer.start(self, 1000, function()
        audio_manager:play_sound("item_fall")
        self.fallen_sprite:set_animation("dead")
        sol.timer.start(self, 2000, function()
          self:set_phase("fade_to_gameover")
        end)
      end)
    end)
  end

  function gameover_menu:initialize_fade_to_gameover()
    -- TODO: Add gameover music.
    -- Move fallen hero to center.
    local width, height = self.surface:get_size()
    local center_x = width / 2
    local center_y = height / 2
    local movement = sol.movement.create("target")
    movement:set_target(center_x, center_y)
    movement:set_speed(40)
    movement:set_ignore_obstacles(true)
    movement:set_smooth(false)
    movement:start(self.fallen_sprite, function()
      -- Then, fade out fallen hero.
      sol.timer.start(self, 400, function()
        self.fallen_sprite:fade_out(30, function()
          sol.timer.start(self, 1000, function() 
            self:set_phase("start_title")
          end)
        end)
      end)
    end)
  end

  function gameover_menu:start_title()
    -- Fade in title.
    --self.game_over_title_img:fade_in(30)
    sol.timer.start(self, 100, function()
      --self:game_over_img:fade_in(30)
      sol.timer.start(self, 1000, function()
        self:set_phase("setup_menu")
      end)
    end)
  end

  function gameover_menu:initialize_menu()
    -- Fade in decisions. 
    -- The player cannot save when it's game over. Save early and often.
    self.menu_choices_text[1]:fade_in(20, function()
      sol.timer.start(self, 750, function()
        self.menu_choices_text[2]:fade_in(20, function()
          self:set_phase("menu")
        end)
      end)
    end)
    --self.menu_choices_img["Continue"]:fade_in(30)
    --[[
    sol.timer.start(self, 1000, function()
      self.menu_choices_img["Quit"]:fade_in(30)
    end)--]]
  end

  function gameover_menu:start_menu()
    -- Play file select music.
    sol.audio.play_music("Peaceful-Evening_Looping")
    -- Make cursor visible.
    self.cursor_position = 1
    self.cursor_sprite:set_animation("question")
    self.cursor_sprite:set_frame(0)
  end

  -- Set cursor index position based on decision (Continue or Quit). 
  -- This does not redraw/move the cursor.
  function gameover_menu:set_cursor_position(index)
    self.cursor_position = index
  end

  function gameover_menu:move_cursor(x, y)
    self.cursor_sprite:set_xy(x, y)
  end

  -- Draw decision components.
  function gameover_menu:on_draw_menu()
    local width, height = self.surface:get_size()
    --self.game_over_title_img:draw(self.surface)
    --self.game_over_img:draw(self.surface)

    self.menu_choices_text[1]:draw(self.surface, width / 2, height / 2 - 20)
    self.menu_choices_text[2]:draw(self.surface, width / 2, height / 2 + 4)

    self.cursor_sprite:draw(self.surface)
    -- Update cursor position.
    if self.cursor_position == 1 then
      self:move_cursor(width / 2 - 48, height / 2 - 16)
    elseif self.cursor_position == 2 then
      self:move_cursor(width / 2 - 30, height / 2 + 9)
    end
  end

  -- Main drawing function.
  function gameover_menu:on_draw(screen)
    -- Clear main surface.
    self.surface:clear()

    self.background:draw(self.surface)
    self.fallen_sprite:draw(self.surface)

    if self.phase == "start_title" then
      --self.game_over_title_img:draw()
      --self.game_over_img:draw()
    elseif self.phase == "setup_menu" or 
        self.phase == "menu" or
        self.phase == "finished" then
      self:on_draw_menu()
    end

    local width, height = screen:get_size()
    self.surface:draw(screen, width / 2 - 160, height / 2 - 120)
  end

  -- Event to handle keyboard input.
  function gameover_menu:on_key_pressed(key)

    local handled = false

    -- Do not allow input when game over transitions are occurring.
    if self.phase ~= "menu" then
      return false
    end

    if self.phase == "menu" then
      if key == "up" or key == "left" then
        handled = self:on_command_pressed("up")
      elseif key == "down" or key == "right" then
        handled = self:on_command_pressed("down")
      elseif key == "return" or key == "space" then
        handled = self:on_command_pressed("action")
      end
    end

    return handled
  end

  -- Event to handle general input.
  function gameover_menu:on_command_pressed(command)

    local handled = false

    -- Do not allow input when game over transitions are occurring.
    if self.phase ~= "menu" then
      return false
    end

    if self.phase == "menu" then
      if command == "up" or command == "down" then
        self:set_cursor_position(3 - self.cursor_position)
        handled = true
      elseif command == "action" then
        audio_manager:play_sound("ok")
        self.cursor_sprite:set_animation("none")
        -- TODO: Flash text of selected decision.
        sol.timer.start(self, 1000, function()
          self:set_phase("finished")
        end)
        handled = true
      end
      -- Play cursor sound.
      if command == "up" or command == "down" then
        audio_manager:play_sound("cursor")
      end
    end

    return handled
  end

  -- Fade out decision texts.
  function gameover_menu:on_pre_decision()
    sol.timer.start(self, 100, function()
      self.menu_choices_text[1]:fade_out(20)
      sol.timer.start(self, 750, function()
        self.menu_choices_text[2]:fade_out(20, function() 
          self:on_decision()
        end)
      end)
    end)
  end

  -- End game over menu, and proceed based on decision.
  function gameover_menu:on_decision()
    
    if self.cursor_position == 1 then
      -- Restart game from last save.
      game:start()
    elseif self.cursor_position == 2 then
      -- Reset game.
      sol.main.reset()
    end
  end
end

function game:initialize_game_over()
  setup_gameover_menu(game)
end