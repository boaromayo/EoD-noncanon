-- Set up pause menu scene.
-- Credit to Christopho and Solarus team for the original
-- version of the pause menu script.
local game = ...

local pause_main_menu = {}

-- Setup the menus for the pause menu scene.
local inventory_menu = require("scripts/menus/pause_inventory")
local settings_menu = require("scripts/menus/pause_settings")
--local map_menu = require("scripts/menus/pause_map")

-- Setup the pause screen if menu disabled. Note the pause screen
-- is independent of the pause menus.
local pause_screen = require("scripts/menus/pause_screen")

local function setup_pause_menus(game)
  game.pause_menus = {
    inventory_menu:new(game),
    settings_menu:new(game),
    --map_menu:new(game)
  }
end

local function clear_pause_menus(game)
  game.pause_menus = {}
end

local function check_pause_index(game, index)
  if index > #game.pause_menus then
    index = 1
  elseif index <= 0 then
    index = #game.pause_menus
  end
  
  return index
end

function game:initialize_pause_menu()

  -- Ignore if pause menu has been created.
  if game.pause_menu ~= nil then
    return
  end

  game.pause_menu = pause_main_menu
end

function game:quit_pause_menu()
  game.pause_menu = nil
end

function pause_main_menu:on_started()
  if game:is_pause_menu_enabled() then
    setup_pause_menus(game)
    local index = game:get_value("pause_last_menu") or 1
    game:set_value("pause_last_menu", index)
  
    -- Check counter before opening menu.
    index = check_pause_index(game, index)

    -- Open pause menu.
    sol.menu.start(self, game.pause_menus[index])
  else
    -- Open pause screen.
    sol.menu.start(self, pause_screen, false)
  end
end

-- Close pause menu, set new index value, 
-- and clear table.
function pause_main_menu:on_finished()
  if game:is_pause_menu_enabled() then
    local index = game:get_value("pause_last_menu")
    sol.menu.stop(game.pause_menus[index])
  
    game:set_value("pause_last_menu", index)
    clear_pause_menus(game)
  else
    -- Close pause screen if menu disabled.
    sol.menu.stop(pause_screen)
  end
end

function pause_main_menu:open()
  sol.menu.start(game, pause_main_menu, false)
end

function pause_main_menu:close()
  sol.menu.stop(pause_main_menu)
end

function game:on_paused()
  pause_main_menu:open()
end

function game:on_unpaused()
  pause_main_menu:close()
end

-- Toggle pause menu.
function game:on_pause_menu_enabled(enabled)
  if enabled ~= self.pause_menu_enabled then
    self.pause_menu_enabled = enabled
  end
end

function game:enable_pause_menu()
  self:on_pause_menu_enabled(true)
end

function game:disable_pause_menu()
  self:on_pause_menu_enabled(false)
end

-- Check if pause is enabled or not.
function game:is_pause_menu_enabled()
  return self.pause_menu_enabled
end