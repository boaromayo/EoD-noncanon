-- File selection screen.

-- Load game manager.
local game_manager = require("scripts/game_manager")

local file_screen = {}

local MAX_SAVEFILES = 3
local COPY_INDEX = MAX_SAVEFILES + 1
local ERASE_INDEX = MAX_SAVEFILES + 2
local SETTINGS_INDEX = MAX_SAVEFILES + 3
local RETURN_INDEX = MAX_SAVEFILES + 1

local fade_delay = 30

function file_screen:on_started()
  self:initialize_layout()
  self:initialize_defaults()
  
  -- Start menu.
  self:initialize()

  -- Fade into file screen.
  self:fade_in_components(fade_delay)

  -- Read saves after fade-in to prevent 
  -- text from appearing before the windows.
  self:read_savefiles()
    
  sol.timer.start(self, 1000, function()
    sol.audio.play_music("peaceful_evening")
  end)
end

-------------------------------------
-- Initialization functions. --
-------------------------------------
function file_screen:initialize_layout()
  local sea_color = { 77, 166, 220 }
  local dusk_color = { 218, 92, 36 }
  local night_color = { 20, 80, 173 }
  
  -- Create graphics.
  self.surface = sol.surface.create(sol.video.get_quest_size())
  
  self.select_box = {
    width = 264,
    height = 192,
    surface = nil,
    position = nil
  }
  self.settings_box = {
    width = 264,
    height = 216,
    surface = nil,
    position = nil
  }
  self.option_box = {
    width = 264,
    height = 32,
    surface = nil,
    position = nil
  }
  self.confirm_box = {
    width = 256,
    height = 84,
    surface = nil,
    position = nil
  }

  self.select_box.surface = sol.surface.create("menus/file_screen_box.png")
  self.settings_box.surface = sol.surface.create("menus/settings_box.png")
  self.option_box.surface = sol.surface.create("menus/file_screen_mini_box.png")
  self.confirm_box.surface = sol.surface.create("hud/dialog_box.png")
  
  local hour = tonumber(os.date("%H"))
  if hour > 6 and hour < 17 then
    self.background_color = sea_color
  elseif hour >= 17 and hour <= 18 then
    self.background_color = dusk_color
  else
    self.background_color = night_color
  end

  -- Load fonts.
  local menu_font = "lunchds_blue"
  local status_font = "8_bit"

  -- Create texts for menu.
  self.title_text = sol.text_surface.create({
    horizontal_alignment = "center"
  })
  self.copy_text = sol.text_surface.create({
    font = menu_font
  })
  self.erase_text = sol.text_surface.create({
    font = menu_font
  })
  self.settings_text = sol.text_surface.create({
    font = menu_font
  })
  self.return_text = sol.text_surface.create({
    font = menu_font
  })
  
  -- Text for savefiles.
  self.savefile_text = {}
  for i = 1, MAX_SAVEFILES do
    self.savefile_text[i] = sol.text_surface.create({
      font = menu_font
    })
  end

  -- Text for confirm dialog.
  self.question_text = sol.text_surface.create({
    font = menu_font
  })
  self.choice1_text = sol.text_surface.create({
    font = menu_font
  })
  self.choice2_text = sol.text_surface.create({
    font = menu_font
  })

  -- Load character and character selection sprites.
  self.boy_sprite = sol.sprite.create("hero/den_boy")
  self.girl_sprite = sol.sprite.create("hero/den_girl")
  
  --self.boy_profile = sol.sprite.create("dialogs/den_boy_profile")
  --self.girl_profile = sol.sprite.create("dialogs/den_girl_profile")
  --self.spotlight = sol.sprite.create("things/spotlight")
end

function file_screen:initialize_defaults()
  -- Toggle to allow user input.
  self.allow_input = false
  -- Show whether confirm dialog visible or not.
  self.dialog = false
  -- Offset value.
  self.offset = 24
  -- Table for saved games.
  self.savefiles = {}
  
  -- Set up positions for windows.
  local window_w, window_h = sol.video.get_quest_size()
  local box_w = self.select_box.width
  local box_h = self.select_box.height
  local x = (window_w - box_w) / 2
  local y = (window_h - (box_h + self.offset + 8)) / 2

  self.select_box.position = { x = x, y = y }
  self.settings_box.position = { x = x, y = y }
  self.option_box.position = { x = x, y = y + box_h }
  self.confirm_box.position = { x = x, y = window_h / 2 }

  -- Cursor.
  self.cursor = 1
  self.cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.allow_input = false
  self:set_cursor_position(self.cursor)

  -- Mode to transfer to subscenes (settings, copy file, erase file).
  self.mode = "file_select"
end

-- Switch menu modes.
function file_screen:set_mode(mode)
  if self.mode ~= mode then
    self.mode = mode
    self:initialize()
  end
end

-- Initialize file screen based on mode.
function file_screen:initialize()
  if self.mode == "file_select" then
    self:initialize_file_select()
  elseif self.mode == "character" then
    self:initialize_character_select()
  elseif self.mode == "copy_file" then
    self:initialize_copy_file_select()
  elseif self.mode == "erase_file" then
    self:initialize_erase_file_select()
  elseif self.mode == "settings" then
    self:initialize_settings()
  elseif self.mode == "confirm" then
    self:initialize_confirm_dialog()
  else
    error("Unable to initialize file screen. Mode not set.")
  end
end

------------------------
-- File screen modes. --
------------------------
function file_screen:initialize_file_select()
  -- Prep the file selection screen (savegames, cursor).
  self:set_cursor_position(1)
  self.title_text:set_font("lunchds_blue")
  self.title_text:set_text_key("file_screen.file_select.title")
  self.copy_text:set_text_key("file_screen.file_select.copy")
  self.erase_text:set_text_key("file_screen.file_select.erase")
  self.settings_text:set_text_key("file_screen.file_select.settings")
  
  if not self.allow_input then
    sol.timer.start(self, 1000, function()
      self.cursor_sprite:set_animation("question")
      self.cursor_sprite:set_frame(0)
      self.allow_input = true
    end)
  end
end

function file_screen:initialize_copy_file_select()
  -- Prepare to copy file selection.
  -- Setup title for copy file.
  self.title_text:set_font("lunchds_green")
  self.title_text:set_text_key("file_screen.copy_file.title")
  self.return_text:set_text_key("file_screen.file_select.return")

  self:set_cursor_position(1)
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)
end

function file_screen:initialize_erase_file_select()
  -- Prepare to erase file selection.
  -- Setup title for erase file.
  self.title_text:set_font("lunchds_gray")
  self.title_text:set_text_key("file_screen.erase_file.title")
  self.return_text:set_text_key("file_screen.file_select.return")
  
  self:set_cursor_position(1)
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)
end

function file_screen:initialize_character_select()
  -- Prepare character selection screen.
  -- Setup title and characters to select.
  self.title_text:set_text_key("file_screen.chara_select.title")
  -- After a small delay, animate selected character.
  sol.timer.start(self, 100, function()
    if self.cursor == 1 then
      self.boy_sprite:set_animation("walking")
    elseif self.cursor == 2 then
      self.girl_sprite:set_animation("walking")
    end
    self.allow_input = true
  end)
  self:set_cursor_position(1)
end

function file_screen:initialize_settings()
  -- Go to settings menu.
  local settings_menu = require("scripts/menus/settings")
  sol.menu.start(self, settings_menu, false)
  self.mode = "main"
  
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)

  -- Settings table.
  self.settings = {
    {
      name = "button_configuration",
      values = {
        weapon = {
          name = "weapon",
          values = { "C", "X", "V" },
          initial_value = nil
        },
        item = {
          name = "item",
          values = { "C", "X", "V" },
          initial_value = nil
        },
        subweapon = {
          name = "subweapon",
          values = { "C", "X", "V" },
          initial_value = nil
        }
      },
      initial_value = self.settings.values[weapon]
    },
    {
      name = "music_volume",
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = sol.audio.get_music_volume()
    },
    {
      name = "sound_volume",
      values = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
      initial_value = sol.audio.get_sound_volume()
    },
    {
      name = "dialog_speed",
      values = { 
        fastest = 10,
        fast = 20,
        normal = 40,
        normal_slow = 50,
        slow = 60,
        slowest = 80
      },
      initial_value = values[normal]
    },
    {
      name = "credits"
    }
  }

  -- Cursor arrows.
  self.weapon_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.item_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.subwpn_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")

  self.weapon_cursor_sprite:set_animation("stop")
  self.item_cursor_sprite:set_animation("stop")
  self.subwpn_cursor_sprite:set_animation("stop")
end

function file_screen:initialize_confirm_dialog()
  local YES = 1
  local NO = 2
  
  self:set_cursor_position(NO)
  self.cursor_sprite:set_animation("question")
  self.cursor_sprite:set_frame(0)
  
  self.question_text:set_text("Are you sure?")
  self:set_confirm_choices("file_screen.decision.yes", "file_screen.decision.no")
end

-- Read saved files.
function file_screen:read_savefiles()
  
  for index = 1, MAX_SAVEFILES do
    local savefile = {}
  
    -- Set numbers of each file.
    self.savefile_text[index]:set_text(tostring(index))
  
    savefile.file_name = self:get_savefile_name(index)
    savefile.game = game_manager:create_game(savefile.file_name)
    savefile.character_value = savefile.game:get_value("character_value") or 0
  
    if savefile.character_value == 1 then
      savefile.player_icon = sol.sprite.create("sprites/entities/misc/den_boy")
    elseif savefile.character_value == 2 then
      savefile.player_icon = sol.sprite.create("sprites/entities/misc/den_girl")
    end
    
    --savefile.attack_icon = sol.sprite.create("sprites/misc/attack_icon")
    --savefile.defense_icon = sol.sprite.create("sprites/misc/defense_icon")
    --savefile.life_icon = sol.sprite.create("sprites/misc/heart")

    savefile.attack_text = sol.text_surface.create({
      font = status_font,
      horizontal_alignment = "right"
    })
    savefile.defense_text = sol.text_surface.create({
      font = status_font,
      horizontal_alignment = "right"
    })
    savefile.life_text = sol.text_surface.create({
      font = status_font,
      horizontal_alignment = "right"
    })
  
    -- Print out progress if file exists and stats are set.
    if sol.game.exists(savefile.file_name) then
      savefile.attack_text:set_text(savefile.game:get_attack())
      savefile.defense_text:set_text(savefile.game:get_defense())
      savefile.life_text:set_text(savefile.game:get_max_life() or 10)
    end
  
    -- Store file.
    self.savefiles[index] = savefile
  end
end

-- Check cursor if index < 0, or index > number of save files.
function file_screen:set_cursor_position(index)
  -- Branch cursor position based on screen mode.
  if self.mode == "file_select" then
    if index < 1 then
      index = SETTINGS_INDEX
    elseif index > SETTINGS_INDEX then
      index = 1
    end
  elseif self.mode == "character" then
    if index < 1 then
      index = 2
    elseif index > 2 then
      index = 1
    end
  elseif self.mode == "copy_file" or self.mode == "erase_file" then
    if index < 1 then
      index = COPY_INDEX
    elseif index > COPY_INDEX then
      index = 1
    end
  end

  self.cursor = index
end

function file_screen:move_cursor(x, y)
  self.cursor_sprite:set_xy(x, y)
end

function file_screen:set_confirm_choices(key1, key2)
  if self.choice1_text:get_text() ~= nil then
    self.choice1_text:set_text_key(key1)
  else
    self.choice1_text:set_text("")
  end
  
  if self.choice2_text:get_text() ~= nil then
    self.choice2_text:set_text_key(key2)
  else
    self.choice2_text:set_text("")
  end
end

function file_screen:get_savefile_name(index)
  return "save" .. index .. ".dat"
end

-- Main drawing method.
function file_screen:on_draw(screen)
  self.surface:clear()

  -- Fill background.
  self.surface:fill_color(self.background_color)
  
  -- Draw based on mode.
  if self.mode == "file_select" then
    self:on_draw_file_select()
  elseif self.mode == "character" then
    self:on_draw_character_select()
  elseif self.mode == "copy_file" or self.mode == "erase_file" then
    self:on_draw_other_file_select()
  elseif self.mode == "settings" then
    self:on_draw_settings()
  elseif self.mode == "confirm" then
    self:on_draw_confirm_dialog()
  end

  -- Draw cursor.
  self.cursor_sprite:draw(self.surface)
  
  -- Draw to screen.
  local width, height = screen:get_size()
  self.surface:draw(screen, width / 2 - 160, height / 2 - 120)
end

--------------------------------------
-- Drawing functions for each mode. --
--------------------------------------
-- File select draw mode.
function file_screen:on_draw_file_select()
  -- Set additional offsets.
  local small_gap = self.offset/4
  local large_gap = self.offset*3

  -- Draw in window and information.
  self:draw_windows()

  -- Draw title.
  self.title_text:draw(self.surface, 
    self.select_box.width / 2 + self.offset, 
    self.select_box.position.y + self.offset)

  -- Draw save files.
  for i = 1, MAX_SAVEFILES do
    self:draw_savefile(i)
  end

  -- Draw other options.
  self.copy_text:draw(self.surface,
    self.option_box.position.x + self.offset,
    self.option_box.position.y + (self.option_box.height / 2))
  self.erase_text:draw(self.surface, 
    self.option_box.position.x + large_gap + small_gap*2, 
    self.option_box.position.y + (self.option_box.height / 2))
  self.settings_text:draw(self.surface, 
    self.option_box.position.x + large_gap*2 + small_gap*2,
    self.option_box.position.y + (self.option_box.height / 2))

  -- Draw cursor position.
  local index = self.cursor
  if index <= MAX_SAVEFILES then
    self:move_cursor(self.select_box.position.x + small_gap + small_gap / 2, 
      (index * self.offset*2) + small_gap*6)
  elseif index == COPY_INDEX then
    self:move_cursor(
      self.option_box.position.x + small_gap + small_gap / 2,
      self.option_box.position.y + 22)
  elseif index == ERASE_INDEX then
    self:move_cursor(
      self.option_box.position.x + large_gap - 1, 
      self.option_box.position.y + 22)
  elseif index == SETTINGS_INDEX then
    self:move_cursor(
      self.option_box.position.x + large_gap*2 - 1,
      self.option_box.position.y + 22)
  end
end

-- Character select draw mode.
function file_screen:on_draw_character_select()
  local large_gap = self.offset*3
  local small_gap = self.offset/3
  local index = self.cursor

  self:draw_big_window()

  -- Draw title.
  self.title_text:draw(self.surface,
    self.settings_box.width / 2 + self.offset,
    self.settings_box.position.y + self.offset)

  -- Draw character sprites.
  self:draw_character_sprites()

  -- Have characters look south.
  self.boy_sprite:set_direction(3)
  self.girl_sprite:set_direction(3)
  
  -- Set animation for selected sprite.
  -- Move cursor based on selected sprite.
  -- Character sprites start walking when cursor does not select them,
  -- so their walking animation is activated opposite their position.
  if index == 1 then
    self.girl_sprite:set_animation("walking")
    self:move_cursor(self.settings_box.width / 2 - 60, 
      self.settings_box.height - large_gap)
  elseif index == 2 then
    self.boy_sprite:set_animation("walking")
    self:move_cursor(self.settings_box.width / 2 + 60,
      self.settings_box.height - large_gap)
  end
end

-- Select copy/erase file draw mode.
function file_screen:on_draw_other_file_select()
  local small_gap = self.offset/4
  local large_gap = self.offset*3

  -- Draw in window and information.
  self:draw_windows()

  if self.mode == "copy_file" then
    self:draw_copy_title()
  elseif self.mode == "erase_file" then
    self:draw_erase_title()
  end

  -- Draw save files.
  for i = 1, MAX_SAVEFILES do
    self:draw_savefile(i)
  end

  -- Draw return command.
  self.return_text:draw(self.surface,
    self.option_box.position.x + self.offset,
    self.option_box.position.y + (self.option_box.height / 2))

  -- Draw cursor position.
  local index = self.cursor
  if index <= MAX_SAVEFILES then
    self:move_cursor(
      self.select_box.position.x + small_gap + small_gap / 2,
      (index * self.offset*2) + small_gap*6)
  else
    self:move_cursor(
      self.option_box.position.x + small_gap + small_gap / 2,
      self.option_box.position.y + 22)
  end
end

-- Settings draw mode.
function file_screen:on_draw_settings()
  -- Draw big window.
  self:draw_big_window()

  -- Draw title.
  self.title_text:draw(self.surface, 
    self.select_box.width / 2 + self.offset, 
    self.select_box.position.y + self.offset)
  
  -- Draw return option.
  self.return_text:draw(self.surface,
    self.option_box.position.x + self.offset,
    self.option_box.position.y + (self.option_box.height / 2))
end

-- Confirm dialog draw mode.
function file_screen:on_draw_confirm_dialog()
  local small_gap = self.offset / 4
  
  -- Draw dialog window.
  self.confirm_box.surface:draw_region(0, 0,
    self.confirm_box.width, self.confirm_box.height,
    self.surface,
    self.confirm_box.position.x,
    self.confirm_box.position.y)
  
  -- Draw dialog text.
  self.question_text:draw(self.surface, 
    self.confirm_box.width / 2,
    self.confirm_box.position.y + small_gap)
  self.choice1_text:draw(self.surface, 
    self.confirm_box.width / 2 - (small_gap * 2), 
    self.confirm_box.height - small_gap)
  self.choice2_text:draw(self.surface,
    self.confirm_box.width / 2 + (small_gap * 2),
    self.confirm_box.height - small_gap)
  
  -- Set cursor position based on choice.
  self:move_cursor(
    self.confirm_box.width / 2 * (self.cursor - 1),
    self.confirm_box.height - small_gap + self.offset)
end

-- Draw windows.
function file_screen:draw_windows()
  self.select_box.surface:draw(self.surface, 
    self.select_box.position.x, 
    self.select_box.position.y)
  self.option_box.surface:draw(self.surface, 
    self.option_box.position.x, 
    self.option_box.position.y)
end

-- Draw big window for settings and character select.
function file_screen:draw_big_window()
  self.settings_box.surface:draw(self.surface,
    self.settings_box.position.x,
    self.settings_box.position.y)
end

-- Draw copy file title.
function file_screen:draw_copy_title()
  -- Draw title.
  self.title_text:draw(self.surface,
    self.select_box.width / 2 + self.offset,
    self.select_box.position.y + self.offset)
end

-- Draw erase file title.
function file_screen:draw_erase_title()
  -- Draw title.
  self.title_text:draw(self.surface, 
    self.select_box.width / 2 + self.offset, 
    self.select_box.position.y + self.offset)
end

-- Draw saved files.
function file_screen:draw_savefile(index)
  local savefile = self.savefiles[index]

  -- Draw texts.
  self.savefile_text[index]:draw(self.surface, 
    self.select_box.position.x + self.offset,
    (index * self.offset*2) + 32)
  
  -- Draw out player and stats if there is a file.
  if sol.game.exists(savefile.file_name) then
    -- Draw player icon.
    --savefile.player_icon:draw(self.surface,)
    
    -- Draw player stats.
    --[[savefile.attack_icon:draw(self.surface, 
      self.select_box.position.x + (self.offset*2) + 40,
      (index * self.offset*2) + 24)--]]
    savefile.attack_text:draw(self.surface, 
      self.select_box.position.x + (self.offset*2) + 60,
      (index * self.offset*2) + 24)
    --[[savefile.defense_icon:draw(self.surface, 
      self.select_box.position.x + (self.offset*2) + 90, 
      (index * self.offset*2) + 24)--]]
    savefile.defense_text:draw(self.surface, 
      self.select_box.position.x + (self.offset*2) + 120,
      (index * self.offset*2) + 24)
    --[[savefile.life_icon:draw(self.surface,
      self.select_box.position.x + (self.offset*2) + 150,
      (index * self.offset*2) + 24)--]]
    savefile.life_text:draw(self.surface, 
      self.select_box.position.x + (self.offset*2) + 180,
      (index * self.offset*2) + 24)
  end
end

-- Draw character sprites.
function file_screen:draw_character_sprites()
  local large_gap = self.offset*3
  local small_gap = self.offset*2

  self.boy_sprite:draw(self.surface, 
    self.settings_box.width / 2 - small_gap + 8, 
    self.settings_box.height - large_gap)
  self.girl_sprite:draw(self.surface,
    self.settings_box.width / 2 + large_gap + 8,
    self.settings_box.height - large_gap)
end

-- Key input events.
function file_screen:on_key_pressed(key)

  local handled = false

  if self.allow_input then
    self:on_key_pressed_sound(key)
    
    if key == "escape" then
      -- Shutdown game.
      handled = true
      sol.main.exit()
    elseif key == "space" or key == "return" then
      self:on_decision(self.cursor)
      handled = true
    elseif key == "b" then
      self:on_cancel()
      handled = true
    elseif key == "up" or key == "left" then
      self:set_cursor_position(self.cursor-1)
      handled = true
    elseif key == "down" or key == "right" then
      self:set_cursor_position(self.cursor+1)
      handled = true
    end
  end

  return handled
end

-- Play cursor sound when moving arrow key.
function file_screen:on_key_pressed_sound(key)

  local handled = false
  self.allow_input = not handled

  if key == "up" or key == "down" or
      key == "left" or key == "right" then
    sol.audio.play_sound("cursor")
    handled = true
  elseif key == "space" or key == "return" then
    if self.cursor ~= SETTINGS_INDEX then
      sol.audio.play_sound("ok")
    else
      sol.audio.play_sound("wrong")
    end
    handled = true
  elseif key == "b" then
    sol.audio.play_sound("hero_hurt")
    handled = true
  end

  return handled
end

-- Event called to branch based on cursor decisions.
function file_screen:on_decision(index)
  if self.mode == "file_select" then
    self:on_decision_file_select(index)
  elseif self.mode == "character" then
    self:on_decision_character_select(index)
  elseif self.mode == "copy_file" then
    self:on_decision_copy_file_select(index)
  elseif self.mode == "erase_file" then
    self:on_decision_erase_file_select(index)
  elseif self.mode == "confirm" then
    self:on_decision_confirm_dialog(index)
  end
end


function file_screen:on_decision_file_select(index)
  if index == COPY_INDEX then
    -- Go to copy file selection.
    self:set_mode("copy_file")
  elseif index == ERASE_INDEX then
    -- Go to erase file selection.
    self:set_mode("erase_file")
  elseif index == SETTINGS_INDEX then
    -- Go to settings.
    self:set_mode("settings")
  else
    -- Select file.
    local savefile = self.savefiles[index]
    local game_exists = sol.game.exists(savefile.file_name)
    if game_exists then
      self.allow_input = false
      sol.audio.play_sound("ok")
      self.cursor_sprite:set_animation("none")
      self:on_pre_finish()
    else
      -- Go to character select screen.
      self:set_mode("character")
    end
  end
end

function file_screen:on_decision_copy_file_select(index)
  if index == RETURN_INDEX then
    -- Go back to file selection.
    self:set_mode("file_select")
  else
    -- Copy selected file.
    -- If file exists, make placeholder cursor.
    local game_exists = sol.game.exists(self:get_savefile_name(index))
    local confirm = false
    if game_exists then
      -- Create new cursor to select file.
      --[[self.copy_cursor_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
      self.copy_cursor_sprite:set_animation("stop")
      self.copy_cursor_sprite:draw(self.surface,
        )
      self.cursor = 1
      -- Select file to copy.
      -- Check for empty savefiles.
      -- Move cursor to any empty file.
      local selected_game = savefiles[self.cursor].game
      --]]
      if confirm then
        sol.audio.play_sound("ok")
        sol.timer.start(self, 1000, function()
          -- Save copied file to selected file.
          --selected_game:save()
        end)
      else
        self:set_mode("file_select")
      end
    else
      sol.audio.play_sound("wrong")
    end
  end
end

function file_screen:on_decision_erase_file_select(index)
  if index == RETURN_INDEX then
    -- Go back to file selection.
    self:set_mode("file_select")
  else
    -- Erase selected file.
    -- If file exists, ask to confirm.
    local game_exists = sol.game.exists(self:get_savefile_name(index))
    if game_exists then
      local savefile = savefiles[index]
      -- Fetch result of file erasing confirmation.
      local confirm = self:get_confirm_decision()
      if confirm then
        sol.audio.play_sound("hero_falls")
        sol.timer.start(self, 3000, function()
          -- Delete, then update erased file.
          sol.game.delete(savefile.file_name)
          self:read_savefiles()
        end)
      else
        -- Return to file screen main menu.
        self:set_mode("file_select")
      end
    else
      sol.audio.play_sound("wrong")
    end
  end
end

function file_screen:on_decision_character_select(index)
  local savegame = self.savefiles[index].game
  self.allow_input = false
  sol.audio.play_sound("ok")
  self.cursor_sprite:set_animation("none")
  
  -- Make placeholder to keep index.
  --[[local temp_index = index
  -- Confirm decision.
  self:set_mode("confirm")
  -- Fetch result of confirm decision.
  local confirm = self:get_confirm_decision()
  -- If so, setup and start game.
  if confirm then
    index = temp_index--]]
    -- Set character chosen in file.
    self:set_character_value(index)
    savegame:save()
  
    -- Update files.
    self:read_savefiles()
    -- Start the game.
    --self:on_pre_finish()
  --else
    -- If player cancels confirm, return to file screen main menu.
    self:set_mode("file_select")
  --end
end

function file_screen:set_character_value(index)
  local savegame = self.savefiles[self.cursor].game
  savegame:set_value("character_value", index)
end

function file_screen:get_character_value()
  local savegame = self.savefiles[self.cursor].game
  return savegame:get_value("character_value")
end

function file_screen:on_decision_confirm_dialog(index)
  self:set_confirm_decision(index)
end

function file_screen:set_confirm_decision(index)
  if index ~= nil or 
      index ~= self.confirm_index then
    self.confirm_index = index
  end
end

function file_screen:get_confirm_decision()
  local NO = 2
  if self.confirm_index ~= nil then
    return self.confirm_index
  else
    return NO
  end
end

function file_screen:fade_in_components(time)
  self.select_box.surface:fade_in(time)
  self.settings_box.surface:fade_in(time)
  self.option_box.surface:fade_in(time)
end

-- Event called for handling decisions when cancelled.
function file_screen:on_cancel()
  if self.mode == "file_select" then
    sol.audio.play_sound("wrong")
  elseif self.mode ~= "file_select" then
    -- Go back to main menu.
    self:set_mode("file_select")
  end
end

function file_screen:on_pre_finish()
  local game = self.savefiles[self.cursor].game
  sol.timer.start(self, 200, function()
    self.surface:fade_out(fade_delay)
    sol.timer.start(self, 2000, function()
      sol.menu.stop(self)
      if game ~= nil then
        game_manager:start_game(game)
      end
    end)
  end)
end

return file_screen