-- This defines a list of all the 
-- scene menus at the startup of the game.

-- Can add or remove scene menu here.

local DEMO = true

-- Convert scripts to scenes.
local function add_scene(scene_file)
  return require("scripts/menus/" .. scene_file)
end

-- List of startup scene menus.
local starting_scenes = {}

if DEMO and not DEBUG then
  starting_scenes = {
    add_scene("solarus_logo"),
    add_scene("boaromayo_logo"),
    add_scene("pre_alpha_title_screen"),
    add_scene("title_screen"),
    add_scene("demo_screen")
  }
elseif DEBUG then
  starting_scenes = {
    add_scene("solarus_logo"),
    add_scene("demo_screen")
  }
else
  starting_scenes = {
    add_scene("solarus_logo"),
    add_scene("boaromayo_logo"),
    add_scene("title_screen"),
    add_scene("file_screen")
  }
end

return starting_scenes