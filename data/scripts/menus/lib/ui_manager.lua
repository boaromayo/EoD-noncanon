-- This script sets up the UI (user interface) manager
-- to ensure that windows can be drawn more efficiently.

-- Constants.
local SCREEN_WIDTH, SCREEN_HEIGHT = sol.video.get_quest_size()

local ui_manager = {
  -- Include default settings.
  src_surface = nil,                            -- Source UI image surface.
  src_img = "menus/ui.png",                     -- Source UI image path.
  src_width = 8,                                -- Source UI slice width.
  src_height = 8,                               -- Source UI slice height.
  window_style = 1,                             -- Window style.
}

-- Main draw window processing.
local function draw_window(surface, src_width, src_height, dest_surface, x, y, dest_width, dest_height, style)
  -- Source UI image position.
  local src_x, src_y = 0, 0
  -- Tiles of source UI image:
  -- tile_x[1] = left, tile_x[2] = middle, tile_x[3] = right
  -- tile_y[1] = top, tile_y[2] = middle, tile_y[3] = bottom
  local tile_x = {0, src_width, src_width * 2}
  local tile_y = {0, src_height, src_height * 2}
  -- Position to draw.
  local draw_x, draw_y = x, y

  -- Check destination window size / source window size.
  -- Round destination size up by a factor of source size if a remainder is given.
  local increment = 0
  if dest_width % src_width ~= 0 then
    increment = dest_width % src_width
    dest_width = dest_width + increment
  end
  if dest_height % src_height ~= 0 then
    increment = dest_height % src_height
    dest_height = dest_height + increment
  end

  -- First, check UI source window sizes.
  -- Then, split UI source image into block section based on window style selected.
  -- Next, start actual drawing by drawing the upper-left corner, shift to next position, then keep drawing the top until hitting the end corner. Shift to next row.
  -- Continue until window finished.
  -- Multiply by 3 counting left, middle, and right tiles.
  local block_surface = sol.surface.create(src_width * 3, src_height * 3)
  local block_width, block_height = block_surface:get_size()
  local block_x = (style - 1) * block_width

  surface:draw_region(block_x, 0, block_width, block_height, block_surface)

  while draw_y <= dest_height do
    while draw_x <= dest_width do
      block_surface:draw_region(src_x, src_y, src_width, src_height, dest_surface, draw_x, draw_y)
      draw_x = draw_x + src_width
      if draw_x == x then
        src_x = tile_x[1]
      elseif dest_width - draw_x > src_width then
        src_x = tile_x[2]
      else
        src_x = tile_x[3]
      end
    end
    draw_y = draw_y + src_height
    if y == init_y then
      src_y = tile_y[1]
    elseif dest_height - draw_y > src_height then
      src_y = tile_y[2]
    else
      src_y = tile_y[3]
    end
    draw_x = x
    src_x = tile_x[1] -- Reset to left tile after finishing a row.
  end
end

-- Create window at (dest_x, dest_y) using a set UI image 
-- that can be split into (src_width * src_height) tiles.
function ui_manager:create_window(dest_x, dest_y, width, height, style)
  -- Make a new blank window.
  local window = {}
  dest_x = dest_x or 0
  dest_y = dest_y or 0
  width = width or SCREEN_WIDTH
  height = height or SCREEN_HEIGHT
  style = style or ui_manager.window_style

  -- Setup drawing surface.
  window.surface = sol.surface.create(width, height)

  -- Setup source UI drawing surface.
  if not self.src_surface then
    self.src_surface = sol.surface.create(self.src_img)
  end

  -- Draw window.
  function window:draw(screen, x, y)
    self.surface:clear()
    x = x or dest_x
    y = y or dest_y
    draw_window(ui_manager.src_surface, ui_manager.src_width, ui_manager.src_height, self.surface, 0, 0, width, height, style)

    self.surface:draw(screen, x, y)
  end

  -- Set window position.
  function window:set_xy(x, y)
    self.surface:set_xy(x, y)
  end

  -- Check window position.
  function window:get_xy()
    return dest_x, dest_y
  end

  -- Get window size.
  function window:get_size()
    return width, height
  end

  -- Get window drawing surface.
  function window:get_surface()
    return self.surface
  end

  return window
end

-- Set source UI image slice size.
function ui_manager:set_src_size(width, height)
  if width <= 0 or height <= 0 then
    return
  end

  self.src_width = width
  self.src_height = height
end

return ui_manager