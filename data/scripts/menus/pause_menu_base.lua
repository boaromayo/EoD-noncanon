-- Setup pause menu base table.
local pause_menu_base = {}

local audio_manager = require("scripts/audio_manager")

function pause_menu_base:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self
  object.game = game
  return object
end

function pause_menu_base:on_started()
  self:load_resources()
  self:initialize_settings()
end

function pause_menu_base:load_resources()
  -- Get window style (1 for normal, 2 for grayscale, 3 for parchment)
  self.style = 1--self.game:get_value("window_style") or 1
  
  -- Load images and surfaces.
  self.menu_img = sol.surface.create("menus/pause_menus" .. self.style .. ".png")
  self.left_arrow_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  self.right_arrow_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  
  -- Load font settings, and text strings.
  local font, small_font, font_size, font_color
  local scroll_color = { 115, 59, 22 }
  local white_color = { 255, 255, 255 }
  if self.style == 1 then
    font = "lunchds_blue"
  else
    font = "lunchds"
    font_size = 16
    if self.style == 2 then
      font_color = white_color
    elseif self.style == 3 then
      font_color = scroll_color
    end
  end
  
  small_font = "white_digits"
  
  self.font = font
  self.small_font = small_font
  self.font_size = font_size
  self.font_color = font_color or white_color
  self.white_color = white_color
  
  self.caption_text = sol.text_surface.create({
    font = self.font,
    font_size = self.font_size,
    color = self.font_color,
    horizontal_alignment = "center",
    vertical_alignment = "middle",
  })
end

function pause_menu_base:initialize_settings()
  -- Load variables.
  -- Size of pause menu.
  local menu_w, menu_h = self.menu_img:get_size()
  self.width, self.height = menu_w / 4, menu_h

  -- Settings to check prompt box open.
  self.prompt_open = false

  self:animate_arrow_sprites()
end

-- Start animation of the arrow sprites in each menu.
-- The arrows indicate that the current pause menu can 
-- be switched to a different menu.
function pause_menu_base:animate_arrow_sprites()
  self.left_arrow_sprite:set_animation("cursor")
  self.left_arrow_sprite:set_frame(0)
  self.left_arrow_sprite:set_direction(0)
  self.right_arrow_sprite:set_animation("cursor")
  self.right_arrow_sprite:set_frame(0)
  self.right_arrow_sprite:set_direction(1)
end

-- Set the caption for the bottom window.
-- Nil case removes previous text.
function pause_menu_base:set_caption(text)
  if text == nil then
    self.caption_text:set_text(nil)
  end
  
  self.caption_text:set_text(text)
end

-- Set the caption text key.
-- Nil case removes previous text key.
function pause_menu_base:set_caption_key(text_key)
  if text_key == nil then
    self:set_caption(nil)
    return
  end
  
  -- Translate text key to set language.
  local text = sol.language.get_string(text_key)
  self:set_caption(text)
end

-- Draw the caption text.
function pause_menu_base:draw_caption(screen)
  local width, height = screen:get_size()
  local text_offset = 6
  local text_x = width / 2
  local text_y = height - 32 + text_offset
  
  -- Draw text on caption window.
  self.caption_text:draw(screen, text_x, text_y)
end

-- Transition to new pause submenu.
function pause_menu_base:transition_menu()
  audio_manager:play_sound("pause_open")
  sol.menu.stop(self)
  local menus = self.game.pause_menus
  local index = self.game:get_value("pause_last_menu")
  return menus, index
end

-- Move to previous pause submenu.
function pause_menu_base:previous_menu()
  local menus, index = self:transition_menu()
  index = index <= 1 and #menus or (index - 1)
  self.game:set_value("pause_last_menu", index)
  sol.menu.start(self.game.pause_menu, menus[index])
end

-- Move to next pause submenu.
function pause_menu_base:next_menu()
  local menus, index = self:transition_menu()
  index = index >= #menus and 1 or (index + 1)
  self.game:set_value("pause_last_menu", index)
  sol.menu.start(self.game.pause_menu, menus[index])
end

-- Draw current menu windows of pause scene.
function pause_menu_base:draw_menu(screen)
  local width, height = screen:get_size()
  local cx, cy = width / 2, height / 2
  local menu_x = cx - self.width / 2
  local menu_y = cy - self.height / 2 + 6
  
  -- Draw the current menu window.
  local menu_index = self.game:get_value("pause_last_menu")
  self.menu_img:draw_region(
    self.width * (menu_index - 1),      -- region_x where to start
    0,                                  -- region_y where to start
    self.width,                         -- region_width
    self.height,                        -- region_height
    screen,                             -- destination screen to draw image on
    menu_x,                             -- destination screen_x
    menu_y                              -- destination screen_y
  )

  -- Draw the main arrow sprites.
  self.left_arrow_sprite:draw(screen, menu_x + 16, cy)
  self.right_arrow_sprite:draw(screen, self.width - 16, cy)
end

-- Function to handle pause menu input.
function pause_menu_base:on_command_pressed(command)
  local handled = false

  if not handled then
    if (command == "attack" or command == "escape") and not self.prompt_open then
      self:show_quit_prompt()
      handled = true
    end
  end

  return handled
end

-- Function to handle pause menu keyboard input.
function pause_menu_base:on_key_pressed(key)

  local handled = false

  if not handled then
    if key == "escape" then
      handled = self:on_command_pressed("escape")
    end
  end

  return handled
end

-- Event to handle pause menu gamepad input.
function pause_menu_base:on_joypad_button_pressed(button)
  local handled = false

  if not handled then
    --TODO: Handle quit option here.
  end

  return handled
end

-- Function that opens quit prompt dialog box.
function pause_menu_base:show_quit_prompt()
  -- TODO: Make this a game metamethod in later build.
  self.prompt_open = true
  if self.game ~= nil then
    self.game:start_dialog("_game.quit_prompt.1", function(answer) 
      if answer == 1 then
        sol.main.reset()
      else
        audio_manager:play_sound("hero_hurt")
      end
      self.prompt_open = false
    end)
  else
    sol.main.reset()
  end
end

-- Function to handle when finished with menu.
function pause_menu_base:on_finished()
end

return pause_menu_base