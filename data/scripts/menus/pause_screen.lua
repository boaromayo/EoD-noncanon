-- Setup pause screen when pause menu scene is disabled. 
-- Note this menu is independent of the pause menus.
local pause_screen = {}

local audio_manager = require("scripts/audio_manager")

-- Setup all information for pause screen.
function pause_screen:on_started()
  
  -- Quest size.
  local width, height = sol.video.get_quest_size()
  -- Font.
  local font, font_size = "lunchds", 16
  -- Background and text color.
  local gray_color = { 40, 40, 25 }
  local white_color = { 255, 255, 255 }
  -- Transparency.
  local opacity = 160
  
  -- Main surface.
  self.surface = sol.surface.create(width, height)
  -- Text.
  self.pause_text = sol.text_surface.create({
    font = font,
    font_size = font_size,
    color = white_color,
    horizontal_alignment = "center",
    vertical_alignment = "top",
  })

  -- Settings for surface and text.
  self.surface:fill_color(gray_color)
  self.surface:set_opacity(opacity)
  self.pause_text:set_text_key("pause_screen.paused")
  
  -- Play when pause screen appears.
  audio_manager:play_sound("cursor")
end

-- Draw "paused" text.
function pause_screen:draw_text(screen)
  local width, height = screen:get_size()
  local text_x = width / 2
  local text_y = (height / 2) - 32
  
  self.pause_text:draw(screen, text_x, text_y)
end

-- Draw pause screen.
function pause_screen:on_draw(screen)
  local x, y = 0, 0

  self:draw_text(screen)
  
  self.surface:draw(screen, x, y)
end

-- Set commands for pause screen.
function pause_screen:on_command_pressed(command)
  local handled = false

  if not handled then
    if command == "escape" then
      self:show_quit_prompt()
      handled = true
    end
  end

  return handled
end

function pause_screen:on_key_pressed(key)
  local handled = false

  if not handled then
    if key == "escape" then
      handled = self:on_command_pressed("escape")
    end
  end

  return handled
end

function pause_screen:show_quit_prompt()
  if self.game ~= nil then
    self.game:start_dialog("_game.quit_prompt.1", function(answer)
      if answer == 1 then
        sol.main.reset()
      else
        audio_manager:play_sound("hero_hurt")
      end
    end)
  else
    sol.main.reset()
  end
end

-- Event called when menu is finished.
function pause_screen:on_finished()
  -- TODO: Make a custom pause screen sound effect.
  audio_manager:play_sound("cursor")
end

return pause_screen