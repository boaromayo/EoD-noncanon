-- Set up dialog box for the game.
-- Dialog box is a menu object.
-- This is a modified version of Children of Solarus's 
-- dialog box script.

local game = ...

-- Load managers.
local audio_manager = require("scripts/audio_manager")
local language_manager = require("scripts/language_manager")

local dialog_box = {
  -- Default dialog settings.
  dialog = nil,          -- No dialog available at start.
  info = nil,            -- Extra info to add to dialog (variables like name, item amounts).
  first = true,          -- If this is the first dialog in game.
  skipped = false,       -- If dialog is skipped.
  choices = {},          -- Array of choices.
  num_choices = 0,       -- Number of choices.
  answer = nil,          -- Selected answer for decision-making.

  -- One-at-a time letter settings.
  next_line = false,     -- Is next line?
  iterator = nil,        -- Counter for next line in dialog.
  lines = {},            -- Table for the lines in dialog.
  line_surfaces = {},    -- Table for the text surfaces.
  index = 0,             -- Index for line shown.
  char_index = 0,        -- Index for character in current line.
  char_delay = 0,        -- Delay when displaying one-character-at-a-time.
  letter_sound_played = false,  -- If letter-at-a-time sound has played.
  
  -- Graphics settings.
  surface = nil,              -- Dialog surface.
  box_surface = nil,          -- Dialog box surface.
  arrow_sprite = nil,         -- Dialog arrow icon (for question or next dialog).

  -- Positions for surfaces.
  box_position = nil,           -- For dialog box.
  arrow_position = nil          -- For decision arrow.
}

-- CONSTANTS.
local num_visible_lines = 4     -- There are four lines in game.
local dialog_box_width = 256    -- Set size of dialog box.
local dialog_box_height = 84

local char_delays = {
  instant = 0,
  fastest = 10, 
  fast = 20, 
  normal = 40, 
  slow = 50, 
  slowest = 60 
} -- Character display delays in-between characters (fastest to slowest).

-- Initialize dialog box.
function game:initialize_dialog_box()
  game.dialog_box = dialog_box

  local window_style = game:get_window_style()
  
  -- Font settings.
  local font, font_size

  -- Change font based on window settings.
  if window_style ~= nil 
      and window_style ~= 1 then
    font, font_size = language_manager:get_default_font()
  else
    font, font_size = language_manager:get_text_font(), 16
  end
  
  -- Initialize drawing surface.
  dialog_box.surface = sol.surface.create(sol.video.get_quest_size())
  
  if window_style ~= nil and window_style ~= 1 then
    dialog_box.box_surface = sol.surface.create("hud/dialog_box" .. window_style .. ".png")
  else
    dialog_box.box_surface = sol.surface.create("hud/dialog_box.png")
  end
  
  dialog_box.arrow_sprite = sol.sprite.create("hud/dialog_box_message_cursor")
  
  -- Initialize text lines.
  for i = 1, num_visible_lines do
    dialog_box.lines[i] = ""
    dialog_box.line_surfaces[i] = sol.text_surface.create({
      horizontal_alignment = "left",
      vertical_alignment = "top",
      font = font,
      font_size = font_size
    })
  end
end

-- Quit dialog box.
function game:quit_dialog_box()
  if dialog_box ~= nil then
    if game:is_dialog_enabled() then
      sol.menu.stop(dialog_box)
    end
    game.dialog_box = nil
  end
end

-- Game method that starts dialog with dialog box.
function game:on_dialog_started(dialog, info)
  dialog_box.dialog = dialog
  dialog_box.info = info
  sol.menu.start(game, dialog_box)
end

-- Game method when dialog box is finished.
function game:on_dialog_finished(dialog)
  sol.menu.stop(dialog_box)
  dialog_box.dialog = nil
  dialog_box.info = nil
end

-- When dialog box is open.
function dialog_box:on_started()
  -- Initialize properties of dialog box.
  self.char_delay = char_delays["normal"]

  -- Position dialog box on screen.
  local camera = game:get_map():get_camera()
  local cam_w, cam_h = camera:get_size()

  local x = cam_w / 2 - (dialog_box_width / 2)
  local y = cam_h - (dialog_box_height + 16)

  -- Set positions of dialog images.
  self.box_position = { x = x, y = y }
  self.arrow_position = { x = x + 8, y = y + 6 }

  self:show_dialog()
end

-- When dialog box is finished and closed.
function dialog_box:on_finished()
end

-- Show dialog box.
function dialog_box:show_dialog()
  local dialog = self.dialog

  -- If there's dialog available, draw text.
  if self.info ~= nil then
    -- Substitute variables and other special characters.
    -- Write every variable if there are a table of special variables.
    if type(self.info) == "table" then
      local infos = #self.info
      for i = 1, infos do
        dialog.text = dialog.text:gsub("%$v%[" .. i .. "%]", self.info[i])
      end
    else
      dialog.text = dialog.text:gsub("%$v", self.info)
    end
  end

  -- Split text into lines.
  dialog.text = dialog.text:gsub("\r\n", "\n"):gsub("\r", "\n")
  self.iterator = dialog.text:gmatch("([^\n]*)\n") -- Every line plus empty lines are counted.
  self.next_line = self.iterator()
  self.index = 1
  self.char_index = 1
  self.skipped = false

  -- Check if icon is visible.
  --[[if dialog.icon == nil then
    dialog_box.icon_index = 1
  else
    dialog_box.icon_index = dialog.icon
  end--]]

  -- Check if dialog is a question.
  if dialog.question == "1" then
    self.num_choices = 0 -- Since this is a question, set up number of choices.
    self.answer = 1 -- Answer yes (1), no (2), or other decisions.
  end

  self:show_more_lines()
end

-- Write out characters to dialog box.
local function show_character()

  while not dialog_box:is_full() and
      dialog_box.char_index > #dialog_box.lines[dialog_box.index] do
    -- Check if line finished.
    dialog_box.char_index = 1
    dialog_box.index = dialog_box.index + 1
  end

  if not dialog_box:is_full() then
    dialog_box:add_character()
  else
    if dialog_box:has_more_lines()
        or dialog_box.dialog.next ~= nil then
      dialog_box.arrow_sprite:set_animation("next")
    elseif dialog_box.answer ~= nil then
      dialog_box.arrow_sprite:set_animation("question")
    else
      dialog_box.arrow_sprite:set_animation("last")
    end
  end
end

-- Check if there are more lines to display.
function dialog_box:has_more_lines()
  return self.next_line ~= nil
end

-- Show 4 more lines of dialog 
-- if no more lines are available.
function dialog_box:show_more_lines()

  if not self:has_more_lines() then
    self:show_next_dialog()
    return
  end

  -- Prep the next 4 lines.
  for i = 1, num_visible_lines do
    self.line_surfaces[i]:set_text("")
    if self:has_more_lines() then
      self.lines[i] = self.next_line
      self.next_line = self.iterator()
    else
      self.lines[i] = ""
    end
  end
  self.index = 1
  self.char_index = 1 

  -- Delay the one-by-one character display.
  sol.timer.start(self, self.char_delay, show_character)
end

-- Show the next lines of dialog.
function dialog_box:show_next_dialog()

  local next_dialog_id
  if self.answer ~= 2 then -- If no question or first answer available
    next_dialog_id = self.dialog.next
  else
    -- If second answer selected
    next_dialog_id = self.dialog.next2
  end

  if next_dialog_id ~= nil then
    -- Show the next dialog if available.
    self.first = false
    self.answer = nil
    self.dialog = sol.language.get_dialog(next_dialog_id)
    self:show_dialog()
  else
    -- Finish dialog and return answer or nil if no question raised.
    local decision = self.answer

    -- Handle shop functions.
    if self.dialog.id == "shop.question" then
      -- Shop function needs an answer before exiting shop screen.
      decision = self.answer == 1
    end

    self.answer = nil -- Prevents decision box from appearing after decision made.
    game:stop_dialog(decision)
  end
end

-- Add a character one-by-one to the dialog box.
-- If the case is a special character, then perform
-- action for character here.
function dialog_box:add_character()
  -- Setup the line and character on line.
  local line = self.lines[self.index]
  local character = line:sub(self.char_index, self.char_index)
  if character == "" then
    error("No character available to add to line.")
  end
  self.char_index = self.char_index + 1
  local more_delay = 0
  local text = self.line_surfaces[self.index]

  -- Check for special characters ($).
  local is_special = false

  -- Special characters:
  -- - $0: delay
  -- - $|: custom delay
  -- - $[1-6]: change dialog speed from fastest to slowest
  -- - $d: decision
  -- - space: remove delay
  if character == "$" then
    is_special = true
    character = line:sub(self.char_index, self.char_index)
    self.char_index = self.char_index + 1

    if character == "0" then
      more_delay = 1000 -- Delay 1 second.
    elseif character == "|" then
      local pattern = "^%[(%d+)%]"
      more_delay = tonumber(line:sub(self.char_index):match(pattern)) -- Delay as long as you like.

      -- Omit characters unimportant to dialog.
      local end_index = line:find("]", self.char_index, true)
      self.char_index = end_index + 1
    elseif character == "1" then
      -- Fastest.
      self.char_delay = char_delays["fastest"]
    elseif character == "2" then
      -- Fast.
      self.char_delay = char_delays["fast"]
    elseif character == "3" then
      -- Normal.
      self.char_delay = char_delays["normal"]
    elseif character == "4" then
      -- Slow.
      self.char_delay = char_delays["slow"]
    elseif character == "5" then
      -- Slowest.
      self.char_delay = char_delays["slowest"]
    elseif character == "d" then -- If this is a choice.
      -- Instant.
      self.char_delay = char_delays["instant"]
      -- Add choice to array.
      self.choices[self.index] = self.char_index - 2
      self.num_choices = self.num_choices + 1
      -- Re-position cursor.
      local new_index = (self.index - self.num_choices) + 1
      self:set_cursor_position(new_index)
    else
      -- If this is not a special char...
      text:set_text(text:get_text() .. "$")
      is_special = false
    end
  end

  -- Remove delay for whitespace.
  if character == " " then
    more_delay = -self.char_delay
  end

  -- Print character to dialog box for the other chars.
  if not is_special then
    text:set_text(text:get_text() .. character)
  end

  -- Play sound for some characters drawn.
  if not is_special and not self.letter_sound_played and
      character ~= nil and character ~= " " then
    audio_manager:play_sound("message_letter")
    self.letter_sound_played = true
    sol.timer.start(self, 50, function()
      self.letter_sound_played = false
    end)
  end

  -- Set delays.
  sol.timer.start(self, self.char_delay + more_delay, show_character)
end

-- Skip showing text one-by-one, show entire text now.
-- If the 4 lines are complete, 
-- show next 4 lines of dialog if more.
function dialog_box:show_all()
  if self:is_full() then
    self:show_more_lines()
  else
    self.skipped = true
    -- Check the end of the current line.
    while not self:is_full() do
      while not self:is_full()
          and self.char_index > #self.lines[self.index] do
        self.char_index = 1
        self.index = self.index + 1
      end

      if not self:is_full() then
        self:add_character()
      end
    end
  end
end


-- Check if dialog box is full. 
-- By "full", this means that all of the lines in
-- the dialog box are fully displayed.
function dialog_box:is_full()
  return (self.index >= num_visible_lines) and
    (self.char_index > #self.lines[num_visible_lines])
end

-- Set cursor y-position.
function dialog_box:set_cursor_position(index)
  local padding = 16
  self.arrow_position.y = self.box_position.y + (index * padding) + 6
end

-- Draw method.
function dialog_box:on_draw(screen)
  local x, y = self.box_position.x, self.box_position.y
  local padding = 8
  local line_spacing = 16

  self.surface:clear()

  -- Draw dialog box.
  self.box_surface:draw_region(0, 0, dialog_box_width, dialog_box_height,
    self.surface, x, y)

  -- Draw text.
  local text_x = x + padding
  local text_y = y + padding - 2
  for i = 1, num_visible_lines do
    -- For any choices in list, move text to decision box. 
    if self.answer ~= nil
        and self.lines[i] == self.choices[i]
        and not self:has_more_lines() then
      text_x = text_x + padding
    end
    -- Draw text to screen and move to next line.
    self.line_surfaces[i]:draw(self.surface, text_x, text_y)
    text_y = text_y + line_spacing
  end

  -- Draw cursor.
  if self.answer ~= nil 
      and self:is_full()
      and not self:has_more_lines() 
      and self.arrow_sprite:get_animation() == "question" then
    self.arrow_sprite:draw(self.surface, 
      self.arrow_position.x, self.arrow_position.y)
  end

  -- Draw next line cursor.
  if self:is_full()
      and self.arrow_sprite:get_animation() == "next" then
    self.arrow_sprite:draw(self.surface, 
      x + dialog_box_width / 2, y + dialog_box_height - 4)
  end

  -- Draw to screen.
  self.surface:draw(screen)
end

-- Event called when command is executed.
function dialog_box:on_command_pressed(command)

  local handled = false

  if command == "action" then
    if self:is_full() then
      self:show_more_lines()
    else
      self:show_all()
    end
    handled = true
  elseif command == "up" or command == "down" 
      or command == "left" or command == "right" then
    if self.answer ~= nil
        and self:is_full()
        and not self:has_more_lines() then
      audio_manager:play_sound("cursor") -- Play cursor sound.
      self.answer = self.answer % self.num_choices + 1 -- Switch answer based on number of choices.
      -- Update cursor position.
      -- Set index based on number of choices.
      local new_index = self.answer + (self.index - (self.num_choices + 1)) 
      if self.num_choices > 2 then
        new_index = self.answer + (self.index - self.num_choices)
      end
      self:set_cursor_position(new_index)
    end
    handled = true
  end

  return handled
end
