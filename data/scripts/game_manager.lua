local game_manager = {}

require("scripts/multi_events")
local init = require("scripts/initializer")

-- Sets initial values to a new savegame file.
-- Overriden by initializer script.
--[[
local function initialize_new_savegame(game)

  -- You can modify this function to initialize life and equipement here.
  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_ability("lift", 1)
  game:set_ability("sword", 1)
end
--]]

-- Loads or creates the savegame from a filename,
-- initializing all resources.
function game_manager:create_game(filename)
  local exists = sol.game.exists(filename)
  local game = sol.game.load(filename)
  -- Include in-game assets.
  init:initialize(game)
  if not exists then
    -- Initialize a new savegame.
    init:initialize_new_savegame(game)
  end
  
  return game
end

-- Starts the game, assuming a game is set.
function game_manager:start_game(game)
  
  function game:on_started()
    -- Initialize components.
    game:initialize_dialog_box()
    game:initialize_hud()
    game:initialize_pause_menu()
    game:initialize_hero()
    game:run_chronometer()
  end

  function game:initialize_hero()
    -- Set gender based on player selection.
    local hero_meta = sol.main.get_metatable("hero")
    hero_meta:register_event("on_created", function(hero)
      local gender = game:get_value("den_gender") or "boy"
      hero:set_tunic_sprite_id("hero/den_" .. gender)
    end)
  end

  function game:on_finished()
    -- Destroy every menu component.
    game:quit_dialog_box()
    game:quit_hud()
    game:quit_pause_menu()
  end
  
  sol.main.game = game
  game:start()
end

-- Start demo version of game.
function game_manager:start_demo_game(game)
  
  local TEST_ROOM = false -- Note this is a different variable than DEMO.
  local location_index = game:get_value("demo_starting_location_index") or 1
  -- Include in-game assets.
  init:initialize(game)
  -- NOTE: Starting locations are now set in demo screen script.
  -- To test map when test flag active.
  if TEST_ROOM then
    game:set_starting_location("debug_room", "debug")
    game:get_item("wallet"):set_variant(1)
    game:get_item("stick"):set_variant(1)
  end
  init:initialize_demo(game, location_index)

  function game:on_started()
    -- Initialize components.
    game:initialize_dialog_box()
    game:initialize_hud()
    game:initialize_game_over()
    game:initialize_pause_menu()
    game:initialize_hero()
    game:run_chronometer()
  end

  function game:initialize_hero()
    -- Call character based on player selection.
    local hero_meta = sol.main.get_metatable("hero")
    hero_meta:register_event("on_created", function(hero)
      local gender = game:get_value("den_gender") or "boy"
      hero:set_tunic_sprite_id("hero/den_" .. gender)
    end)
  end

  function game:end_demo()
    -- Call dialog and start game from beginning.
    game:start_dialog("_demo.end.1", function()
      sol.main.reset()
    end)
  end

  function game:on_finished()
    -- Destroy every menu component.
    game:quit_dialog_box()
    game:quit_hud()
    game:quit_pause_menu()
  end

  -- This event is a workaround to ensure player can use weapon/subweapon.
  function game:on_command_pressed(command)
    if command == "attack" or command == "item_1" then
      local slot = 1 -- Weapon used
      return game:item_used(slot)
    elseif command == "item_2" then
      local slot = 2 -- Subweapon used
      return game:item_used(slot)
    else
      return false
    end
  end

  function game:item_used(slot)
    local item = game:get_item_assigned(slot)
    if item then
      if game:is_suspended() then
        return true
      end
      item:on_using()
      return true
    end
    return false
  end

  sol.main.game = game
  game:start()
end

return game_manager