-- Make the pesos, or money, display for the game.
local pesos = {}
local width = 32
local height = 12

function pesos:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

-- Setup money counter.
function pesos:initialize(game)
  self.game = game
  self.surface = sol.surface.create(width * 2, height)
  self.pesos_icon = sol.surface.create("hud/peso_icon.png")
  self.pesos_display = sol.text_surface.create {
    font = "white_digits",
    horizontal_alignment = "right"
  }
  self.current_money = game:get_money()
  self.max_money = game:get_max_money()
  self.pesos_display:set_text(self.current_money)
end

-- Automatically called after initialized.
function pesos:on_started()
  self:update()
  self:redraw()
end

-- Update method.
function pesos:update()
  local redraw = false
  local money = self.game:get_money()
  local max_money = self.game:get_max_money()

  -- If money counter max different,
  -- update and redraw.
  if max_money ~= self.max_money then
    redraw = true
    -- Set current money to max if current > max.
    if money > max_money then
      self.current_money = max_money
    end
  end

  -- Gradually update to current money.
  if money ~= self.current_money then
    redraw = true
    local increment = 0
    if money > self.current_money then
      increment = 1
    elseif money < self.current_money then
      increment = -1
    end
    -- When increment different, update until at actual current money.
    if increment ~= 0 then
      self.current_money = self.current_money + increment
    end
    -- Play keep counting sound for every 5 pesos counted.
    if self.current_money % 5 == 0 then
      sol.audio.play_sound("rupee_counter")
    
    -- Play sound after reaching end of money count.    
    elseif money == self.current_money then
      sol.audio.play_sound("rupee_counter_end")
    end
   end

  -- Redraw pesos counter if changed.
  if redraw then
    self:redraw()
  end

  -- Update every 1/50 of a second.
  sol.timer.start(self.game, 20, function()
    self:update()
  end)
end

-- Method to redraw counter to current number of pesos.
function pesos:redraw()
  self.surface:clear()

  -- Draw money icon to surface based on type of wallet hero has.
  local wallet = self.game:get_item("wallet")
  local has_wallet = wallet:has_variant()
  local variant = has_wallet and wallet:get_variant() or 1
  self.pesos_icon:draw_region(
    (variant - 1) * width / 2,  -- region_x to start drawing
    0,                         -- region_y to start drawing
    width / 2,                  -- region_width
    height,                     -- region_height
    self.surface,               -- destination surface to draw image on
    0,                          -- destination x
    0                           -- destination y
  )

  -- Update text to current money on hand.
  self.pesos_display:set_text(self.current_money)
  --[[if self.current_money < 100 then
    self.money_display:set_text("x" .. self.current_money) 
    end
  --]]

  -- Draw current money counter to surface.
  self.pesos_display:draw(self.surface, width + 16, 6)
end

-- Set position of money counter on-screen.
function pesos:set_dst_position(x, y)
  self.dest_x = x
  self.dest_y = y
end

-- Drawing method.
function pesos:on_draw(screen)
  -- Draw if player has wallet.
  local has_wallet = self.game:get_item("wallet"):has_variant()

  if has_wallet then
    local x, y = self.dest_x, self.dest_y
    self.surface:draw(screen, x, y)
  end
end

return pesos