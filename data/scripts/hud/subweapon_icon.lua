-- subweapon icon (right icon) for the HUD
-- to indicate equipped subweapon.

-- Initialize empty table for subweapon icon.
local subweapon_icon = {}

-- Create new icon object.
function subweapon_icon:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

-- Initialize subweapon icon.
function subweapon_icon:initialize(game)
  local width = 32
  local height = 32

  self.game = game
  self.slot = 2
  self.surface = sol.surface.create(width, height)
  self.icon_img = sol.surface.create("hud/icon_slot.png")
  self.sprite = sol.sprite.create("entities/items")
  self.amount_display = sol.text_surface.create({
    font = "white_digits",
    horizontal_alignment = "right",
    vertical_alignment = "bottom"
  })
  self.dest_x = 0
  self.dest_y = 0
  self.subweapon_equipped = nil
  self.current_amount = nil
  self.max_amount = nil
end

-- Function called automatically when HUD starts.
function subweapon_icon:on_started()
  self:update()
  self:redraw()
end

-- Update method.
function subweapon_icon:update()
  local redraw = false
  local subweapon = self.game:get_item_assigned(self.slot)

  -- Assign subweapon to icon.
  if self.subweapon_equipped ~= subweapon then
    redraw = true
    self.subweapon_equipped = subweapon
    if subweapon ~= nil then
      self.sprite:set_animation(subweapon:get_name())
    end
  
    if subweapon ~= nil then
      
      -- Check if subweapon has an amount, then update.
      if subweapon:has_amount() then
        local amount = subweapon:get_amount()
        local max_amount = subweapon:get_max_amount()
        if self.current_amount ~= amount and 
            self.max_amount ~= max_amount then
          self.current_amount = amount
          self.max_amount = max_amount
          self.amount_display:set_text(tostring(self.current_amount))
          redraw = true
        end
      elseif self.current_amount ~= nil then
        redraw = true
        self.current_amount = nil
        self.max_amount = nil
      end
    elseif self.current_amount ~= nil then
      redraw = true
      self.current_amount = nil
      self.max_amount = nil
    end
  end
  
  if redraw then
    self:redraw()
  end

  -- Update every 1/20th of a second.
  sol.timer.start(self.game, 50, function()
    self:update()
  end)
end

-- When called by update, redraw icon.
function subweapon_icon:redraw()
  self.surface:clear()
  
  -- Draw image of icon.
  self.icon_img:draw(self.surface)
  
  local icon_w, icon_h = self.icon_img:get_size()

  -- Draw equipped subweapon to screen.
  if self.subweapon_equipped ~= nil then
    self.sprite:draw(self.surface, icon_w / 2, icon_h / 2 + 5)

    -- If an amount exists, draw in amount.
    if self.current_amount ~= nil then
      -- Check if amount is at maximum.
      if self.current_amount >= self.max_amount then
        self.current_amount = self.max_amount
        self.amount_display:set_font("green_digits")
      else
        self.amount_display:set_font("white_digits")
      end
      self.amount_display:draw(self.surface, icon_w / 2 + 10, icon_h / 2 + 10)
    end
  end
end

-- Set subweapon icon's destination position.
function subweapon_icon:set_dst_position(x,y)
  self.dest_x = x
  self.dest_y = y
end

-- Draw subweapon icon.
function subweapon_icon:on_draw(screen)
  -- Draw if player has a subweapon. Ensure HUD will stay regardless
  -- if player loses subweapon later.
  if self.game:has_subweapon() and 
    not self.game:get_value("possession_subweapon") then
      self.game:set_value("possession_subweapon", true)
  end
  local got_subweapon = self.game:get_value("possession_subweapon")

  if got_subweapon then
    local x, y = self.dest_x, self.dest_y
    self.surface:draw(screen, x, y)
  end
end

return subweapon_icon