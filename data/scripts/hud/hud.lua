-- Make a HUD or heads-up display for the game.
-- Display health, tech (if available), currency,
-- and stuff equipped.
local game = ...

-- Initialize HUD on top-left of screen.
function game:initialize_hud()
  -- Load HUD components.
  local hud_components = require("scripts/hud/hud_components")
  -- HUD component builder, and name of component.
  local hud_builder = nil
  local hud_name = nil

  -- Create table for HUD components.
  self.hud = {
    dialog_visible = false
  }

  -- Initialize and add components to HUD.
  for i, hud_component in ipairs(hud_components) do
    hud_name = hud_component.name
    hud_builder = require("scripts/hud/" .. hud_component.script)
    -- Build component into menu.
    local menu = hud_builder:new(self)
    menu:set_dst_position(hud_component.x, hud_component.y)
    self.hud[#self.hud + 1] = menu

    if hud_name == "life_bar" then
      self.hud.life_bar = menu
    elseif hud_name == "tech_bar" then
      self.hud.tech_bar = menu
    elseif hud_name == "peso" then
      self.hud.peso = menu
    elseif hud_name == "weapon" then
      self.hud.weapon = menu
    elseif hud_name == "subweapon" then
      self.hud.subweapon = menu
    end
  end
  
  self:set_hud_visible(true)

  self:update_hud()
end

-- Hide or show HUD by piece-by-piece.
function set_hud_part_visible(hud_part, visible)
  if visible then
    if not sol.menu.is_started(hud_part) then
      sol.menu.start(game, hud_part)
    end
  else
    sol.menu.stop(hud_part)
  end
end

-- Update player's HUD on top-left.
function game:update_hud()

  local map = game:get_map()

  -- If hero is below the HUD, make HUD slightly transparent.
  if map ~= nil then
    local camera = map:get_camera()
    local hero = game:get_hero()
    local hero_x, hero_y = hero:get_position()      
    local cam_x, cam_y = camera:get_bounding_box()
    local x = hero_x - cam_x
    local y = hero_y - cam_y
    local opacity = nil
    local money_opacity = nil
    local items_opacity = nil

    -- Adjust transparency if player is below gauges.
    if (x < 88 and y < 80) then
      opacity = 128
    elseif (x >= 88 or y >= 80) then
      opacity = 255
    end

    -- Adjust transparency if player is below items icons.
    if (x > 124 and y < 80) or (x > 224 and y < 80) then
      items_opacity = 128
    elseif (x <= 124 and y >= 80) or (x <= 224 and y >= 80) then
      items_opacity = 255
    end

    -- Adjust transparency if player is below money counter.
    if (x > 248 and y < 68) then
      money_opacity = 128
    elseif (x <= 248 or y >= 68) then
      money_opacity = 255
    end

    if opacity ~= nil then
      --self.hud.heart_icon.surface:set_opacity(opacity)
      self.hud.life_bar.surface:set_opacity(opacity)
      self.hud.tech_bar.surface:set_opacity(opacity)
      self.hud.peso.surface:set_opacity(money_opacity)
    end

    if items_opacity ~= nil then
      self.hud.weapon.surface:set_opacity(items_opacity)
      self.hud.subweapon.surface:set_opacity(items_opacity)
    end
  end

  -- Update HUD every 1/20s of a second.
  sol.timer.start(self, 50, function()
    self:update_hud()
  end)
end

-- When game shuts down, dispose HUD.
function game:quit_hud()
  if self:is_hud_visible() then
    self:hide_hud()
  end
  -- Take everything out.
  self.hud = nil
end

function game:set_hud_visible(visible)
  if visible ~= self.hud_visible then
    self.hud_visible = visible

    -- Generate HUD components one-by-one.
    for i, hud_part in ipairs(self.hud) do
      if visible then
        sol.menu.start(self, hud_part)
      else
        sol.menu.stop(hud_part)
      end
    end
  end
end

-- Shortcut functions to show/hide HUD.
function game:show_hud()
  game:set_hud_visible(true)
end

function game:hide_hud()
  game:set_hud_visible(false)
end

function game:is_hud_visible()
  return self.hud_visible
end

function game:hud_on_map_changed(map)
  if self:is_hud_visible() then
    -- For each HUD part, do action when map changes.
    for i, hud in ipairs(self.hud) do
      if hud.on_map_changed ~= nil then
        hud:on_map_changed(map)
      end
    end
  end
end

function game:hud_on_paused()
  if self:is_hud_visible() then
    -- For each HUD part, do action when game paused.
    for i, hud in ipairs(self.hud) do
      if hud.on_paused ~= nil then
        hud:on_paused()
      end
    end
  end
end

function game:hud_on_unpaused()
  if self:is_hud_visible() then
    -- For each HUD part, do action when game unpaused.
    for i, hud in ipairs(self.hud) do
      if hud.on_unpaused ~= nil then
        hud:on_unpaused()
      end
    end
  end
end

function game:hud_on_dialog_started(dialog)
  if self:is_hud_visible() then
    -- For each HUD part, do action when dialog started.
    for i, hud in ipairs(self.hud) do
      if hud.on_dialog_started ~= nil then
        hud:on_dialog_started(dialog)
      end
    end
  end
end

function game:hud_on_dialog_finished()
  if self:is_hud_visible() then
    -- For each HUD part, do action when dialog finished.
    for i, hud in ipairs(self.hud) do
      if hud.on_dialog_finished ~= nil then
        hud:on_dialog_finished()
      end
    end
  end
end

---------------------------------------------------------------------
-- HUD functions.
---------------------------------------------------------------------
function game:set_life_bar_visible(visible)
  local life_bar = self.hud.life_bar
  if life_bar then
    set_hud_part_visible(life_bar, visible)
  end
end

function game:set_tech_bar_visible(visible)
  local tech_bar = self.hud.tech_bar
  if tech_bar then
    set_hud_part_visible(tech_bar, visible)
  end
end

function game:set_peso_visible(visible)
  local peso_hud = self.hud.peso
  if peso_hud then
    set_hud_part_visible(peso_hud, visible)
  end
end

function game:set_weapon_visible(visible)
  local weapon_hud = self.hud.weapon
  if weapon_hud then
    set_hud_part_visible(weapon_hud, visible)
  end
end

function game:set_subweapon_visible(visible)
  local subweapon_hud = self.hud.subweapon
  if subweapon_hud then
    set_hud_part_visible(subweapon_hud, visible)
  end
end

function game:show_life_bar()
  game:set_life_bar_visible(true)
end

function game:show_tech_bar()
  game:set_tech_bar_visible(true)
end

function game:show_peso_icon()
  game:set_peso_visible(true)
end

function game:show_weapon_icon()
  game:set_weapon_visible(true)
end

function game:show_subweapon_icon()
  game:set_subweapon_visible(true)
end

function game:hide_life_bar()
  game:set_life_bar_visible(false)
end

function game:hide_tech_bar()
  game:set_tech_bar_visible(false)
end

function game:hide_peso_icon()
  game:set_peso_visible(false)
end

function game:hide_weapon_icon()
  game:set_weapon_visible(false)
end

function game:hide_subweapon_icon()
  game:set_subweapon_visible(false)
end