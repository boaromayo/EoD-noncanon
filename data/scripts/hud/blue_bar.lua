--Create tech bar for the HUD for player's tech points.
-- Make a table for tech bar.
local tech_bar = {}

-- Return as object in-game.
function tech_bar:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

-- Setup tech bar.
function tech_bar:initialize(game)
  local surface_width
  local surface_height
  
  self.width = 42
  self.height = 6
  
  surface_width = self.width * 3
  surface_height = self.height * 2
  
  self.game = game
  self.surface = sol.surface.create(surface_width, surface_height)
  self.text_surface = sol.text_surface.create {
    font = "white_digits",
    horizontal_alignment = "left"
  }
  self.tech_gauge = sol.surface.create("hud/gauge.png")
  self.tech_bar_background = sol.sprite.create("hud/blue_bar")
  self.current_tech = game:get_magic()
  self.max_tech = game:get_max_magic()
end

-- Automatically called when started.
function tech_bar:on_started()
  self:update()
  self:redraw()
end

-- Tech bar update method.
function tech_bar:update()
  local redraw = false
  local current_tech = self.game:get_magic()
  local max_tech = self.game:get_max_magic()

  -- Update tech gauge if not maxed.
  if max_tech ~= self.max_tech then
    redraw = true
    -- Set current points to max if current > max.
    if self.current_tech > max_tech then
      self.current_tech = max_tech
    end
    self.max_tech = max_tech
    -- Show gauge if maximum tech is more than zero.
    if max_tech > 0 then
      self.tech_gauge:set_direction(max_tech / 24 - 1)
    end
  end

  -- Gradually update tech gauge based on current tech player has.
  if current_tech ~= self.current_tech then
    redraw = true
    local increment = 1
    if current_tech > self.current_tech then
      increment = 1
    elseif current_tech < self.current_tech then
      increment = -1
    end
    -- When increment is different, change life gauge length.
    if increment ~= 0 then
      self.current_tech = self.current_tech + increment
    end
  end

  -- Check to see if tech is low, or 1/5 max.
  self:check_for_alert()
  
  -- Draw text version of tech rate.
  self.text_surface:set_text(self.current_tech .. "/" .. self.max_tech)

  -- Redraw tech gauge, if needed.
  if redraw then
    self:redraw()
  end

  -- Update gauge.
  sol.timer.start(self.game, 60, function()
    self:update()
  end)
end

-- Tech bar redraw per frame method.
function tech_bar:redraw()
  local tech_rate = self.current_tech * self.width / self.max_tech
  self.surface:clear()

  -- Draw tech bar background.
  self.tech_bar_background:draw(self.surface)

  -- Draw tech gauge.
  self.tech_gauge:draw_region(54, 10, 
    math.floor(tech_rate), self.height, self.surface)

  -- Draw text tech to screen.
  self.text_surface:draw(self.surface, self.width + 4, 4)
end

-- Set tech bar to alert if tech is low.
function tech_bar:check_for_alert()
  if self.current_tech <= self.max_tech / 5 then
    -- Make gauge blink red if it hasn't done so.
    if self.tech_bar_background:get_animation() ~= "danger" then
      self.tech_bar_background:set_animation("danger")
    end
  elseif self.tech_bar_background:get_animation() ~= "normal" then
    -- Turn back to normal if tech is not low.
    self.tech_bar_background:set_animation("normal")
  end
end

-- Set tech bar's position on-screen.
function tech_bar:set_dst_position(x, y)
  self.dest_x = x
  self.dest_y = y
end

-- Tech bar drawing method.
function tech_bar:on_draw(screen)
  -- Draw if player has special skills.
  if self.max_tech > 0 then
    local x, y = self.dest_x, self.dest_y
    self.surface:draw(screen, x, y)
  end
end

return tech_bar