-- weapon icon (center icon) for the HUD
-- to indicate equipped weapon
-- note this does not take any amounts.
local weapon_icon = {}

-- Make new object of weapon icon.
function weapon_icon:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

-- Initialize weapon icon.
function weapon_icon:initialize(game)
  local width = 32
  local height = 32

  self.game = game
  self.slot = 1
  self.surface = sol.surface.create(width, height)
  self.icon_surface = sol.surface.create("hud/icon_slot.png")
  --self.gauge = sol.surface.create("hud/gauge.png")
  self.sprite = sol.sprite.create("entities/items")
  self.dest_x = 0
  self.dest_y = 0
  self.weapon_equipped = nil
  self.weapon_variant_displayed = 0
  self.weapon_life = 0
  self.weapon_max_life = 0
end

-- Function called automatically when HUD starts.
function weapon_icon:on_started()
  self:update()
  self:redraw()
end

-- Update method.
function weapon_icon:update()
  local redraw = false
  local weapon = self.game:get_item_assigned(self.slot)
  --local weapon_max_life = weapon:get_max_weapon_life()
  --local weapon_life = weapon:get_weapon_life()

  -- Update to equipped weapon.
  if self.weapon_equipped ~= weapon then
    redraw = true
    self.weapon_equipped = weapon
    if weapon ~= nil then
      self.sprite:set_animation(weapon:get_name())
    end
  end

  -- Get the variant of the equipped weapon.
  if weapon ~= nil then
    local variant = weapon:get_variant()
    if self.weapon_variant_displayed ~= variant then
      redraw = true
      self.weapon_variant_displayed = variant
      self.sprite:set_direction(variant - 1)
    end
  end

  -- Update weapon life if gauge not maximum.
  --[[if weapon_max_life ~= self.weapon_max_life then
    redraw = true
    if weapon_life > weapon_max_life then
      weapon_life = weapon_max_life
    end
    self.weapon_max_life = weapon_max_life
  end
  
  -- Gradually update weapon life based on current weapon life.
  if weapon_life ~= self.weapon_life then
    redraw = true
    local increment = 1
    if weapon_life > self.weapon_life then
      increment = 1
    elseif weapon_life < self.weapon_life then
      increment = -1
    end
    if increment ~= 0 then
      self.weapon_life = self.weapon_life + increment
    end
  end--]]
  
  -- Redraw if a different weapon has been equipped.
  if redraw then
    self:redraw()
  end

  -- Update every 1/20th of a second.
  sol.timer.start(self, 50, function()
    self:update()
  end)
end

-- Redraw weapon icon and gauge.
function weapon_icon:redraw()
  self.surface:clear()
  
  -- Draw icon border to screen.
  self.icon_surface:draw(self.surface)
  
  -- Draw equipped weapon to screen if weapon equipped.
  local icon_w, icon_h = self.icon_surface:get_size()
  
  if self.weapon_equipped ~= nil then
    self.sprite:draw(self.surface, icon_w / 2, icon_h / 2 + 5)
  end
end

-- Set weapon icon's drawing destination.
function weapon_icon:set_dst_position(x, y)
  self.dest_x = x
  self.dest_y = y
end

-- Draw icon to screen.
function weapon_icon:on_draw(screen)
  -- Check if player has weapon. 
  -- Ensure HUD will stay if player loses weapon later.
  if self.game:has_weapon() and 
    not self.game:get_value("possession_weapon") then
      self.game:set_value("possession_weapon", true)
  end
  local has_weapon = self.game:get_value("possession_weapon")

  if has_weapon then
    local x, y = self.dest_x, self.dest_y
    self.surface:draw(screen, x, y)
  end
end

return weapon_icon