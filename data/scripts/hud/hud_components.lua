-- Make a table of HUD, or heads-up display components.
-- Credit to Max from his Ocean's Heart game for this idea.

-- Each entry has a name for the component, 
-- a script to load the component, 
-- and the destination coordinates.
-- More items can be added.
local hud_components = {
  -- Health or life bar.
	{
    name = "life_bar",
		script = "red_bar",
		x = 8,
		y = 8
	},
  -- Technique bar.
	{
    name = "tech_bar",
		script = "blue_bar",
		x = 8,
		y = 20
	},
  -- Item icon.
	--[[{
    name = "item",
		script = "item_icon",
		x = 188,
		y = 4,
    slot = 1
	},--]]
  -- Weapon icon.
	{
    name = "weapon",
		script = "weapon_icon",
		x = 156,
		y = 4,
    slot = 1
	},
  -- Subweapon icon.
	{
    name = "subweapon",
		script = "subweapon_icon",
		x = 188,
		y = 4,
    slot = 2
	},
  -- Currency.
	{
    name = "peso",
    script = "pesos",
    x = 258,
    y = 8
	}
}

return hud_components