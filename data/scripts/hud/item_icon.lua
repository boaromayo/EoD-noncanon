-- item icon (left icon) for the HUD
-- to indicate equipped item.

-- Initialize table for item icon.
local item_icon = {}

-- Create item icon object.
function item_icon:new(game)
  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

-- Initialize item icon.
function item_icon:initialize(game)
  local width = 32
  local height = 32

  self.game = game
  self.slot = 1
  self.surface = sol.surface.create(width, height)
  self.icon_surface = sol.surface.create("hud/icon_slot.png")
  self.sprite = sol.sprite.create("entities/items")
  self.amount_display = sol.text_surface.create{
    horizontal_alignment = "right",
    vertical_alignment = "bottom"
  }
  self.dest_x = 0
  self.dest_y = 0
  self.item_equipped = nil
  self.current_amount = nil
  self.max_amount = nil
end

-- Function called automatically when HUD starts.
function item_icon:on_started()
  self:update()
  self:redraw()
end

-- Update method.
function item_icon:update()
  local redraw = false
  local item = self.game:get_item_assigned(self.slot)

  -- If amount is at max, fix current to maximum.
  if self.item_equipped ~= nil then
    redraw = true
    self.item_equipped = item
    if item ~= nil then
      self.sprite:set_animation(item:get_name())
    end
  end

  -- Get variant of item equipped.
  if item ~= nil then
    local variant = item:get_variant()
    if self.item_equipped ~= variant then
      redraw = true
      self.item_equipped = variant
    end

    if current_amount ~= nil and max_amount ~= nil then
      if current_amount > max_amount then
        current_amount = max_amount
        redraw = true
      end
    end
  end

  -- If needed to redraw icon, do so.
  if redraw then
    self:redraw()
  end

  -- Update every 1/20th of a second.
  sol.timer.start(self.game, 50, function()
    self:update()
  end)
end

-- Redraw item icon.
function item_icon:redraw()
  self.surface:clear()

  self.icon_surface:draw(self.surface)
  
  local icon_w, icon_h = self.icon_surface:get_size()
  
  -- Draw equipped item to screen.
  if self.item_equipped ~= nil then
    self.item_sprite:draw(self.surface, icon_w / 2, icon_h / 2 + 4)
  end

  -- If player has an amount of more than one item, update text.
  if self.current_amount ~= nil then
    self.current_amount:draw(self.surface, icon_w / 2 + 3, icon_h / 2 + 7)
  end
end

-- Set item icon's destination position
function item_icon:set_dst_position(x,y)
  self.dest_x = x
  self.dest_y = y
end

-- Draw item icon.
function item_icon:on_draw(screen)
  local x, y = self.dest_x, self.dest_y
  self.surface:draw(screen, x, y)
end

return item_icon