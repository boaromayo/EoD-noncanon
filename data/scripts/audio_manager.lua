-- This feature is used to manipulate
-- sounds and music, and enable features such
-- as fade, or play music and sounds.

-- Based on the audio manager script
-- created by Christopho, under the terms
-- of the General Public License (GPL).

-- To add this script in the game, call:
-- require("scripts/audio_manager")

local audio_manager = {}

local sounds_played = {}

local function music_exists(music, format)
  local format = format or ".ogg"
  if music == nil then
    return false
  end

  return sol.file.exists("musics/" .. music .. format)
end

local function sound_exists(sound, format)
  local format = format or ".ogg"
  if sound == nil then
    return false
  end

  return sol.file.exists("sounds/" .. sound .. format)
end

-- Softer version of sol.audio.play_music(), where
-- a warning instead of error is produced
-- when a music does not exist.
function audio_manager:play_music(music)
  if music == nil then
    print("WARNING: No music was included for playback.")
    return
  end

  if music_exists(music) then
    sol.audio.play_music(music)
  else
    print("WARNING: Cannot play music file " .. music .. " as it doesn't exist.")
  end
end

-- Softer version of sol.audio.play_sound(), where
-- a warning instead of error is produced
-- when a sound does not exist.
function audio_manager:play_sound(sound)
  if sound == nil then
    print("WARNING: No sound was included for playback.")
    return
  end

  if sound_exists(sound) then
    -- Do not duplicate sounds too quickly if it has been played.
    if sounds_played[sound] then
      return
    end
    sol.audio.play_sound(sound)
    sounds_played[sound] = true
    sol.timer.start(sol.main, 50, function() 
      sounds_played[sound] = nil
    end)
  else
    print("WARNING: Cannot play sound file " .. sound .. " as it doesn't exist.")
  end
end

-- Softer version of sol.audio.stop_music().
function audio_manager:stop_music()
  if sol.audio.get_music() ~= nil then
    sol.audio.stop_music()
  end
end


-- Fades-out the music based on the delay and final volume.
-- NOTE: This is experimental.
--[[function sol.audio:fade_out_music(volume, delay)

  -- Get initial in-game volume and save.
  local init_volume = sol.audio.get_music_volume()
  local diff = init_volume - volume

  if diff <= 0
    return 
  end

  -- Start fade-out music.
  for i = init_volume, volume, math.floor(diff/10) do
    sol.timer.start(self.game, delay, function()
      sol.audio.set_music_volume(i)
    end)
  end
end--]]

return audio_manager