-- This script is a custom state that 
-- addresses when the player is climbing (ladder, cliff, etc.)
-- Start climbing stopped animation when stopped, and 
-- make a custom climbing animation while moving.

-- The following features will be added:

--  hero:start_climbing()

local state = sol.state.create("climbing") -- Make custom climbing state.
local hero_meta = sol.main.get_metatable("hero") -- Fetch metatable to add another method to hero.
state:set_can_control_movement(true)
state:set_can_control_direction(true)
state:set_can_use_sword(false)
state:set_can_use_item(false)
state:set_can_interact(false)
state:set_can_push(false)
state:set_can_grab(false)

function state:on_started()
  -- Testing purposes
  print("Climbing state started")
end

function state:on_finished()
  local hero = state:get_map():get_hero()
  hero:set_state("free")

  print("Climbing state finished")
end

function state:on_map_started()
  local hero = state:get_map():get_hero()

  if hero:get_state_object():get_description() == "climbing" then
    hero:set_state(state)
  end
end

function state:on_movement_changed(movement)
  local map = state:get_map()
  local hero = map:get_hero()

  movement = movement or hero:get_movement()
  local x, y, layer = hero:get_position()
  local speed = hero:get_walking_speed()
  local ladder_speed = 48
  local state_speed = map:get_ground(x, y, layer) == "ladder" and ladder_speed or speed
  local animation = speed == 0 and "climbing_stopped" or "climbing_walking"

  if hero:get_animation() ~= animation then
    hero:set_animation(animation)
    hero:get_sprite():set_frame(0)
  end
  if speed ~= state_speed then
    hero:set_walking_speed(state_speed)
  end
end

-- New feature: hero start climbing.
function hero_meta:start_climbing()

  local hero = self
  hero:start_state(state)
end

return state