-- Debug manager script.
-- Only enable if a file called debug exists in the
-- directory where the save files are written.

-- To add this script in-game, call:
-- require("scripts/debug_manager")

-- Debug print function.
function debug_print(...)
  local timestring = "[" .. sol.main.get_elapsed_time() .. "] "
  print(timestring, (...))
end

if not sol.file.exists("debug") then
  return
end

local debug_manager = {}
local console = require("scripts/console")

function debug_manager:on_key_pressed(key, modifiers)
  
  local handled = true
  -- Ensure debug console is off before triggering in-game debug features.
  if not console.enabled then
    if key == "f12" then
      sol.menu.start(sol.main, console)
    elseif sol.main.game ~= nil then
      local game = sol.main.game
      local map = game:get_map()
      local hero = nil
      if game ~= nil and map ~= nil then
        hero = map:get_hero()
      end
   
      -- In-game cheat keys.
      if key == "p" then
        game:add_life(5)
        debug_print("add life by 5")
      elseif key == "l" then
        game:remove_life(1)
        debug_print("take life by 1")
      elseif key == "o" then
        game:add_magic(1)
        debug_print("add tech by 1")
      elseif key == "k" then
        game:remove_magic(1)
        debug_print("take tech by 1")
      elseif key == "i" then
        game:add_money(50)
        debug_print("add money by 50")
      elseif key == "j" then
        game:add_money(15)
        debug_print("add money by 15")
      elseif key == "kp 5" then
        if game:get_max_life() <= 100 then
          game:set_max_life(game:get_max_life() + 10)
          debug_print("max life up by 10, max life: " .. tostring(game:get_max_life()))
        else
          debug_print("max life too high")
        end
      elseif key == "kp 6" then
        if game:get_max_life() > 10 then
          game:set_max_life(game:get_max_life() - 10)
          debug_print("max life down by 10, max life: " .. tostring(game:get_max_life()))
        else
          debug_print("max life too low")
        end
      elseif key == "kp 7" then
        game:set_max_magic(0)
        debug_print("max tech set to 0")
      elseif key == "kp 8" then
        if game:get_max_magic() < 30 then
          game:set_max_magic(game:get_max_magic() + 10)
          debug_print("max tech up by 10, max tech: " .. tostring(game:get_max_magic()))
        else
          debug_print("max tech too high")
        end
      elseif key == "kp 9" then
        if game:get_max_magic() > 10 then
          game:set_max_magic(game:get_max_magic() - 10)
          debug_print("max tech down by 10, max tech: " .. tostring(game:get_max_magic()))
        else
          debug_print("max tech too low")
        end
      elseif key == "kp 1" then
        if game:get_attack() < 100 then
          game:set_attack(game:get_attack() + 1)
          debug_print("attack_power up by 1")
        else
          debug_print("attack_power is at max")
        end
      elseif key == "kp 3" then
        if game:get_attack() > 1 then
          game:set_attack(game:get_attack() - 1)
          debug_print("attack_power down by 1")
        else
          debug_print("attack_power is at 1")
        end
      elseif key == "kp 2" then
        if game:get_defense() < 100 then
          game:set_defense(game:get_defense() + 1)
          debug_print("defense_power up by 1")
        else
          debug_print("defense_power is at max")
        end
      elseif key == "kp 4" then
        if game:get_defense() > 1 then
          game:set_defense(game:get_defense() - 1)
          debug_print("defense_power down by 1")
        else
          debug_print("defense_power is at 1")
        end
      elseif key == "t" then
        local x, y, layer = hero:get_position()
        if layer >= -9 then
          hero:set_position(x, y, layer + 1)
          debug_print("hero layer up by 1, layer: " .. tostring(layer + 1))
        end
      elseif key == "g" then
        local x, y, layer = hero:get_position()
        if layer <= 9 then
          hero:set_position(x, y, layer - 1)
          debug_print("hero layer down by 1, layer: " .. tostring(layer - 1))
        end
      elseif key == "r" then
        debug_manager.super_fast_speed = 300
        if hero:get_walking_speed() == debug_manager.super_fast_speed then
          hero:set_walking_speed(debug_manager.normal_speed)
          debug_print("walking speed: normal")
        else
          debug_manager.normal_speed = hero:get_walking_speed()
          hero:set_walking_speed(debug_manager.super_fast_speed)
          debug_print("walking speed: super fast")
        end
      elseif key == "y" then
        if not debug_manager.ignore_walls then
          debug_manager.ignore_walls = true
          hero:get_movement():set_ignore_obstacles(debug_manager.ignore_walls)
          debug_print("ignore obstacles: yes")
        else
          debug_manager.ignore_walls = false
          hero:get_movement():set_ignore_obstacles(debug_manager.ignore_walls)
          debug_print("ignore obstacles: no")
        end
      elseif key == "h" then
        game:set_hud_visible(not game:is_hud_visible())
        debug_print("hud visible: " .. tostring(game:is_hud_visible()))
      else
        -- Not an in-game debug key.
        handled = false
      end

    else
      -- Not a debug key.
      handled = false
    end

  end
  return handled
end

sol.menu.start(sol.main, debug_manager)

return debug_manager