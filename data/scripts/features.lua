-- Assemble all extra gameplay features for this quest.

-- To add this script into the game, call: 
-- require("scripts/features")

-- Features can be enabled or disabled by
-- commenting or uncommenting lines below.
require("scripts/debug_manager")
require("scripts/multi_events")
require("scripts/maps/map_manager")
require("scripts/meta/game")
require("scripts/meta/map")
require("scripts/meta/camera")
require("scripts/meta/destructible")
require("scripts/meta/item")
require("scripts/meta/enemy")
--require("scripts/coroutine_helper") -- Uncomment for versions >= 1.6

return true