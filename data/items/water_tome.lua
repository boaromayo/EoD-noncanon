local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("water_tome")
  self:set_assignable(true)
  self:set_shadow(nil)
end

-- Event called when the game is initialized.
function item:on_started()
  -- Set spell cost based on hero's max tech.
  if game:get_max_magic() >= 2 then
    self.tech_cost = 4
  else
    self.tech_cost = 1
  end
end

-- Perform water secular magic.
function item:water_magic()
  -- Unleash a water torrent and heal hero.
  -- Spell strength depends on how much tech points are consumed.
  -- Get location of hero to place source of water magic.
  -- Healing factor variable.
  local heal = 0

  if self.tech_cost == 4 then
    -- Heal hero based on maximum amount of health.
    heal = game:get_max_life() / 2
  else
    -- Heal hero based on maximum amount of health.
    heal = game:get_max_life() / 4
  end

  game:add_life(heal)
end

-- Event called when the hero is using this item.
function item:on_using()
  -- Check costs before casting water secular magic.
  if game:get_magic() >= self.tech_cost then
    game:remove_magic(self.tech_cost)
    item:water_magic()
  end

  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
