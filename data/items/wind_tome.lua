local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("wind_tome")
  self:set_assignable(true)
  self:set_shadow(nil)
end

-- Event called when the game is initialized.
function item:on_started()
  -- Set spell cost.
  self.tech_cost = 2
end

-- Perform wind secular magic.
function item:wind_magic()
end

-- Event called when the hero is using this item.
function item:on_using()
  -- Check costs before casting wind secular magic.
  if game:get_magic() >= self.tech_cost then
    game:remove_magic(self.tech_cost)
    item:wind_magic()
  end

  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
