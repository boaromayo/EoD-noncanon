local item = ...
local game = item:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("potion")
  self:set_amount_savegame_variable("potion_amount")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(true)
  self:set_shadow("small")
  self:set_assignable(false)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(15)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()
  -- Check if hero has at least 1 and is injured.
  if item:has_amount(1) then
    if game:get_life() < game:get_max_life() then
      -- Calculate healing amount.
      local heal = item:get_recovery_rate()
      -- Recover hero's health and remove 1 potion.
      game:add_life(heal)
      item:remove_amount(1)
      -- Play recovery sound.
      audio_manager:play_sound("heart")
    elseif game:get_life() == game:get_max_life() then
      audio_manager:play_sound("wrong")
    end
  end
end

-- Calculate healing amount.
function item:get_recovery_rate()
  -- Recovery cap.
  local MAX_RECOVERY = 20
  -- Get recovery amount based on half of player's max health.
  local heal = game:get_max_life() / 2
  -- Cap recovery at max recovery amount.
  if heal > MAX_RECOVERY then
    heal = MAX_RECOVERY
  end
  return heal
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
