-- Lua script of item sword.
-- This script is executed only once for the whole game.
local item = ...
local game = item:get_game()

local entities_touched = {}

--local DURABILITY = 60

-- Event called when the item is first created.
function item:on_created()
  -- Make sure it can disappear
  -- and when picked up the first time, brandished.
  self:set_savegame_variable("possession_sword")
  self:set_can_disappear(false)
  self:set_assignable(true)
  self:set_brandish_when_picked(true)
  self:set_sound_when_picked(nil)
  self:set_shadow(nil)
  self:set_is_weapon(true)
  self:set_attack(3)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_obtainable(not game:has_item("sword"))
end

-- Event called when the hero obtains the item.
function item:on_obtaining()
  -- Automatically assign sword to weapon key
  -- and enable player to use a sword.
  item:set_obtainable(not game:has_item("sword"))
  -- Set sword durability.
  --game:set_value("weapon_max_life", DURABILITY)
  --game:set_value("weapon_life", game:get_value("weapon_max_life"))
  -- Assign item to slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    game:set_item_assigned(slot, item)
    game:set_attack(item:get_attack())
  end
end

-- Function to check sword hit enemy.
local function on_collided_enemy(sword, enemy)
  -- Ensure entity collided with weapon.
  if entities_touched[enemy] then
    return
  end
  entities_touched[enemy] = true
  
  -- Place reaction to enemy getting hurt here. In this case,
  -- a sword attack causes enemy to get damage unless they are
  -- immune to swords or has a high defense.
  local reaction = enemy:calculate_damage(game:get_attack())
  enemy:set_attack_consequence("sword", reaction)
end

-- Function to check if sword collided with switch.
local function on_collided_switch(sword, switch, sword_sprite, switch_sprite)
  -- Ensure entity collided with weapon.
  if entities_touched[switch] then
    return
  end
  entities_touched[switch] = true

  if switch_sprite ~= nil and
      switch_sprite:get_animation_set() == "entities/switch_hit" then
    
    if not switch.is_activated and
        switch.on_activated then
      switch:set_activated(true)
      switch:on_activated()
    end
  end
end

-- Function to check if destructible can be destroyed by sword.
local function can_be_destroyed(destructible)
  local sprite = destructible:get_sprite()
  local sprite_id = sprite:get_animation_set()
  return not sprite_id:match("^destructibles/(stone_|rock_|thorn_)")
end

-- Function to check if a destructible was hit.
local function on_collided_destructible(sword, destructible, sword_sprite, sprite)
  local destr_x, destr_y, destr_layer = destructible:get_position()
  local treasure = { destructible:get_treasure() }
  
  -- Ensure entity collided with weapon.
  if entities_touched[destructible] then
    return
  end
  entities_touched[destructible] = true

  if sprite ~= nil then
    if sprite:get_animation() ~= "on_ground" then
      return
    end
    if can_be_destroyed(destructible) then
      if destructible:get_destruction_sound() ~= nil then
        sol.audio.play_sound(destructible:get_destruction_sound())
      end
      if sprite:has_animation("destroy") then
        sprite:set_animation("destroy", function()
          destructible:remove()
        end)
      else
        destructible:remove()
      end
    end
  end
  
  if treasure ~= nil then
    local map = destructible:get_map()
    local pickable = map:create_pickable({
      x = destr_x,
      y = destr_y,
      layer = destr_layer,
      treasure_name = treasure[1],
      treasure_variant = treasure[2],
      treasure_savegame_variable = treasure[3],
    })
  end
end

-- Function to get sword sprite based on hero character chosen.
local function get_weapon_sprite()
  local sprite = game:get_value("den_gender") == "girl" and "sword_offset" or "sword"
  return sprite
end

-- Function to calculate weapon durability damage.
--[[local function check_weapon_damage()
  local damage = game:get_attack() - math.random(0, 1)
  local current_life = game:get_value("weapon_life") - damage
  game:set_value("weapon_life", current_life)
end--]]

-- Event called when the hero is using this item.
function item:on_using()
  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, layer = hero:get_position()
  local direction = hero:get_direction()
  local sword = map:create_custom_entity({
    x = x,
    y = y,
    layer = layer,
    width = 16,
    height = 16,
    direction = direction,
    sprite = "hero/" .. get_weapon_sprite(),
  })
  sol.audio.play_sound("sword1")

  -- Reset collision checks before swinging.
  entities_touched = {}

  -- Check collisions for different interactions
  -- while sword swinging.
  sword:add_collision_test("sprite", function(weapon, entity, weapon_sprite, entity_sprite)
    if entity:get_type() == "enemy" then
      on_collided_enemy(weapon, entity)
      -- Degrade sword for every hit on an enemy.
      --check_weapon_damage()
    end
    if entity:get_type() == "destructible" then
      on_collided_destructible(weapon, entity, weapon_sprite, entity_sprite)
    end
    if entity:get_type() == "switch" then
      on_collided_switch(weapon, entity, weapon_sprite, entity_sprite)
    end
  end)

  -- Start sword animation.
  hero:freeze()
  hero:set_animation("sword", function()
    sword:remove()
    hero:unfreeze()
    item:set_finished()
  end)

  -- When sword is broken, player cannot use sword until fixed.
  --[[if item:get_amount() == 0 then
    game:set_ability("sword", 0)
    item:set_obtainable(not game:has_ability("sword"))
    item:remove()
  end--]]
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end