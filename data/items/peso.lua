local item = ...
local game = item:get_game()

-- Make settings when game is initialized.
function item:on_created()
    self:set_can_disappear(true)
    self:set_brandish_when_picked(false)
    --self:set_sound_when_picked("picked_rupee")
end

function item:on_started()
   self:set_obtainable(game:get_item("wallet"):has_variant())
end

-- Set values when item picked up.
function item:on_obtaining(variant, savegame_variable)
  -- set weights of peso.
  local amounts = { 1, 5, 10, 20, 50, 100 }
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'peso'")
  end

  game:add_money(amount)
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- Create custom, gradual falling-down type of movement.
  -- Animation should be like paper falling down from the sky.
  if pickable:get_falling_height() ~= 0 then
    local trajectory = {
      {0, 0},
      {0, -2},
      {0, -2},
      {0, -2},
      {0, -2},
      {0, -2},
      {0, 0},
      {0, 0},
      {1, 1},
      {1, 1},
      {1, 0},
      {1, 1},
      {1, 1},
      {0, 0},
      {-1, 0},
      {-1, 1},
      {-1, 0},
      {-1, 1},
      {-1, 0},
      {-1, 1},
      {0, 1},
      {1, 1},
      {1, 1},
      {-1, 0}
    }
    local movement = sol.movement.create("pixel")
    movement:set_trajectory(trajectory)
    movement:set_loop(true)
    movement:set_delay(150)
    movement:set_ignore_obstacles(false)
    movement:start(pickable)
    pickable:get_sprite():set_animation("peso_falling")
  end
end