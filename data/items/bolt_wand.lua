-- Lua script of item bolt_wand.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("bolt_wand")
  self:set_can_disappear(false)
  self:set_assignable(true)
  self:set_sound_when_picked(nil)
  self:set_shadow(nil)
  self:set_is_weapon(true)
  self:set_attack(1)
end

-- Event called when the game is initialized.
function item:on_started()

  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.
  self:set_obtainable(not game:has_item("bolt_wand"))
end

-- Event called after the hero obtains the item.
function item:on_obtained()
  -- Ensure no seal staves appear after one is obtained.
  item:set_obtainable(not game:has_item(item:get_name()))
  -- Assign item to slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    assert(self:is_assignable(), "Cannot assign weapon")
    game:set_item_assigned(slot, item)
    game:set_attack(item:get_attack())
  end
end

-- Shoot small lightning bolt ball.
function item:shoot_bolt()
  
  -- Get map, hero's location and direction to setup bolt creation.
  local map = item:get_map()
  local hero = game:get_hero()
  local direction = hero:get_direction()

  local x, y, layer = hero:get_center_position()
  local bolt = map:create_custom_entity({
    x = x,
    y = y + 4,
    layer = layer,
    width = 10,
    height = 10,
    direction = direction--,
    --model = ""
  })

  local angle = direction * (math.pi / 2)
  local movement = sol.movement.create("straight")
  movement:set_speed(192)
  movement:set_angle(angle)
  movement:set_smooth(false)
  movement:start(bolt)
end

-- Unleash lightning!
function item:shoot_lightning()
  -- Get map, hero's location and direction to setup bolt creation.
  local map = item:get_map()
  local hero = game:get_hero()
  local direction = hero:get_direction()

  local x, y, layer = hero:get_center_position()
  local lightning = map:create_custom_entity({
    x = x,
    y = y + 4,
    layer = layer,
    width = 10,
    height = 10,
    direction = direction--,
    --model = ""
  })

  local angle = direction * (math.pi / 2)
  local movement = sol.movement.create("straight")
  movement:set_speed(200)
  movement:set_angle(angle)
  movement:set_smooth(false)
  movement:start(lightning)
end

-- Event called when the hero is using this item.
function item:on_using()

  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  local map = item:get_map()
  local hero = game:get_hero()
  local direction = hero:get_direction()
  local x, y, layer = hero:get_position()

  --hero:set_animation("rod")
  local bolt_wand = map:create_custom_entity({
    x = x,
    y = y,
    layer = layer,
    direction = direction--,
    --sprite = "hero/bolt_wand"
  })

  -- Fire a bolt of electricity, no cost.
  item:shoot_bolt()

  -- Take out bolt wand after delay.
  sol.timer.start(hero, 200, function()
    bolt_wand:remove()
    item:set_finished()
  end)
end

-- Event called to set custom commands to weapon.
function item:on_command_pressed(command)
  local map = item:get_map()
  local hero = game:get_hero()
  local direction = hero:get_direction()
  local x, y, layer = hero:get_center_position()

  -- Only charge wand when player has enough tech power.
  if command == "charge" and game:get_magic() >= tech_cost then
    hero:set_animation("rod_brandish")
    local spark = map:create_custom_entity({
      x = x - 4,
      y = y + 4,
      layer = layer,
      direction = direction--,
      --sprite = "hero/wand_spark"
    })
  end
  item:set_finished()
end

-- Event called to release after charge.
function item:on_command_released(command)
  local map = item:get_map()
  local hero = map:get_hero()
  local direction = hero:get_direction()
  local x, y, layer = hero:get_position()
  
  if command == "charge" then
    hero:set_animation("rod")

    local bolt_wand = map:create_custom_entity({
      x = x,
      y = y,
      layer = layer,
      direction = direction,
      --sprite = "hero/bolt_wand"
    })

    -- If player has enough tech, unleash lightning!
    if game:get_magic() >= tech_cost then
      item:shoot_lightning()
      game:remove_magic(tech_cost)
    end
    --else
      --sol.audio.play_sound("buzzer")
    --end

    -- Take out bolt wand after small delay.
    sol.timer.start(hero, 200, function()
      bolt_wand:remove()
      item:set_finished()
    end)
  end
end