local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("wallet")
  self:set_brandish_when_picked(true)
  self:set_sound_when_brandished(nil)
end

-- Set values for when hero obtains item.
function item:on_variant_changed(variant)
  -- If wallet is not in player's possession, set amount to 0
  -- and ensure pesos in-game cannot be obtained.
  local max_amount = 0
  local peso = game:get_item("peso")
  if variant == 0 then
    peso:set_obtainable(false)
  else
    local max_amounts = { 1000, 5000, 10000 }
    max_amount = max_amounts[variant]
    if max_amount == nil then
      error("Invalid variant '" .. variant .. "' for item 'wallet'")
    end

    peso:set_obtainable(true)
  end

  game:set_max_money(max_amount)
end