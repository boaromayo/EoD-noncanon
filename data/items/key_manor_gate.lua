-- Key to open the manor gates.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_manor_gate_key")
end

-- Event called after hero gets key.
function item:on_obtained()
  local manor_gate_key_obtained = 14
  game:set_main_story(manor_gate_key_obtained)
end
