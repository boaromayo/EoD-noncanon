-- Key to open the graveyard.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_grave_key")
end

-- Event called after hero gets key.
function item:on_obtained()
  local grave_key_obtained = 12
  game:set_main_story(grave_key_obtained)
end
