-- Lua script of item lightbrand.
-- This script is executed only once for the whole game.
local item = ...
local game = item:get_game()

local entities_touched = {}

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("lightbrand")
  self:set_assignable(true)
  self:set_shadow(nil)
  self:set_is_weapon(true)
  self:set_attack(20)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_obtainable(not game:has_item("lightbrand"))
end

-- Event called when the hero obtains the item.
function item:on_obtaining()
  -- Automatically set weapon to weapon key
  -- and enable player to use weapon.
  item:set_obtainable(not game:has_item("lightbrand"))
  -- Assign item to slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    game:set_item_assigned(slot, item)
    game:set_attack(item:get_attack())
  end
end

-- Function to check weapon hit enemy.
local function on_collided_enemy(weapon, enemy)
  -- Ensure entity collided with weapon 
  -- to prevent redundant collisions.
  if entities_touched[enemy] then
    return
  end
  entities_touched[enemy] = true

  local reaction = enemy:calculate_damage(game:get_attack())
  enemy:set_attack_consequence("lightbrand", reaction)
end

-- Function to check if weapon collided with switch.
local function on_collided_switch(weapon, switch, weapon_sprite, switch_sprite)
  -- No redundant collisions.
  if entities_touched[switch] then
    return
  end
  entities_touched[switch] = true

  if switch_sprite ~= nil and
      switch_sprite:get_animation_set() == "entities/switch_hit" then

    if not switch.is_activated then
      switch:set_activated(true)
      if switch.on_activated then
        switch:on_activated()
      end
    else
      switch:set_activated(false)
      if switch.on_inactivated then
        switch:on_inactivated()
      end
    end
  end
end

-- Function to check if weapon collided with destructible.
local function on_collided_destructible(weapon, destructible, weapon_sprite, sprite)
  local destr_x, destr_y, destr_layer = destructible:get_position()
  local treasure = { destructible:get_treasure() }

  -- No redundant collisions.
  if entities_touched[destructible] then
    return
  end
  entities_touched[destructible] = true

  if sprite ~= nil then
    -- Destructible is already being destroyed.
    if sprite:get_animation() == "on_ground" then
      return
    end
    if destructible:get_destruction_sound() ~= nil then
      sol.audio.play_sound(destructible:get_destruction_sound())
    end
    if sprite:has_animation("destroy") then
      sprite:set_animation("destroy", function()
        destructible:remove()
      end)
    else
      destructible:remove()
    end
  end

  if treasure ~= nil then
    local map = destructible:get_map()
    local pickable = map:create_pickable({
      x = destr_x,
      y = destr_y,
      layer = destr_layer,
      treasure_name = treasure[1],
      treasure_variant = treasure[2],
      treasure_savegame_variable = treasure[3],
    })
  end
end

-- Get weapon sprite based on player chosen.
local function get_weapon_sprite()
  local sprite = game:get_value("den_gender") == "girl" and "lightbrand_offset" or "lightbrand"
  return sprite
end

-- Event called when the hero is using this item.
function item:on_using()

  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, layer = hero:get_position()
  local direction = hero:get_direction()
  local lightbrand = map:create_custom_entity({
    x = x,
    y = y,
    layer = layer,
    width = 16,
    height = 16,
    direction = direction,
    sprite = "hero/" .. get_weapon_sprite(),
  })

  -- Reset collision checks before swinging.
  entities_touched = {}

  -- Check collisions for different interactions
  -- while swinging weapon.
  lightbrand:add_collision_test("sprite", function(weapon, entity, weapon_sprite, entity_sprite)
    if entity:get_type() == "enemy" then
      on_collided_enemy(weapon, entity)
    end
    if entity:get_type() == "switch" then
      on_collided_switch(weapon, entity, weapon_sprite, entity_sprite)
    end
    if entity:get_type() == "destructible" then
      on_collided_destructible(weapon, entity, weapon_sprite, entity_sprite)
    end
  end)

  -- Perform weapon animation.
  hero:freeze()
  hero:set_animation("sword", function()
    lightbrand:remove()
    hero:unfreeze() 
    item:set_finished()
  end)
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
