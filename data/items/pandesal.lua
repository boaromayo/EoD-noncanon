local item = ...
local game = item:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("pandesal")
  self:set_amount_savegame_variable("pandesal_amount")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
  self:set_shadow(nil)
  self:set_assignable(false)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(15)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()
-- Check if hero is fully healed.
  -- If so, buzz and do not use.
  if item:has_amount(1) then
    if game:get_life() < game:get_max_life() then
      -- Recovery cap.
      local MAX_RECOVERY = 10
      -- Heal hero and remove 1 bread.
      game:add_life(MAX_RECOVERY)
      item:remove_amount(1)
      -- Play recovery sound.
      audio_manager:play_sound("heart")
    elseif game:get_life() == game:get_max_life() then
      -- Play buzzer sound.
      audio_manager:play_sound("wrong")
    end
  end
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
