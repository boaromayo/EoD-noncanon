local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("nitro_bottle")
  self:set_amount_savegame_variable("nitro_bottle_amount")
  self:set_assignable(true)
  self:set_can_disappear(true)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(15)
end

-- Event called when hero obtains the item.
function item:on_obtaining()
  -- Automatically assign the subweapon to the subweapon slot
  -- if nothing is assigned.
  local slot = 2
  if game:get_item_assigned(slot) == nil then
    game:set_item_assigned(slot, item)
  end

  item:add_amount(1)
end

-- Event called when the hero uses the nitro bottles of his inventory by pressing
-- the corresponding item key.
function item:on_using()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, layer = hero:get_position()
  local direction = hero:get_direction()

  if direction == 0 then
    x = x + 16
  elseif direction == 1 then
    y = y - 16
  elseif direction == 2 then
    x = x - 16
  elseif direction == 3 then
    y = y + 16
  end

  item:get_map():create_bomb{
    x = x,
    y = y,
    layer = layer
  }
end

