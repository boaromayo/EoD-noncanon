local item = ...
local game = item:get_game()

-- Event called when the is still being created.
function item:on_created()
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
  self:set_shadow("small")
  self:set_sound_when_picked("picked_item")
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_obtainable(game:get_max_magic() > 0)
end

-- Event called when hero picks it up on the ground.
function item:on_obtaining(variant, savegame_variable)
  -- Set amounts.
  local amounts = { 5, 10 }
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'tonic_pickable'")
  end

  if game:get_magic() < game:get_max_magic() then
    game:add_magic(amount)
  end
end
