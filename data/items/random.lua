local item = ...

-- When it is created, this item creates another item randomly chosen
-- and then destroys itself.

-- Original script by Christopho.

-- Probability of each item between 0 and 100.
local probabilities = {
  [{"potion_pickable", 1}] = 10,  -- 1 small pickable potion.
  [{"potion_pickable", 2}] = 5,   -- 1 medium pickable potion.
  [{"potion_pickable", 3}] = 1,   -- 1 large pickable potion.
  [{"peso", 1}] = 10,             -- 1 peso.
  [{"peso", 2}] = 5,              -- 5 pesos.
  [{"peso", 3}] = 2,              -- 10 pesos.
  [{"peso", 4}] = 1,              -- 20 pesos.
}

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)
  local treasure_name, treasure_variant = item:choose_random_item()
  if treasure_name ~= nil then
    local map = pickable:get_map()
    local x, y, layer = pickable:get_position()
    map:create_pickable({
      x = x,
      y = y,
      layer = layer,
      treasure_name = treasure_name,
      treasure_variant = treasure_variant,
    })
  end
  pickable:remove()
end

-- Returns an item name and variant.
function item:choose_random_item()
  local random = math.random(100)
  local sum = 0
  for key, chance in pairs(probabilities) do
    sum = sum + chance
    if sum > random then
      return key[1], key[2]
    end
  end
  
  return nil
end