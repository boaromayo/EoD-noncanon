local item = ...
local game = item:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("august_water")
  self:set_amount_savegame_variable("august_water_amount")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
  self:set_shadow("small")
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(5)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  
  -- Limit to one only.
  --[[if game:get_savegame_variable() == "august_water" then
    -- Set to indicate whether hero cannot get this item.
    game:start_dialog("_treasure.august_drops.2")
  end--]]
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()
  -- Check if hero has at least 1 and is injured.
  if item:has_amount(1) then
    if game:get_life() < game:get_max_life() then
      -- Restore hero's health to full when used.
      game:add_life(game:get_max_life())
      item:remove_amount(1)    
      -- Play recovery sound.
      audio_manager:play_sound("heart")
    elseif game:get_life() == game:get_max_life() then
      audio_manager:play_sound("wrong")
    end
  end
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
