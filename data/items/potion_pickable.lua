local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
  self:set_shadow("small")
  self:set_sound_when_picked("picked_item")
end

-- Event called when hero picks it up on the ground.
function item:on_obtaining(variant, savegame_variable)
  -- Set amounts.
  local amounts = { 4, 10, 20 }
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'potion_pickable'")
  end

  if game:get_life() < game:get_max_life() then
    game:add_life(amount)
  end
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  --[[ Create custom, leaping movement.
  if pickable:get_falling_height() ~= 0 then
    local trajectory = {
      {0, 0}
    }
    local movement = sol.movement.create("pixel")
    movement:set_trajectory(trajectory)
    movement:set_loop(true)
    movement:delay(50)
    movement:set_ignore_obstacles(false)
    movement:start()
    pickable:get_pixel():set_animation(
  end--]]
end