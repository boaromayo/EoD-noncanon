-- Lua script of item seal_staff.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("seal_staff")
  self:set_can_disappear(false)
  self:set_assignable(true)
  self:set_sound_when_picked(nil)
  self:set_shadow(nil)
  self:set_is_weapon(true)
  self:set_attack(1)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_obtainable(not game:has_item("seal_staff"))
end

-- Event called after the hero obtains the item.
function item:on_obtained()
  -- Ensure no seal staves appear after one is obtained.
  item:set_obtainable(not game:has_item(item:get_name()))
  -- Assign item to slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    assert(self:is_assignable(), "Cannot assign weapon")
    game:set_item_assigned(slot, item)
    game:set_attack(item:get_attack())
  end
end

-- Event called when the hero is using this item.
function item:on_using()

  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
