-- Fire Cane item.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("fire_cane")
  self:set_can_disappear(false)
  self:set_assignable(true)
  self:set_sound_when_picked(nil)
  self:set_shadow(nil)
  self:set_is_weapon(true)
  self:set_attack(1)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_obtainable(not game:has_item("fire_cane"))
end

-- Event called after the hero obtains the item.
function item:on_obtained()
  -- Ensure no seal staves appear after one is obtained.
  item:set_obtainable(not game:has_item(item:get_name()))
  -- Assign item to slot if no other weapon found.
  local slot = 1
  if game:get_item_assigned(slot) == nil then
    assert(self:is_assignable(), "Cannot assign weapon")
    game:set_item_assigned(slot, item)
    game:set_attack(item:get_attack())
  end
end

-- Call this function to shoot fire from fire cane.
function item:shoot_fire()

  -- Get the map and hero objects for creating fire entity.
  local map = item:get_map()
  local hero = map:get_hero()
  local direction = hero:get_direction()

  local x, y, layer = hero:get_center_position()
  local fire = map:create_custom_entity({
    x = x,
    y = y,
    layer = layer,
    width = 8,
    height = 8,
    direction = direction,
    model = "fire"
  })

  local angle = direction * (math.pi / 2)
  local movement = sol.movement.create("straight")
  movement:set_speed(192)
  movement:set_angle(angle)
  movement:set_smooth(false)
  movement:start(fire)
end

-- Event called when the hero is using this item.
function item:on_using()

  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  item:shoot_fire()
  
  item:set_finished()
end

-- Event called to set custom charge command to cane.
function item:on_command_pressed(command)
  
end

function item:on_command_released(command)

end