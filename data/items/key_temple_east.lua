-- Key that opens doors in the east wing of the mountain temple.
local item = ...

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_east_temple_key")
  self:set_sound_when_picked(nil)
  self:set_sound_when_brandished("")
end
