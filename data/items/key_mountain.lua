-- Key to open mountain temple.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_sound_when_picked(nil)
  self:set_savegame_variable("possession_mountain_key")
end

-- Event called after player obtains item.
function item:on_obtained()
  local mountain_key_get = 6
  game:set_main_story(mountain_key_get)
end