local item = ...
local game = item:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("tonic")
  self:set_amount_savegame_variable("tonic_amount")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(true)
  self:set_shadow("small")
  self:set_assignable(false)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(15)
  self:set_obtainable(game:get_max_magic() > 0)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()  
  -- Check if has at least 1, has tech skills, and is low on tech. 
  if item:has_amount(1) then
    if game:get_max_magic() > 0 and 
        game:get_magic() < game:get_max_magic() then
      -- Calculate tech recovery rate.
      local heal = item:get_recovery_rate()
      -- Recover player's tech and remove 1 tonic.
      game:add_magic(heal)
      item:remove_amount(1)    
      -- Play recovery sound.
      audio_manager:play_sound("heart")
    elseif game:get_magic() == game:get_max_magic() then
      audio_manager:play_sound("wrong")
    end
  end
end

-- Calculate tech recovery.
function item:get_recovery_rate()
  -- Recovery cap.
  local MAX_RECOVERY = 20
  -- Get recovery amount based on half of hero's max tech.
  local heal = game:get_max_magic() / 2
  -- Cap recovery at max recovery amount.
  if heal > MAX_RECOVERY then
    heal = MAX_RECOVERY
  end
  return heal
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
