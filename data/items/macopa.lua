local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("macopa")
  self:set_amount_savegame_variable("macopa_amount")
  self:set_can_disappear(true)
  self:set_shadow(nil)
  self:set_assignable(false)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(15)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()
  -- Check if hero has at least 1 of this amount and is hurt.
  -- If not hurt, buzz and do not consume.
  if item:has_amount(1) then
    if game:get_life() < game:get_max_life() then
      -- Calculate healing amount.
      local health = self:get_recovery_rate()
      -- Recover hero's health and consume 1.
      game:add_life(health)
      item:remove_amount(1)
      -- Play recovery sound.
      sol.audio.play_sound("heart")
    else
      -- Play buzzer sound.
      sol.audio.play_sound("wrong")
    end
  end
  item:set_finished()
end

-- Calculate healing amount.
function item:get_recovery_rate()
  -- Get recovery amount based on hero's maximum life.
  return game:get_max_life() / 20
end
