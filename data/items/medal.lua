-- Medal item.
-- Collect a few of them and exchange it somewhere...
local item = ...
local MAX_MEDALS = 20

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("medal")
  self:set_amount_savegame_variable("possession_medal_amount")
  self:set_sound_when_picked(nil)
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(MAX_MEDALS)
end

-- Event called when hero gets this item.
function item:on_obtaining()
  self:add_amount(1)
end

-- Event called after hero gets this item.
function item:on_obtained()
  local game = self:get_game()
  local total_medals = self:get_amount()

  if total_medals > 1 and total_medals < self:get_max_amount() then
    game:start_dialog("_treasure.medal.amount.1", total_medals)
  elseif total_medals == self:get_max_amount() then
    game:start_dialog("_treasure.medal.amount.2", total_medals)
  end
end