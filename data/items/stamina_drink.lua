-- Lua script of item stamina_drink.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()
local audio_manager = require("scripts/audio_manager")

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("stamina_drink")
  self:set_amount_savegame_variable("stamina_drink_amount")
  self:set_assignable(false)
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
  self:set_shadow("small")
end

-- Event called when the game is initialized.
function item:on_started()
  self:set_max_amount(10)
end

-- Event called when the hero obtains the item.
function item:on_obtaining(variant, savegame_variable)
  item:add_amount(1)
end

-- Event called when the hero is using this item.
function item:on_using()
  -- To prevent player from using item when effect is happening.
  local drink_used = false
  -- Double attack power of hero for a short time.
  if item:has_amount(1) then
    if not drink_used then
      local MAX_DURATION = 60000
      local base_atk = game:get_attack()
      game:set_attack(base_atk * 2)
      item:remove_amount(1)
      drink_used = true
      -- Set effect to last for a minute. 
      -- Return hero attack back to original after that.
      sol.timer.start(game, MAX_DURATION, function()
        game:set_attack(base_atk)
        drink_used = false
      end)
      -- Play drink potion, or recovery sound.
      audio_manager:play_sound("heart")
    else
      -- Play buzzer sound.
      audio_manager:play_sound("wrong")
    end
  end
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
