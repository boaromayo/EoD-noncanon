-- Key that opens a door in the manor.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_shadow("small")
  self:set_brandish_when_picked(false)
  self:set_sound_when_picked("open_lock")
end

-- Event called when hero gets key.
function item:on_obtaining(variant, savegame_variable)
  --game:add_small_key()
end