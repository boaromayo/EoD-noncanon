local item = ...

-- Event called when the game is initialized.
function item:on_started()
  self:set_brandish_when_picked(false)
  self:set_sound_when_brandished("picked_rupee")
  self:set_savegame_variable("possession_prison_key")
end
