local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
end

-- Event called after hero obtains the item.
function item:on_obtained(variant, savegame_variable)
  -- Set values of rainbow drink.
  local amounts = { 5, 10, 20 }
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'rainbow_drink'")
  end
  
  -- Add extra max life for hero.
  game:add_max_life(amount)
  game:add_life(game:get_max_life())
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end