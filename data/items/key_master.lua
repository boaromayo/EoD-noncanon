-- Key to open master room in the manor.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_manor_master_key")
end

-- Event called after hero gets key.
function item:on_obtained()
end