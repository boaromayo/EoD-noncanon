-- Key item ruby drop, or the "strange stone."
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_ruby_drop")
  self:set_brandish_when_picked(false)
end
