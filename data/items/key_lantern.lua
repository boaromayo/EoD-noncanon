-- Key that opens the lighthouse.
local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable("possession_lantern_key")
end

-- Event called after hero gets key.
function item:on_obtained()
  local lantern_key_obtained = 20
  self:set_main_story(lantern_key_obtained)
end
