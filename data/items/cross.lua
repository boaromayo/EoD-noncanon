-- Cross subweapon.
local item = ...
local game = item:get_game()

-- Event called when the item is first created.
function item:on_created()
  self:set_savegame_variable("cross")
  self:set_assignable(true)
  self:set_brandish_when_picked(true)
  self:set_shadow(nil)
end

-- Event called when the hero is using this item.
-- TODO: Make custom version of cross instead of using pre-made boomerang script.
function item:on_using()
  local hero = game:get_hero()
  hero:start_boomerang(128, 160, "boomerang1", "entities/cross")
end
